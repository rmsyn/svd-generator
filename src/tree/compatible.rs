use alloc::string::String;

/// Represents the `compatible` property of a DeviceTree [`Node`].
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Compatible {
    /// Cadence QSPI NOR peripherals
    CdnsQspiNor,
    /// Cadence XSPI NOR XIP (eXecute-In-Place) Flash region
    CdnsXspiNor,
    /// Cadence USB3 peripherals
    CdnsUsb3,
    /// Synopsys DesignWare APB I2C peripherals
    DwApbI2c,
    /// Synopsys DesignWare APB UART peripherals
    DwApbUart,
    /// Synopsys DesignWare AXI DMAC peripherals
    DwAxiDmac,
    /// Synopsys DesignWare Ethernet MAC peripherals
    DwMac,
    /// Synopsys DesignWare MMC peripherals
    DwMmc,
    /// Starfive JH7110 AON CRG peripherals
    Jh7110AonCrg,
    /// Starfive JH7110 AON Pinctrl peripherals
    Jh7110AonPinctrl,
    /// Starfive JH7110 AON Syscon peripherals
    Jh7110AonSyscon,
    /// Starfive JH7110 Crypto peripherals
    Jh7110Crypto,
    /// Starfive JH7110 ISP CRG peripherals
    Jh7110IspCrg,
    /// Starfive JH7110 ISP SYSCON peripherals
    Jh7110IspSyscon,
    /// Starfive JH7110 MIPI TX DPHY peripherals
    Jh7110MipiTxDphy,
    /// Starfive JH7110 PMU peripherals
    Jh7110Pmu,
    /// Starfive JH7110 STG CRG peripherals
    Jh7110StgCrg,
    /// Starfive JH7110 STG Syscon peripherals
    Jh7110StgSyscon,
    /// Starfive JH7110 SYS CRG peripherals
    Jh7110SysCrg,
    /// Starfive JH7110 SYS Pinctrl peripherals
    Jh7110SysPinctrl,
    /// Starfive JH7110 SYS Syscon peripherals
    Jh7110SysSyscon,
    /// Starfive JH7110 TDM peripherals
    Jh7110Tdm,
    /// Starfive JH7110 TRNG peripherals
    Jh7110Trng,
    /// Starfive JH7110 USB peripherals
    Jh7110Usb,
    /// Starfive JH7110 USB PHY peripherals
    Jh7110UsbPhy,
    /// Starfive JH7110 VOUTCRG peripherals
    Jh7110VoutCrg,
    /// Starfive JH7110 VOUT Syscon peripherals
    Jh7110VoutSyscon,
    /// Starfive JH7110 WDT peripherals
    Jh7110Wdt,
    /// ARM pl022 SSP SPI peripherals
    Pl022SspSpi,
    /// ARM PL080 DMAC peripherals
    Pl080Dmac,
    /// RISC-V CLINT (Core-local Interrupt Controller)
    RiscvClint,
    /// RISC-V PLIC (Platform-level Interrupt Controller)
    RiscvPlic,
    /// Opencores PTC PWM v1 (PWM/Timer/Counter)
    OcPwm,
    /// OpenEdges Orbit Memory Controller
    OeOmc,
    /// OpenEdges Orbit PHY
    OeOphy,
    /// SiFive U74(MC) L2 Performance Monitor
    SiFiveU74L2pm,
    /// SiFive U74(MC) SRAM (L2 LIM) core memory region
    SiFiveU74Sram,
    /// Unknown compatibility string
    Unknown(String),
    /// No compatible property
    None,
}

impl Compatible {
    /// Gets whether this is a known [Compatible] peripheral.
    pub const fn is_known(&self) -> bool {
        !(matches!(self, Self::Unknown(_)) || matches!(self, Self::None))
    }
}

impl From<&str> for Compatible {
    fn from(val: &str) -> Self {
        if val.contains("cdns,qspi-nor") {
            Self::CdnsQspiNor
        } else if val.contains("cdns,xspi-nor") {
            Self::CdnsXspiNor
        } else if val.contains("cdns,usb3") {
            Self::CdnsUsb3
        } else if val.contains("dw-apb-i2c") || val.contains("designware-i2c") {
            Self::DwApbI2c
        } else if val.contains("dw-apb-uart") {
            Self::DwApbUart
        } else if val.contains("snps,dw-axi-dmac") {
            Self::DwAxiDmac
        } else if val.contains("snps,dwmac") {
            Self::DwMac
        } else if val.contains("snps,dwmmc") || val.contains("starfive,jh7110-mmc") {
            // NOTE: add more matching strings for additional models as implementations are added.
            Self::DwMmc
        } else if val.contains("starfive,jh7110-aoncrg") {
            Self::Jh7110AonCrg
        } else if val.contains("starfive,jh7110-aon-pinctrl") {
            Self::Jh7110AonPinctrl
        } else if val.contains("starfive,jh7110-aon-syscon") {
            Self::Jh7110AonSyscon
        } else if val.contains("starfive,jh7110-crypto") {
            Self::Jh7110Crypto
        } else if val.contains("starfive,jh7110-ispcrg") {
            Self::Jh7110IspCrg
        } else if val.contains("starfive,jh7110-isp-syscon") {
            Self::Jh7110IspSyscon
        } else if val.contains("starfive,jh7110-mipitx-dphy") {
            Self::Jh7110MipiTxDphy
        } else if val.contains("starfive,jh7110-pmu") {
            Self::Jh7110Pmu
        } else if val.contains("starfive,jh7110-stgcrg") {
            Self::Jh7110StgCrg
        } else if val.contains("starfive,jh7110-stg-syscon") {
            Self::Jh7110StgSyscon
        } else if val.contains("starfive,jh7110-syscrg") {
            Self::Jh7110SysCrg
        } else if val.contains("starfive,jh7110-sys-pinctrl") {
            Self::Jh7110SysPinctrl
        } else if val.contains("starfive,jh7110-sys-syscon") {
            Self::Jh7110SysSyscon
        } else if val.contains("starfive,jh7110-tdm") {
            Self::Jh7110Tdm
        } else if val.contains("starfive,jh7110-trng") {
            Self::Jh7110Trng
        } else if val.contains("starfive,jh7110-usb-phy") {
            Self::Jh7110UsbPhy
        } else if val.contains("starfive,jh7110-usb") {
            Self::Jh7110Usb
        } else if val.contains("starfive,jh7110-vout-syscon") {
            Self::Jh7110VoutSyscon
        } else if val.contains("starfive,jh7110-voutcrg") {
            Self::Jh7110VoutCrg
        } else if val.contains("starfive,jh7110-wdt") {
            Self::Jh7110Wdt
        } else if val.contains("arm,pl022") {
            Self::Pl022SspSpi
        } else if val.contains("arm,pl080") {
            Self::Pl080Dmac
        } else if val.contains("riscv,clint") {
            Self::RiscvClint
        } else if val.contains("riscv,plic") {
            Self::RiscvPlic
        } else if val.contains("opencores,pwm-v1") {
            Self::OcPwm
        } else if val.contains("openedges,omc") {
            Self::OeOmc
        } else if val.contains("openedges,ophy") {
            Self::OeOphy
        } else if val.contains("sifive,u74-mc-l2pm")
            || val.contains("sifive,u74-l2pm")
            || val.contains("sifive,ccache0")
        {
            Self::SiFiveU74L2pm
        } else if val.contains("sifive,u74-mc-sram")
            || val.contains("sifive,u74-sram")
            || val.contains("sifive,u74-mc-l2-lim")
            || val.contains("sifive,u74-l2-lim")
        {
            Self::SiFiveU74Sram
        } else if val.is_empty() {
            Self::None
        } else {
            Self::Unknown(val.into())
        }
    }
}

impl From<&Compatible> for &'static str {
    fn from(val: &Compatible) -> Self {
        match val {
            Compatible::CdnsQspiNor => "cdns,qspi-nor",
            Compatible::CdnsXspiNor => "cdns,xspi-nor",
            Compatible::CdnsUsb3 => "cdns,usb3",
            Compatible::DwApbI2c => "snps,dw-apb-i2c",
            Compatible::DwApbUart => "snps,dw-apb-uart",
            Compatible::DwAxiDmac => "snps,dw-axi-dmac",
            Compatible::DwMac => "snps,dwmac",
            Compatible::DwMmc => "snps,dwmmc",
            Compatible::Jh7110AonCrg => "starfive,jh7110-aoncrg",
            Compatible::Jh7110AonPinctrl => "starfive,jh7110-aon-pinctrl",
            Compatible::Jh7110AonSyscon => "starfive,jh7110-aon-syscon",
            Compatible::Jh7110Crypto => "starfive,jh7110-crypto",
            Compatible::Jh7110IspCrg => "starfive,jh7110-ispcrg",
            Compatible::Jh7110IspSyscon => "starfive,jh7110-isp-syscon",
            Compatible::Jh7110MipiTxDphy => "starfive,jh7110-mipitx-dphy",
            Compatible::Jh7110Pmu => "starfive,jh7110-pmu",
            Compatible::Jh7110StgCrg => "starfive,jh7110-stgcrg",
            Compatible::Jh7110StgSyscon => "starfive,jh7110-stg-syscon",
            Compatible::Jh7110SysCrg => "starfive,jh7110-syscrg",
            Compatible::Jh7110SysPinctrl => "starfive,jh7110-sys-pinctrl",
            Compatible::Jh7110SysSyscon => "starfive,jh7110-sys-syscon",
            Compatible::Jh7110Tdm => "starfive,jh7110-tdm",
            Compatible::Jh7110Trng => "starfive,jh7110-trng",
            Compatible::Jh7110UsbPhy => "starfive,jh7110-usb-phy",
            Compatible::Jh7110Usb => "starfive,jh7110-usb",
            Compatible::Jh7110VoutSyscon => "starfive,jh7110-vout-syscon",
            Compatible::Jh7110VoutCrg => "starfive,jh7110-voutcrg",
            Compatible::Jh7110Wdt => "starfive,jh7110-wdt",
            Compatible::Pl022SspSpi => "arm,pl022",
            Compatible::Pl080Dmac => "arm,pl080",
            Compatible::RiscvClint => "riscv,clint",
            Compatible::RiscvPlic => "riscv,plic",
            Compatible::OcPwm => "opencores,pwm-v1",
            Compatible::OeOmc => "openedges,omc",
            Compatible::OeOphy => "openedges,ophy",
            Compatible::SiFiveU74L2pm => "sifive,u74-",
            Compatible::SiFiveU74Sram => "sifive,u74-sram",
            Compatible::Unknown(_val) => "unknown",
            Compatible::None => "",
        }
    }
}

impl From<Compatible> for &'static str {
    fn from(val: Compatible) -> Self {
        (&val).into()
    }
}

impl<'b, 'a: 'b> From<&fdt::node::FdtNode<'b, 'a>> for Compatible {
    fn from(val: &fdt::node::FdtNode<'b, 'a>) -> Self {
        match val.property("compatible") {
            Some(compatible) => match compatible.as_str() {
                Some(comp) => Compatible::from(comp),
                None => Self::None,
            },
            None => Self::None,
        }
    }
}

impl<'b, 'a: 'b> From<fdt::node::FdtNode<'b, 'a>> for Compatible {
    fn from(val: fdt::node::FdtNode<'b, 'a>) -> Self {
        (&val).into()
    }
}
