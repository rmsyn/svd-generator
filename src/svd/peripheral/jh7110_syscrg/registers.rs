use crate::Result;

pub mod clk_ahb;
pub mod clk_apb;
pub mod clk_apb_bus;
pub mod clk_audio;
pub mod clk_axi_cfg0;
pub mod clk_axi_cfg0_dec;
pub mod clk_axi_cfg1_dec;
pub mod clk_aximem_128b_axi;
pub mod clk_bus;
pub mod clk_can_ctrl;
pub mod clk_cpu;
pub mod clk_ddr;
pub mod clk_gclk;
pub mod clk_gmac;
pub mod clk_gpu;
pub mod clk_hifi4;
pub mod clk_i2c;
pub mod clk_i2s;
pub mod clk_img_gpu;
pub mod clk_internal_ctrl_apb;
pub mod clk_isp;
pub mod clk_isp_dom;
pub mod clk_jpeg;
pub mod clk_jtag_trng;
pub mod clk_mbox_apb;
pub mod clk_mclk;
pub mod clk_noc_bus;
pub mod clk_noc_stg_axi;
pub mod clk_nocstg_bus;
pub mod clk_osc_div2;
pub mod clk_pclk;
pub mod clk_pdm;
pub mod clk_peripheral;
pub mod clk_pll1_div4;
pub mod clk_pll1_div8;
pub mod clk_pll_div2;
pub mod clk_pwm_apb;
pub mod clk_pwmdac;
pub mod clk_qspi;
pub mod clk_sd;
pub mod clk_spdif;
pub mod clk_spi;
pub mod clk_stg_axiahb;
pub mod clk_tdm;
pub mod clk_temp_sensor;
pub mod clk_tim;
pub mod clk_u7mc;
pub mod clk_uart;
pub mod clk_usb_125m;
pub mod clk_vdec;
pub mod clk_venc;
pub mod clk_vout;
pub mod clk_wdt;
pub mod rst;

/// Creates StarFive JH7110 SYS CRG (compatible) register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        clk_cpu::create()?,
        clk_gpu::create()?,
        clk_peripheral::create()?,
        clk_bus::create()?,
        clk_nocstg_bus::create()?,
        clk_axi_cfg0::create()?,
        clk_stg_axiahb::create()?,
        clk_ahb::create()?,
        clk_apb_bus::create()?,
        clk_apb::create()?,
        clk_pll_div2::create()?,
        clk_audio::create()?,
        clk_mclk::create()?,
        clk_isp::create()?,
        clk_gclk::create()?,
        clk_u7mc::create()?,
        clk_noc_bus::create()?,
        clk_osc_div2::create()?,
        clk_pll1_div4::create()?,
        clk_pll1_div8::create()?,
        clk_ddr::create()?,
        clk_img_gpu::create()?,
        clk_isp_dom::create()?,
        clk_hifi4::create()?,
        clk_axi_cfg1_dec::create()?,
        clk_vout::create()?,
        clk_jpeg::create()?,
        clk_vdec::create()?,
        clk_venc::create()?,
        clk_axi_cfg0_dec::create()?,
        clk_aximem_128b_axi::create()?,
        clk_qspi::create()?,
        clk_sd::create()?,
        clk_usb_125m::create()?,
        clk_noc_stg_axi::create()?,
        clk_gmac::create()?,
        clk_pclk::create()?,
        clk_mbox_apb::create()?,
        clk_internal_ctrl_apb::create()?,
        clk_can_ctrl::create()?,
        clk_pwm_apb::create()?,
        clk_wdt::create()?,
        clk_tim::create()?,
        clk_temp_sensor::create()?,
        clk_spi::create()?,
        clk_i2c::create()?,
        clk_uart::create()?,
        clk_pwmdac::create()?,
        clk_spdif::create()?,
        clk_i2s::create()?,
        clk_pdm::create()?,
        clk_tdm::create()?,
        clk_jtag_trng::create()?,
        rst::create()?,
    ]
    .into())
}
