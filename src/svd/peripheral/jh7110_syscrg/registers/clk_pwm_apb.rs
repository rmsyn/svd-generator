use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock PWM APB register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_pwm_apb", "Clock PWM APB", 0x1e4, None, None)
}
