use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock NOC STG Bus register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_nocstg_bus",
        "Clock NOC STG Bus",
        0x18,
        [3, 3, 3, 3],
        None,
    )
}
