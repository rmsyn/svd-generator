use crate::svd::create_cluster;
use crate::Result;

pub mod axi;
pub mod jpg;
pub mod noc_axi;
pub mod wave511;

/// Creates a StarFive JH7110 SYSCRG Video Decoder registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_vdec",
        "Clock Video Decoder registers",
        0x114,
        &[
            axi::create()?,
            wave511::create()?,
            jpg::create()?,
            noc_axi::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
