use crate::svd::register::{create_cluster, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock Audio registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_audio",
        "Clock Audio registers",
        0x40,
        &[jh7110::create_register_divcfg(
            "root",
            "Clock Audio Root",
            0,
            [8, 2, 2, 2],
            None,
        )?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
