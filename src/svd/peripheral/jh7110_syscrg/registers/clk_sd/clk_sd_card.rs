use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG SD Card register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "clk_sd_card",
        "Clock SD Card",
        0x8,
        [15, 2, 2, 2],
        Some((1 << 31) | 2),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some((0..2).map(|i| format!("_u{i}")).collect()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
}
