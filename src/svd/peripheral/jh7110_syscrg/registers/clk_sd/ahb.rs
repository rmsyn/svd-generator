use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG SD AHB register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "ahb",
        "Clock SD AHB",
        0x0,
        Some(1 << 31),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some((0..2).map(|i| format!("_u{i}")).collect()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
}
