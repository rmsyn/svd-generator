use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock Timer TIM register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "tim23",
        "Clock Timer: 2-3",
        0xc,
        None,
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some((2..4).map(|i| format!("_{i}")).collect()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
}
