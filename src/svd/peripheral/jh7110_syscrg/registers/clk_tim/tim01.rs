use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock Timer TIM register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "tim01",
        "Clock Timer 0-1",
        0x4,
        Some(1 << 31),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some((0..2).map(|i| format!("_{i}")).collect()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
}
