use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock APB Bus register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_pll",
        "Clock PLL Divider 2",
        0x34,
        [2, 2, 2, 2],
        Some(
            svd::DimElement::builder()
                .dim(3)
                .dim_increment(0x4)
                .dim_index(Some((0..3).map(|i| format!("{i}_div2")).collect()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
}
