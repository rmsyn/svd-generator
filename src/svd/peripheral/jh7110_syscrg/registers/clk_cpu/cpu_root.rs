use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock CPU Root register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_mux_sel_enum(
        "root",
        "Clock CPU Root",
        0x0,
        "clk_osc, clk_pll0",
        &[
            create_enum_value("clk_osc", "Select `clk_osc` as the CPU Root clock.", 0b0)?,
            create_enum_value("clk_pll0", "Select `clk_pll0` as the CPU Root clock.", 0b1)?,
        ],
        None,
    )
}
