use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock CPU Core register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("core", "Clock CPU Core", 0x4, [7, 1, 1, 1], None)
}
