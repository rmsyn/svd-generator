use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock WDT register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("wdt", "Clock WDT", 0x4, Some(1 << 31), None)
}
