use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock I2C APB registers.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "apb",
        "Clock I2C APB",
        0x0,
        None,
        Some(
            svd::DimElement::builder()
                .dim(7)
                .dim_increment(0x4)
                .dim_index(Some((0..7).map(|i| format!("_u{i}")).collect()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
}
