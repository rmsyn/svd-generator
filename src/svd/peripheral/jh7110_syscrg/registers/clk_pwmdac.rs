use crate::svd::create_cluster;
use crate::Result;

pub mod apb;
pub mod core;

/// Creates a StarFive JH7110 SYSCRG Clock PWMDAC registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_pwmdac",
        "Clock PWMDAC registers",
        0x274,
        &[apb::create()?, core::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
