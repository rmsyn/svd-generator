use crate::svd::create_cluster;
use crate::Result;

pub mod ahb;
pub mod clk_sd_card;

/// Creates a StarFive JH7110 SYSCRG SD registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_sd",
        "Clock SD registers",
        0x16c,
        &[ahb::create()?, clk_sd_card::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
