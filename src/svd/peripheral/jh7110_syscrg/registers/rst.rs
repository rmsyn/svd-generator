use crate::svd::create_cluster;
use crate::Result;

pub mod software_address_selector;
pub mod syscrg_status;

/// Creates a StarFive JH7110 SYSCRG Software RESET Address Selector registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "rst",
        "SYSCRG RESET registers",
        0x2f8,
        &[
            software_address_selector::create()?,
            syscrg_status::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
