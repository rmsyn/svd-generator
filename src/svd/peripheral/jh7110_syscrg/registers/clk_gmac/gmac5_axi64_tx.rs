use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG GMAC GMAC5 AXI64 TX register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_mux_sel_enum(
        "gmac5_axi64_tx",
        "Clock GMAC5 AXI64 TX",
        0x20,
        "clk_gmac1_gtx_clk, clk_gmac1_rmii_rtx",
        &[
            create_enum_value(
                "clk_gmac1_gtx_clk",
                "Select `clk_gmac1_gtx_clk` as the GMAC5 AXI64 TX clock.",
                0b0,
            )?,
            create_enum_value(
                "clk_gmac1_rmii_rtx",
                "Select `clk_gmac1_rmii_rtx` as the GMAC5 AXI64 TX clock.",
                0b1,
            )?,
        ],
        None,
        None,
    )
}
