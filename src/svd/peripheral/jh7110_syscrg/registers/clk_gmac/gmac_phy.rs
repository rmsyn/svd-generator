use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG GMAC PHY register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "gmac_phy",
        "Clock GMAC PHY",
        0x34,
        [31, 10, 15, 25],
        None,
        None,
    )
}
