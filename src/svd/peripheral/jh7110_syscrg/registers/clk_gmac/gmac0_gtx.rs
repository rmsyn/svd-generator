use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG GMAC GMAC0 GTX register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "gmac0_gtx",
        "Clock GMAC0 GTX",
        0x2c,
        [15, 8, 12, 10],
        None,
        None,
    )
}
