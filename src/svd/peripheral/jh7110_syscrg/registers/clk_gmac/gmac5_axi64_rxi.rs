use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG GMAC GMAC5 AXI64 RXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_clk_polarity(
        "gmac5_axi64_rxi",
        "Clock GMAC5 AXI64 RX Inverter",
        0x1c,
        Some(1 << 30),
    )
}
