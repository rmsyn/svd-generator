use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG GMAC GMAC1 GTXC register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_dly_chain_sel("gmac1_gtxc", "Clock GMAC1 GTXC", 0x28, None)
}
