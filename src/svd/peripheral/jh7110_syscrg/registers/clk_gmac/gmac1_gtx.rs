use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG GMAC GMAC1 GTX register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("gmac1_gtx", "Clock GMAC1 GTX", 0xc, [15, 8, 12, 10], None)
}
