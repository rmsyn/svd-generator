use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG GMAC GMAC5 AXI64 RX register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_dly_chain_sel("gmac5_axi64_rx", "Clock GMAC5 AXI64 RX", 0x18, None)
}
