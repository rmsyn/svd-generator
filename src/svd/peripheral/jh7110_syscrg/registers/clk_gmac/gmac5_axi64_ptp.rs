use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG GMAC GMAC5 AXI64 PTP register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "gmac5_axi64_ptp",
        "Clock GMAC AXI64 PTP",
        0x14,
        [31, 10, 15, 10],
        None,
        None,
    )
}
