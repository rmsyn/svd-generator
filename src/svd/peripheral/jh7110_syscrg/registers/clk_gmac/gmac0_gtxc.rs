use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG GMAC GMAC0 GTXC register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_dly_chain_sel("gmac0_gtxc", "Clock GMAC0 GTXC", 0x38, None)
}
