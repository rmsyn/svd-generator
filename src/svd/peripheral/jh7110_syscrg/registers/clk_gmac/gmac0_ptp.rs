use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG GMAC GMAC0 PTP register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "gmac0_ptp",
        "Clock GMAC0 PTP",
        0x30,
        [31, 10, 15, 25],
        None,
        None,
    )
}
