use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG GMAC GMAC5 AXI64 register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "gmac5_axi64",
        "Clock GMAC5 AXI64",
        0x0,
        None,
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some(["_ahb", "_axi"].map(String::from).into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
}
