use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG GMAC GMAC1 RMII RTX register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "gmac1_rmii_rtx",
        "Clock GMAC RMII RTX",
        0x10,
        [30, 2, 2, 2],
        None,
    )
}
