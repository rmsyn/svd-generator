use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Output SRC register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "src",
        "clk_u0_dom_vout_top_clk_dom_vout_top_clk_vout_src",
        0x0,
        None,
        None,
    )
}
