use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Output MIPI PHY register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "mipi_phy",
        "Clock Video Output MIPI PHY Reference",
        0x18,
        [2, 2, 2, 2],
        None,
    )
}
