use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Output AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("axi", "Clock Video Output AXI", 0x10, None, None)
}
