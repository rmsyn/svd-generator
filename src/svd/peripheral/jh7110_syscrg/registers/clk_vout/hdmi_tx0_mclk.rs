use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Output AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "hdmi_tx0_mclk",
        "Clock Video Output HDMI TX0 MCLK",
        0x14,
        None,
        None,
    )
}
