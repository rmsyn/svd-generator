use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Output AHB register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("ahb", "Clock Video Output AHB", 0xc, None, None)
}
