use crate::svd::register::create_cluster;
use crate::Result;

pub mod gclk0;
pub mod gclk1;
pub mod gclk2;

/// Creates a StarFive JH7110 SYSCRG Clock ISP AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_gclk",
        "Clock GCLK registers",
        0x58,
        &[gclk0::create()?, gclk1::create()?, gclk2::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
