use crate::svd::register::create_cluster;
use crate::Result;

pub mod axi;
pub mod noc_axi;
pub mod wave420l;

/// Creates a StarFive JH7110 SYSCRG Video Encoder registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_venc",
        "Clock Video Encoder registers",
        0x134,
        &[axi::create()?, wave420l::create()?, noc_axi::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
