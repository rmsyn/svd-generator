use crate::svd::create_cluster;
use crate::Result;

pub mod apb;
pub mod core;

/// Creates a StarFive JH7110 SYSCRG Clock SPDIF registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_spdif",
        "Clock SPDIF registers",
        0x27c,
        &[apb::create()?, core::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
