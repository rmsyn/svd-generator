use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG QSPI APB register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("apb", "Clock QSPI APB", 0x4, Some(1 << 31), None)
}
