use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG QSPI Clock REF SRC register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_ref_src",
        "Clock QSPI Reference Source",
        0x8,
        [16, 10, 10, 10],
        None,
    )
}
