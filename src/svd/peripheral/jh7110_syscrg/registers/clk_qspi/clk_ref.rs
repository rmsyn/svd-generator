use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG QSPI Clock REF register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_mux_sel_enum(
        "clk_ref",
        "Clock QSPI Reference",
        0xc,
        "clk_osc, clk_qspi_ref_src",
        &[
            create_enum_value(
                "clk_osc",
                "Select `clk_osc` as the QSPI Reference clock.",
                0b0,
            )?,
            create_enum_value(
                "clk_qspi_ref_src",
                "Select `clk_qspi_ref_src` as the QSPI Reference clock.",
                0b1,
            )?,
        ],
        Some(1 << 31),
        None,
    )
}
