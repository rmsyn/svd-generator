use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG QSPI AHB register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("ahb", "Clock QSPI AHB", 0x0, Some(1 << 31), None)
}
