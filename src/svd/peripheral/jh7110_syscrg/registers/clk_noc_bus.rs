use crate::svd::create_cluster;
use crate::Result;

pub mod axicfg0_axi;
pub mod cpu_axi;

/// Creates a StarFive JH7110 SYSCRG NOC Bus registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_noc_bus",
        "Clock NOC Bus registers",
        0x98,
        &[cpu_axi::create()?, axicfg0_axi::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
