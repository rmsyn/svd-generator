use crate::svd::create_cluster;
use crate::Result;

pub mod axi;
pub mod bus;

/// Creates a StarFive JH7110 SYSCRG DDR registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_ddr",
        "Clock DDR registers",
        0xac,
        &[bus::create()?, axi::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
