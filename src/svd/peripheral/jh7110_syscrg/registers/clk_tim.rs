use crate::svd::create_cluster;
use crate::Result;

pub mod apb;
pub mod tim01;
pub mod tim23;

/// Creates a StarFive JH7110 SYSCRG Clock Timer register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_tim",
        "Clock Timer",
        0x1f0,
        &[apb::create()?, tim01::create()?, tim23::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
