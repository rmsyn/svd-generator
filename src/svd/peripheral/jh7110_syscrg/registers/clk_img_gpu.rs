use crate::svd::create_cluster;
use crate::Result;

pub mod apb;
pub mod core;
pub mod divcfg;
pub mod noc_bus_axi;
pub mod rtc_toggle;
pub mod sys;

/// Creates a StarFive JH7110 SYSCRG IMG GPU registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_img_gpu",
        "Clock IMG GPU registers",
        0xb4,
        &[
            divcfg::create()?,
            core::create()?,
            sys::create()?,
            apb::create()?,
            rtc_toggle::create()?,
            noc_bus_axi::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
