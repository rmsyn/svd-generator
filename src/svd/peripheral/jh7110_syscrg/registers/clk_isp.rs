use crate::svd::create_cluster;
use crate::Result;

pub mod axi;
pub mod isp_2x;

/// Creates a StarFive JH7110 SYSCRG ISP registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_isp",
        "Clock ISP registers",
        0x50,
        &[isp_2x::create()?, axi::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
