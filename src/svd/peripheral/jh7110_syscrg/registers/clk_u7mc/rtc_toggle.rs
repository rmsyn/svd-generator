use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG U7MC RTC Toggle register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "rtc_toggle",
        "Clock U7MC RTC Toggle",
        0x18,
        [6, 6, 6, 6],
        None,
    )
}
