use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG U7MC Trace register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "trace",
        "Clock U7MC Trace",
        0x1c,
        Some(1 << 31),
        Some(
            svd::DimElement::builder()
                .dim(5)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
}
