use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG U7MC Debug register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("debug", "Clock U7MC Debug", 0x14, Some(1 << 31), None)
}
