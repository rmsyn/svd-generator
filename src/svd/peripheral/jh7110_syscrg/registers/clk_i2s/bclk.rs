use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock I2S BCLK register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_mux_sel_enum(
        "bclk",
        "Clock I2S BCLK",
        0x10,
        "clk_i2s_bclk_mst, clk_i2s_bclk_ext",
        &[
            create_enum_value(
                "clk_i2s_bclk_mst",
                "Select  `clk_i2s_bclk_mst` as the I2S BCLK clock.",
                0b0,
            )?,
            create_enum_value(
                "clk_i2s_bclk_ext",
                "Select  `clk_i2s_bclk_ext` as the I2S BCLK clock.",
                0b1,
            )?,
        ],
        None,
    )
}
