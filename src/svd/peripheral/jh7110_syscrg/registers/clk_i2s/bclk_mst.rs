use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock I2S BCLK MST register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "bclk_mst",
        "Clock I2S BCLK MST",
        0x4,
        [32, 4, 4, 4],
        None,
        None,
    )
}
