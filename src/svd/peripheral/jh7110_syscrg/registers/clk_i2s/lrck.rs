use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock I2S LRCK register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_mux_sel_enum(
        "lrck",
        "Clock I2S LRCK",
        0x18,
        "clk_i2s_lrck_mst, clk_i2s_lrck_ext",
        &[
            create_enum_value(
                "clk_i2s_lrck_mst",
                "Select  `clk_i2s_lrck_mst` as the I2S LRCK clock.",
                0b0,
            )?,
            create_enum_value(
                "clk_i2s_lrck_ext",
                "Select  `clk_i2s_lrck_ext` as the I2S LRCK clock.",
                0b1,
            )?,
        ],
        None,
    )
}
