use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock I2S BCLK MST register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_clk_polarity(
        "bclk_mst_inv",
        "U0 Clock I2S BCLK MST Inverter",
        0x8,
        Some(1 << 30),
    )
}
