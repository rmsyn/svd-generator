use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock I2S LRCK MST register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_mux_sel_divcfg_enum(
        "lrck_mst",
        "Clock I2S LRCK MST",
        0xc,
        "clk_i2stx_bclk_mst_inv, clk_i2stx_bclk_mst",
        &[
            create_enum_value(
                "clk_i2stx_bclk_mst_inv",
                "Select `clk_i2stx_bclk_mst_inv` as the I2S LRCK MST clock.",
                0b0,
            )?,
            create_enum_value(
                "clk_i2stx_bclk_mst",
                "Select `clk_i2stx_bclk_mst` as the I2S LRCK MST clock.",
                0b1,
            )?,
        ],
        [64, 64, 64, 64],
    )
}
