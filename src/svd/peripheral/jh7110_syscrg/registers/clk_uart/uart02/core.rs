use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock UART 0-2 Core registers.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("core", "Clock UART Core", 0x4, None, None)
}
