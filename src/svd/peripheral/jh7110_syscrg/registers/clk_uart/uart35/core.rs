use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock UART 3-5 Core register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "core",
        "Clock UART Core",
        0x4,
        [131_071, 2_560, 2_560, 2_560],
        None,
        None,
    )
}
