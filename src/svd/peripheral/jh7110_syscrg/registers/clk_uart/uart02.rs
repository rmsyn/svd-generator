use crate::svd::create_cluster;
use crate::Result;

pub mod apb;
pub mod core;

/// Creates a StarFive JH7110 SYSCRG Clock UART 0-2 registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "uart02",
        "Clock UART U0-U2",
        0x0,
        &[apb::create()?, core::create()?],
        Some(
            svd::DimElement::builder()
                .dim(3)
                .dim_increment(0x8)
                .dim_index(Some((0..3).map(|i| format!("_u{i}")).collect()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Cluster)
}
