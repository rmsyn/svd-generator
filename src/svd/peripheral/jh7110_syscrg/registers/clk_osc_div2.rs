use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock OSC Divider 2 register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("clk_osc_div2", "clk_osc_div2", 0xa0, [2, 2, 2, 2], None)
}
