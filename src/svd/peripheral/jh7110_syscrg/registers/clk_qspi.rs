use crate::svd::create_cluster;
use crate::Result;

pub mod ahb;
pub mod apb;
pub mod clk_ref;
pub mod clk_ref_src;

/// Creates a StarFive JH7110 SYSCRG QSPI registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_qspi",
        "Clock QSPI registers",
        0x15c,
        &[
            ahb::create()?,
            apb::create()?,
            clk_ref_src::create()?,
            clk_ref::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
