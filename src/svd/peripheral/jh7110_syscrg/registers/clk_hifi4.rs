use crate::svd::create_cluster;
use crate::Result;

pub mod hifi4_axi;
pub mod hifi4_core;

/// Creates a StarFive JH7110 SYSCRG HIFI4 registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_hifi4",
        "Clock HIFI4 registers",
        0xd8,
        &[hifi4_core::create()?, hifi4_axi::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
