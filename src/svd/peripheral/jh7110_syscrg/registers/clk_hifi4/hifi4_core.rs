use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG HIFI4 Core register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("core", "clk_hifi4_core", 0x0, [15, 3, 3, 3], None)
}
