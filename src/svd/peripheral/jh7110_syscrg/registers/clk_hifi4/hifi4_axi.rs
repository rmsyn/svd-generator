use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG HIFI4 Core register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("axi", "clk_hifi4_axi", 0x4, [2, 2, 2, 2], None)
}
