use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG MCLK register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_mux_sel_enum(
        "mclk",
        "Clock MCLK",
        0x4,
        "clk_mclk_inner, clk_mclk_ext",
        &[
            create_enum_value(
                "clk_mclk_inner",
                "Select `clk_mclk_inner` as the MCLK clock.",
                0b0,
            )?,
            create_enum_value(
                "clk_mclk_ext",
                "Select `clk_mclk_ext` as the MCLK clock.",
                0b1,
            )?,
        ],
        None,
    )
}
