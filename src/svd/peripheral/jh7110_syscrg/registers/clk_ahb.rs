use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock AHB register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "clk_ahb",
        "Clock AHB",
        0x24,
        Some(1 << 31),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
}
