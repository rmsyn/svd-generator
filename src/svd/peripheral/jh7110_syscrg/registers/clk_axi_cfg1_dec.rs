use crate::svd::create_cluster;
use crate::Result;

pub mod ahb;
pub mod dec_main;

/// Creates a StarFive JH7110 SYSCRG AXI CFG 1 DEC registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_axi_cfg1_dec",
        "Clock AXI CFG 1 DEC registers",
        0xe0,
        &[dec_main::create()?, ahb::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
