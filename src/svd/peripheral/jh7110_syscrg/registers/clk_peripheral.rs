use crate::svd::create_cluster;
use crate::Result;

pub mod peripheral_root;

/// Creates a StarFive JH7110 SYSCRG Clock Perihperal registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_peripheral",
        "Clock Peripheral registers",
        0x10,
        &[peripheral_root::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
