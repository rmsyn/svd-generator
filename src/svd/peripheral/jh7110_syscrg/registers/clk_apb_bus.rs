use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock APB Bus register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("clk_apb_bus", "Clock APB Bus", 0x2c, [8, 4, 4, 4], None)
}
