use crate::svd::create_cluster;
use crate::Result;

pub mod apb;

/// Creates a StarFive JH7110 SYSCRG Clock I2C registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_i2c",
        "Clock I2C registers",
        0x228,
        &[apb::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
