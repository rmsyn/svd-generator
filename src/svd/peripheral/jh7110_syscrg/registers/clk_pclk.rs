use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock PCLK register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_pclk", "Clock PCLK", 0x1c0, Some(1 << 31), None)
}
