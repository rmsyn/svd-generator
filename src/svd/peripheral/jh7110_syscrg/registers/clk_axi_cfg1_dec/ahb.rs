use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG AXI CFG 1 DEC AHB register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "ahb",
        "clk_u0_axi_cfg1_dec_clk_ahb",
        0x4,
        Some(1 << 31),
        None,
    )
}
