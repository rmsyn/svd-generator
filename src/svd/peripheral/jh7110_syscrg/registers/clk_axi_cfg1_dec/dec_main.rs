use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG AXI CFG 1 DEC Main register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "dec_main",
        "clk_u0_axi_cfg1_dec_clk_main",
        0x0,
        Some(1 << 31),
        None,
    )
}
