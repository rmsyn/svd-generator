use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock USB 125M register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_usb_125m",
        "Clock USB 125M",
        0x17c,
        [15, 8, 12, 10],
        None,
    )
}
