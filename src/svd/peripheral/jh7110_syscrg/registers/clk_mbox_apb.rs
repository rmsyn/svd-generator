use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock Mailbox APB register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "clk_mbox_apb",
        "Clock Mailbox APB",
        0x1c4,
        Some(1 << 31),
        None,
    )
}
