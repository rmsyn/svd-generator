use crate::svd::create_cluster;
use crate::Result;

pub mod gpu_root;

/// Creates a StarFive JH7110 SYSCRG Clock GPU registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_gpu",
        "Clock GPU registers",
        0xc,
        &[gpu_root::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
