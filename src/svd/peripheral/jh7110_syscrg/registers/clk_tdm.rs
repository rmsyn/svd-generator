use crate::svd::create_cluster;
use crate::Result;

pub mod ahb;
pub mod apb;
pub mod internal;
pub mod tdm;
pub mod tdm_neg;

/// Creates a StarFive JH7110 SYSCRG Clock TDM registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_tdm",
        "Clock TDM",
        0x2e0,
        &[
            ahb::create()?,
            apb::create()?,
            internal::create()?,
            tdm::create()?,
            tdm_neg::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
