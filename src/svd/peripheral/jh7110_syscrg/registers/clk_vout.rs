use crate::svd::create_cluster;
use crate::Result;

pub mod ahb;
pub mod axi;
pub mod axi_divcfg;
pub mod hdmi_tx0_mclk;
pub mod mipi_phy;
pub mod noc_display_axi;
pub mod src;

/// Creates a StarFive JH7110 SYSCRG Video Output registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_vout",
        "Clock Video Output registers",
        0xe8,
        &[
            src::create()?,
            axi_divcfg::create()?,
            noc_display_axi::create()?,
            ahb::create()?,
            hdmi_tx0_mclk::create()?,
            mipi_phy::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
