use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock Mailbox APB register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "apb",
        "Clock Internal Controller APB",
        0x0,
        Some(1 << 31),
        None,
    )
}
