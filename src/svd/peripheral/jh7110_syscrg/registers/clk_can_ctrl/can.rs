use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock Mailbox CAN register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "can",
        "Clock Internal Controller CAN",
        0x8,
        [63, 8, 8, 8],
        None,
        None,
    )
}
