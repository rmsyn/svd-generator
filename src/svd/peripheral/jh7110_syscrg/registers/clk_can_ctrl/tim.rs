use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock Mailbox TIM register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "tim",
        "Clock Internal Controller Timer",
        0x4,
        [24, 24, 6, 24],
        None,
        None,
    )
}
