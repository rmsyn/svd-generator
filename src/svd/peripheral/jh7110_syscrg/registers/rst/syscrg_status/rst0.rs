use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Software RESET Address Selector SYSCRG Status RESET 0 register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_rst_sel("rst0", "RESET 0", 0x0, 0, Some((0b11 << 21) | 0b1))
}
