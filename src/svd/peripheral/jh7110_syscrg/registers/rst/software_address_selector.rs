use crate::svd::create_cluster;
use crate::Result;

pub mod rst0;
pub mod rst1;
pub mod rst2;
pub mod rst3;

/// Creates a StarFive JH7110 SYSCRG Software RESET Address Selector Software Address Selector registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "software_address_selector",
        "Software RESET Address Selector",
        0x0,
        &[
            rst0::create()?,
            rst1::create()?,
            rst2::create()?,
            rst3::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
