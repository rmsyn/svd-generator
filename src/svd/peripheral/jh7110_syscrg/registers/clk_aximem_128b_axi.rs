use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock AXIMEM 128B AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "clk_aximem_128b_axi",
        "Clock AXIMEM 128B AXI",
        0x158,
        Some(1 << 31),
        None,
    )
}
