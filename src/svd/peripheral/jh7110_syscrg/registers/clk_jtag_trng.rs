use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock JTAG TRNG registers.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_jtag_trng",
        "Clock JTAG TRNG",
        0x2f4,
        [4, 4, 4, 4],
        None,
    )
}
