use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG AXI CFG 0 DEC Main register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "dec_main",
        "Clock AXI Config 0 DEC Main",
        0x4,
        Some(1 << 31),
        None,
    )
}
