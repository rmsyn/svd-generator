use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG AXI CFG 0 DEC HIFI4 register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "hifi4",
        "Clock AXI Config 0 DEC HIFI4",
        0x8,
        Some(1 << 31),
        None,
    )
}
