use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG AXI CFG 0 DEC Main Div register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "main_div",
        "Clock AXI Config 0 DEC Main Divider",
        0x0,
        Some(1 << 31),
        None,
    )
}
