use crate::svd::create_cluster;
use crate::Result;

pub mod core;
pub mod debug;
pub mod rtc_toggle;
pub mod trace;
pub mod trace_com;

/// Creates a StarFive JH7110 SYSCRG U7MC registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_u7mc",
        "Clock U7MC registers",
        0x64,
        &[
            core::create()?,
            debug::create()?,
            rtc_toggle::create()?,
            trace::create()?,
            trace_com::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
