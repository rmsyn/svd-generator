use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock NOC STG AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_noc_stg_axi", "Clock NOC STG AXI", 0x180, None, None)
}
