use crate::svd::create_cluster;
use crate::Result;

pub mod uart02;
pub mod uart35;

/// Creates a StarFive JH7110 SYSCRG Clock I2C APB register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_uart",
        "Clock UART",
        0x244,
        &[uart02::create()?, uart35::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
