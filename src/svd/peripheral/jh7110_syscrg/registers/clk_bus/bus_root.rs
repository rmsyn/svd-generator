use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock Bus Root register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_mux_sel_enum(
        "root",
        "Clock Bus Root",
        0,
        "clk_osc, clk_pll2",
        &[
            create_enum_value("clk_osc", "Select `clk_osc` as the Bus Root clock.", 0b0)?,
            create_enum_value("clk_pll2", "Select `clk_pll2` as the Bus Root clock.", 0b1)?,
        ],
        None,
    )
}
