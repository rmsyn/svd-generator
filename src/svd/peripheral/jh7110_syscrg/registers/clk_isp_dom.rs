use crate::svd::create_cluster;
use crate::Result;

pub mod axi;
pub mod ispcore_2x;
pub mod noc_bus_axi;

/// Creates a StarFive JH7110 SYSCRG ISP DOM registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_isp_dom",
        "Clock ISP DOM registers",
        0xcc,
        &[
            ispcore_2x::create()?,
            axi::create()?,
            noc_bus_axi::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
