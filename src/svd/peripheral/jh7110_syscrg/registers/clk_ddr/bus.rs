use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG DDR Bus register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_mux_sel_enum(
        "bus",
        "clk_ddr_bus",
        0x0,
        "clk_osc_div2, clk_pll1_div2, clk_pll1_div4, clk_pll1_div8",
        &[
            create_enum_value(
                "clk_osc_div2",
                "Select `clk_osc_div2` as the DDR Bus clock.",
                0b00,
            )?,
            create_enum_value(
                "clk_pll1_div2",
                "Select `clk_pll1_div2` as the DDR Bus clock.",
                0b01,
            )?,
            create_enum_value(
                "clk_pll1_div4",
                "Select `clk_pll1_div4` as the DDR Bus clock.",
                0b10,
            )?,
            create_enum_value(
                "clk_pll1_div8",
                "Select `clk_pll1_div8` as the DDR Bus clock.",
                0b11,
            )?,
        ],
        None,
    )
}
