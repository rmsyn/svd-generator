use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock PWMDAC Core register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "core",
        "Clock PWMDAC Core",
        0x4,
        [256, 12, 12, 12],
        None,
        None,
    )
}
