use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Decoder NOC AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("noc_axi", "Clock Video Decoder NOC AXI", 0x1c, None, None)
}
