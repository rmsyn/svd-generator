use crate::svd::create_cluster;
use crate::Result;

pub mod arb;
pub mod main;

/// Creates a StarFive JH7110 SYSCRG Video Decoder JPG registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "jpg",
        "Clock Video Decoder JPG registers",
        0x14,
        &[arb::create()?, main::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
