use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Decoder AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("axi", "Clock Video Decoder AXI", 0x0, [7, 3, 3, 3], None)
}
