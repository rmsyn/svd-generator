use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Decoder WAVE511 BPU register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg("bpu", "Clock WAVE511 BPU", 0x4, [7, 3, 3, 3], None, None)
}
