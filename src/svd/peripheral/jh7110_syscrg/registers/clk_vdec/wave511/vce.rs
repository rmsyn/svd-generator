use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Decoder WAVE511 VCE register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg("vce", "Clock WAVE511 VCE", 0x8, [7, 2, 3, 2], None, None)
}
