use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Decoder JPG ARB register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "arb",
        "Clock Video Decoder JPG ARB",
        0x0,
        Some(1 << 31),
        None,
    )
}
