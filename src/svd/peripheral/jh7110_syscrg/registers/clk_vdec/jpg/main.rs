use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Decoder JPG Main register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "main",
        "Clock Video Decoder JPG Main",
        0x4,
        Some(1 << 31),
        None,
    )
}
