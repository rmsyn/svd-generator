use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock PLL1 Divider 4 register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("clk_pll1_div4", "clk_pll1_div4", 0xa4, [2, 2, 2, 2], None)
}
