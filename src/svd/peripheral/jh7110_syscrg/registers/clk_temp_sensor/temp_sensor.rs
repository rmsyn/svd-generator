use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock Temperature Sensor register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "temp_sensor",
        "Clock Temperature Sensor",
        0x4,
        [24, 24, 24, 24],
        None,
        None,
    )
}
