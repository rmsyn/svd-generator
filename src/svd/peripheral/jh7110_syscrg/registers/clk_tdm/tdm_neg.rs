use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock TDM Negative register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_clk_polarity("tdm_neg", "Clock TDM Negative", 0x10, Some(1 << 30))
}
