use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock TDM register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_mux_sel_enum(
        "tdm",
        "Clock TDM (clock selector)",
        0xc,
        "clk_tdm_internal, clk_tdm_ext",
        &[
            create_enum_value(
                "clk_tdm_internal",
                "Select `clk_tdm_internal` as the TDM clock.",
                0b0,
            )?,
            create_enum_value("clk_tdm_ext", "Select `clk_tdm_ext` as the TDM clock.", 0b1)?,
        ],
        None,
    )
}
