use crate::svd::create_cluster;
use crate::Result;

pub mod apb;
pub mod wdt;

/// Creates a StarFive JH7110 SYSCRG Clock WDT registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_wdt",
        "Clock WDT registers",
        0x1e8,
        &[apb::create()?, wdt::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
