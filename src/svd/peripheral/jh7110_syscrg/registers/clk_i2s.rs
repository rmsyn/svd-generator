use crate::svd::create_cluster;
use crate::Result;

pub mod apb;
pub mod bclk;
pub mod bclk_mst;
pub mod bclk_mst_inv;
pub mod bclk_neg;
pub mod lrck;
pub mod lrck_mst;

/// Creates a StarFive JH7110 SYSCRG Clock I2S registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_i2s",
        "Clock I2S",
        0x284,
        &[
            apb::create()?,
            bclk_mst::create()?,
            bclk_mst_inv::create()?,
            lrck_mst::create()?,
            bclk::create()?,
            bclk_neg::create()?,
            lrck::create()?,
        ],
        Some(
            svd::DimElement::builder()
                .dim(3)
                .dim_increment(0x1c)
                .dim_index(Some(["tx_u0", "tx_u1", "rx"].map(String::from).into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Cluster)
}
