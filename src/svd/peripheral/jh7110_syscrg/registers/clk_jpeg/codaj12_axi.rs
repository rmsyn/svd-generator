use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG JPEG CODAJ12 AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("codaj12_axi", "Clock CODAJ12 AXI", 0x4, None, None)
}
