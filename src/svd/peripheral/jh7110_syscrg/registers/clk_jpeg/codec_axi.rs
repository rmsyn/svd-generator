use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG JPEG Codec AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "codec_axi",
        "Clock JPEG Codec AXI",
        0x0,
        [16, 6, 6, 6],
        None,
    )
}
