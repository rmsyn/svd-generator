use crate::svd::create_cluster;
use crate::Result;

pub mod inner;
pub mod mclk;
pub mod out;

/// Creates a StarFive JH7110 SYSCRG MCLK registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_mclk",
        "Clock MCLK registers",
        0x44,
        &[inner::create()?, mclk::create()?, out::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
