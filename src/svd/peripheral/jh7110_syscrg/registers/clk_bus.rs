use crate::svd::create_cluster;
use crate::Result;

pub mod bus_root;

/// Creates a StarFive JH7110 SYSCRG Clock Bus registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_bus",
        "Clock Bus registers",
        0x14,
        &[bus_root::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
