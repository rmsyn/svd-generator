use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG U7MC RTC Toggle register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "u7mc_rtc_toggle",
        "U7MC RTC Toggle",
        0x7c,
        [6, 6, 6, 6],
        None,
    )
}
