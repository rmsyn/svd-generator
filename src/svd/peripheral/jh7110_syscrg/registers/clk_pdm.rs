use crate::svd::create_cluster;
use crate::Result;

pub mod apb;
pub mod dmic;

/// Creates a StarFive JH7110 SYSCRG Clock PDM registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_pdm",
        "Clock PDM",
        0x2d8,
        &[dmic::create()?, apb::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
