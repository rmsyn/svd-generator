use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG ISP 2X register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_mux_sel_divcfg_enum(
        "isp_2x",
        "Clock ISP 2x",
        0x0,
        "clk_pll2, clk_pll1",
        &[
            create_enum_value("clk_pll2", "Select `clk_pll2` as the ISP 2x clock.", 0b0)?,
            create_enum_value("clk_pll1", "Select `clk_pll1` as the ISP 2x clock.", 0b1)?,
        ],
        [8, 2, 2, 2],
    )
}
