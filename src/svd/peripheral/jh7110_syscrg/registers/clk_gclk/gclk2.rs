use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock ISP AXI GCLK0 register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "clk_gclk2",
        "Clock GCLK 2",
        0x8,
        [62, 12, 12, 12],
        Some((1 << 31) | 12),
        None,
    )
}
