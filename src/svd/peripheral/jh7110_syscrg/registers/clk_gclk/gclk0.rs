use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock ISP AXI GCLK0 register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "clk_gclk0",
        "Clock GCLK 0",
        0x0,
        [62, 20, 16, 20],
        Some((1 << 31) | 20),
        None,
    )
}
