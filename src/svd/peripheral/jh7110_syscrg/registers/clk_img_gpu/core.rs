use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG IMG GPU Core register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("core", "clk_gpu_core", 0x4, None, None)
}
