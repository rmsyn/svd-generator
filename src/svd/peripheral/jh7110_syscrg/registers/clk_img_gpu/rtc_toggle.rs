use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG IMG GPU RTC Toggle register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "rtc_toggle",
        "clk_u0_img_gpu_rtc_toggle",
        0x10,
        [12, 12, 12, 12],
        None,
        None,
    )
}
