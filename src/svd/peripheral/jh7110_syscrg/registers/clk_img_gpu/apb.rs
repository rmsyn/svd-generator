use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG IMG GPU APB register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("apb", "clk_u0_img_gpu_apb_clk", 0xc, None, None)
}
