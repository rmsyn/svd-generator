use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG IMG GPU SYS register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("sys", "clk_u0_img_gpu_sys_clk", 0x8, None, None)
}
