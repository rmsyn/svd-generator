use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG IMG GPU DIVCFG register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "divcfg",
        "Clock IMG GPU Core DIVCFG",
        0x0,
        [7, 3, 3, 3],
        None,
    )
}
