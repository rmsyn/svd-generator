use crate::svd::create_cluster;
use crate::Result;

pub mod apb;
pub mod temp_sensor;

/// Creates a StarFive JH7110 SYSCRG Clock Temperature Sensor registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "clk_temp_sensor",
        "Clock Temperature registers",
        0x204,
        &[apb::create()?, temp_sensor::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
