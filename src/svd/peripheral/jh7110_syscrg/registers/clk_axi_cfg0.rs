use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock AXI Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_axi_cfg0",
        "Clock AXI Configuration",
        0x1c,
        [3, 3, 3, 3],
        None,
    )
}
