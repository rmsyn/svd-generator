use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Encoder AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("axi", "Clock Video Encoder AXI", 0x0, [15, 5, 5, 5], None)
}
