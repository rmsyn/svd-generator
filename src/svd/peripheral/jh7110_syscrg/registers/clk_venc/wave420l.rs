use crate::svd::create_cluster;
use crate::Result;

pub mod apb;
pub mod axi;
pub mod bpu;
pub mod vce;

/// Creates a StarFive JH7110 SYSCRG Video Encoder WAVE420Lregisters.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "wave420l",
        "Clock Video Encoder WAVE420L registers",
        0x4,
        &[
            axi::create()?,
            bpu::create()?,
            vce::create()?,
            apb::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
