use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Encoder WAVE420L AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("axi", "Clock WAVE420L AXI", 0x0, None, None)
}
