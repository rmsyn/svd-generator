use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Encoder WAVE420L BPU register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg("bpu", "Clock WAVE420L BPU", 0x4, [15, 5, 5, 5], None, None)
}
