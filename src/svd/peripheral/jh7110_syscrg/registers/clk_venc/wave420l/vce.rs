use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Encoder WAVE420L VCE register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "wave420l_vce",
        "Clock WAVE420L VCE",
        0x8,
        [15, 5, 5, 5],
        None,
        None,
    )
}
