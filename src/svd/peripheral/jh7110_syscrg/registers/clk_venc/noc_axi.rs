use crate::svd::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Video Encoder NOC AXI register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("noc_axi", "Clock Video Encoder NOC AXI", 0x14, None, None)
}
