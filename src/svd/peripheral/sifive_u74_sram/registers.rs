use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the SiFive U74(MC) SRAM (L2 LIM) registers.
pub fn create(size: u32) -> Result<Vec<svd::RegisterCluster>> {
    create_register(
        "word",
        "SiFive U74(MC) SRAM (L2 LIM) word",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "word",
            "SiFive U74(MC) SRAM (L2 LIM) word",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(size.saturating_div(4))
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(|r| vec![svd::RegisterCluster::Register(r)])
}
