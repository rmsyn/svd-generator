use crate::svd::create_peripheral;
use crate::Result;

pub mod registers;

/// Represents a SiFive U74(MC) SRAM (L2 LIM) core memory region.
pub struct SiFiveU74Sram {
    peripheral: svd::Peripheral,
}

impl SiFiveU74Sram {
    /// Creates a new [SiFiveU74Sram] peripheral.
    pub fn create(
        name: &str,
        base_address: u64,
        size: u32,
        interrupt: Option<Vec<svd::Interrupt>>,
        dim: u32,
    ) -> Result<Self> {
        let dim_element = if dim > 0 {
            Some(
                svd::DimElement::builder()
                    .dim(dim)
                    .dim_increment(size)
                    .build(svd::ValidateLevel::Strict)?,
            )
        } else {
            None
        };

        let peripheral = create_peripheral(
            name,
            format!("SiFive U74(MC) SRAM (L2 LIM): {name}").as_str(),
            base_address,
            size,
            interrupt,
            Some(registers::create(size)?),
            dim_element,
            Some("SRAM".into()),
        )?;

        Ok(Self { peripheral })
    }

    /// Gets a reference to the SVD [`Peripheral`](svd::Peripheral).
    pub const fn peripheral(&self) -> &svd::Peripheral {
        &self.peripheral
    }

    /// Gets a mutable reference to the SVD [`Peripheral`](svd::Peripheral).
    pub fn peripheral_mut(&mut self) -> &mut svd::Peripheral {
        &mut self.peripheral
    }

    /// Converts the [SiFiveU74Sram] into the inner SVD [`Peripheral`](svd::Peripheral).
    pub fn to_inner(self) -> svd::Peripheral {
        self.peripheral
    }
}
