use crate::svd::create_peripheral;
use crate::Result;

pub mod registers;

/// Represents the Synopsys Databook Version of the DWGMAC peripheral.
#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum DwGmacVersion {
    /// Version 3.40
    Version340 = 0x34,
    /// Version 3.50
    Version350 = 0x35,
    /// Version 4.00
    Version400 = 0x40,
    /// Version 4.10
    Version410 = 0x41,
    /// Version 5.00
    Version500 = 0x50,
    /// Version 5.10
    Version510 = 0x51,
    /// Version 5.20
    Version520 = 0x52,
}

impl From<&str> for DwGmacVersion {
    fn from(val: &str) -> Self {
        if val.contains("3.4") {
            Self::Version340
        } else if val.contains("3.5") {
            Self::Version350
        } else if val.contains("4.0") {
            Self::Version400
        } else if val.contains("4.1") {
            Self::Version410
        } else if val.contains("5.0") {
            Self::Version500
        } else if val.contains("5.1") {
            Self::Version510
        } else if val.contains("5.2") {
            Self::Version520
        } else {
            // Use the latest 3.xx version as a sane default.
            // Not all DeviceTree will include the version in the "compatible" property.
            Self::Version350
        }
    }
}

/// Represents a Synopsys DesignWare Gigabit Ethernet MAC (compatible) peripheral
pub struct DwGmac {
    peripheral: svd::Peripheral,
}

impl DwGmac {
    /// Creates a new [DwGmac] peripheral.
    pub fn create(
        name: &str,
        base_address: u64,
        size: u32,
        interrupt: Option<Vec<svd::Interrupt>>,
        version: DwGmacVersion,
        dim: u32,
    ) -> Result<Self> {
        let dim_element = if dim > 0 {
            Some(
                svd::DimElement::builder()
                    .dim(dim)
                    .dim_increment(size)
                    .build(svd::ValidateLevel::Strict)?,
            )
        } else {
            None
        };

        let peripheral = create_peripheral(
            name,
            format!("Synopsys DesignWare Gigabit Ethernet MAC: {name}").as_str(),
            base_address,
            size,
            interrupt,
            Some(registers::create(version)?),
            dim_element,
            Some("MAC".into()),
        )?;

        Ok(Self { peripheral })
    }

    /// Gets a reference to the SVD [`Peripheral`](svd::Peripheral).
    pub const fn peripheral(&self) -> &svd::Peripheral {
        &self.peripheral
    }

    /// Gets a mutable reference to the SVD [`Peripheral`](svd::Peripheral).
    pub fn peripheral_mut(&mut self) -> &mut svd::Peripheral {
        &mut self.peripheral
    }

    /// Converts the [DwGmac] into the inner SVD [`Peripheral`](svd::Peripheral).
    pub fn to_inner(self) -> svd::Peripheral {
        self.peripheral
    }
}
