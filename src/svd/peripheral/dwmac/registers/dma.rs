use crate::svd::create_cluster;
use crate::svd::register::dwmac_dma;
use crate::Result;

pub mod bus_mode;
pub mod control;
pub mod missed_frame_ctr;

/// Creates a Synopsys DesignWare 10/100 Ethernet DMA register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "dma",
        "MAC DMA registers",
        0x1000,
        &[
            bus_mode::create()?,
            dwmac_dma::poll_demand::create()?,
            dwmac_dma::base_addr::create()?,
            dwmac_dma::status::create()?,
            control::create()?,
            dwmac_dma::intr_ena::create()?,
            missed_frame_ctr::create()?,
        ],
        None,
    )?))
}
