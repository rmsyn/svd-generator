use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates a Synopsys DesignWare 10/100 Ethernet DMA Missed Frame Counter register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "missed_frame_ctr",
        "DMA Missed Frame Counter",
        0x20,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "m_cntr",
                "Missed Frame Counter",
                create_bit_range("[15:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ove_m",
                "Missed Frame Overflow",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ove_cntr",
                "Overflow Frame Counter",
                create_bit_range("[27:17]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ove",
                "FIFO Overflow",
                create_bit_range("[28:28]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
