use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates a Synopsys DesignWare 10/100 Ethernet DMA Control register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "control",
        "DMA Control",
        0x18,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "sr",
                "Start/Stop Receive",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "osf",
                "Operate on Second Frame",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "se",
                "Stop On Empty",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "st",
                "Start/Stop Transmission",
                create_bit_range("[13:13]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "ttc",
                "Transmit Threshold Control",
                create_bit_range("[15:14]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("ttc32_18", "ttc_lo = 0 (bit 22): Threshold is 32 DWORDS, ttc_lo = 1 (bit 22): Threshold is 18 DWORDS.", 0b00)?,
                    create_enum_value("ttc64_24", "ttc_lo = 0 (bit 22): Threshold is 64 DWORDS, ttc_lo = 1 (bit 22): Threshold is 24 DWORDS.", 0b01)?,
                    create_enum_value("ttc128_32", "ttc_lo = 0 (bit 22): Threshold is 128 DWORDS, ttc_lo = 1 (bit 22): Threshold is 32 DWORDS.", 0b10)?,
                    create_enum_value("ttc256_40", "ttc_lo = 0 (bit 22): Threshold is 256 DWORDS, ttc_lo = 1 (bit 22): Threshold is 40 DWORDS.", 0b11)?,
                ])?],
                None,
            )?,
            create_field(
                "ftf",
                "Flush Transmit FIFO",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "sf",
                "Store and Forward",
                create_bit_range("[21:21]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ttc_lo",
                "Interpret TTC levels as their lower values.",
                create_bit_range("[22:22]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
