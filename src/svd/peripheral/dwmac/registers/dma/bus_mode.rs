use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates a Synopsys DesignWare 10/100 Ethernet DMA Bus Mode register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "bus_mode",
        "DMA Bus Mode",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "sft_reset",
                "Software Reset",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "bar_bus",
                "Bar-Bus Arbitration",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "dsl",
                "Descriptor Skip Length",
                create_bit_range("[6:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ble",
                "Big/Little Endian Mode",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "pbl",
                "Programmable Burst Length",
                create_bit_range("[13:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "dbo",
                "Descriptor Byte Ordering",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
