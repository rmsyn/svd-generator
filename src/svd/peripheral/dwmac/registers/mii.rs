use crate::svd::{
    create_bit_range, create_cluster, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a Synopsys DesignWare 10/100 Ethernet Multicast Hash Table register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "mii",
        "MAC MII registers",
        0x14,
        &[
            svd::RegisterCluster::Register(create_register(
                "addr",
                "MII Address",
                0x0,
                create_register_properties(32, 0)?,
                Some(&[
                    create_field(
                        "busy",
                        "MII Busy",
                        create_bit_range("[0:0]")?,
                        svd::Access::ReadWrite,
                        None,
                    )?,
                    create_field(
                        "write",
                        "MII Write",
                        create_bit_range("[1:1]")?,
                        svd::Access::ReadWrite,
                        None,
                    )?,
                ]),
                None,
            )?),
            svd::RegisterCluster::Register(create_register(
                "data",
                "MII Data",
                0x4,
                create_register_properties(32, 0)?,
                Some(&[create_field(
                    "data",
                    "MII Data",
                    create_bit_range("[31:0]")?,
                    svd::Access::ReadWrite,
                    None,
                )?]),
                None,
            )?),
        ],
        None,
    )?))
}
