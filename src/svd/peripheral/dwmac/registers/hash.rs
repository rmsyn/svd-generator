use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates a Synopsys DesignWare 10/100 Ethernet Multicast Hash Table register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "hash",
        "Multicast Hash Table",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "hash",
            "Multicast Hash Table",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some([String::from("_high"), String::from("_low")].into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
