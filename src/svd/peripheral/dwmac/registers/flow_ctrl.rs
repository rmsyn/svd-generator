use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates a Synopsys DesignWare 10/100 Ethernet MAC Flow Control register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "flow_ctrl",
        "Flow Control",
        0x1c,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "pause",
                "Flow Control Busy",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "enable",
                "Flow Control Enable",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "pass",
                "Pass Control Frames",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "pt",
                "Pause Time",
                create_bit_range("[31:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
