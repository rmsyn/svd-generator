use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates a Synopsys DesignWare 10/100 Ethernet MAC Address register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "addr",
        "MAC Address",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "addr",
            "MAC Address",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some([String::from("_high"), String::from("_low")].into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
