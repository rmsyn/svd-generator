use crate::Result;

pub mod addr;
pub mod control;
pub mod dma;
pub mod flow_ctrl;
pub mod hash;
pub mod mii;
pub mod vlan;

/// Creates Synopsys DesignWare 10/100 Ethernet MAC register definitions.
///
/// Based on the register definitions from the Linux driver: <https://elixir.bootlin.com/linux/latest/source/drivers/net/ethernet/stmicro/stmmac/dwmac100.h>
///
/// Original Author: Giuseppe Cavallaro <peppe.cavallaro@st.com>
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        control::create()?,
        addr::create()?,
        hash::create()?,
        mii::create()?,
        flow_ctrl::create()?,
        vlan::create()?,
        dma::create()?,
    ]
    .into())
}
