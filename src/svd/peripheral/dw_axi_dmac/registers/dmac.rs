use crate::svd::register::create_cluster;
use crate::Result;

pub mod cfg;
pub mod chabort;
pub mod chen;
pub mod chen2;
pub mod chsusp;
pub mod common;
pub mod compver;
pub mod id;
pub mod intstatus;
pub mod intstatus2;
pub mod lowpower_cfg;
pub mod reserved;
pub mod reset;

/// Creates Synopsys DesignWare AXI DMAC registers.
pub fn create(dma_channels: u64) -> Result<svd::RegisterCluster> {
    let chen = match dma_channels {
        ch if ch <= 8 => chen::create(ch)?,
        ch => chen2::create(ch)?,
    };

    let intstatus = match dma_channels {
        ch if ch <= 8 => intstatus::create(ch)?,
        ch => intstatus2::create(ch)?,
    };

    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "dmac",
        "DesignWare DMAC registers",
        0x0,
        &[
            id::create()?,
            compver::create()?,
            cfg::create()?,
            chen,
            chsusp::create(dma_channels)?,
            chabort::create(dma_channels)?,
            intstatus,
            common::create()?,
            reset::create()?,
            lowpower_cfg::create()?,
            reserved::create()?,
        ],
        None,
    )?))
}
