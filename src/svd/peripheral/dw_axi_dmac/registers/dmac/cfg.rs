use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cfg",
        "DMAC Configuration register contains the DMAC config settings.",
        0x10,
        create_register_properties(64, 0)?,
        Some(&[
            create_field(
                "en",
                "DMAC Enable value - 0: disable DMAC, 1: enable DMAC",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ie",
                "DMAC Interrupt Enable value - 0: disable interrupt, 1: enable interrupt",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
