use crate::svd::register::{
    create_bit_range, create_cluster, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Common registers.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "common",
        "DesignWare DMAC Common registers",
        0x38,
        &[
            svd::RegisterCluster::Register(create_register(
                "intclear",
                "DMAC Interrupt Clear register contains the DMAC interrupt clear settings.",
                0x0,
                create_register_properties(64, 0)?,
                Some(&[
                    create_field(
                        "slv_if",
                        "DMAC Channel Interrupt Clear Slave Interface - 0: no-op, 1: clear interrupt",
                        create_bit_range("[0:0]")?,
                        svd::Access::WriteOnly,
                        Some(svd::DimElement::builder()
                            .dim(9)
                            .dim_increment(1)
                            .dim_index(Some([
                                String::from("_dec_err"),
                                String::from("_wr2_ro_err"),
                                String::from("_rd2_wo_err"),
                                String::from("_wron_hold_err"),
                                String::from("_rsvd0"),
                                String::from("_rsvd1"),
                                String::from("_rsvd2"),
                                String::from("_rsvd3"),
                                String::from("_undef_reg_dec_err"),
                            ].into()))
                            .build(svd::ValidateLevel::Strict)?),
                    )?,
                ]),
                None,
            )?),
            svd::RegisterCluster::Register(create_register(
                "intstatus_enable",
                "DMAC Interrupt Status Enable register contains the DMAC interrupt status enable settings.",
                0x8,
                create_register_properties(64, 0)?,
                Some(&[
                    create_field(
                        "slv_if",
                        "DMAC Channel Interrupt Status Enable Slave Interface - 0: disable interrupt status, 1: enable interrupt status",
                        create_bit_range("[0:0]")?,
                        svd::Access::ReadWrite,
                        Some(svd::DimElement::builder()
                            .dim(9)
                            .dim_increment(1)
                            .dim_index(Some([
                                String::from("_dec_err"),
                                String::from("_wr2_ro_err"),
                                String::from("_rd2_wo_err"),
                                String::from("_wron_hold_err"),
                                String::from("_rsvd0"),
                                String::from("_rsvd1"),
                                String::from("_rsvd2"),
                                String::from("_rsvd3"),
                                String::from("_undef_reg_dec_err"),
                            ].into()))
                            .build(svd::ValidateLevel::Strict)?),
                    )?,
                ]),
                None,
            )?),
            svd::RegisterCluster::Register(create_register(
                "intsignal_enable",
                "DMAC Interrupt Signal Enable register contains the DMAC interrupt signal enable settings.",
                0x10,
                create_register_properties(64, 0)?,
                Some(&[
                    create_field(
                        "slv_if",
                        "DMAC Channel Interrupt Signal Enable Slave Interface - 0: disable interrupt signal, 1: enable interrupt signal",
                        create_bit_range("[0:0]")?,
                        svd::Access::ReadWrite,
                        Some(svd::DimElement::builder()
                            .dim(9)
                            .dim_increment(1)
                            .dim_index(Some([
                                String::from("_dec_err"),
                                String::from("_wr2_ro_err"),
                                String::from("_rd2_wo_err"),
                                String::from("_wron_hold_err"),
                                String::from("_rsvd0"),
                                String::from("_rsvd1"),
                                String::from("_rsvd2"),
                                String::from("_rsvd3"),
                                String::from("_undef_reg_dec_err"),
                            ].into()))
                            .build(svd::ValidateLevel::Strict)?),
                    )?,
                ]),
                None,
            )?),
            svd::RegisterCluster::Register(create_register(
                "intstatus",
                "DMAC Interrupt Status register contains the DMAC interrupt status.",
                0x18,
                create_register_properties(64, 0)?,
                Some(&[
                    create_field(
                        "slv_if",
                        "DMAC Channel Interrupt Status Slave Interface - 0: no interrupt, 1: interrupt active",
                        create_bit_range("[0:0]")?,
                        svd::Access::ReadOnly,
                        Some(svd::DimElement::builder()
                            .dim(9)
                            .dim_increment(1)
                            .dim_index(Some([
                                String::from("_dec_err"),
                                String::from("_wr2_ro_err"),
                                String::from("_rd2_wo_err"),
                                String::from("_wron_hold_err"),
                                String::from("_rsvd0"),
                                String::from("_rsvd1"),
                                String::from("_rsvd2"),
                                String::from("_rsvd3"),
                                String::from("_undef_reg_dec_err"),
                            ].into()))
                            .build(svd::ValidateLevel::Strict)?),
                    )?,
                ]),
                None,
            )?),
        ],
        None,
    )?))
}
