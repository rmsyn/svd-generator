use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Interrupt Status 2 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "reset",
        "DMAC Channel Interrupt Status register contains the DMAC channel interrupt status",
        0x58,
        create_register_properties(64, 0)?,
        Some(&[
            create_field(
                "rst",
                "DMAC Reset - 0: no-op, 1: request reset. **NOTE** Software is not allowed to write 0 to this bit.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
