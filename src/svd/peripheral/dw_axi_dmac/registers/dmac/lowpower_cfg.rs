use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Low Power Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "lowpower_cfg",
        "DMAC Low Power Configuration register.",
        0x60,
        create_register_properties(64, 0)?,
        Some(&[
            create_field(
                "clsp_en",
                "DMAC Context Sensitive Low Power feature enable - 0: disable, 1: enable. GBL: Global, CHNL: Channel, SBIU: SBIU, MXI: AXI Master Interface",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(4)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_gbl"),
                        String::from("_chnl"),
                        String::from("_sbiu"),
                        String::from("_mxif"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field_constraint(
                "lpdly",
                "DMAC Low Power Delay counter. GLCH: Global and DMA channel, SBIU: SBIU, MXI: AXI Master Interface",
                create_bit_range("[39:32]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                Some(svd::DimElement::builder()
                    .dim(3)
                    .dim_increment(8)
                    .dim_index(Some([
                        String::from("_glch"),
                        String::from("_sbiu"),
                        String::from("_mxif"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    )?))
}
