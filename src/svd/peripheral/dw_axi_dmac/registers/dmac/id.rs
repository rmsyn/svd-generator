use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC ID register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "id",
        "DMAC ID register contains the 64-bit identification value.",
        0x0,
        create_register_properties(64, 0)?,
        Some(&[create_field(
            "id",
            "DMAC ID value",
            create_bit_range("[63:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
