use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Interrupt Status 2 register.
pub fn create(dma_channels: u64) -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "intstatus2",
        "DMAC Channel Interrupt Status register contains the DMAC channel interrupt status. Only exists when DMAX_NUM_CHANNELS > 8",
        0x30,
        create_register_properties(64, 0)?,
        Some(&[
            create_field(
                "intstat_ch",
                "DMAC Channel Interrupt Status - 0: interrupt inactive, 1: interrupt active",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                Some(svd::DimElement::builder()
                    .dim(dma_channels as u32)
                    .dim_increment(1)
                    .dim_index(Some((1..=dma_channels).map(|s| format!("{s}")).collect::<Vec<String>>()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "common_intstat",
                "DMAC Common Interrupt Status - 0: interrupt inactive, 1: interrupt active",
                create_bit_range("[32:32]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
