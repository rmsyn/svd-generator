use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Component Version register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "compver",
        "DMAC Component Version register contains the 32-bit component version.",
        0x8,
        create_register_properties(64, 0)?,
        Some(&[create_field(
            "compver",
            "DMAC Component Version value",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
