use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Linked List Pointer register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "llp",
        "Linked list pointer register.",
        0x28,
        create_register_properties(64, 0)?,
        Some(&[
            create_field_enum(
                "lms",
                "LLI master select - 0: AXI Master 1, 1: AXI Master 2.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("axi_master1", "Select LLI AXI master 1", 0)?,
                    create_enum_value("axi_master2", "Select LLI AXI master 2", 1)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "loc",
                "Starting address memory of LLI block - **NOTE** the lower six bits are unassigned because addresses are assumed to be 64-byte aligned.",
                create_bit_range("[63:6]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3ff_ffff_ffff_ffff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
