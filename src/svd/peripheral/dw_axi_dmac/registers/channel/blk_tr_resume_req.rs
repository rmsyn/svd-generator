use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Channel Block Transfer Resume Request register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "blk_tr_resume_req",
        "Block Transfer Resume Request register.",
        0x48,
        create_register_properties(64, 0)?,
        Some(&[
            create_field(
                "blk_tr_resume_req",
                "Block Transfer Resume Request during Linked-List or Shadow-Register-based multi-block transfer - 0: no request, 1: request",
                create_bit_range("[0:0]")?,
                svd::Access::WriteOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
