use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Destination Address register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "dar",
        "DMAC Channel Destination address of DMA transfer.",
        0x08,
        create_register_properties(64, 0)?,
        Some(&[create_field_constraint(
            "dar",
            "Destination address of DMA transfer",
            create_bit_range("[63:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff_ffff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
