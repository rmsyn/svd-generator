use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Channel Status register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "status",
        "DMAC Channel Status register.",
        0x30,
        create_register_properties(64, 0)?,
        Some(&[
            create_field(
                "cmpltd_blk_tr_size",
                "Completed Block Transfer Size.",
                create_bit_range("[21:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "data_left_in_fifo",
                "Data left in FIFO",
                create_bit_range("[46:32]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
