use crate::svd::register::{
    create_bit_range, create_cluster, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Channel Interrupt registers.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "int",
        "Channel Interrupt registers.",
        0x80,
        &[
            create_int_register(
                "status_enable",
                "Channel Interrupt Status Enable",
                0x0,
                svd::Access::ReadWrite,
            )?,
            create_int_register(
                "status",
                "Channel Interrupt Status",
                0x8,
                svd::Access::ReadOnly,
            )?,
            create_int_register(
                "signal_enable",
                "Channel Interrupt Signal Enable",
                0x10,
                svd::Access::ReadWrite,
            )?,
            create_int_register(
                "clear",
                "Channel Interrupt Clear",
                0x18,
                svd::Access::WriteOnly,
            )?,
        ],
        None,
    )?))
}

fn create_int_register(
    name: &str,
    desc: &str,
    addr_offset: u32,
    access: svd::Access,
) -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        name,
        desc,
        addr_offset,
        create_register_properties(64, 0xffff_ffff_ffff_ffff)?,
        Some(&[
            create_field(
                "blk_tr_done",
                "Channel Block Transfer Done - 0: disabled, 1: enabled",
                create_bit_range("[0:0]")?,
                access,
                None,
            )?,
            create_field(
                "dma_tr_done",
                "Channel DMA Transfer Done - 0: disabled, 1: enabled",
                create_bit_range("[1:1]")?,
                access,
                None,
            )?,
            create_field(
                "transcomp",
                "Channel Transfer Complete - 0: disabled, 1: enabled",
                create_bit_range("[3:3]")?,
                access,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_src"),
                        String::from("_dst"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "dec_err",
                "Channel Decoding Error - 0: disabled, 1: enabled",
                create_bit_range("[5:5]")?,
                access,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_src"),
                        String::from("_dst"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "slv_err",
                "Channel Slave Error - 0: disabled, 1: enabled",
                create_bit_range("[7:7]")?,
                access,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_src"),
                        String::from("_dst"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "lli_dec_err",
                "Channel Linked List Item Decoding Error - 0: disabled, 1: enabled",
                create_bit_range("[9:9]")?,
                access,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_rd"),
                        String::from("_wr"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "lli_slv_err",
                "Channel Linked List Item Slave Error - 0: disabled, 1: enabled",
                create_bit_range("[11:11]")?,
                access,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_rd"),
                        String::from("_wr"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "shadow_or_invalid_lli_err",
                "Channel Shadow Register or Linked List Item Invalid Error - 0: disabled, 1: enabled",
                create_bit_range("[13:13]")?,
                access,
                None,
            )?,
            create_field(
                "slvif_multiblktype_err",
                "Channel Slave Interface Multi Block Type Error - 0: disabled, 1: enabled",
                create_bit_range("[14:14]")?,
                access,
                None,
            )?,
            create_field(
                "slvif_dec_err",
                "Channel Slave Interface Decoding Error - 0: disabled, 1: enabled",
                create_bit_range("[16:16]")?,
                access,
                None,
            )?,
            create_field(
                "slvif_wr2ro_err",
                "Channel Slave Interface Write to Read-only Error - 0: disabled, 1: enabled",
                create_bit_range("[17:17]")?,
                access,
                None,
            )?,
            create_field(
                "slvif_r2wro_err",
                "Channel Slave Interface Read to Write-only Error - 0: disabled, 1: enabled",
                create_bit_range("[18:18]")?,
                access,
                None,
            )?,
            create_field(
                "slvif_wronchen_err",
                "Channel Slave Interface Write On Channel Enabled Error - 0: disabled, 1: enabled",
                create_bit_range("[19:19]")?,
                access,
                None,
            )?,
            create_field(
                "slvif_shadow_wron_valid_err",
                "Channel Slave Interface Shadow Register Write On Valid Error - 0: disabled, 1: enabled",
                create_bit_range("[20:20]")?,
                access,
                None,
            )?,
            create_field(
                "slvif_wron_hold_err",
                "Channel Slave Interface Write On Hold Error - 0: disabled, 1: enabled",
                create_bit_range("[21:21]")?,
                access,
                None,
            )?,
            create_field(
                "ch_lock_cleared",
                "Channel Lock Cleared - 0: disabled, 1: enabled",
                create_bit_range("[27:27]")?,
                access,
                None,
            )?,
            create_field(
                "ch_src_suspended",
                "Channel Source Suspended - 0: disabled, 1: enabled",
                create_bit_range("[28:28]")?,
                access,
                None,
            )?,
            create_field(
                "ch_suspended",
                "Channel Suspended - 0: disabled, 1: enabled",
                create_bit_range("[29:29]")?,
                access,
                None,
            )?,
            create_field(
                "ch_disabled",
                "Channel Disabled - 0: disabled, 1: enabled",
                create_bit_range("[30:30]")?,
                access,
                None,
            )?,
            create_field(
                "ch_aborted",
                "Channel Aborted - 0: disabled, 1: enabled",
                create_bit_range("[31:31]")?,
                access,
                None,
            )?,
        ]),
        None,
    )?))
}
