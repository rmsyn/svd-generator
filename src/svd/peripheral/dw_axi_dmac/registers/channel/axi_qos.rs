use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Channel AXI ID register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "axi_qos",
        "Channel AXI QOS register - **NOTE** this register is only allowed to be modified when the channel is disabled.",
        0x58,
        create_register_properties(64, 0)?,
        Some(&[
            create_field_constraint(
                "qos",
                "AXI QOS suffix",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(4)
                    .dim_index(Some([
                        String::from("_aw"),
                        String::from("_ar"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    )?))
}
