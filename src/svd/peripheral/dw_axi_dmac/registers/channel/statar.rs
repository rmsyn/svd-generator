use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Channel Status Fetch Address registers.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "statar",
        "Channel Status Fetch Address register.",
        0x70,
        create_register_properties(64, 0)?,
        Some(&[create_field(
            "statar",
            "Channel Status",
            create_bit_range("[63:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x8)
                .dim_index(Some([String::from("_src"), String::from("_dst")].into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
