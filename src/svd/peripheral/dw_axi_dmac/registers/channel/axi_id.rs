use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Channel AXI ID register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "axi_id",
        "Channel AXI ID register.",
        0x50,
        create_register_properties(64, 0)?,
        Some(&[create_field_constraint(
            "suffix",
            "AXI ID suffix",
            create_bit_range("[15:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff)?,
            Some(
                svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(16)
                    .dim_index(Some([String::from("_read"), String::from("_write")].into()))
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        None,
    )?))
}
