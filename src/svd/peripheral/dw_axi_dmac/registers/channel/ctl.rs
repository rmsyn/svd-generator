use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare AXI DMAC Channel Control register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ctl",
        "DMAC Channel Control.",
        0x18,
        create_register_properties(64, 0)?,
        Some(&[
            create_field_enum(
                "sms",
                "Source Master Select - 0: AXI Master 1, 1: AXI Master 2",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("axi_master1", "Select AXI master 1", 0b0)?,
                    create_enum_value("axi_master2", "Select AXI master 2", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "dms",
                "Destination Master Select - 0: AXI Master 1, 1: AXI Master 2",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("axi_master1", "Select AXI master 1", 0b0)?,
                    create_enum_value("axi_master2", "Select AXI master 2", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "sinc",
                "Source address increment - 0: increment, 1: no change. Indicates whether to increment the address on every transfer.",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("increment", "Enable source address increment on every transfer", 0b0)?,
                    create_enum_value("no_change", "Do not enable source address increment on every transfer", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "dinc",
                "Destination address increment - 0: increment, 1: no change. Indicates whether to increment the address on every transfer.",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("increment", "Enable source address increment on every transfer", 0b0)?,
                    create_enum_value("no_change", "Do not enable source address increment on every transfer", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "tr_width",
                "Transfer width - 0: 8-bits, 1: 16-bits, 2: 32-bits, 3: 64-bits, 4: 128-bits, 5: 256-bits, 6: 512-bits.",
                create_bit_range("[10:8]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("bits8", "8-bit transfer width", 0)?,
                    create_enum_value("bits16", "16-bit transfer width", 1)?,
                    create_enum_value("bits32", "32-bit transfer width", 2)?,
                    create_enum_value("bits64", "64-bit transfer width", 3)?,
                    create_enum_value("bits128", "128-bit transfer width", 4)?,
                    create_enum_value("bits256", "256-bit transfer width", 5)?,
                    create_enum_value("bits512", "512-bit transfer width", 6)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(3)
                    .dim_index(Some([
                        String::from("_src"),
                        String::from("_dst"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field_enum(
                "msize",
                "Burst transaction length - 0: 1 data item, 1: 4 data items, 2: 8 data items, 3: 16 data items, 4: 32 data items, 5: 64 data items, 6: 128 data items, 7: 256 data items, 8: 512 data items, 9: 1024 data items.",
                create_bit_range("[17:14]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("items1", "1 data item", 0)?,
                    create_enum_value("items4", "4 data items", 1)?,
                    create_enum_value("items8", "8 data items", 2)?,
                    create_enum_value("items16", "16 data items", 3)?,
                    create_enum_value("items32", "32 data items", 4)?,
                    create_enum_value("items64", "64 data items", 5)?,
                    create_enum_value("items128", "128 data items", 6)?,
                    create_enum_value("items256", "256 data items", 7)?,
                    create_enum_value("items512", "512 data items", 8)?,
                    create_enum_value("items1024", "1024 data items", 9)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(4)
                    .dim_index(Some([
                        String::from("_src"),
                        String::from("_dst"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field_constraint(
                "cache",
                "AXI cache signal",
                create_bit_range("[25:22]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(4)
                    .dim_index(Some([
                        String::from("_ar"),
                        String::from("_aw"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field_enum(
                "non_posted_last_write_en",
                "Non-posted Last Write Enable - 0: posted writes can be used throughout the block transfer, 1: posted writes can be used up to the last write, the last write must be non-posted.",
                create_bit_range("[30:30]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disable", "Posted writes can be used throughout the block transfer", 0)?,
                    create_enum_value("enable", "Posted writes can be used up to the last write, the last write must be non-posted", 1)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "prot",
                "AXI prot signal",
                create_bit_range("[34:32]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(3)
                    .dim_index(Some([
                        String::from("_ar"),
                        String::from("_aw"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "arlen_en",
                "Source burst length enable - 0: disable, 1: enable",
                create_bit_range("[38:38]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "arlen",
                "Source burst length",
                create_bit_range("[46:39]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field(
                "awlen_en",
                "Destination burst length enable - 0: disable, 1: enable",
                create_bit_range("[47:47]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "awlen",
                "Destination burst length",
                create_bit_range("[55:48]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field(
                "stat_en",
                "Status enable",
                create_bit_range("[56:56]")?,
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_src"),
                        String::from("_dst"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "ioc_block_tr",
                "Interrupt-on-completion block transfer - 0: disable, 1: enable",
                create_bit_range("[58:58]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "shadow_or_lli",
                "Shadow or Linked List Item - 0: not last/valid, 1: last/valid",
                create_bit_range("[62:62]")?,
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_last"),
                        String::from("_valid"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    )?))
}
