use crate::Result;

pub mod channel;
pub mod dmac;

/// Creates Synopsys DesignWare AXI DMAC register definitions.
pub fn create(dma_channels: u64) -> Result<Vec<svd::RegisterCluster>> {
    Ok([dmac::create(dma_channels)?, channel::create(dma_channels)?].into())
}
