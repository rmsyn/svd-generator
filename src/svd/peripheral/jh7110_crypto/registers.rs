//! StarFive JH7110 Crypto peripheral register definitions.
//!
//! Definitions based on the Linux driver: <https://github.com/starfive-tech/linux/blob/06ad134b6efeb393868e300058752ecfbf7258d1/drivers/crypto/starfive/jh7110/jh7110-regs.h>
//!
//! Original author: William Qiu <william.qiu@starfivetech.com>

use crate::Result;

pub mod aes;
pub mod alg;
pub mod crypto;
pub mod dma;
pub mod ie;
pub mod sha;

/// Creates StarFive JH7110 Crypto peripheral register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        alg::create()?,
        ie::create()?,
        dma::create()?,
        aes::create()?,
        sha::create()?,
        crypto::create()?,
    ]
    .into())
}
