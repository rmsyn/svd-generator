use crate::svd::create_cluster;
use crate::Result;

pub mod aesdio0r;
pub mod alen;
pub mod csr;
pub mod iv;
pub mod ivlen;
pub mod key;
pub mod mlen;
pub mod nonce;

/// Creates StarFive JH7110 Crypto AES registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "aes",
        "JH7110 Crypto AES registers",
        0x100,
        &[
            aesdio0r::create()?,
            key::create()?,
            csr::create()?,
            iv::create()?,
            nonce::create()?,
            alen::create()?,
            mlen::create()?,
            ivlen::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
