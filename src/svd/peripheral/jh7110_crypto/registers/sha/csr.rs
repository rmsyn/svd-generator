use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 Crypto SHA CSR register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "csr",
        "JH7110 Crypto SHA CSR",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "start",
                "SHA CSR Start",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "reset",
                "SHA Reset",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ie",
                "SHA Interrupt Enable",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "firstb",
                "SHA First B",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "mode",
                "SHA Mode - 0: SM3, 1: SHA0, 2: SHA1, 3: SHA224, 4: SHA256, 5: SHA384, 6: SHA512",
                create_bit_range("[6:4]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("sm3", "SHA Mode: SM3", 0)?,
                    create_enum_value("sha0", "SHA Mode: SHA0", 1)?,
                    create_enum_value("sha1", "SHA Mode: SHA1", 2)?,
                    create_enum_value("sha224", "SHA Mode: SHA224", 3)?,
                    create_enum_value("sha256", "SHA Mode: SHA256", 4)?,
                    create_enum_value("sha384", "SHA Mode: SHA384", 5)?,
                    create_enum_value("sha512", "SHA Mode: SHA512", 6)?,
                ])?],
                None,
            )?,
            create_field(
                "sha_final",
                "SHA Final",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "hmac",
                "SHA HMAC",
                create_bit_range("[11:11]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "key_done",
                "SHA Key Done",
                create_bit_range("[13:13]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "key_flag",
                "SHA Key Flag",
                create_bit_range("[14:14]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "hmac_done",
                "SHA HMAC Done",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "busy",
                "SHA Busy",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "shadone",
                "SHA Done",
                create_bit_range("[17:17]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
