use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 Crypto SHA WDR register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "wlen",
        "JH7110 Crypto SHA WLEN",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "wlen",
            "SHA WLEN",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(4)
                .dim_increment(0x4)
                .dim_index(Some(["3", "2", "1", "0"].map(String::from).into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
