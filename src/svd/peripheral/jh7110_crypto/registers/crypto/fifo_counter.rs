use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 Crypto FIFO Counter register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "fifo_counter",
        "JH7110 Crypto FIFO Counter",
        0x314,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "fifo_counter",
            "Crypto FIFO Counter",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
