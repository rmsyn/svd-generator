use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 Crypto Interrupt Enable Mask register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "mask",
        "JH7110 Crypto Interrupt Enable Mask",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "aes_ie_mask",
                "AES Interrupt Enable Mask",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "des_ie_mask",
                "DES Interrupt Enable Mask",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "sha_ie_mask",
                "SHA Interrupt Enable Mask",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "crypto_ie_mask",
                "Crypto Interrupt Enable Mask",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
