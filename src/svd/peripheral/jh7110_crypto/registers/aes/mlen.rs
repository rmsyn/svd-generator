use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 Crypto AES MLEN register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "mlen",
        "JH7110 Crypto AES MLEN",
        0x54,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "mlen",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
