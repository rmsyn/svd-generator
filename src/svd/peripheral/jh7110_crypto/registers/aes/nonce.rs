use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 Crypto AES Nonce register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "nonce",
        "JH7110 Crypto AES Nonce",
        0x3c,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "nonce",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(4)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
