use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 Crypto AES IV register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "iv",
        "JH7110 Crypto AES IV",
        0x28,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "iv",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(4)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
