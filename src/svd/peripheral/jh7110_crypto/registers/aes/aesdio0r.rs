use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 Crypto AES AESDIO0R register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "aesdio0r",
        "JH7110 Crypto AES AESDIO0R",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "aesdio0r",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
