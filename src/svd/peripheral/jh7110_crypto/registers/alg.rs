use crate::svd::create_cluster;
use crate::Result;

pub mod cr;
pub mod fifo;

/// Creates StarFive JH7110 Crypto Algorithm registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "alg",
        "JH7110 Crypto Algorithm registers",
        0x0,
        &[cr::create()?, fifo::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
