use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 Crypto Algorithm Control register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "cr",
        "JH7110 Crypto Control",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "start",
                "",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "aes_dma_en",
                "",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "des_dma_en",
                "",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "sha_dma_en",
                "",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "alg_done",
                "",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "clear",
                "",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
