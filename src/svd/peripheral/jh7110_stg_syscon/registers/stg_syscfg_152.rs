use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 44 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_152",
        "STG SYSCONSAIF SYSCFG 608",
        0x260,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "u1_pcie_axi4_slv0_aruser_40_32",
                "PCIE AXI4 SLV0 ARUSER (little-endian)",
                create_bit_range("[8:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1ff)?,
                None,
            )?,
            create_field_constraint(
                "u1_pcie_axi4_slv0_awfunc",
                "PCIE AXI SLV0 AWFUNC",
                create_bit_range("[23:9]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7fff)?,
                None,
            )?,
            create_field_constraint(
                "u1_pcie_axi4_slv0_awregion",
                "PCIE AXI4 SLV0 AWREGION",
                create_bit_range("[27:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
        ]),
        None,
    )?))
}
