use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 12 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_12",
        "STG SYSCONSAIF SYSCFG 48",
        0x30,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "u0_hifi4_breakin",
                "Debug signal",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_hifi4_breakinack",
                "Debug signal",
                create_bit_range("[1:1]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_hifi4_breakout",
                "Debug signal",
                create_bit_range("[2:2]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_hifi4_breakoutack",
                "Debug signal",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_hifi4_debugmode",
                "Debug signal",
                create_bit_range("[4:4]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_hifi4_doubleexceptionerror",
                "Fault Handling Signals",
                create_bit_range("[5:5]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_hifi4_iram0loadstore",
                "Indicates that iram0 works",
                create_bit_range("[6:6]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_hifi4_iram1loadstore",
                "Indicates that iram1 works",
                create_bit_range("[7:7]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_hifi4_ocdhaltonreset",
                "Debug signal",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_hifi4_pfatalerror",
                "Fault Handling Signals",
                create_bit_range("[9:9]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
