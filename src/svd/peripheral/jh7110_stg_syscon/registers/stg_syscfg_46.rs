use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 46 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_46",
        "STG SYSCONSAIF SYSCFG 184",
        0xb8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "u0_pcie_axi4_slv0_awuser_40_32",
                "PCIE AXI4 SLV0 AWUSER (little-endian)",
                create_bit_range("[8:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1ff)?,
                None,
            )?,
            create_field(
                "u0_pcie_axi4_slv0_rderr",
                "PCIE AXI4 SLV0 RDERR",
                create_bit_range("[16:9]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
