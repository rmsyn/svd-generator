use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 48 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_48",
        "STG SYSCONSAIF SYSCFG 192",
        0xc0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "u0_pcie_axi4_slv0_wderr",
                "PCIE AXI4 SLV0 WDERR",
                create_bit_range("[7:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field_constraint(
                "u0_pcie_axi4_slvl_arfunc",
                "PCIE AXI4 SLV1 ARFUNC",
                create_bit_range("[22:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7fff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
