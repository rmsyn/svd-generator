use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 232 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_232",
        "STG SYSCONSAIF SYSCFG 928",
        0x3a0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "u1_pcie_tl_ctrl_hotplug",
                "PCIE TL Control Hotplug",
                create_bit_range("[15:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field_constraint(
                "u1_pcie_tl_report_hotplug",
                "PCIE TL Report Hotplug",
                create_bit_range("[31:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
