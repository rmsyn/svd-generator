use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 82 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_82",
        "STG SYSCONSAIF SYSCFG 328",
        0x148,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "u0_pcie_pf3_offset",
                "PCIE PF3 Offset",
                create_bit_range("[19:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf_ffff)?,
                None,
            )?,
            create_field_constraint(
                "u0_pcie_phy_mode",
                "PCIE PHY Mode",
                create_bit_range("[21:20]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "u0_pcie_pl_clkrem_allow",
                "PCIE PL Clock REM Allow",
                create_bit_range("[22:22]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_pcie_pl_clkreq_oen",
                "PCIE PL Clock Request OEN",
                create_bit_range("[23:23]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_pcie_pl_equ_phase",
                "PCIE PL EQU Phase",
                create_bit_range("[25:24]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_pcie_pl_ltssm",
                "PCIE PL LTSSM",
                create_bit_range("[30:26]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
