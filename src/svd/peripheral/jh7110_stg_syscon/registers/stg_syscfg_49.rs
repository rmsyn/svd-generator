use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 49 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_49",
        "STG SYSCONSAIF SYSCFG 196",
        0xc4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "u0_pcie_axi4_slvl_awfunc",
                "PCIE AXI4 SLV1 AWFUNC",
                create_bit_range("[14:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7fff)?,
                None,
            )?,
            create_field(
                "u0_pcie_bus_width_o",
                "PCIE Bus width",
                create_bit_range("[16:15]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_pcie_bypass_codec",
                "PCIE Bypass Codec",
                create_bit_range("[17:17]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "u0_pcie_ckref_src",
                "PCIE Clock Reference Source",
                create_bit_range("[19:18]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "u0_pcie_clk_sel",
                "PCIE Clock Select",
                create_bit_range("[21:20]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "u0_pcie_clkreq",
                "PCIE Clock Req",
                create_bit_range("[22:22]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
