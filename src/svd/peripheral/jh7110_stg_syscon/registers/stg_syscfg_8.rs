use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 8 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        32,
        "u0_e2_nmi_interrupt_vector",
        "",
        "[31:0]",
        svd::Access::ReadWrite,
        None,
    )
}
