use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 9 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        36,
        "u0_e2_reset_vector_0",
        "",
        "[31:0]",
        svd::Access::ReadWrite,
        None,
    )
}
