use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 151 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        604,
        "u1_pcie_axi4_slv0_aruser_31_0",
        "PCIE AXI4 SLV0 ARUSER (little-endian)",
        "[31:0]",
        svd::Access::ReadWrite,
        None,
    )
}
