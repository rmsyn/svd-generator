use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 4 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        16,
        "u0_usb_xhci_debug_bus",
        "",
        "[31:0]",
        svd::Access::ReadOnly,
        None,
    )
}
