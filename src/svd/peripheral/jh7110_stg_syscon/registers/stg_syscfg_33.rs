use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 33 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        132,
        "u0_pcie_axi4_mst0_wderr",
        "PCIE AXI4 WDERR",
        "[7:0]",
        svd::Access::ReadOnly,
        None,
    )
}
