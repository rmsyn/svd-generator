use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 123 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_123",
        "STG SYSCONSAIF SYSCFG 492",
        0x1ec,
        create_register_properties(32, 0xc80)?,
        Some(&[
            create_field_constraint(
                "u0_pcie_test_sel",
                "PCIE Test Selector",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "u0_pcie_tl_clock_freq",
                "PCIE TL Clock Frequency",
                create_bit_range("[25:4]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3f_ffff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
