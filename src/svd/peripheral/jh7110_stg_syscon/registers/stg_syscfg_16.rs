use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 16 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        64,
        "u0_hifi4_scfg_dsp_slv_offset",
        "The value indicates the slave port remap address",
        "[31:0]",
        svd::Access::ReadWrite,
        Some(0x40000000),
    )
}
