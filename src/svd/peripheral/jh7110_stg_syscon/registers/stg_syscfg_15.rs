use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 60 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_15",
        "STG SYSCONSAIF SYSCFG 60",
        0x3c,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "u0_hifi4_scfg_dsp_mst_offset_master",
                "Indicates that master port remap address",
                create_bit_range("[11:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xfff)?,
                None,
            )?,
            create_field_constraint(
                "u0_hifi4_scfg_dsp_mst_offset_dma",
                "Indicates the DMA port remap address",
                create_bit_range("[27:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xfff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
