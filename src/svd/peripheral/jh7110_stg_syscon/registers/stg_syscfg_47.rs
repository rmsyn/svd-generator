use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 47 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        188,
        "u0_pcie_axi4_slv0_ruser",
        "PCIE AXI4 SLV0 RUSER",
        "[31:0]",
        svd::Access::ReadOnly,
        None,
    )
}
