use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 13 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        52,
        "u0_hifi4_pfaultinfo",
        "Fault Handling Signals",
        "[31:0]",
        svd::Access::ReadOnly,
        None,
    )
}
