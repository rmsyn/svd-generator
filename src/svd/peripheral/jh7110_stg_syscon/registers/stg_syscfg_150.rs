use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 150 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_150",
        "STG SYSCONSAIF SYSCFG 600",
        0x258,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "u1_pcie_axi4_slv0_aratomop_257_256",
                "PCIE AXI4 ARATOMOP SLV0 (little-endian)",
                create_bit_range("[1:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "u1_pcie_axi4_slv0_arfunc",
                "PCIE AXI4 SLV0 ARFUNC",
                create_bit_range("[16:2]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7fff)?,
                None,
            )?,
            create_field_constraint(
                "u1_pcie_axi4_slv0_arregion",
                "PCIE AXI4 SLV0 ARREGION",
                create_bit_range("[20:17]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
        ]),
        None,
    )?))
}
