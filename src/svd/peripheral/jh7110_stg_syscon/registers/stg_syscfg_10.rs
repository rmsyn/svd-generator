use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 10 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        40,
        "u0_e2_wfi_from_tile_0",
        "",
        "[0:0]",
        svd::Access::ReadOnly,
        None,
    )
}
