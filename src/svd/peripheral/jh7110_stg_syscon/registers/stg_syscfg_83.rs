use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 83 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        332,
        "u0_pcie_pl_pclk_rate",
        "PCIE PL PCLK Rate",
        "[4:0]",
        svd::Access::ReadOnly,
        None,
    )
}
