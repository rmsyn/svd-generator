use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 184 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_184",
        "STG SYSCONSAIF SYSCFG 736",
        0x2e0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "u1_pcie_k_phyparam_839_832",
                "PCIE PHY Parameter (little-endian)",
                create_bit_range("[7:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field(
                "u1_pcie_k_rp_nep",
                "PCIE RP NEP",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u1_pcie_l1sub_entack",
                "PCIE L1SUB ENTACK",
                create_bit_range("[9:9]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u1_pcie_l1sub_entreq",
                "PCIE L1SUB ENREQ",
                create_bit_range("[10:10]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
