use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 134 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_134",
        "STG SYSCONSAIF SYSCFG 536",
        0x218,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "u1_pcie_axi4_mst0_aratomop_257_256",
                "PCIE AXI4 ARATOMOP (little-endian)",
                create_bit_range("[1:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u1_pcie_axi4_mst0_arfunc",
                "PCIE AXI4 ARFUNC",
                create_bit_range("[16:2]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u1_pcie_axi4_mst0_arregion",
                "PCIE AXI4 ARREGION",
                create_bit_range("[20:17]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
