use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 5 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        20,
        "u0_usb_xhci_debug_link_state",
        "",
        "[30:0]",
        svd::Access::ReadOnly,
        None,
    )
}
