use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 7 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        28,
        "u0_e2_nmi_exception_vector",
        "",
        "[31:0]",
        svd::Access::ReadWrite,
        None,
    )
}
