use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 56 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "stg_syscfg_14",
        "STG SYSCONSAIF SYSCFG 56",
        0x38,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "u0_hifi4_pfaultinfovalid",
                "Fault Handling Signals",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field_constraint(
                "u0_hifi4_prid",
                "Module ID",
                create_bit_range("[16:1]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
            create_field(
                "u0_hifi4_pwaitmode",
                "Wait Mode",
                create_bit_range("[17:17]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_hifi4_runstall",
                "Run Stall",
                create_bit_range("[18:18]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
