use super::create_register_stg_syscfg;
use crate::Result;

/// Creates a StarFive JH7110 STG Syscon SYSCFG 153 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register_stg_syscfg(
        612,
        "u1_pcie_axi4_slv0_awuser_31_0",
        "PCIE AXI4 SLV0 AWUSER (little-endian)",
        "[31:0]",
        svd::Access::ReadWrite,
        None,
    )
}
