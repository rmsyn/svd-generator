use crate::Result;

pub mod pcmdiv;
pub mod pcmgbcr;
pub mod pcmrxcr;
pub mod pcmtxcr;
pub mod tdm_fifo;

/// Creates StarFive JH7110 TDM (compatible) register definitions.
///
/// Based on register definitions from the Linux driver:
///
/// <https://github.com/starfive-tech/linux/blob/06ad134b6efeb393868e300058752ecfbf7258d1/sound/soc/starfive/starfive_tdm.h>
///
/// Original author: Walker Chen <walker.chen@starfivetech.com>
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        pcmgbcr::create()?,
        pcmtxcr::create()?,
        pcmrxcr::create()?,
        pcmdiv::create()?,
        tdm_fifo::create()?,
    ]
    .into())
}
