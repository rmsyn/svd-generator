use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 TDM PCM GB Control register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "pcmgbcr",
        "TDM PCM GB Control Register",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "enable",
                "PCM GB enable",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "ms",
                "PCM GB master-slave mode - 0: master, 1: slave",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("master", "PCM GB master mode", 0)?,
                    create_enum_value("slave", "PCM GB slave mode", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "syncm",
                "PCM GB sync mode - 0: short, 1: long.",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("short", "PCM GB short sync mode", 0)?,
                    create_enum_value("long", "PCM GB long sync mode", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "elm",
                "PCM GB early-late mode - 0: late, 1: early. Only works while syncm is 0.",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("late", "PCM GB late mode", 0)?,
                    create_enum_value("early", "PCM GB early mode", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "clkpol",
                "PCM GB clock polarity - 0: TX rising / RX falling, 1: TX falling / RX rising.",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value(
                        "tx_rising_rx_falling",
                        "PCM GB clock polarity: TX rising / RX falling",
                        0,
                    )?,
                    create_enum_value(
                        "tx_falling_rx_rising",
                        "PCM GB clock polarity: TX falling / RX rising",
                        1,
                    )?,
                ])?],
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
