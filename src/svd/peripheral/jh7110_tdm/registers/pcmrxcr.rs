use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 TDM PCM RX Control register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "pcmrxcr",
        "TDM PCM RX Control Register",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "rx_en",
            "TDM RX enable - 0: disable, 1: enable.",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("disable", "TDM RX disable", 0)?,
                create_enum_value("enable", "TDM RX enable", 1)?,
            ])?],
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
