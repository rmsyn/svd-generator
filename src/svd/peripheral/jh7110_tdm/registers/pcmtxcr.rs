use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 TDM PCM TX Control register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "pcmtxcr",
        "TDM PCM TX Control Register",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "tx_en",
                "TDM TX enable - 0: disable, 1: enable.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disable", "TDM TX disable", 0)?,
                    create_enum_value("enable", "TDM TX enable", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "lrj",
                "TDM left-right justify - 0: right-justify, 1: left-justify.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("right_justify", "TDM right-justify", 0)?,
                    create_enum_value("left_justify", "TDM left-justify", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "sl",
                "TDM slot length - 0: 8-bit, 1: 16-bit, 2: 32-bit.",
                create_bit_range("[3:2]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("bit8", "TDM slot length 8-bit", 0)?,
                    create_enum_value("bit16", "TDM slot length 16-bit", 1)?,
                    create_enum_value("bit32", "TDM slot length 32-bit", 2)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "sscale",
                "TDM slot scale.",
                create_bit_range("[7:4]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_enum(
                "wl",
                "TDM word length - 0: 8-bit, 1: 16-bit, 2: 20-bit, 3: 24-bit, 4: 32-bit.",
                create_bit_range("[10:8]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("bit8", "TDM word length 8-bit", 0)?,
                    create_enum_value("bit16", "TDM word length 16-bit", 1)?,
                    create_enum_value("bit20", "TDM word length 20-bit", 2)?,
                    create_enum_value("bit24", "TDM word length 24-bit", 3)?,
                    create_enum_value("bit32", "TDM word length 32-bit", 4)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "ifl",
                "TDM FIFO Length - 0: half, 1: quarter.",
                create_bit_range("[11:11]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("half", "TDM FIFO length half", 0)?,
                    create_enum_value("quarter", "TDM FIFO length quarter", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
