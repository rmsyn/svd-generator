use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 TDM PCM Divisor register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "pcmdiv",
        "TDM PCM Divisor register",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "pcmdiv",
            "TDM PCM divisor.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
