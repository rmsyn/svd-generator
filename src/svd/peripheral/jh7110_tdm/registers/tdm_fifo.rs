use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 TDM FIFO register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "tdm_fifo",
        "TDM FIFO registers",
        0x7030000,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "fifo",
            "TDM FIFO",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(32)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
