use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the OpenEdges Orbit PHY AC Base register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ac_base",
        "DDR Memory Control PHY AC Base register",
        0x4000,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "ac_base",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(0x800)
                .dim_increment(4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
