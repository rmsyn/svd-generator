use crate::Result;

pub mod ac_base;
pub mod base;
pub mod csr;

/// Creates register definitions for OpenEdges Orbit PHY (OPHY) peripherals.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([csr::create()?, base::create()?, ac_base::create()?].into())
}
