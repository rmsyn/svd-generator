use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 ISPCRG Clock ISPv2 Top Wrapper Clock C register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_mux_sel_enum(
        "clk_ispv2_top_wrapper",
        "Clock ISPv2 Top Wrapper Clock C: clk_ispv2_top_wrapper_clk_c",
        0x34,
        "clk_mipi_rx0_pxl, clk_dvp_inv",
        &[
            create_enum_value(
                "clk_mipi_rx0_pxl",
                "Select `clk_mipi_rx0_pxl` as the ISPv2 Top Wrapper clock.",
                0b0,
            )?,
            create_enum_value(
                "clk_dvp_inv",
                "Select `clk_dvp_inv` as the ISPv2 Top Wrapper clock.",
                0b1,
            )?,
        ],
        Some(1 << 31),
        None,
    )
}
