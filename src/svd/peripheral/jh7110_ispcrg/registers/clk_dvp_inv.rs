use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock DVP Inverter register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_clk_polarity("clk_dvp_inv", "Clock DVP Inverter", 0x8, Some(1 << 30))
}
