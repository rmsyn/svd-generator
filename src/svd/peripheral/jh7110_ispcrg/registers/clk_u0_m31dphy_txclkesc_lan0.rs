use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock U0 M31 DPHY TX Clock Escape LAN0 register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_u0_m31dphy_txclkesc_lan0",
        "Clock U0 M31 DPHY TX Clock Escape LAN0",
        0x14,
        [60, 30, 15, 30],
        None,
    )
}
