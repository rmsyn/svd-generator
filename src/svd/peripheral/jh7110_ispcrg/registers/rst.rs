use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 ISPCRG RESET registers.
pub fn create() -> Result<svd::RegisterCluster> {
    const FIELD_DESC: &str = "0: De-assert reset, 1: Assert reset";
    Ok(svd::RegisterCluster::Register(create_register(
        "reset",
        "ISPCRG Reset registers",
        0x38,
        create_register_properties(32, 0xfff)?,
        Some(&[
            create_field(
                "rst_u0_ispv2_top_wrapper_rst_p",
                FIELD_DESC,
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rst_u0_ispv2_top_wrapper_rst_c",
                FIELD_DESC,
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_m31dphy_hw_rstn",
                FIELD_DESC,
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_m31dphy_rstb09_aon",
                FIELD_DESC,
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_vin_rst_n_pclk",
                FIELD_DESC,
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_vin_rst_n_pixel_clk_if0",
                FIELD_DESC,
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_vin_rst_n_pixel_clk_if1",
                FIELD_DESC,
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_vin_rst_n_pixel_clk_if2",
                FIELD_DESC,
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_vin_rst_n_pixel_clk_if3",
                FIELD_DESC,
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_vin_rst_n_sys_clk",
                FIELD_DESC,
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_vin_rst_p_axird",
                FIELD_DESC,
                create_bit_range("[10:10]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_vin_rst_p_axiwr",
                FIELD_DESC,
                create_bit_range("[11:11]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some(
                    [
                        String::from("_software_assert0_addr_assert_sel"),
                        String::from("_ispcrg_status"),
                    ]
                    .into(),
                ))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
