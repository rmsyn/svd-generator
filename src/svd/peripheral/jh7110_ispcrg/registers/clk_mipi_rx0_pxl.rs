use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock MIPI RX 0 Pixel register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_mipi_rx0_pxl",
        "Clock MIPI RX 0 Pixel",
        0x4,
        [8, 3, 2, 3],
        None,
    )
}
