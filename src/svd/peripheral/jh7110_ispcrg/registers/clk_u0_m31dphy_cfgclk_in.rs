use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock U0 M31 DPHY Config In register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_u0_m31dphy_cfgclk_in",
        "Clock U0 M31 DPHY Config In",
        0xc,
        [16, 6, 4, 6],
        None,
    )
}
