use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Cadence USB3 Device TDL Behavior Configuration register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "beh",
        "TDL behavior configuration.",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "ep_num",
                "TDL behavior configuration endpoint number.",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_enum(
                "ep_dir",
                "TDL behavior configuration endpoint direction.",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("out", "Outbound endpoint", 0b0)?,
                    create_enum_value("in", "Inbound endpoint", 0b1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
