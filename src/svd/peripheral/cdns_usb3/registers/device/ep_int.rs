use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 Device Endpoint Interrupt register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ep_int",
        "USB3 Endpoint interrupt registers - ep_int0: enable, ep_int1: status.",
        0x3c,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "ep_out",
                "OUT endpoint.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                Some(
                    svd::DimElement::builder()
                        .dim(16)
                        .dim_increment(1)
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field(
                "ep_in",
                "IN endpoint.",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                Some(
                    svd::DimElement::builder()
                        .dim(16)
                        .dim_increment(1)
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
        ]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some([String::from("_en"), String::from("_sts")].into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
