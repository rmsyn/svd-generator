use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Cadence USB3 Device Endpoint Configuration register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ep_cfg",
        "USB3 Endpoint configuration.",
        0x24,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "enable",
                "Endpoint enable.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "eptype",
                "Endpoint type.",
                create_bit_range("[2:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("isochronous", "Isochronous endpoint", 0x1)?,
                    create_enum_value("bulk", "Bulk endpoint", 0x2)?,
                    create_enum_value("interrupt", "Interrupt endpoint", 0x3)?,
                ])?],
                None,
            )?,
            create_field(
                "stream_en",
                "Stream support enable - only in SS mode.",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "tdl_chk",
                "TDL check - only in SS mode for BULK EP.",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "sid_chk",
                "SID check - only in SS mode for BULK OUT EP.",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ependian",
                "DMA transfer endianness.",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "maxburst",
                "Max burst size - used only in SS mode.",
                create_bit_range("[11:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_enum(
                "mult",
                "ISO max burst size.",
                create_bit_range("[15:14]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("mult0", "ISO burst: 0", 0x0)?,
                    create_enum_value("mult1", "ISO burst: 1", 0x1)?,
                    create_enum_value("mult2", "ISO burst: 2", 0x2)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "maxpktsize",
                "ISO max packet size.",
                create_bit_range("[26:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7ff)?,
                None,
            )?,
            create_field_constraint(
                "buffering",
                "Max number of buffered packets.",
                // NOTE: Linux driver defines the field bit-range as `[31:27]`.
                // However, the max `buffering` value is set to `15`.
                // So, just limit the field size to computationally limit the max value.
                create_bit_range("[31:27]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
        ]),
        None,
    )?))
}
