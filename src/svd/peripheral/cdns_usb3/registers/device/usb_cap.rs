use crate::svd::create_cluster;
use crate::Result;

pub mod cap1;
pub mod cap2;
pub mod cap3;
pub mod cap4;
pub mod cap5;
pub mod cap6;

/// Creates Cadence USB3 Device Global Capability register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "usb_cap",
        "USB3 Global Capability registers.",
        0x4c,
        &[
            cap1::create()?,
            cap2::create()?,
            cap3::create()?,
            cap4::create()?,
            cap5::create()?,
            cap6::create()?,
        ],
        None,
    )?))
}
