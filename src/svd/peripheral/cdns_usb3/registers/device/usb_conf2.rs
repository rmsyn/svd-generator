use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 Device Global Configuration 2 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "usb_conf2",
        "USB3 Global configurartion 2.",
        0x48,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "tdl_trb",
                "TDL calculation based on TRB feature in controller for DMULT mode, only supported in DEV_VER_V2 version - tdl_trb0: disable, tdl_trb1: enable.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_dis"),
                        String::from("_en")
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    )?))
}
