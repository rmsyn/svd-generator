use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Cadence USB3 Device Global ITP/SOF Number register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "usb_itpn",
        "ITP (SS) / SOF (FS/HS) number - SS: last ITP number, FS/HS: last SOF number.",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "itpn",
            "SS: last ITP number, FS/HS: last SOF number.",
            create_bit_range("[13:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0x3fff)?,
            None,
        )?]),
        None,
    )?))
}
