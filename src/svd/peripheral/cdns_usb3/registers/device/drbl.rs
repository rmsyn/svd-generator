use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 Device Doorbell register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "drbl",
        "USB3 doorbell.",
        0x38,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "out_ep",
                "Doorbell OUT.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                Some(
                    svd::DimElement::builder()
                        .dim(16)
                        .dim_increment(1)
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field(
                "in_ep",
                "Doorbell IN.",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                Some(
                    svd::DimElement::builder()
                        .dim(16)
                        .dim_increment(1)
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
        ]),
        None,
    )?))
}
