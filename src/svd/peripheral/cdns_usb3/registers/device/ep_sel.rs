use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Cadence USB3 Device Endpoint Select register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ep_sel",
        "USB3 Endpoint select.",
        0x1c,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "epno",
                "Endpoint number.",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_enum(
                "dir",
                "Endpoint direction.",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("out", "OUT direction", 0b0)?,
                    create_enum_value("in", "IN direction", 0b1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
