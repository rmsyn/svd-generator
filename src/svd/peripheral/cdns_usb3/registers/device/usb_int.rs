use crate::svd::{create_bit_range, create_cluster, create_field};
use crate::Result;

pub mod en;
pub mod sts;

/// Creates Cadence USB3 Device Global Interrupt register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "usb_int",
        "USB Interrupt registers.",
        0x14,
        // NOTE: cannot combine into a repeated register definition.
        // The `int_en` has a different `Reset` value.
        &[en::create()?, sts::create()?],
        None,
    )?))
}

/// Creates Global USB Interrupt field definitions.
pub fn create_int_fields() -> Result<[svd::Field; 23]> {
    Ok([
        create_field(
            "con",
            "SS connection interrupt.",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "dis",
            "SS disconnection interrupt.",
            create_bit_range("[1:1]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "uwres",
            "SS warm reset interrupt.",
            create_bit_range("[2:2]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "uhres",
            "SS hot reset interrupt.",
            create_bit_range("[3:3]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "u3ent",
            "SS link U3 state enter interrupt - suspend.",
            create_bit_range("[4:4]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "u3ext",
            "SS link U3 state exit interrupt - wakeup.",
            create_bit_range("[5:5]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "u2ent",
            "SS link U2 state enter interrupt.",
            create_bit_range("[6:6]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "u2ext",
            "SS link U2 state exit interrupt.",
            create_bit_range("[7:7]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "u1ent",
            "SS link U1 state enter interrupt.",
            create_bit_range("[8:8]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "u1ext",
            "SS link U1 state exit interrupt.",
            create_bit_range("[9:9]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "itp",
            "ITP/SOF packet detected interrupt.",
            create_bit_range("[10:10]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "wake",
            "Wakeup interrupt.",
            create_bit_range("[11:11]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "spkt",
            "Send Custom Packet interrupt.",
            create_bit_range("[12:12]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "con2",
            "FS/HS mode connection interrupt.",
            create_bit_range("[16:16]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "dis2",
            "FS/HS mode disconnection interrupt.",
            create_bit_range("[17:17]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "u2res",
            "USB reset interrupt - FS/HS mode.",
            create_bit_range("[18:18]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "l2ent",
            "LPM L2 state enter interrupt.",
            create_bit_range("[20:20]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "l2ext",
            "LPM L2 state exit interrupt.",
            create_bit_range("[21:21]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "l1ent",
            "LPM L1 state enter interrupt.",
            create_bit_range("[24:24]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "l1ext",
            "LPM L1 state exit interrupt.",
            create_bit_range("[25:25]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "cfgres",
            "Configuration reset interrupt.",
            create_bit_range("[26:26]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "uwress",
            "Start of the USB SS warm reset interrupt.",
            create_bit_range("[28:28]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "uwrese",
            "End of the USB SS warm reset interrupt.",
            create_bit_range("[29:29]")?,
            svd::Access::ReadWrite,
            None,
        )?,
    ])
}
