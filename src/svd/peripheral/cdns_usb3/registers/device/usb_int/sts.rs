use super::create_int_fields;
use crate::svd::{create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 Device Global Interrupt Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "sts",
        "Global Interrupt Status.",
        0x4,
        create_register_properties(32, 0)?,
        Some(&create_int_fields()?),
        None,
    )?))
}
