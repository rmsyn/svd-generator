use crate::svd::create_cluster;
use crate::Result;

pub mod addr;
pub mod ctrl;
pub mod data;

/// Creates Cadence USB3 Device On-chip Buffer register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "buf",
        "USB3 On-chip buffer registers.",
        0x74,
        &[addr::create()?, data::create()?, ctrl::create()?],
        None,
    )?))
}
