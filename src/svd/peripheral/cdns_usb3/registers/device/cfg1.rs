use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 Device Configuration 1 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cfg1",
        "Device configuration 1.",
        0x100,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "cfg1",
            "Device configuration 1.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
