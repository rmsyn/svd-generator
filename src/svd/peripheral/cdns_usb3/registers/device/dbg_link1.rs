use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Cadence USB3 Device Debug Link 1 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "dbg_link1",
        "Device debug link 1.",
        0x104,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "lfps_min_det_u1_exit",
                "LFPS_MIN_DET_U1_EXIT value - configures the minimum time required for decoding the received LFPS as an LFPS.U1_Exit.",
                create_bit_range("[7:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field_constraint(
                "lfps_gen_u1_exit",
                "LFPS_MIN_GEN_U1_EXIT value - configures the minimum time for phytxelecidle deassertion when LFPS.U1_Exit.",
                create_bit_range("[15:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field_enum(
                "rxdet_break_dis",
                "RXDET_BREAK_DIS value configures terminating the Far-end Receiver termination detection sequence.",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("possible", "It is possible that USBSS_DEV will terminate far-end receiver termination detection sequence.", 0b0)?,
                    create_enum_value("not_possible", "USBSS_DEV will not terminate far-end receiver termination detection sequence.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "lfps_gen_ping",
                "LFPS_GEN_PING value - configures the LFPS.Ping generation.",
                create_bit_range("[21:17]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
            create_field_enum(
                "lfps_min_det_u1_exit_set",
                "Set the LFPS_MIN_DET_U1_EXIT value - This bit is automatically cleared, writing `0` has no effect.",
                create_bit_range("[24:24]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("set", "Writes the LPFS_MIN_DET_U1_EXIT field value to the device.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "lfps_min_gen_u1_exit_set",
                "Set the LFPS_MIN_GEN_U1_EXIT value - This bit is automatically cleared, writing `0` has no effect.",
                create_bit_range("[25:25]")?,
                svd::Access::WriteOnly,
                &[create_enum_values(&[
                    create_enum_value("set", "Writes the LPFS_MIN_GEN_U1_EXIT field value to the device.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "rxdet_break_dis_set",
                "Set the RXDET_BREAK_DIS value - This bit is automatically cleared, writing `0` has no effect.",
                create_bit_range("[26:26]")?,
                svd::Access::WriteOnly,
                &[create_enum_values(&[
                    create_enum_value("set", "Writes the RXDET_BREAK_DIS field value to the device.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "lfps_gen_ping_set",
                "Set the LFPS_GEN_PING value - This bit is automatically cleared, writing `0` has no effect.",
                create_bit_range("[27:27]")?,
                svd::Access::WriteOnly,
                &[create_enum_values(&[
                    create_enum_value("set", "Writes the LPFS_GEN_PING field value to the device.", 0b1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
