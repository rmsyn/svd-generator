use crate::svd::create_cluster;
use crate::Result;

pub mod cap;
pub mod ctrl;
pub mod ctrl0;
pub mod ctrl1;
pub mod id;

/// Creates Cadence USB3 Device DMA AXI register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "dma_axi",
        "Device DMA registers.",
        0x300,
        &[
            ctrl::create()?,
            id::create()?,
            cap::create()?,
            ctrl0::create()?,
            ctrl1::create()?,
        ],
        None,
    )?))
}
