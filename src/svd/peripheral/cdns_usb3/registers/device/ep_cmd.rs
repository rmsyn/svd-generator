use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Cadence USB3 Device Endpoint Command register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ep_cmd",
        "USB3 Endpoint command.",
        0x28,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "eprst",
                "Endpoint reset.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "stall",
                "Endpoint STALL set/clear.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_set"),
                        String::from("_clear"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "erdy",
                "Send ERDY TP.",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "req_cmpl",
                "Request complete.",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "drdy",
                "Transfer descriptor ready.",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "dflush",
                "Data flush.",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "stdl",
                "Transfer Descriptor Length write - only for SS mode BULK mode endpoints, removed in `DEV_VER_V3`.",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "tdl",
                "Transfer Descriptor Length - only for SS mode BULK mode endpoints, removed in `DEV_VER_V3`.",
                create_bit_range("[15:9]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7f)?,
                None,
            )?,
            create_field_constraint(
                "erdy_sid",
                "ERDY Stream ID value - used in SS mode.",
                create_bit_range("[31:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
