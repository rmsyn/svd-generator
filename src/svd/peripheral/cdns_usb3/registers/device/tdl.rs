use crate::svd::create_cluster;
use crate::Result;

pub mod beh;
pub mod beh2;
pub mod cfg_from_trb;
pub mod dma_adv;
pub mod ep;

pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "tdl",
        "USB3 Device TDL registers.",
        0x84,
        &[
            cfg_from_trb::create()?,
            beh::create()?,
            ep::create()?,
            beh2::create()?,
            dma_adv::create()?,
        ],
        None,
    )?))
}
