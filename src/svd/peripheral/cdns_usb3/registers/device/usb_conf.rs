use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates Cadence USB3 Device Global Configuration register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "usb_conf",
        "USB3 Global configuration.",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "cfg",
                "Reset/Set USB device configuration.",
                create_bit_range("[1:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "Do not set USB device configuration.", 0b00)?,
                    create_enum_value("rst", "Reset USB device configuration.", 0b01)?,
                    create_enum_value("set", "Set configuration.", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "dis",
                "Disconnect USB device.",
                create_bit_range("[4:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "Do not disconnect USB device.", 0b00)?,
                    create_enum_value("usb3", "Disconnect USB3 device in SuperSpeed mode.", 0b01)?,
                    create_enum_value("usb2", "Disconnect USB2 device in FS/HS mode.", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "endian",
                "Endian access.",
                create_bit_range("[6:5]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "No endian access setting", 0b00)?,
                    create_enum_value("little", "Little endian access - default", 0b01)?,
                    create_enum_value("big", "Big endian access", 0b10)?,
                ])?],
                None,
            )?,
            create_field(
                "swrst",
                "Device software reset.",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "dmaoff",
                "DMA clock turn-off.",
                create_bit_range("[11:10]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "No DMA clock turn-off setting.", 0b00)?,
                    create_enum_value("en", "DMA clock turn-off enable.", 0b01)?,
                    create_enum_value("ds", "DMA clock turn-off disable.", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "force_fs",
                "Force Full Speed.",
                create_bit_range("[13:12]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "No Force Full Speed setting.", 0b00)?,
                    create_enum_value("clear", "Clear Force Full Speed.", 0b01)?,
                    create_enum_value("set", "Set Force Full Speed.", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "dev",
                "Device enable/disable.",
                create_bit_range("[15:14]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "No Device enable/disable setting.", 0b00)?,
                    create_enum_value("en", "Device enable.", 0b01)?,
                    create_enum_value("ds", "Device disable.", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "l1",
                "L1 LPM state entry enable/disable (used in HS/FS mode).",
                create_bit_range("[17:16]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "No L1 LPM state entry setting.", 0b00)?,
                    create_enum_value("en", "L1 LPM state entry enable.", 0b01)?,
                    create_enum_value("ds", "L1 LPM state entry disable.", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "clk2off",
                "USB 2.0 clock gate turn-off enable/disable.",
                create_bit_range("[19:18]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "No USB 2.0 clock gate turn-off setting.", 0b00)?,
                    create_enum_value("en", "USB 2.0 clock gate turn-off enable.", 0b01)?,
                    create_enum_value("ds", "USB 2.0 clock gate turn-off disable.", 0b10)?,
                ])?],
                None,
            )?,
            create_field(
                "lgo_l0",
                "L0 LPM state entry request (used in HS/FS mode).",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "clk3off",
                "USB 3.0 clock gate turn-off enable/disable.",
                create_bit_range("[22:21]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "USB 3.0 clock gate turn-off setting.", 0b00)?,
                    create_enum_value("en", "USB 3.0 clock gate turn-off enable.", 0b01)?,
                    create_enum_value("ds", "USB 3.0 clock gate turn-off disable.", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u1",
                "U1 state entry enable/disable (used in SS mode).",
                create_bit_range("[25:24]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "No U1 state entry setting.", 0b00)?,
                    create_enum_value("en", "U1 state entry enable.", 0b01)?,
                    create_enum_value("ds", "U1 state entry disable.", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u2",
                "U2 state entry enable/disable (used in SS mode).",
                create_bit_range("[27:26]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "No U2 state entry setting.", 0b00)?,
                    create_enum_value("en", "U2 state entry enable", 0b01)?,
                    create_enum_value("ds", "U2 state entry disable", 0b10)?,
                ])?],
                None,
            )?,
            create_field(
                "lgo_u",
                "U0-U2 state entry request - used in SS mode.",
                create_bit_range("[28:28]")?,
                svd::Access::ReadWrite,
                Some(
                    svd::DimElement::builder()
                        .dim(3)
                        .dim_increment(1)
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field(
                "lgo_ssinact",
                "SS.Inactive state entry request - used in SS mode.",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
