use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Cadence USB3 Device Global Custom Packet register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "usb_cpkt",
        "USB3 Global custom packet.",
        0x64,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "cpkt",
            "Custom packet.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(3)
                .dim_increment(0x4)
                .dim_index(Some(
                    [String::from("1"), String::from("2"), String::from("3")].into(),
                ))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
