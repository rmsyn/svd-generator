use crate::svd::create_cluster;
use crate::Result;

pub mod en;
pub mod sid;
pub mod status;

/// Creates Cadence USB3 Device Endpoint Status register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "ep_sts",
        "USB3 Endpoint status registers.",
        0x2c,
        &[status::create()?, sid::create()?, en::create()?],
        None,
    )?))
}
