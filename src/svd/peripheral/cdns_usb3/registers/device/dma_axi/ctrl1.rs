use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 Device DMA AXI Control 1 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ctrl1",
        "Device DMA AXI control 1.",
        0x10,
        create_register_properties(32, 0)?,
        // FIXME: no available public documentation for valid values...
        Some(&[create_field(
            "ctrl1",
            "DMA AXI control 1.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
