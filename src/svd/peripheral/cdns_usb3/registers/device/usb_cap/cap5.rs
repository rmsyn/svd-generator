use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 Device Global Capability 5 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cap5",
        "USB3 Global capability 5.",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "ep_support_stream",
            "Endpoint supports streaming mode.",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            Some(
                svd::DimElement::builder()
                    .dim(32)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        None,
    )?))
}
