use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates Cadence USB3 Device Global Capability 6 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cap6",
        "USB3 Global capability 6.",
        0x14,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "dev_base_version",
                "USBSS-DEV Controller Internal build number.",
                create_bit_range("[23:0]")?,
                svd::Access::ReadOnly,
                &[create_enum_values(&[
                    create_enum_value("nxp_v1", "NXP version 1", 0x024502)?,
                    create_enum_value("ti_v1", "TI version 1", 0x024509)?,
                    create_enum_value("v2", "Version 2", 0x02450c)?,
                    create_enum_value("v3", "Version 3", 0x02450d)?,
                ])?],
                None,
            )?,
            create_field(
                "dev_custom_version",
                "USBSS-DEV Controller version number.",
                create_bit_range("[31:24]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
