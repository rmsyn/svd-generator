use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 Device Global Capability 3 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cap3",
        "USB3 Global capability 3.",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "ep_is_implemented",
            "Endpoint is implemented.",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            Some(
                svd::DimElement::builder()
                    .dim(32)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        None,
    )?))
}
