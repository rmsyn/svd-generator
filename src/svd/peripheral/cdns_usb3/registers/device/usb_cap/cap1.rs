use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates Cadence USB3 Device Global Capability 1 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cap1",
        "USB3 Global capability 1.",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "sfr_type",
                "SFR interface type.",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("ocp", "SFR OCP interface.", 0x0)?,
                    create_enum_value("ahb", "SFR AHB interface.", 0x1)?,
                    create_enum_value("plb", "SFR PLB interface.", 0x2)?,
                    create_enum_value("axi", "SFR AXI interface.", 0x3)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "sfr_width",
                "SFR interface width.",
                create_bit_range("[7:4]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("bit8", "SFR 8-bit interface.", 0x0)?,
                    create_enum_value("bit16", "SFR 16-bit interface.", 0x1)?,
                    create_enum_value("bit32", "SFR 32-bit interface.", 0x2)?,
                    create_enum_value("bit64", "SFR 64-bit interface.", 0x3)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "dma_type",
                "DMA interface type.",
                create_bit_range("[11:8]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("ocp", "DMA OCP interface.", 0x0)?,
                    create_enum_value("ahb", "DMA AHB interface.", 0x1)?,
                    create_enum_value("plb", "DMA PLB interface.", 0x2)?,
                    create_enum_value("axi", "DMA AXI interface.", 0x3)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "dma_width",
                "DMA interface width.",
                create_bit_range("[15:12]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("bit32", "DMA 32-bit interface.", 0x2)?,
                    create_enum_value("bit64", "DMA 64-bit interface.", 0x3)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u3phy_type",
                "USB3 PHY interface type.",
                create_bit_range("[19:16]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("usb_pipe", "USB PIPE interface.", 0x0)?,
                    create_enum_value("rmmi", "RMMI interface.", 0x1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u3phy_width",
                "USB3 PHY interface width.",
                create_bit_range("[23:20]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("bit8", "USB3 PHY 8-bit interface.", 0x0)?,
                    create_enum_value("bit16", "USB3 PHY 16-bit interface.", 0x1)?,
                    create_enum_value("bit32", "USB3 PHY 32-bit interface.", 0x2)?,
                    create_enum_value("bit64", "USB3 PHY 64-bit interface.", 0x3)?,
                ])?],
                None,
            )?,
            create_field(
                "u2phy_en",
                "USB2 PHY interface enable.",
                create_bit_range("[24:24]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "u2phy_type",
                "USB2 PHY interface type.",
                create_bit_range("[25:25]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("utmi", "USB2 PHY UTMI interface.", 0x0)?,
                    create_enum_value("ulpi", "USB2 PHY ULPI interface.", 0x1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u2phy_width",
                "USB2 PHY interface width - **NOTE**: The ULPI interface is always 8-bit wide.",
                create_bit_range("[26:26]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("bit8", "USB2 PHY 8-bit interface width.", 0x0)?,
                    create_enum_value("bit16", "USB2 PHY 16-bit interface width.", 0x1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "otg_ready",
                "OTG mode ready.",
                create_bit_range("[27:27]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("dev_only", "Pure device mode.", 0x0)?,
                    create_enum_value("otg", "Some features and ports for CDNS USB OTG controller are implemented.", 0x1)?,
                ])?],
                None,
            )?,
            create_field(
                "tdl_from_trb",
                "Indicates the capability to automatically calculate internal TDL from TRB value for DMULT mode.",
                create_bit_range("[28:28]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
