use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 Device Global Capability 4 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cap4",
        "USB3 Global capability 4.",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "ep_support_iso",
            "Endpoint supports ISO mode.",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            Some(
                svd::DimElement::builder()
                    .dim(32)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        None,
    )?))
}
