use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 Device Configuration register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cfg",
        "Device configuration.",
        0x10c,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "cfg",
            "Device configuration.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(74)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
