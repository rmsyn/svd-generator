use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Cadence USB3 Device Endpoint Transfer Address register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ep_traddr",
        "USB3 Endpoint transfer address.",
        0x20,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "ep_traddr",
            "Endpoint transfer address",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
