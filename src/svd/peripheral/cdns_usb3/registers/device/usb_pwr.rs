use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 Device Global Power register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "usb_pwr",
        "USB3 Global power.",
        0x44,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "pso",
                "Power Shutoff capability enable/disable - pso0: enable, pso1: disable",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_en"),
                        String::from("_ds"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "stb_clk_switch",
                "Reference clock switch, only enabled if OTG_READY set to `1` - stb_clk_switch0: enable, stb_clk_switch1: done",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_en"),
                        String::from("_done"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
            create_field(
                "fast_reg_access",
                "Fast Register Access - fast_reg_access0: status, fast_reg_access1: enable",
                create_bit_range("[30:30]")?,
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(1)
                    .dim_index(Some([
                        String::from("_stat"),
                        String::from("_en"),
                    ].into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None
    )?))
}
