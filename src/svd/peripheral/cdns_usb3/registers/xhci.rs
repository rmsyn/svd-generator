use crate::svd::create_cluster;
use crate::Result;

// cap: the only register cluster with a fixed offset (zero offset from xhci base)
// op: dynamically defined by the cap.hc_capbase register value
// run: dynamically defined by the cap.run_regs_off register value
//
// because `op` and `run` are dynamically defined per-platform,
// statically defining them will require:
//
// - getting the value from the board
// - defining the values in the DTS somehow
// - require users of new boards/SoCs to repeat the process
// - is there a way to define "floating" registers like this in SVD?
//   - probably not
// - may be better to define the skeleton structures, and fill in the address in HAL/BSP crates
//
// defined by `struct xhci_cap_regs` in Linux
pub mod cap;
// defined by `struct xhci_op_regs` in Linux
//pub mod op;
// defined by `struct xhci_run_regs` in Linux
//pub mod run;

/// Creates Cadence USB3 XHCI register definitions.
///
/// Based on the register definitions from the Linux driver:
///
/// - <https://elixir.bootlin.com/linux/latest/source/drivers/usb/host/xhci.h>
///
/// Original Author(s):
///
/// - Sarah Sharp <sarah.a.sharp@linux.intel.com>
/// - Matt Evans <matt@ozlabs.org>
/// - Andiry Xu <andiry.xu@amd.com>
/// - Lu Baolu <baolu.lu@linux.intel.com>
/// - Mathias Nyman <mathias.nyman@linux.intel.com>
pub fn create(offset: u32) -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "xhci",
        "USB3 XHCI registers",
        offset,
        &[
            cap::create()?,
            //op::create()?,
            //run::create()?,
        ],
        None,
    )?))
}
