use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates Cadence USB3 OTG State register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "state",
        "USB3 OTG state.",
        0x18,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "dev_state",
                "USB3 OTG Device state.",
                create_bit_range("[2:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[create_enum_value(
                    "idle",
                    "Bus is idle.",
                    0x0,
                )?])?],
                None,
            )?,
            create_field_enum(
                "host_state",
                "USB3 OTG Device state.",
                create_bit_range("[5:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("idle", "Bus is idle.", 0x0)?,
                    create_enum_value("vbus_fall", "VBUS fall.", 0x7)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
