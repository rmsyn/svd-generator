use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 OTG Capabilities register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "capabilities",
        "USB3 OTG capabilities.",
        0x8,
        create_register_properties(32, 0)?,
        // FIXME: no available public documentation for valid values...
        Some(&[create_field(
            "capabilitites",
            "USB3 OTG capabilitites.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
