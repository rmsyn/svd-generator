use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates Cadence USB3 OTG Command register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "sts",
        "USB3 OTG status.",
        0x14,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "id_value",
                "USB3 OTG current value of the ID pin - only valid when idpullup in OTGCTRL1_TYPE set to `1`.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "vbus_valid",
                "USB3 OTG current value of the vbus_valid pin.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "session_valid",
                "USB3 OTG current value of the b_sess_vld pin.",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "active",
                "USB3 OTG active mode.",
                create_bit_range("[4:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "No OTG mode is active.", 0b00)?,
                    create_enum_value("dev", "OTG Device mode is active.", 0b01)?,
                    create_enum_value("host", "OTG Host mode is active.", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "otg_nrdy",
                "USB3 OTG Controller (not) readiness status.",
                create_bit_range("[11:11]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("ready", "OTG Controller is ready.", 0b0)?,
                    create_enum_value("not_ready", "OTG Controller is not ready.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "strap",
                "USB3 OTG value of the strap pins.",
                create_bit_range("[14:12]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("no_default_cfg", "No default configuration.", 0x0)?,
                    create_enum_value("host_otg", "Initially configured as Host in OTG mode.", 0x1)?,
                    create_enum_value("host", "Initially configured as Host.", 0x2)?,
                    create_enum_value("gadget", "Initially configured as Device.", 0x4)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "ready",
                "USB3 OTG readiness status.",
                create_bit_range("[27:26]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "OTG is not ready.", 0b00)?,
                    // NOTE: in the CDNSP definitions, these fields are swapped
                    create_enum_value("xhci", "OTG Host is ready - Host mode turned on.", 0b01)?,
                    create_enum_value("dev", "OTG Device is ready - Device mode turned on.", 0b10)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
