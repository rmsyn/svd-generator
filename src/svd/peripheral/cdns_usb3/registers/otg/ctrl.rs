use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 OTG Reference Clock register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ctrl",
        "USB3 OTG control registers.",
        0x58,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "ctrl",
            "USB3 OTG control.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some([String::from("1"), String::from("2")].into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
