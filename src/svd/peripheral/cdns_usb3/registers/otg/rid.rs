use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Cadence USB3 OTG RID register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rid",
        "USB3 OTG RID.",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "rid",
            "USB3 OTG RID.",
            create_bit_range("[15:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff)?,
            None,
        )?]),
        None,
    )?))
}
