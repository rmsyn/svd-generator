use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates Cadence USB3 OTG Timer register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "over",
        "USB3 OTG override.",
        0x44,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "idpullup",
                "USB3 OTG override ID pullup pin.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "session_valid_select",
                "USB3 OTG override session valid select.",
                create_bit_range("[10:10]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("vbus", "VBUS session valid select", 0b0)?,
                    create_enum_value("ses", "SES session valid select", 0b1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
