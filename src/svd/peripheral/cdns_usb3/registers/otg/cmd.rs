use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates Cadence USB3 OTG Command register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cmd",
        "USB3 OTG command.",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "bus_req",
                "OTG bus request.",
                create_bit_range("[1:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "No request for bus mode.", 0b00)?,
                    create_enum_value("dev", "Request the bus for Device mode.", 0b01)?,
                    create_enum_value("host", "Request the bus for Host mode.", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "otg",
                "OTG control.",
                create_bit_range("[3:2]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "OTG none.", 0b00)?,
                    create_enum_value("en", "OTG enable.", 0b01)?,
                    create_enum_value("dis", "OTG disable.", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "a_dev",
                "Configure OTG as A-device.",
                create_bit_range("[5:4]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "No OTG A-device configuration.", 0b00)?,
                    create_enum_value("en", "Enable OTG configuration as A-device.", 0b01)?,
                    create_enum_value("dis", "Disable OTG configuration as A-device.", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "bus_drop",
                "OTG drop the bus.",
                create_bit_range("[9:8]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "No OTG bus drop.", 0b00)?,
                    create_enum_value("dev", "Drop the OTG bus for Device mode.", 0b01)?,
                    create_enum_value("host", "Drop the OTG bus for Host mode.", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "power_off",
                "OTG power down.",
                create_bit_range("[12:11]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("none", "No power down.", 0b00)?,
                    create_enum_value("dev", "Power down USBSS-DEV.", 0b01)?,
                    create_enum_value("host", "Power down CDNSXHCI.", 0b10)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
