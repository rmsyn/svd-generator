use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 OTG PHY Reset Configuration register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "phyrst_cfg",
        "USB3 OTG PHY reset configuration.",
        0x4c,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "a_enable",
            "USB3 OTG PHY A-device enable.",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
