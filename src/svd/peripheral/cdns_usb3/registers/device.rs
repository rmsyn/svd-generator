use crate::svd::create_cluster;
use crate::Result;

pub mod buf;
pub mod cfg;
pub mod cfg1;
pub mod dbg_link1;
pub mod dbg_link2;
pub mod dma_axi;
pub mod drbl;
pub mod dtrans;
pub mod ep_cfg;
pub mod ep_cmd;
pub mod ep_dma_ext_addr;
pub mod ep_int;
pub mod ep_sel;
pub mod ep_sts;
pub mod ep_traddr;
pub mod tdl;
pub mod usb_cap;
pub mod usb_cmd;
pub mod usb_conf;
pub mod usb_conf2;
pub mod usb_cpkt;
pub mod usb_int;
pub mod usb_itpn;
pub mod usb_lpm;
pub mod usb_pwr;
pub mod usb_sts;

/// Creates Cadence USB3 Device register definitions.
///
/// Based on the register definitions from the Linux driver:
///
/// - <https://elixir.bootlin.com/linux/latest/source/drivers/usb/cdns3/cdns3-gadget.h>
///
/// Original Author(s):
///
/// - Pawel Laszczak <pawell@cadence.com>
/// - Pawel Jez <pjez@cadence.com>
/// - Peter Chen <peter.chen@nxp.com>
pub fn create(offset: u32) -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "device",
        "USB3 device registers",
        offset,
        &[
            usb_conf::create()?,
            usb_sts::create()?,
            usb_cmd::create()?,
            usb_itpn::create()?,
            usb_lpm::create()?,
            usb_int::create()?,
            ep_sel::create()?,
            ep_traddr::create()?,
            ep_cfg::create()?,
            ep_cmd::create()?,
            ep_sts::create()?,
            drbl::create()?,
            ep_int::create()?,
            usb_pwr::create()?,
            usb_conf2::create()?,
            usb_cap::create()?,
            usb_cpkt::create()?,
            ep_dma_ext_addr::create()?,
            buf::create()?,
            dtrans::create()?,
            tdl::create()?,
            cfg1::create()?,
            dbg_link1::create()?,
            dbg_link2::create()?,
            cfg::create()?,
            dma_axi::create()?,
        ],
        None,
    )?))
}
