use crate::svd::create_cluster;
use crate::Result;

pub mod db_off;
pub mod hc_capbase;
pub mod hcc_params;
pub mod hcc_params2;
pub mod hcs_params1;
pub mod hcs_params2;
pub mod hcs_params3;
pub mod run_regs_off;

/// Creates Cadence USB3 XHCI Capability register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "cap",
        "USB3 XHCI Capability registers.",
        0x0,
        &[
            hc_capbase::create()?,
            hcs_params1::create()?,
            hcs_params2::create()?,
            hcs_params3::create()?,
            hcc_params::create()?,
            db_off::create()?,
            run_regs_off::create()?,
            hcc_params2::create()?,
        ],
        None,
    )?))
}
