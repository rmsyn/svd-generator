use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 XHCI Host Controller Parameters 3 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "hcs_params3",
        "USB3 XHCI host controller structural parameters 3.",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "hcs_params3",
            "USB3 XHCI host controller structural parameters 3.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
