use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 XHCI Host Controller Parameters 2 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "hcs_params2",
        "USB3 XHCI host controller structural parameters 2.",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "hcs_params2",
            "USB3 XHCI host controller structural parameters 2.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
