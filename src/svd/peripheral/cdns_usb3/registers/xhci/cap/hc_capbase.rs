use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 XHCI Host Controller Capability Base register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "hc_capbase",
        "USB3 XHCI host controller capability base - defines the offset of the `op` register cluster.",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "hc_length",
                "USB3 XHCI length of the `hc_capbase` register.",
                create_bit_range("[7:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "hc_version",
                "USB3 XHCI length of the `hc_capbase` register.",
                create_bit_range("[31:8]")?,
                svd::Access::ReadOnly,
                None,
            )?
        ]),
        None,
    )?))
}
