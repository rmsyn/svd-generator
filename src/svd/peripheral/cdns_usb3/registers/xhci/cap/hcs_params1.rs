use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 XHCI Host Controller Parameters 1 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "hcs_params1",
        "USB3 XHCI host controller structural parameters 1.",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "max_intrs",
            "USB3 XHCI host controller max interrupts.",
            create_bit_range("[18:8]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
