use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 XHCI Host Controller Run Register Cluster Offset register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "run_regs_off",
        "USB3 XHCI host controller run register cluster offset - runtime register space offset.",
        0x18,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "run_regs_off",
            "USB3 XHCI host controller `run` register cluster offset.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
