use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Cadence USB3 XHCI Host Controller Capability Parameters 2 (XHCI v1.1) register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "hcc_params2",
        "USB3 XHCI host controller capabilities parameters - XHCI v1.1.",
        0x1c,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "hcc_params2",
            "USB3 XHCI host controller capability parameters.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
