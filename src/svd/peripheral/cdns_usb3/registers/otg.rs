use crate::svd::create_cluster;
use crate::Result;

pub mod adp_ramp_time;
pub mod anasts;
pub mod capabilities;
pub mod cmd;
pub mod ctrl;
pub mod did;
pub mod int;
pub mod over;
pub mod phyrst_cfg;
pub mod refclk;
pub mod rid;
pub mod simulate;
pub mod state;
pub mod sts;
pub mod susp_ctrl;
pub mod tmr;

/// Creates Cadence USB3 Device register definitions.
///
/// Based on the register definitions from the Linux driver:
///
/// - <https://elixir.bootlin.com/linux/latest/source/drivers/usb/cdns3/drd.h>
///
/// Original Author(s):
///
/// - Pawel Laszczak <pawell@cadence.com>
///
/// **NOTE**: only supports CDNS3 v1 registers from the Linux driver. CDNSP and v0 are omitted.
///
/// Please open an issue and/or merge request if other registers are needed.
pub fn create(offset: u32) -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "otg",
        "USB3 OTG registers",
        offset,
        &[
            did::create()?,
            rid::create()?,
            capabilities::create()?,
            cmd::create()?,
            sts::create()?,
            state::create()?,
            int::create()?,
            refclk::create()?,
            tmr::create()?,
            simulate::create()?,
            over::create()?,
            susp_ctrl::create()?,
            phyrst_cfg::create()?,
            anasts::create()?,
            adp_ramp_time::create()?,
            ctrl::create()?,
        ],
        None,
    )?))
}
