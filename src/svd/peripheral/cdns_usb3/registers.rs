use super::CdnsUsb3Offsets;
use crate::Result;

pub mod device;
pub mod otg;
pub mod xhci;

/// Creates Cadence USB3 register definitions.
pub fn create(offsets: CdnsUsb3Offsets) -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        otg::create(offsets.otg)?,
        xhci::create(offsets.xhci)?,
        device::create(offsets.dev)?,
    ]
    .into())
}
