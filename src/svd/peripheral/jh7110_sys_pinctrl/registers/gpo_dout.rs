use crate::svd::register::{
    create_bit_range, create_cluster, create_default_register, create_field_constraint,
    create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS Pinctrl GPO DOEN register.
pub fn create() -> Result<svd::RegisterCluster> {
    const RESET_LEN: usize = 16;
    #[rustfmt::skip]
    const RESET_VALUES: [u64; RESET_LEN] = [
        //       16           17           18           19
        0x1600_0000, 0x0000_1400, 0x1500_0000, 0x0000_0000,
        //       20           21           22           23
        0x2000_0000, 0x0055_0000, 0x0000_0000, 0x0000_0000,
        //       24           25           26           27
        0x0d00_0000, 0x5453_0f0e, 0x004e_4f00, 0x005b_5c00,
        //       28           29           30           31
        0x2000_1e1f, 0x4b00_494a, 0x5800_5657, 0x5f00_5d5e,
    ];
    const FIELD_DESC: &str = "The register value indicates the selected GPIO (Digital Output) DOUT index from GPIO DOUT list 0-49. See Table 2-41: GPIO DOUT List for SYS_IOMUX (on page 97) for more information.";

    let default_reg = create_default_register()?;

    let mut regs = array![default_reg.clone(); RESET_LEN];

    for (idx, (reg, rst)) in regs.iter_mut().zip(RESET_VALUES).enumerate() {
        let gpo_num = idx as u32;

        // addr serves as the address offset, and the GPIO number
        //
        // This is a coincidence, since there are 4 GPIOs per register.
        let gpo_base = gpo_num * 4;

        let (a, b, c, d) = (gpo_base, gpo_base + 1, gpo_base + 2, gpo_base + 3);

        *reg = create_register(
            format!("gpo_dout{idx}").as_str(),
            format!("SYS IOMUX CFG SAIF SYSCFG FMUX GPIO {a}-{d} DOUT").as_str(),
            gpo_base,
            create_register_properties(32, rst)?,
            Some(&[
                create_field_constraint(
                    format!("dout{a}").as_str(),
                    format!("The selected output signal for GPIO{a}. {FIELD_DESC}").as_str(),
                    create_bit_range("[6:0]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0x7f)?,
                    None,
                )?,
                create_field_constraint(
                    format!("dout{b}").as_str(),
                    format!("The selected output signal for GPIO{b}. {FIELD_DESC}").as_str(),
                    create_bit_range("[14:8]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0x7f)?,
                    None,
                )?,
                create_field_constraint(
                    format!("dout{c}").as_str(),
                    format!("The selected output signal for GPIO{c}. {FIELD_DESC}").as_str(),
                    create_bit_range("[22:16]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0x7f)?,
                    None,
                )?,
                create_field_constraint(
                    format!("dout{d}").as_str(),
                    format!("The selected output signal for GPIO{d}. {FIELD_DESC}").as_str(),
                    create_bit_range("[30:24]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0x7f)?,
                    None,
                )?,
            ]),
            None,
        )
        .map(svd::RegisterCluster::Register)?;
    }

    create_cluster(
        "gpo_dout",
        format!("SYS IOMUX CFG SAIF SYSCFG FMUX GPIO DOUT - {FIELD_DESC}").as_str(),
        0x40,
        regs.as_ref(),
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
