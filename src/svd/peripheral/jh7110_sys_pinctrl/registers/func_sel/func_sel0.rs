use super::{create_register_func_sel, FuncSel, NameFuncRange};
use crate::svd::{create_bit_range, create_write_constraint};
use crate::Result;

/// Creates the JH7110 SYS PINCTRL Function Selector 0 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let mut nfras = array![NameFuncRange::new(); 11];

    for (idx, nfra) in nfras.iter_mut().enumerate() {
        *nfra = if idx == 0 {
            NameFuncRange {
                name: "pad_gmac1_rxc".into(),
                func: FuncSel::Gmac,
                range: create_bit_range("[1:0]")?,
                constraint: create_write_constraint(0, 0x3)?,
            }
        } else {
            let pad = idx - 1;
            let bit = 2 + (pad * 3);
            let bit_end = bit + 2;

            NameFuncRange {
                name: format!("pad_gpio1{pad}"),
                func: FuncSel::Gpio,
                range: create_bit_range(format!("[{bit_end}:{bit}]").as_str())?,
                constraint: create_write_constraint(0, 0x7)?,
            }
        };
    }

    create_register_func_sel(0, nfras)
}
