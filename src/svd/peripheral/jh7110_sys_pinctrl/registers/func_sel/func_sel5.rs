use super::{create_register_func_sel, FuncSel, NameFuncRange};
use crate::svd::{create_bit_range, create_write_constraint};
use crate::Result;

/// Creates the JH7110 SYS PINCTRL Function Selector 5 register.
pub fn create() -> Result<svd::RegisterCluster> {
    // indexes get a little weird here...
    let pads = [6, 7, 8, 9, 0, 10, 11, 1, 2, 3, 4];
    let mut nfras = array![NameFuncRange::new(); 11];

    for (pad, (idx, nfra)) in pads.iter().zip(nfras.iter_mut().enumerate()) {
        *nfra = match idx {
            0 => NameFuncRange {
                name: "pad_gpio6".into(),
                func: FuncSel::Gpio,
                range: create_bit_range("[1:0]")?,
                constraint: create_write_constraint(0, 0x3)?,
            },
            1..5 => {
                let bit = idx * 2;
                let bit_end = bit + 2;

                NameFuncRange {
                    name: format!("pad_gpio{pad}"),
                    func: FuncSel::Gpio,
                    range: create_bit_range(format!("[{bit_end}:{bit}]").as_str())?,
                    constraint: create_write_constraint(0, 0x7)?,
                }
            }
            _ => {
                let bit = 11 + ((idx - 5) * 3);
                let bit_end = bit + 2;

                NameFuncRange {
                    name: format!("vin_dvp_data{pad}"),
                    func: FuncSel::DvpData,
                    range: create_bit_range(format!("[{bit_end}:{bit}]").as_str())?,
                    constraint: create_write_constraint(0, 0x7)?,
                }
            }
        };
    }

    create_register_func_sel(5, nfras)
}
