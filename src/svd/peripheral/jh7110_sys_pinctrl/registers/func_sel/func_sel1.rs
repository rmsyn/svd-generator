use super::{create_register_func_sel, FuncSel, NameFuncRange};
use crate::svd::{create_bit_range, create_write_constraint};
use crate::Result;

/// Creates the JH7110 SYS PINCTRL Function Selector 1 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let mut nfras = array![NameFuncRange::new(); 10];

    for (idx, nfra) in nfras.iter_mut().enumerate() {
        let pad = 20 + idx;
        let bit = idx * 3;
        let bit_end = bit + 2;

        *nfra = NameFuncRange {
            name: format!("pad_gpio{pad}"),
            func: FuncSel::Gpio,
            range: create_bit_range(format!("[{bit_end}:{bit}]").as_str())?,
            constraint: create_write_constraint(0, 0x7)?,
        };
    }

    create_register_func_sel(1, nfras)
}
