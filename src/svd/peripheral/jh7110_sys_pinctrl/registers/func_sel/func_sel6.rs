use super::{create_register_func_sel, FuncSel, NameFuncRange};
use crate::svd::{create_bit_range, create_write_constraint};
use crate::Result;

/// Creates the JH7110 SYS PINCTRL Function Selector 6 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let mut nfras = array![NameFuncRange::new(); 8];

    for (idx, nfra) in nfras.iter_mut().enumerate() {
        *nfra = match idx {
            0..6 => {
                let data = 5 + idx;
                let bit = idx * 3;
                let bit_end = bit + 2;

                NameFuncRange {
                    name: format!("vin_dvp_data{data}"),
                    func: FuncSel::DvpData,
                    range: create_bit_range(format!("[{bit_end}:{bit}]").as_str())?,
                    constraint: create_write_constraint(0, 0x7)?,
                }
            }
            6 => NameFuncRange {
                name: "vin_dvp_hvalid".into(),
                func: FuncSel::DvpHsync,
                range: create_bit_range("[17:15]")?,
                constraint: create_write_constraint(0, 0x7)?,
            },
            7 => NameFuncRange {
                name: "vin_dvp_vvalid".into(),
                func: FuncSel::DvpVsync,
                range: create_bit_range("[20:18]")?,
                constraint: create_write_constraint(0, 0x7)?,
            },
            _ => NameFuncRange {
                name: "dvp_clk".into(),
                func: FuncSel::DvpClock,
                range: create_bit_range("[23:21]")?,
                constraint: create_write_constraint(0, 0x7)?,
            },
        };
    }

    create_register_func_sel(6, nfras)
}
