use super::{create_register_func_sel, FuncSel, NameFuncRange};
use crate::svd::{create_bit_range, create_write_constraint};
use crate::Result;

/// Creates the JH7110 SYS PINCTRL Function Selector 4 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let mut nfras = array![NameFuncRange::new(); 12];

    for (idx, nfra) in nfras.iter_mut().enumerate() {
        let pad = idx + 52;

        *nfra = match idx {
            0..4 => {
                let bit = idx * 2;
                let bit_end = bit + 1;

                NameFuncRange {
                    name: format!("pad_gpio{pad}"),
                    func: FuncSel::Gpio,
                    range: create_bit_range(format!("[{bit_end}:{bit}]").as_str())?,
                    constraint: create_write_constraint(0, 0x3)?,
                }
            }
            4..11 => {
                let bit = idx * 3;
                let bit_end = bit + 2;

                NameFuncRange {
                    name: format!("pad_gpio{pad}"),
                    func: FuncSel::Gpio,
                    range: create_bit_range(format!("[{bit_end}:{bit}]").as_str())?,
                    constraint: create_write_constraint(0, 0x7)?,
                }
            }
            _ => NameFuncRange {
                name: "pad_gpio63".into(),
                func: FuncSel::Gpio,
                range: create_bit_range("[31:30]")?,
                constraint: create_write_constraint(0, 0x3)?,
            },
        };
    }

    create_register_func_sel(4, nfras)
}
