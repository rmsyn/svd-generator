/// Function selector category.
#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum FuncSel {
    Gmac = 0,
    Gpio = 1,
    DvpData = 2,
    DvpHsync = 3,
    DvpVsync = 4,
    DvpClock = 5,
}

impl FuncSel {
    /// Creates a new [FuncSel].
    pub const fn new() -> Self {
        Self::Gmac
    }
}

impl Default for FuncSel {
    fn default() -> Self {
        Self::new()
    }
}

impl From<FuncSel> for &'static str {
    fn from(val: FuncSel) -> Self {
        match val {
            FuncSel::Gmac => "Function selector of GMAC1_RXC: * Function 0: u0_sys_crg.clk_gmac1_rgmii_rx, * Function 1: u0_sys_crg.clk_gmac1_rmii_ref, * Function 2: None, * Function 3: None",
            FuncSel::Gpio => "GPIO function selector: * Function 0: See Function Description no page 84 for more information, * Function 1: See Full Multiplexing for more information, * Function 2: See Function 2 for more information, * Function 3: See Function 3 for more information",
            FuncSel::DvpData => "Function Selector of DVP_DATA[idx], see Function 2 for more information",
            FuncSel::DvpHsync => "Function Selector of DVP_HSYNC, see Function 2 for more information",
            FuncSel::DvpVsync => "Function Selector of DVP_VSYNC, see Function 2 for more information",
            FuncSel::DvpClock => "Function Selector of DVP_CLK, see Function 2 for more information",
        }
    }
}

impl From<&FuncSel> for &'static str {
    fn from(val: &FuncSel) -> Self {
        (*val).into()
    }
}
