use crate::svd::register::{
    create_bit_range, create_cluster, create_default_register, create_enum_value,
    create_enum_values, create_field_constraint, create_field_enum, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS Pinctrl GPIO Pad Configuration registers.
pub fn create() -> Result<svd::RegisterCluster> {
    const RST_LEN: usize = 64;
    const SD0_NAMES_LEN: usize = 11;
    const PAD_LEN: usize = RST_LEN + SD0_NAMES_LEN + 4;
    const SD0_NAMES: [&str; SD0_NAMES_LEN] = [
        "sd0_clk",
        "sd0_cmd",
        "sd0_data0",
        "sd0_data1",
        "sd0_data2",
        "sd0_data3",
        "sd0_data4",
        "sd0_data5",
        "sd0_data6",
        "sd0_data7",
        "sd0_strb",
    ];

    let default_reg = create_default_register()?;
    let mut regs = array![default_reg.clone(); PAD_LEN];

    for (rst, reg) in regs.iter_mut().take(RST_LEN).enumerate() {
        *reg = create_register_padcfg(rst as u32, format!("gpio{rst}").as_str())?;
    }

    for (idx, (reg, name)) in regs
        .iter_mut()
        .skip(RST_LEN)
        .take(SD0_NAMES_LEN)
        .zip(SD0_NAMES)
        .enumerate()
    {
        *reg = create_register_padcfg((RST_LEN + idx) as u32, name)?;
    }

    regs[RST_LEN + SD0_NAMES_LEN] = create_register(
        "gmac1",
        "SYS IOMUX CFG SAIF SYSCFG PADCFG: GPIO GMAC1 pads",
        0x12c,
        create_register_properties(32, 0x2)?,
        Some(&[create_field_constraint(
            "cfg",
            "",
            create_bit_range("[1:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0x3)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(14)
                .dim_increment(0x4)
                .dim_index(Some(
                    [
                        "_mdc", "_mdio", "_rxd_0", "_rxd_1", "_rxd_2", "_rxd_3", "_rxdv", "_rxc",
                        "_txd_0", "_txd_1", "_txd_2", "_txd_3", "_txen", "_txc",
                    ]
                    .map(String::from)
                    .into(),
                ))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)?;

    for (reg, (idx, name)) in regs
        .iter_mut()
        .skip(RST_LEN + SD0_NAMES_LEN + 1)
        .take(2)
        .zip([(89, "qspi_sclk"), (90, "qspi_csn0")])
    {
        *reg = create_register_padcfg(idx, name)?;
    }

    regs[RST_LEN + SD0_NAMES_LEN + 3] = create_register(
        "qspi_data",
        "SYS IOMUX CFG SAIF SYSCFG PADCFG 28c-298: QSPI_DATA 0-3",
        0x16c,
        create_register_properties(32, 0x1)?,
        Some(&create_fields_padcfg()?),
        Some(
            svd::DimElement::builder()
                .dim(4)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)?;

    create_cluster(
        "padcfg",
        "SYS IOMUX CFG SAIF SYSCFG PADCFG: GPIO pad configuration",
        0x120,
        regs.as_ref(),
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}

fn create_register_padcfg(idx: u32, name: &str) -> Result<svd::RegisterCluster> {
    #[rustfmt::skip]
    const RESET_VALUES: [u64; 91] = [
        // GPIO DATA
        // 0     1     2     3     4     5     6     7
        0x11, 0x09, 0x01, 0x01, 0x09, 0x00, 0x09, 0x01,
        // 8     9    10    11    12    13    14    15
        0x01, 0x09, 0x01, 0x01, 0x09, 0x09, 0x01, 0x01,
        //16    17    18    19    20    21    22    23
        0x01, 0x01, 0x09, 0x09, 0x01, 0x01, 0x01, 0x01,
        //24    25    26    27    28    29    30    31
        0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
        //32    33    34    35    36    37    38    39
        0x01, 0x00, 0x01, 0x11, 0x09, 0x09, 0x09, 0x09,
        //40    41    42    43    44    45    46    47
        0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09, 0x09,
        //48    49    50    51    52    53    54    55
        0x09, 0x01, 0x01, 0x01, 0x09, 0x01, 0x01, 0x01,
        //56    57    58    59    60    61    62    63
        0x09, 0x01, 0x01, 0x01, 0x09, 0x01, 0x01, 0x01,
        // SD0 CLK (64)
        0x01,
        // SD0 CMD (65)
        0x01,
        // SD0 DATA (66-73)
        // 0     1     2     3     4     5     6     7
        0x09, 0x01, 0x01, 0x09, 0x09, 0x01, 0x01, 0x01,
        // SD0 STRB (74)
        0x01,
        // Placeholders for non-PADCFG registers (75-88)
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
        // QSPI (89)
        0x01,
        // QSPI CSn0 (90)
        0x08,
    ];

    let addr = idx * 4;
    let pad_num = 0x120 + addr;

    create_register(
        name,
        format!(
            "SYS IOMUX CFG SAIF SYSCFG PADCFG {pad_num}: {}",
            name.to_uppercase()
        )
        .as_str(),
        addr,
        create_register_properties(32, RESET_VALUES[idx as usize])?,
        Some(&create_fields_padcfg()?),
        None,
    )
    .map(svd::RegisterCluster::Register)
}

fn create_fields_padcfg() -> Result<[svd::Field; 7]> {
    Ok([
        create_field_enum(
            "ie",
            "Input Enable (IE) Controller",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("disable", "Disable the receiver", 0)?,
                create_enum_value("enable", "Enable the receiver", 1)?,
            ])?],
            None,
        )?,
        create_field_enum(
            "ds",
            "Output Drive Strength (DS)",
            create_bit_range("[2:1]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("ma2", "The rated output drive strength: 2 mA", 0b00)?,
                create_enum_value("ma4", "The rated output drive strength: 4 mA", 0b01)?,
                create_enum_value("ma8", "The rated output drive strength: 8 mA", 0b10)?,
                create_enum_value("ma12", "The rated output drive strength: 12 mA", 0b11)?,
            ])?],
            None,
        )?,
        create_field_enum(
            "pu",
            "Pull-Up (PU) settings",
            create_bit_range("[3:3]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("no", "Pull-up setting: no", 0)?,
                create_enum_value("yes", "Pull-up setting: yes", 1)?,
            ])?],
            None,
        )?,
        create_field_enum(
            "pd",
            "Pull-Down (PD) settings",
            create_bit_range("[4:4]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("no", "Pull-down setting: no", 0)?,
                create_enum_value("yes", "Pull-down setting: yes", 1)?,
            ])?],
            None,
        )?,
        create_field_enum(
            "slew",
            "Slew Rate Control - 0: Slow (Half frequency), 1: Fast",
            create_bit_range("[5:5]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("slow", "Slow rate control: slow (half frequency)", 0)?,
                create_enum_value("fast", "Slow rate control: fast", 1)?,
            ])?],
            None,
        )?,
        create_field_enum(
            "smt",
            "Active high Schmitt (SMT) trigger selector",
            create_bit_range("[6:6]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value(
                    "no_hysteresis",
                    "Active high Schmitt (SMT) trigger selector: no hysteresis",
                    0,
                )?,
                create_enum_value(
                    "schmitt",
                    "Active high Schmitt (SMT) trigger selector: Schmitt trigger enabled",
                    1,
                )?,
            ])?],
            None,
        )?,
        create_field_enum(
            "pos",
            "Power-on-Start (POS) enabler",
            create_bit_range("[7:7]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value(
                    "disable",
                    "Power-on-Start (POS) active pull down capability for loss of power: disabled",
                    0,
                )?,
                create_enum_value(
                    "enable",
                    "Power-on-Start (POS) active pull down capability for loss of power: enabled",
                    1,
                )?,
            ])?],
            None,
        )?,
    ])
}
