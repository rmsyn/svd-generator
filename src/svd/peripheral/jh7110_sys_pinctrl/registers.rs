use crate::Result;

pub mod func_sel;
pub mod gpi;
pub mod gpo_doen;
pub mod gpo_dout;
pub mod ioirq;
pub mod padcfg;

/// Creates StarFive JH7110 SYS Pinctrl (compatible) register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        gpo_doen::create()?,
        gpo_dout::create()?,
        gpi::create()?,
        ioirq::create()?,
        padcfg::create()?,
        func_sel::create()?,
    ]
    .into())
}
