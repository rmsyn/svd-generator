use crate::svd::{create_derived_peripheral, create_peripheral};
use crate::Result;

pub mod registers;

/// Represents a Synopsys DesignWare APB I2C (compatible) peripheral
pub struct DwApbI2c {
    peripheral: svd::Peripheral,
}

impl DwApbI2c {
    /// Creates a new [DwApbI2c] peripheral.
    pub fn create(
        name: &str,
        base_address: u64,
        size: u32,
        interrupt: Option<Vec<svd::Interrupt>>,
        dim: u32,
        count: usize,
    ) -> Result<Self> {
        let full_name = format!("{name}{count}");

        let dim_element = if dim > 0 {
            Some(
                svd::DimElement::builder()
                    .dim(dim)
                    .dim_increment(size)
                    .build(svd::ValidateLevel::Strict)?,
            )
        } else {
            None
        };

        let desc = format!("Synopsys DesignWare APB I2C: {full_name}");

        if count == 0 {
            create_peripheral(
                full_name.as_str(),
                desc.as_str(),
                base_address,
                size,
                interrupt,
                Some(registers::create()?),
                dim_element,
                Some("I2C".into()),
            )
        } else {
            create_derived_peripheral(
                full_name.as_str(),
                desc.as_str(),
                base_address,
                size,
                interrupt,
                "I2C",
                format!("{name}0").as_str(),
            )
        }
        .map(|peripheral| Self { peripheral })
    }

    /// Gets a reference to the SVD [`Peripheral`](svd::Peripheral).
    pub const fn peripheral(&self) -> &svd::Peripheral {
        &self.peripheral
    }

    /// Gets a mutable reference to the SVD [`Peripheral`](svd::Peripheral).
    pub fn peripheral_mut(&mut self) -> &mut svd::Peripheral {
        &mut self.peripheral
    }

    /// Converts the [DwApbI2c] into the inner SVD [`Peripheral`](svd::Peripheral).
    pub fn to_inner(self) -> svd::Peripheral {
        self.peripheral
    }
}
