use crate::svd::create_peripheral;
use crate::Result;

pub mod registers;

/// Represents a RISC-V PLIC (Platform-level Interrupt Controller) peripheral.
///
/// The PLIC description is compliant with the [RISC-V PLIC v1.0.0 specification](https://raw.githubusercontent.com/riscv/riscv-plic-spec/master/riscv-plic-1.0.0.pdf).
pub struct RiscvPlic {
    peripheral: svd::Peripheral,
}

impl RiscvPlic {
    /// Creates a new [RiscvPlic] peripheral.
    pub fn create(base_address: u64, size: u32, harts: usize, interrupts: usize) -> Result<Self> {
        let name = "plic";
        let peripheral = create_peripheral(
            name,
            format!("RISC-V PLIC: {name}").as_str(),
            base_address,
            size,
            None,
            Some(registers::create(harts, interrupts)?),
            None,
            Some("PLIC".into()),
        )?;

        Ok(Self { peripheral })
    }

    /// Gets a reference to the SVD [`Peripheral`](svd::Peripheral).
    pub const fn peripheral(&self) -> &svd::Peripheral {
        &self.peripheral
    }

    /// Gets a mutable reference to the SVD [`Peripheral`](svd::Peripheral).
    pub fn peripheral_mut(&mut self) -> &mut svd::Peripheral {
        &mut self.peripheral
    }

    /// Converts the [DwApUart] into the inner SVD [`Peripheral`](svd::Peripheral).
    pub fn to_inner(self) -> svd::Peripheral {
        self.peripheral
    }
}
