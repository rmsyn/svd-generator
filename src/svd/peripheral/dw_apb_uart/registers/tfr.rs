use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Transmit FIFO Read.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "tfr",
            "Transmit FIFO Read",
            0x74,
            create_register_properties(32, 0)?,
            Some(&[
                create_field(
                    "tfr",
                    "Transmit FIFO Read. These bits are only valid when FIFO access mode is enabled (FAR[0] is set to one). When FIFOs are implemented and enabled, reading this register gives the data at the top of the transmit FIFO. Each consecutive read pops the transmit FIFO and gives the next data value that is currently at the top of the FIFO. When FIFOs are not implemented or not enabled, reading this register gives the data in the THR.",
                    create_bit_range("[7:0]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
            ]),
            None,
        )?
    ))
}
