use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the Interrupt Enable Register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "ier",
            "Interrupt Enable Register",
            0x4,
            create_register_properties(32, 0)?,
            Some(&[
                create_field_enum(
                    "ptime",
                    "Programmable THRE Interrupt Mode Enable that can be written to only when THRE_MODE_USER == Enabled, always readable. This is used to enable/disable the generation of THRE Interrupt. 0 = disabled 1 = enabled",
                    create_bit_range("[7:7]")?,
                    svd::Access::ReadWrite,
                    &[create_enum_values(&[
                        create_enum_value("disable", "Programmable THRE Interrupt Mode disabled", 0b0)?,
                        create_enum_value("enable", "Programmable THRE Interrupt Mode enabled", 0b1)?,
                    ])?],
                    None,
                )?,
                create_field_enum(
                    "edssi",
                    "Enable Modem Status Interrupt. This is used to enable/disable the generation of Modem Status Interrupt. This is the fourth highest priority interrupt. 0 = disabled 1 = enabled",
                    create_bit_range("[3:3]")?,
                    svd::Access::ReadWrite,
                    &[create_enum_values(&[
                        create_enum_value("disable", "Modem Status Interrupt disabled", 0b0)?,
                        create_enum_value("enable", "Modem Status Interrupt enabled", 0b1)?,
                    ])?],
                    None,
                )?,
                create_field_enum(
                    "elsi",
                    "Enable Receiver Line Status Interrupt. This is used to enable/disable the generation of Receiver Line Status Interrupt. This is the highest priority interrupt. 0 = disabled 1 = enabled",
                    create_bit_range("[2:2]")?,
                    svd::Access::ReadWrite,
                    &[create_enum_values(&[
                        create_enum_value("disable", "Enable Receiver Line Status Interrupt disabled", 0b0)?,
                        create_enum_value("enable", "Enable Receiver Line Status Interrupt enabled", 0b1)?,
                    ])?],
                    None,
                )?,
                create_field_enum(
                    "etbei",
                    "Enable Transmit Holding Register Empty Interrupt. This is used to enable/disable the generation of Transmitter Holding Register Empty Interrupt. This is the third highest priority interrupt. 0 = disabled 1 = enabled",
                    create_bit_range("[1:1]")?,
                    svd::Access::ReadWrite,
                    &[create_enum_values(&[
                        create_enum_value("disable", "Enable Transmit Holding Register Empty Interrupt disabled", 0b0)?,
                        create_enum_value("enable", "Enable Transmit Holding Register Empty Interrupt enabled", 0b1)?,
                    ])?],
                    None,
                )?,
                create_field_enum(
                    "erbfi",
                    "Enable Received Data Available Interrupt. This is used to enable/disable the generation of Received Data Available Interrupt and the Character Timeout Interrupt (if in FIFO mode and FIFOs enabled). These are the second highest priority interrupts. 0 = disabled 1 = enabled",
                    create_bit_range("[0:0]")?,
                    svd::Access::ReadWrite,
                    &[create_enum_values(&[
                        create_enum_value("disable", "Enable Received Data Available Interrupt disabled", 0b0)?,
                        create_enum_value("enable", "Enable Received Data Available Interrupt enabled", 0b1)?,
                    ])?],
                    None
                )?,
            ]),
            None,
        )?
    ))
}
