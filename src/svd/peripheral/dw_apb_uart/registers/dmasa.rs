use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the DMA Software Acknowledge.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "dmasa",
            "DMA Software Acknowledge",
            0xa8,
            create_register_properties(32, 0)?,
            Some(&[
                create_field(
                    "dmasa",
                    "This register is use to perform a DMA software acknowledge if a transfer needs to be terminated due to an error condition. For example, if the DMA disables the channel, then the DW_apb_uart should clear its request. This causes the TX request, TX single, RX request and RX single signals to de-assert. Note that this bit is 'self-clearing'. It is not necessary to clear this bit.",
                    create_bit_range("[0:0]")?,
                    svd::Access::WriteOnly,
                    None,
                )?,
            ]),
            None,
        )?
    ))
}
