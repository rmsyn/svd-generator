use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the UART Status Register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "usr",
            "UART Status Register",
            0x7C,
            create_register_properties(32, 0)?,
            Some(&[
                create_field(
                    "rff",
                    "Receive FIFO Full. This bit is only valid when FIFO_STAT == YES. This is used to indicate that the receive FIFO is completely full. 0 = Receive FIFO not full 1 = Receive FIFO Full This bit is cleared when the RX FIFO is no longer full.",
                    create_bit_range("[4:4]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field(
                    "rfne",
                    "Receive FIFO Not Empty. This bit is only valid when FIFO_STAT == YES. This is used to indicate that the receive FIFO contains one or more entries. 0 = Receive FIFO is empty 1 = Receive FIFO is not empty This bit is cleared when the RX FIFO is empty.",
                    create_bit_range("[3:3]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field(
                    "tfe",
                    "Transmit FIFO Empty. This bit is only valid when FIFO_STAT == YES. This is used to indicate that the transmit FIFO is completely empty. 0 = Transmit FIFO is not empty 1 = Transmit FIFO is empty This bit is cleared when the TX FIFO is no longer empty.",
                    create_bit_range("[2:2]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field(
                    "tfnf",
                    "Transmit FIFO Not Full. This bit is only valid when FIFO_STAT == YES. This is used to indicate that the transmit FIFO in not full. 0 = Transmit FIFO is full 1 = Transmit FIFO is not full This bit is cleared when the TX FIFO is full.",
                    create_bit_range("[1:1]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field(
                    "busy",
                    "UART Busy. This is indicates that a serial transfer is in progress, when cleared indicates that the DW_apb_uart is idle or inactive. 0 = DW_apb_uart is idle or inactive 1 = DW_apb_uart is busy (actively transferring data) NOTE: It is possible for the UART Busy bit to be cleared even though a new character may have been sent from another device. That is, if the DW_apb_uart has no data in THR and RBR and there is no transmission in progress and a start bit of a new character has just reached the DW_apb_uart. This is due to the fact that a valid start is not seen until the middle of the bit period and this duration is dependent on the baud divisor that has been programmed. If a second system clock has been implemented (CLOCK_MODE == Enabled), the assertion of this bit is also delayed by several cycles of the slower clock.",
                    create_bit_range("[0:0]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
            ]),
            None,
        )?
    ))
}
