use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates the Line Control Register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "lcr",
            "Line Control Register",
            0xc,
            create_register_properties(32, 0)?,
            Some(&[
                create_field(
                    "dlab",
                    "Divisor Latch Access Bit. Writeable only when UART is not busy (USR[0] is zero), always readable. This bit is used to enable reading and writing of the Divisor Latch register (DLL and DLH) to set the baud rate of the UART. This bit must be cleared after initial baud rate setup in order to access other registers.",
                    create_bit_range("[7:7]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field(
                    "bc",
                    "Break Control Bit.This is used to cause a break condition to be transmitted to the receiving device. If set to one the serial output is forced to the spacing (logic 0) state. When not in Loopback Mode, as determined by MCR[4], the sout line is forced low until the Break bit is cleared. If SIR_MODE == Enabled and active (MCR[6] set to one) the sir_out_n line is continuously pulsed. When in Loopback Mode, the break condition is internally looped back to the receiver and the sir_out_n line is forced low.",
                    create_bit_range("[6:6]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field(
                    "eps",
                    "Even Parity Select. Writeable only when UART is not busy (USR[0] is zero), always readable. This is used to select between even and odd parity, when parity is enabled (PEN set to one). If set to one, an even number of logic 1s is transmitted or checked. If set to zero, an odd number of logic 1s is transmitted or checked.",
                    create_bit_range("[4:4]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field(
                    "pen",
                    "Parity Enable. Writeable only when UART is not busy (USR[0] is zero), always readable. This bit is used to enable and disable parity generation and detection in transmitted and received serial character respectively. 0 = parity disabled 1 = parity enabled",
                    create_bit_range("[3:3]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field_enum(
                    "stop",
                    "Number of stop bits. Writeable only when UART is not busy (USR[0] is zero), always readable. This is used to select the number of stop bits per character that the peripheral transmits and receives. If set to zero, one stop bit is transmitted in the serial data. If set to one and the data bits are set to 5 (LCR[1:0] set to zero) one and a half stop bits is transmitted. Otherwise, two stop bits are transmitted. Note that regardless of the number of stop bits selected, the receiver checks only the first stop bit. 0 = 1 stop bit 1 = 1.5 stop bits when DLS (LCR[1:0]) is zero, else 2 stop bit",
                    create_bit_range("[2:2]")?,
                    svd::Access::ReadWrite,
                    &[create_enum_values(&[
                        create_enum_value("bit1", "1 stop bit", 0b0)?,
                        create_enum_value("bit2", "1.5 stop bits when LCR is zero, else 2 stop bits", 0b1)?,
                    ])?],
                    None,
                )?,
                create_field_enum(
                    "dls",
                    "Data Length Select. Writeable only when UART is not busy (USR[0] is zero), always readable. This is used to select the number of data bits per character that the peripheral transmits and receives. The number of bit that may be selected areas follows: 00 = 5 bits 01 = 6 bits 10 = 7 bits 11 = 8 bits",
                    create_bit_range("[1:0]")?,
                    svd::Access::ReadWrite,
                    &[create_enum_values(&[
                        create_enum_value("five", "Data length 5-bits", 0b00)?,
                        create_enum_value("six", "Data length 6-bits", 0b01)?,
                        create_enum_value("seven", "Data length 7-bits", 0b10)?,
                        create_enum_value("eight", "Data length 8-bits", 0b11)?,
                    ])?],
                    None,
                )?,
            ]),
            None,
        )?
    ))
}
