use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates the FIFO Control Register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "fcr",
            "FIFO Control Register",
            0x8,
            create_register_properties(32, 0)?,
            Some(&[
                create_field_enum(
                    "rt",
                    "RCVR Trigger. This is used to select the trigger level in the receiver FIFO at which the Received Data Available Interrupt is generated. In auto flow control mode it is used to determine when the rts_n signal is de-asserted. It also determines when the dma_rx_req_n signal is asserted in certain modes of operation. For details on DMA support, refer to “DMA Support” on page 58. The following trigger levels are supported: 00 = 1 character in the FIFO 01 = FIFO ¼ full 10 = FIFO ½ full 11 = FIFO 2 less than full",
                    create_bit_range("[7:6]")?,
                    svd::Access::WriteOnly,
                    &[create_enum_values(&[
                        create_enum_value("near_empty", "Trigger when 1 character in the FIFO", 0b00)?,
                        create_enum_value("quarter", "Trigger when FIFO is 1/4 full", 0b01)?,
                        create_enum_value("half", "Trigger when FIFO is 1/2 full", 0b10)?,
                        create_enum_value("near_full", "Trigger when FIFO is 2 bytes less than full", 0b11)?,
                    ])?],
                    None
                )?,
                create_field_enum(
                    "tet",
                    "TX Empty Trigger. Writes have no effect when THRE_MODE_USER == Disabled. This is used to select the empty threshold level at which the THRE Interrupts are generated when the mode is active. It also determines when the dma_tx_req_n signal is asserted when in certain modes of operation. For details on DMA support, refer to “DMA Support” on page 58. The following trigger levels are supported: 00 = FIFO empty 01 = 2 characters in the FIFO 10 = FIFO ¼ full 11 = FIFO ½ full",
                    create_bit_range("[5:4]")?,
                    svd::Access::WriteOnly,
                    &[create_enum_values(&[
                        create_enum_value("empty", "Trigger FIFO when empty", 0b00)?,
                        create_enum_value("near_empty", "Trigger when 2 characters in the FIFO", 0b01)?,
                        create_enum_value("quarter", "Trigger when FIFO is 1/4 full", 0b10)?,
                        create_enum_value("half", "Trigger when FIFO is 1/2 full", 0b11)?,
                    ])?],
                    None,
                )?,
                create_field_enum(
                    "dmam",
                    "DMA Mode. This determines the DMA signalling mode used for the dma_tx_req_n and dma_rx_req_n output signals when additional DMA handshaking signals are not selected (DMA_EXTRA == No). For details on DMA support, refer to “DMA Support” on page 58. 0 = mode 0 1 = mode 1",
                    create_bit_range("[3:3]")?,
                    svd::Access::WriteOnly,
                    &[create_enum_values(&[
                        create_enum_value("mode0", "Mode 0", 0b0)?,
                        create_enum_value("mode1", "Mode 1", 0b1)?,
                    ])?],
                    None,
                )?,
                create_field(
                    "xfifor",
                    "XMIT FIFO Reset. This resets the control portion of the transmit FIFO and treats the FIFO as empty. This also de-asserts the DMA TX request and single signals when additional DMA handshaking signals are selected (DMA_EXTRA == YES). Note that this bit is 'self-clearing'. It is not necessary to clear this bit.",
                    create_bit_range("[2:2]")?,
                    svd::Access::WriteOnly,
                    None,
                )?,
                create_field(
                    "rfifor",
                    "RCVR FIFO Reset. This resets the control portion of the receive FIFO and treats the FIFO as empty. This also de-asserts the DMA RX request and single signals when additional DMA handshaking signals are selected (DMA_EXTRA == YES). Note that this bit is 'self-clearing'. It is not necessary to clear this bit.",
                    create_bit_range("[1:1]")?,
                    svd::Access::WriteOnly,
                    None,
                )?,
                create_field(
                    "fifoe",
                    "FIFO Enable. This enables/disables the transmit (XMIT) and receive (RCVR) FIFOs. Whenever the value of this bit is changed both the XMIT and RCVR controller portion of FIFOs is reset.",
                    create_bit_range("[0:0]")?,
                    svd::Access::WriteOnly,
                    None,
                )?,
            ]),
            None,
        )?
    ))
}
