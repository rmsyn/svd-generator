use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Modem Control Register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "mcr",
            "Modem Control Register",
            0x10,
            create_register_properties(32, 0)?,
            Some(&[
                create_field(
                    "sire",
                    "SIR Mode Enable. Writeable only when SIR_MODE == Enabled, always readable. This is used to enable/disable the IrDA SIR Mode features as described in “IrDA 1.0 SIR Protocol” on page 47. 0 = IrDA SIR Mode disabled 1 = IrDA SIR Mode enabled",
                    create_bit_range("[6:6]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field(
                    "afce",
                    "Auto Flow Control Enable. Writeable only when AFCE_MODE == Enabled, always readable. When FIFOs are enabled and the Auto Flow Control Enable (AFCE) bit is set, Auto Flow Control features are enabled as described in “Auto Flow Control” on page 51. 0 = Auto Flow Control Mode disabled 1 = Auto Flow Control Mode enabled",
                    create_bit_range("[5:5]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field(
                    "lb",
                    "LoopBack Bit. This is used to put the UART into a diagnostic mode for test purposes. If operating in UART mode (SIR_MODE != Enabled or not active, MCR[6] set to zero), data on the sout line is held high, while serial data output is looped back to the sin line, internally. In this mode all the interrupts are fully functional. Also, in loopback mode, the modem control inputs (dsr_n, cts_n, ri_n, dcd_n) are disconnected and the modem control outputs (dtr_n, rts_n, out1_n, out2_n) are looped back to the inputs, internally. If operating in infrared mode (SIR_MODE == Enabled AND active, MCR[6] set to one), data on the sir_out_n line is held low, while serial data output is inverted and looped back to the sir_in line",
                    create_bit_range("[4:4]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field(
                    "out2",
                    "OUT2. This is used to directly control the user-designated Output2 (out2_n) output. The value written to this location is inverted and driven out on out2_n, that is: 0 = out2_n de-asserted (logic 1) 1 = out2_n asserted (logic 0) Note that in Loopback mode (MCR[4] set to one), the out2_n output is held inactive high while the value of this location is internally looped back to an input.",
                    create_bit_range("[3:3]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field(
                    "out1",
                    "OUT1. This is used to directly control the user-designated Output1 (out1_n) output. The value written to this location is inverted and driven out on out1_n, that is: 0 = out1_n de-asserted (logic 1) 1 = out1_n asserted (logic 0) Note that in Loopback mode (MCR[4] set to one), the out1_n output is held inactive high while the value of this location is internally looped back to an input.",
                    create_bit_range("[2:2]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field(
                    "rts",
                    "Request to Send. This is used to directly control the Request to Send (rts_n) output. The Request To Send (rts_n) output is used to inform the modem or data set that the UART is ready to exchange data. When Auto RTS Flow Control is not enabled (MCR[5] set to zero), the rts_n signal is set low by programming MCR[1] (RTS) to a high.In Auto Flow Control, AFCE_MODE == Enabled and active (MCR[5] set to one) and FIFOs enable (FCR[0] set to one), the rts_n output is controlled in the same way, but is also gated with the receiver FIFO threshold trigger (rts_n is inactive high when above the threshold). The rts_n signal is de-asserted when MCR[1] is set low. Note that in Loopback mode (MCR[4] set to one), the rts_n output is held inactive high while the value of this location is internally looped back to an input.",
                    create_bit_range("[1:1]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field(
                    "dtr",
                    "Data Terminal Ready. This is used to directly control the Data Terminal Ready (dtr_n) output. The value written to this location is inverted and driven out on dtr_n, that is: 0 = dtr_n de-asserted (logic 1) 1 = dtr_n asserted (logic 0) The Data Terminal Ready output is used to inform the modem or data set that the UART is ready to establish communications. Note that in Loopback mode (MCR[4] set to one), the dtr_n output is held inactive high while the value of this location is internally looped back to an input.",
                    create_bit_range("[0:0]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
            ]),
            None,
        )?
    ))
}
