use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Component Type Register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "ctr",
            "Component Type Register: This register is only valid when the DW_apb_uart is configured to have additional features implemented (ADDITIONAL_FEATURES == YES). If additional features are not implemented, this register does not exist and reading from this register address returns zero.",
            0xf8,
            create_register_properties(32, 0x4457_0110)?,
            Some(&[
                create_field(
                    "ctr",
                    "This register contains the peripherals identification code.",
                    create_bit_range("[31:0]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
            ]),
            None
        )?
    ))
}
