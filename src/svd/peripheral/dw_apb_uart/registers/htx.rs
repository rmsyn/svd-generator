use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Halt TX.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "htx",
            "Halt TX",
            0xa4,
            create_register_properties(32, 0)?,
            Some(&[
                create_field(
                    "htx",
                    "This register is use to halt transmissions for testing, so that the transmit FIFO can be filled by the master when FIFOs are implemented and enabled. 0 = Halt TX disabled 1 = Halt TX enabled Note, if FIFOs are implemented and not enabled, the setting of the halt TX register has no effect on operation.",
                    create_bit_range("[0:0]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
            ]),
            None,
        )?,
    ))
}
