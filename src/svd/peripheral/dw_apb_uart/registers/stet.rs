use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the Shadow TX Empty Trigger.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "stet",
            "Shadow TX Empty Trigger: This register is only valid when the DW_apb_uart is configured to have FIFOs implemented (FIFO_MODE != NONE) and THRE interrupt support implemented (THRE_MODE_USER == Enabled) and additional shadow registers implemented (SHADOW == YES). If FIFOs are not implemented or THRE interrupt support is not implemented or shadow registers are not implemented, this register does not exist and reading from this register address returns zero.",
            0xa0,
            create_register_properties(32, 0)?,
            Some(&[
                create_field_enum(
                    "stet",
                    "Shadow TX Empty Trigger. This is a shadow register for the TX empty trigger bits (FCR[5:4]). This can be used to remove the burden of having to store the previously written value to the FCR in memory and having to mask this value so that only the TX empty trigger bit gets updated. This is used to select the empty threshold level at which the THRE Interrupts are generated when the mode is active. The following trigger levels are supported: 00 = FIFO empty 01 = 2 characters in the FIFO 10 = FIFO ¼ full 11 = FIFO ½ full",
                    create_bit_range("[1:0]")?,
                    svd::Access::ReadWrite,
                    &[create_enum_values(&[
                        create_enum_value("empty", "Trigger when FIFO is empty", 0b00)?,
                        create_enum_value("near_empty", "Trigger when FIFO has two characters", 0b01)?,
                        create_enum_value("quarter", "Trigger when FIFO is 1/4 full", 0b10)?,
                        create_enum_value("half", "Trigger when FIFO is 1/2 full", 0b11)?,
                    ])?],
                    None,
                )?,
            ]),
            None
        )?
    ))
}
