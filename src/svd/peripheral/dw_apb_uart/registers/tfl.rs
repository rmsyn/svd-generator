use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Transmit FIFO Level.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "tfl",
            "Transmit FIFO Level: This register is only valid when the DW_apb_uart is configured to have additional FIFO status registers implemented (FIFO_STAT == YES). If status registers are not implemented, this register does not exist and reading from this register address returns zero.",
            0x80,
            create_register_properties(32, 0)?,
            Some(&[
                create_field(
                    "tfl",
                    "Transmit FIFO Level. This is indicates the number of data entries in the transmit FIFO.",
                    create_bit_range("[31:0]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
            ]),
            None,
        )?
    ))
}
