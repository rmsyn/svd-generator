use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the Transmit Holding Register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "thr",
        "Transmit Holding Register",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "thr",
                "Data to be transmitted on the serial output port (sout) in UART mode or the serial infrared output (sir_out_n) in infrared mode. Data should only be written to the THR when the THR Empty (THRE) bit (LSR[5]) is set. If in non-FIFO mode or FIFOs are disabled (FCR[0] = 0) and THRE is set, writing a single character to the THR clears the THRE. Any additional writes to the THR before the THRE is set again causes the THR data to be overwritten. If in FIFO mode and FIFOs are enabled (FCR[0] = 1) and THRE is set, x number of characters of data may be written to the THR before the FIFO is full. The number x (default=16) is determined by the value of FIFO Depth that you set during configuration. Any attempt to write data when the FIFO is full results in the write data being lost.",
                create_bit_range("[7:0]")?,
                svd::Access::WriteOnly,
                create_write_constraint(0, 0xff)?,
                None,
            )?
        ]),
        None,
    )?))
}
