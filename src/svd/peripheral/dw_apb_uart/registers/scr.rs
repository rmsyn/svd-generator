use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the Scratch Pad Register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "scr",
            "Scratch Pad Register",
            0x1c,
            create_register_properties(32, 0)?,
            Some(&[
                create_field_constraint(
                    "scr",
                    "This register is for programmers to use as a temporary storage space. It has no defined purpose in the DW_apb_uart.",
                    create_bit_range("[7:0]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0xff)?,
                    None,
                )?,
            ]),
            None,
        )?
    ))
}
