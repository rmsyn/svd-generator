use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the Shadow RCVR Trigger.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "srt",
            "Shadow RCVR Trigger: This register is only valid when the DW_apb_uart is configured to have additional FIFO registers implemented (FIFO_MODE != None) and additional shadow registers implemented (SHADOW == YES). If these registers are not implemented, this register does not exist and reading from this register address returns zero.",
            0x9c,
            create_register_properties(32, 0)?,
            Some(&[
                create_field_enum(
                    "srt",
                    "Shadow RCVR Trigger. This is a shadow register for the RCVR trigger bits (FCR[7:6]). This can be used to remove the burden of having to store the previously written value to the FCR in memory and having to mask this value so that only the RCVR trigger bit gets updated. This is used to select the trigger level in the receiver FIFO at which the Received Data Available Interrupt is generated. It also determines when the dma_rx_req_n signal is asserted when DMA Mode (FCR[3]) = 1. The following trigger levels are supported: 00 = 1 character in the FIFO 01 = FIFO ¼ full 10 = FIFO ½ full 11 = FIFO 2 less than full",
                    create_bit_range("[1:0]")?,
                    svd::Access::ReadWrite,
                    &[create_enum_values(&[
                        create_enum_value("near_empty", "Trigger when the FIFO has 1 character left", 0b00)?,
                        create_enum_value("quarter", "Trigger when the FIFO is 1/4 full", 0b01)?,
                        create_enum_value("half", "Trigger when the FIFO is 1/2 full", 0b10)?,
                        create_enum_value("near_full", "Trigger when the FIFO is 2 bytes less than full", 0b11)?,
                    ])?],
                    None,
                )?,
            ]),
            None,
        )?
    ))
}
