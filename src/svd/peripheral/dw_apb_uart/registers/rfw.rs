use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the Receive FIFO Write.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "rfw",
            "Receive FIFO Write",
            0x78,
            create_register_properties(32, 0)?,
            Some(&[
                create_field(
                    "rffe",
                    "Receive FIFO Framing Error. These bits are only valid when FIFO access mode is enabled (FAR[0] is set to one). When FIFOs are implemented and enabled, this bit is used to write framing error detection information to the receive FIFO. When FIFOs are not implemented or not enabled, this bit is used to write framing error detection information to the RBR.",
                    create_bit_range("[9:9]")?,
                    svd::Access::WriteOnly,
                    None,
                )?,
                create_field(
                    "rfpe",
                    "Receive FIFO Parity Error. These bits are only valid when FIFO access mode is enabled (FAR[0] is set to one). When FIFOs are implemented and enabled, this bit is used to write parity error detection information to the receive FIFO. When FIFOs are not implemented or not enabled, this bit is used to write parity error detection information to the RBR.",
                    create_bit_range("[8:8]")?,
                    svd::Access::WriteOnly,
                    None,
                )?,
                create_field_constraint(
                    "rfwd",
                    "Receive FIFO Write Data. These bits are only valid when FIFO access mode is enabled (FAR[0] is set to one). When FIFOs are implemented and enabled, the data that is written to the RFWD is pushed into the receive FIFO. Each consecutive write pushes the new data to the next write location in the receive FIFO. When FIFOs are not implemented or not enabled, the data that is written to the RFWD is pushed into the RBR.",
                    create_bit_range("[7:0]")?,
                    svd::Access::WriteOnly,
                    create_write_constraint(0, 0xff)?,
                    None,
                )?,
            ]),
            None,
        )?
    ))
}
