use crate::svd::create_peripheral;
use crate::Result;

pub mod registers;

/// Represents a StarFive JH7110 AON Pinctrl (compatible) peripheral
pub struct Jh7110AonPinctrl {
    peripheral: svd::Peripheral,
}

impl Jh7110AonPinctrl {
    /// Creates a new [Jh7110AonPinctrl] peripheral.
    pub fn create(
        name: &str,
        base_address: u64,
        size: u32,
        interrupt: Option<Vec<svd::Interrupt>>,
    ) -> Result<Self> {
        let peripheral = create_peripheral(
            name,
            format!("StarFive JH7110 AON Pinctrl: {name}").as_str(),
            base_address,
            size,
            interrupt,
            Some(registers::create()?),
            None,
            Some("PINCTRL".into()),
        )?;

        Ok(Self { peripheral })
    }

    /// Gets a reference to the SVD [`Peripheral`](svd::Peripheral).
    pub const fn peripheral(&self) -> &svd::Peripheral {
        &self.peripheral
    }

    /// Gets a mutable reference to the SVD [`Peripheral`](svd::Peripheral).
    pub fn peripheral_mut(&mut self) -> &mut svd::Peripheral {
        &mut self.peripheral
    }

    /// Converts the [Jh7110AonPinctrl] into the inner SVD [`Peripheral`](svd::Peripheral).
    pub fn to_inner(self) -> svd::Peripheral {
        self.peripheral
    }
}
