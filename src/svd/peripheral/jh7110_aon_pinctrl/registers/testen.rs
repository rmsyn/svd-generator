use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the StarFive JH7110 AON Pinctrl Enable Test Power-on-Start (POS) Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "testen",
        "AON IOMUX CFG SAIF SYSCFG 48 - Enable test Power-on-Start (POS)",
        0x30,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "pos",
                "Power-on-Start (POS) enabler - 0: Active pull-down capability disabled, 1: Enable active pull down for loss of core power.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disabled", "Power-on-Start active pull-down is disabled", 0)?,
                    create_enum_value("enabled", "Power-on-Start active pull-down is enabled", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
