use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the StarFive JH7110 AON Pinctrl OSC Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "osc",
        "AON IOMUX CFG SAIF SYSCFG 76 - OSC",
        0x54,
        create_register_properties(32, 0x2)?,
        Some(&[
            create_field_enum(
                "ds",
                "Output Drive Strength (DS): * 00: The rated drive strength is 2 mA. * 01: The rated drive strength is 4 mA. * 10: The rated drive strength is 8 mA. * 11: The rated drive strength is 12 mA.",
                create_bit_range("[1:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("ma2", "The rated drive strength is 2 mA", 0b00)?,
                    create_enum_value("ma4", "The rated drive strength is 4 mA", 0b01)?,
                    create_enum_value("ma8", "The rated drive strength is 8 mA", 0b10)?,
                    create_enum_value("ma12", "The rated drive strength is 12 mA", 0b11)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
