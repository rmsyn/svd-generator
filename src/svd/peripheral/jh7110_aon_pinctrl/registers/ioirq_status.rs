use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the StarFive JH7110 AON Pinctrl IO IRQ Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ioirq_status",
        "Always-on GPIO IO IRQ configuration.",
        0x24,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "ioirq",
                "ris - Status of the edge trigger, can be cleared by writing gpioic.\nmis - The masked GPIO IRQ status.\nin_sync2 - Status of gpio_in after synchronization.",
                create_bit_range("[3:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        Some(svd::DimElement::builder()
            .dim(3)
            .dim_increment(0x4)
            .dim_index(Some([
                    String::from("_ris"),
                    String::from("_mis"),
                    String::from("_in_sync2"),
            ].into()))
            .build(svd::ValidateLevel::Strict)?),
    )?))
}
