use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the StarFive JH7110 AON Pinctrl GMAC0 Configuration registers.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "gmac0",
        "AON IOMUX CFG SAIF SYSCFG - GMAC0",
        0x58,
        create_register_properties(32, 0x2)?,
        Some(&[
            create_field_enum(
                "cfg",
                "rxd0 - 0: GMAC0 IO voltage select 3.3V, 1: GMAC0 IO voltage select 2.5V, 2: GMAC0 IO voltage select 1.8V\nrxc_func_sel - Function selector of GMAC0_RXC: * Function 0: u0_aon_crg_clk_gmac0_rgmii_rx, * Function 1: u0_aon_crg_clk_gmac0_rmii_ref, * Function 2: None, * Function 3: None",
                create_bit_range("[1:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("v3_3", "GMAC IO voltage select 3.3V", 0x0)?,
                    create_enum_value("v2_5", "GMAC IO voltage select 2.5V", 0x1)?,
                    create_enum_value("v1_8", "GMAC IO voltage select 1.8V", 0x2)?,
                ])?],
                None,
            )?,
        ]),
        Some(svd::DimElement::builder()
            .dim(15)
            .dim_increment(0x4)
            .dim_index(Some([
                String::from("_mdc"),
                String::from("_mdio"),
                String::from("_rxd0"),
                String::from("_rxd1"),
                String::from("_rxd2"),
                String::from("_rxd3"),
                String::from("_rxdv"),
                String::from("_rxc"),
                String::from("_txd0"),
                String::from("_txd1"),
                String::from("_txd2"),
                String::from("_txd3"),
                String::from("_txen"),
                String::from("_txc"),
                String::from("_rxc_func_sel"),
            ].into()))
            .build(svd::ValidateLevel::Strict)?),
    )?))
}
