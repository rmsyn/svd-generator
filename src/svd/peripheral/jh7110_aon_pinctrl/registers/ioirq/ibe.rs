use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the StarFive JH7110 AON Pinctrl IO IRQ IBE Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ibe",
        "Always-on GPIO IO IRQ configuration: IBE.",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "ibe_field",
            "0: Trigger on a single edge, 1: Trigger on both edges",
            create_bit_range("[3:0]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("single_edge", "IO IRQ trigger on a single edge", 0)?,
                create_enum_value("both_edges", "IO IRQ trigger on both edges", 1)?,
            ])?],
            None,
        )?]),
        None,
    )?))
}
