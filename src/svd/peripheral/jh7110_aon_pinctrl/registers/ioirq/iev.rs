use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the StarFive JH7110 AON Pinctrl IO IRQ IEV Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "iev",
        "Always-on GPIO IO IRQ configuration: IEV.",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "iev_field",
            "0: Negative/High, 1: Positive/Low",
            create_bit_range("[3:0]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("negative_high", "IO IRQ negative/high", 0)?,
                create_enum_value("positive_low", "IO IRQ positive/low", 1)?,
            ])?],
            None,
        )?]),
        None,
    )?))
}
