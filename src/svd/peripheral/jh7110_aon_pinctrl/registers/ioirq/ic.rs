use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the StarFive JH7110 AON Pinctrl IO IRQ IC Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ic",
        "Always-on GPIO IO IRQ configuration: IC.",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "ic_field",
            "0: Clear the register, 1: Do not clear the register",
            create_bit_range("[3:0]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("clear", "IO IRQ clear the register", 0)?,
                create_enum_value("no_clear", "IO IRQ do not clear the register", 1)?,
            ])?],
            None,
        )?]),
        None,
    )?))
}
