use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the StarFive JH7110 AON Pinctrl IO IRQ IE Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ie",
        "Always-on GPIO IO IRQ configuration: IE.",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "ie_field",
            "0: Mask, 1: Unmask",
            create_bit_range("[3:0]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("mask", "IO IRQ mask", 0)?,
                create_enum_value("unmask", "IO IRQ unmask", 1)?,
            ])?],
            None,
        )?]),
        None,
    )?))
}
