use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the StarFive JH7110 AON Pinctrl IO IRQ IS Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "is",
        "Always-on GPIO IO IRQ configuration: IS.",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "is_field",
            "0: Level trigger, 1: Edge trigger",
            create_bit_range("[3:0]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("level_trigger", "IO IRQ level trigger", 0)?,
                create_enum_value("edge_trigger", "IO IRQ edge trigger", 1)?,
            ])?],
            None,
        )?]),
        None,
    )?))
}
