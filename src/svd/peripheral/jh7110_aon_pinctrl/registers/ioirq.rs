use crate::svd::create_cluster;
use crate::Result;

pub mod ibe;
pub mod ic;
pub mod ie;
pub mod iev;
pub mod is;

/// Creates the StarFive JH7110 AON Pinctrl IO IRQ Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "ioirq",
        "Always-on GPIO IO IRQ configuration",
        0x10,
        &[
            is::create()?,
            ic::create()?,
            ibe::create()?,
            iev::create()?,
            ie::create()?,
        ],
        None,
    )?))
}
