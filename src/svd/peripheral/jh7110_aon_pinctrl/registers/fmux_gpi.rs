use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the StarFive JH7110 AON Pinctrl FMUX2 (GPI PMU Wakeup) Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "fmux_gpi",
        "The register can be used to configure the selected GPIO connector number for input signals.",
        0x8,
        create_register_properties(32, 0x05040302)?,
        Some(&[
            create_field_constraint(
                "gpi_pmu_wakeup0",
                "The register value indicates the selected GPIO number + 2 (GPIO2-GPIO63, GPIO0 and GPIO1 are not available) for the input signal.",
                create_bit_range("[2:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "gpi_pmu_wakeup1",
                "The register value indicates the selected GPIO number + 2 (GPIO2-GPIO63, GPIO0 and GPIO1 are not available) for the input signal.",
                create_bit_range("[10:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "gpi_pmu_wakeup2",
                "The register value indicates the selected GPIO number + 2 (GPIO2-GPIO63, GPIO0 and GPIO1 are not available) for the input signal.",
                create_bit_range("[18:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "gpi_pmu_wakeup3",
                "The register value indicates the selected GPIO number + 2 (GPIO2-GPIO63, GPIO0 and GPIO1 are not available) for the input signal.",
                create_bit_range("[26:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
        ]),
        None,
    )?))
}
