use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the StarFive JH7110 AON Pinctrl RGPIO Configuration registers.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rgpio",
        "AON IOMUX CFG SAIF SYSCFG - RGPIO",
        0x34,
        create_register_properties(32, 0x1)?,
        Some(&[
            create_field_enum(
                "ie",
                "Input Enable (IE) Controller - 1: Enable the receiver, 0: Disable the receiver",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable the receiver", 0)?,
                    create_enum_value("disable", "Disable the receiver", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "ds",
                "Output Drive Strength (DS) - 00: The rated drive strength is 2 mA, 01: The rated drive strength is 4 mA, 10: The rated drive strength is 8 mA, 11: The rated drive strength is 12 mA",
                create_bit_range("[2:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("ma2", "The rated drive strength is 2 mA", 0b00)?,
                    create_enum_value("ma4", "The rated drive strength is 4 mA", 0b01)?,
                    create_enum_value("ma8", "The rated drive strength is 8 mA", 0b10)?,
                    create_enum_value("ma12", "The rated drive strength is 12 mA", 0b11)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "pu",
                "Pull-Up (PU) settings - 1: Yes, 0: No",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("clear", "Pull-up setting is clear", 0)?,
                    create_enum_value("set", "Pull-up setting is set", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "pd",
                "Pull-Down (PD) settings - 1: Yes, 0: No",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("clear", "Pull-down setting is clear", 0)?,
                    create_enum_value("set", "Pull-down setting is set", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "slew",
                "Slew Rate Control - 0: Slow (Half frequency), 1: Fast",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("slow", "Slew rate control is slow (half frequency)", 0)?,
                    create_enum_value("fast", "Slew rate control is fast", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "smt",
                "Active high Schmitt (SMT) trigger selector - 0: No hysteresis, 1: Schmitt trigger enabled",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disabled", "Active high Schmitt (SMT) trigger is configured for no hysteresis", 0)?,
                    create_enum_value("enabled", "Active high Schmitt (SMT) trigger is enabled", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "pos",
                "Power-on-Start (POS) enabler - 1: Enable active pull down for loss of core power, 0: Active pull-down capability disabled",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disabled", "Power-on-Start active pull-down is disabled", 0)?,
                    create_enum_value("enabled", "Power-on-Start active pull-down is enabled", 1)?,
                ])?],
                None,
            )?,
        ]),
        Some(svd::DimElement::builder()
            .dim(4)
            .dim_increment(0x4)
            .build(svd::ValidateLevel::Strict)?),
    )?))
}
