use crate::Result;

pub mod isp_syscfg0;
pub mod isp_syscfg1;
pub mod isp_syscfg10;
pub mod isp_syscfg2;
pub mod isp_syscfg3;
pub mod isp_syscfg4;
pub mod isp_syscfg5;
pub mod isp_syscfg6;
pub mod isp_syscfg7;
pub mod isp_syscfg8;
pub mod isp_syscfg9;

/// Creates StarFive JH7110 ISP SYSCON compatible register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        isp_syscfg0::create()?,
        isp_syscfg1::create()?,
        isp_syscfg2::create()?,
        isp_syscfg3::create()?,
        isp_syscfg4::create()?,
        isp_syscfg5::create()?,
        isp_syscfg6::create()?,
        isp_syscfg7::create()?,
        isp_syscfg8::create()?,
        isp_syscfg9::create()?,
        isp_syscfg10::create()?,
    ]
    .into())
}
