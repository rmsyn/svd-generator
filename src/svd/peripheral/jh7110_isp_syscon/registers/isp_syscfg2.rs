use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 ISP SYSCON SYSCFG 2 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "isp_syscfg2",
        "ISP SYSCFG 2: isp_sysconsaif_syscfg_8",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "u0_vin_cfg_axird_intr_clean",
                "Use this bit to clean the AXI output interrupt. Write 1 then write 0 to execute the cleaning.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_vin_cfg_axird_intr_mask",
                "Use this bit to mask the AXI output interrupt.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "u0_vin_cfg_axird_line_cnt_end",
                "This bit represents the valid end pixel of the AXI input test image line.",
                create_bit_range("[13:2]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xfff)?,
                None,
            )?,
            create_field_constraint(
                "u0_vin_cfg_axird_line_cnt_start",
                "This bit represents the valid start pixel of the AXI input test image line.",
                create_bit_range("[25:14]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xfff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
