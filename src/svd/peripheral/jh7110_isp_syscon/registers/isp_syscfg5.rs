use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 ISP SYSCON SYSCFG 5 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "isp_syscfg5",
        "ISP SYSCFG 5: isp_sysconsaif_syscfg_20",
        0x14,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "u0_vin_cfg_axiwr0_channel_sel",
                "Select 1 channel output of the 8 MIPI channels",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field(
                "u0_vin_cfg_axiwr0_en",
                "Set this bit to 1 to enable the image output to AXI.",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
