use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 ISP SYSCON SYSCFG 7 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "isp_syscfg7",
        "ISP SYSCFG 7: isp_sysconsaif_syscfg_28",
        0x1c,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "u0_vin_cfg_axiwr0_intr_clean",
                "Use this bit to clean the AXI output interrupt. Write 1 then write 0 to execute the cleaning.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_vin_cfg_axiwr0_intr_mask",
                "Use this bit to mask the AXI output interrupt.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "u0_vin_cfg_axiwr0_pix_cnt_end",
                "This bit represents the valid end pixel of the AXI input test image line.",
                create_bit_range("[12:2]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7ff)?,
                None,
            )?,
            create_field_enum(
                "u0_vin_cfg_axiwr0_pix_ct",
                "00: 1 64-bit equals to 2 pixels, 01: 1 64-bit equals to 4 pixels, 10: 1 64-bit equals to 8 pixels",
                create_bit_range("[14:13]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("two", "1 64-bit equals to 2 pixels", 0b00)?,
                    create_enum_value("four", "1 64-bit equals to 4 pixels", 0b01)?,
                    create_enum_value("eight", "1 64-bit equals to 8 pixels", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u0_vin_cfg_axiwr0_pixel_high_bit_sel",
                "When you configure the above bit as '10' - 1 64-bit equals to 8 pixels, the 8 pixels will use some of the RAW data - 00: 1 64-bit pix_data[7:0], 01: 1 pix_data[9:2], 10: pix_data[11:4], 11: pix_data[13:6]",
                create_bit_range("[16:15]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("data0_7", "1 64-bit pix_data[7:0]", 0b00)?,
                    create_enum_value("data2_9", "1 64-bit pix_data[9:2]", 0b01)?,
                    create_enum_value("data4_11", "1 64-bit pix_data[11:4]", 0b10)?,
                    create_enum_value("data6_13", "1 64-bit pix_data[13:6]", 0b11)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
