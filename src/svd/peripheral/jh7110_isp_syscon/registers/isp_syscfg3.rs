use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 ISP SYSCON SYSCFG 2 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "isp_syscfg3",
        "ISP SYSCFG 3: isp_sysconsaif_syscfg_12",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "u0_vin_cfg_axird_pix_cnt_end",
                "This bit represents the valid end pixel of the AXI input test image line.",
                create_bit_range("[12:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1fff)?,
                None,
            )?,
            create_field_constraint(
                "u0_vin_cfg_axird_pix_cnt_start",
                "This bit represents the valid start pixel of the AXI input test image line.",
                create_bit_range("[25:13]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1fff)?,
                None,
            )?,
            create_field_enum(
                "u0_vin_cfg_axird_pix_ct",
                "00: 1 64-bit equals to 2 pixels, 01: 1 64-bit equals to 4 pixels, 10: 1 64-bit equals to 8 pixels",
                create_bit_range("[27:26]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("two", "1 64-bit equals to 2 pixels", 0b00)?,
                    create_enum_value("four", "1 64-bit equals to 4 pixels", 0b01)?,
                    create_enum_value("eight", "1 64-bit equals to 8 pixels", 0b10)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
