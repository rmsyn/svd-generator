use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 ISP SYSCON SYSCFG 4 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "isp_syscfg4",
        "ISP SYSCFG 4: isp_sysconsaif_syscfg_16",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "u0_vin_cfg_axird_start_addr",
            "This bit represents the valid start address of the AXI input test image's first line.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
