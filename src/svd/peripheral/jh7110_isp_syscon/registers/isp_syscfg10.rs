use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 ISP SYSCON SYSCFG 10 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "isp_syscfg10",
        "ISP SYSCFG 10: isp_sysconsaif_syscfg_40",
        0x28,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "u0_vin_test_generic_ctrl",
                "This configuration is not used by JH7110.",
                create_bit_range("[15:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field_constraint(
                "u0_vin_test_generic_status",
                "This configuration is not used by JH7110.",
                create_bit_range("[31:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
