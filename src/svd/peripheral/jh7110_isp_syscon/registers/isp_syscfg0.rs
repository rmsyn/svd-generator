use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 ISP SYSCON SYSCFG 0 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "isp_syscfg0",
        "ISP SYSCFG 0: isp_sysconsaif_syscfg_0",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "u0_vin_scfg_sram_config",
                "",
                create_bit_range("[1:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "u0_vin_cfg_axi_dvp_en",
                "0: Output to AXI is DVP, 1: Output to AXI is MIPI",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "u0_vin_cfg_axird_axi_cnt_end",
                "The valid pixel of the AXI image. 1 pixel equals 64 bit.",
                create_bit_range("[13:3]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7ff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
