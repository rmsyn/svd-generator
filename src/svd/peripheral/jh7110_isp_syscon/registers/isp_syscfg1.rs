use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 ISP SYSCON SYSCFG 1 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "isp_syscfg1",
        "ISP SYSCFG 1: isp_sysconsaif_syscfg_4",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "u0_vin_cfg_axird_end_addr",
            "the start address of the next frame",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
