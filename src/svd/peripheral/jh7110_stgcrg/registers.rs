use crate::Result;

pub mod clk_dma_ahb;
pub mod clk_dma_axi;
pub mod clk_e2_core;
pub mod clk_e2_dbg;
pub mod clk_e2_rtc;
pub mod clk_hifi4_core;
pub mod clk_pcie;
pub mod clk_pcie_u1_slv_dec_main;
pub mod clk_sec_hclk;
pub mod clk_sec_misc_ahb;
pub mod clk_stg_mtrx;
pub mod clk_stg_mtrx_group1_hifi;
pub mod clk_usb_apb;
pub mod clk_usb_app125;
pub mod clk_usb_axi;
pub mod clk_usb_ipm;
pub mod clk_usb_refclk;
pub mod clk_usb_stb;
pub mod clk_usb_utmi_apb;
pub mod rst;

/// Creates StarFive JH7110 STG Syscon (compatible) register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        clk_hifi4_core::create()?,
        clk_usb_apb::create()?,
        clk_usb_utmi_apb::create()?,
        clk_usb_axi::create()?,
        clk_usb_ipm::create()?,
        clk_usb_stb::create()?,
        clk_usb_app125::create()?,
        clk_usb_refclk::create()?,
        clk_pcie::create()?,
        clk_pcie_u1_slv_dec_main::create()?,
        clk_sec_hclk::create()?,
        clk_sec_misc_ahb::create()?,
        clk_stg_mtrx::create()?,
        clk_stg_mtrx_group1_hifi::create()?,
        clk_e2_rtc::create()?,
        clk_e2_core::create()?,
        clk_e2_dbg::create()?,
        clk_dma_axi::create()?,
        clk_dma_ahb::create()?,
        rst::create()?,
    ]
    .into())
}
