use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG PCIe U1 SLV Dec Main Clock register.
// NOTE: separated for PCIE clusters (all other registers are the same).
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "clk_pcie_u1_slv_dec_main",
        "Clock PCIe01 SLV DEC Main",
        0x38,
        Some(1),
        None,
    )
}
