use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG E2 Core Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_e2_core", "Clock E2 Core", 0x64, Some(1), None)
}
