use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG USB IPM Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "clk_usb_ipm",
        "Clock USB IPM",
        0x10,
        [2, 2, 2, 2],
        None,
        None,
    )
}
