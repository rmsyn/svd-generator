use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG USB APB Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_usb_apb", "Clock USB APB", 0x4, None, None)
}
