use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG STG Matrix Group 1 HIFI Clock register.
// NOTE: separated for STG Matrix Groups clusters (all other group registers are the same).
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "clk_stg_mtrx_group1_hifi",
        "Clock STG Matrix Group 1 HIFI",
        0x5c,
        None,
        None,
    )
}
