use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG Software RESET Address Selector Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_rst_stat(
        "rst",
        "STGCRG RESET",
        0x74,
        Some(0b111_1111_1111_1111_1111_1110),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some(
                    [String::from("_soft_addr_sel"), String::from("_stgcrg_stat")].into(),
                ))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
}
