use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG DMA AHB Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_dma_ahb", "Clock DMA AHB", 0x70, None, None)
}
