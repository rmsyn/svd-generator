use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG E2 RTC Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_divcfg(
        "clk_e2_rtc",
        "Clock E2 RTC",
        0x60,
        [24, 24, 24, 24],
        None,
        None,
    )
}
