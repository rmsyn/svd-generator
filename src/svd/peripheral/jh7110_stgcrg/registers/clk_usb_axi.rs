use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG USB AXI Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_usb_axi", "Clock USB AXI", 0xc, None, None)
}
