use crate::svd::register::{create_cluster, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 STG CRG STG Matrix Group Clock registers.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "clk_stg_mtrx",
        "Clock STG Matrix Group configuration",
        0x44,
        &[
            jh7110::create_register_icg("main", "Clock STG Matrix Group Main", 0x0, None, None)?,
            jh7110::create_register_icg("bus", "Clock STG Matrix Group Bus", 0x4, None, None)?,
            jh7110::create_register_icg("stg", "Clock STG Matrix Group STG", 0x8, None, None)?,
        ],
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0xc)
                .dim_index(Some(["_group0", "_group1"].map(String::from).into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
