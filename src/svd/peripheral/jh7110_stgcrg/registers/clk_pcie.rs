use crate::svd::register::{create_cluster, jh7110};
use crate::Result;

/// Creates a StarFive JH7110 STG CRG PCIe Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "clk_pcie",
        "Clock PCIe configuration",
        0x20,
        &[
            jh7110::create_register_icg("axi_mst0", "Clock PCIe AXI MST0", 0x0, None, None)?,
            jh7110::create_register_icg("apb", "Clock PCIe APB", 0x4, None, None)?,
            jh7110::create_register_icg("tl", "Clock PCIe TL", 0x8, None, None)?,
        ],
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0xc)
                .dim_index(Some(["_u0", "_u1"].map(String::from).into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
