use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG SEC HCLK Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_sec_hclk", "Clock SEC HCLK", 0x3c, None, None)
}
