use crate::svd::register::jh7110;
use crate::Result;

/// Creates a StarFive JH7110 STG CRG E2 Debug Clock register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_e2_dbg", "Clock E2 Debug", 0x68, None, None)
}
