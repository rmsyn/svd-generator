use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

pub fn create(size: u32) -> Result<Vec<svd::RegisterCluster>> {
    Ok([svd::RegisterCluster::Register(create_register(
        "word",
        "Cadence XSPI NOR Flash word",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "word",
            "Cadence XSPI NOR Flash word",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(size.saturating_div(4))
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?)]
    .into())
}
