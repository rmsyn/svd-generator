use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the DesignWare I2C TX Overrun register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "clr_tx_over",
        "DesignWare I2C Clear TX Overrun",
        0x4c,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "clr_tx_over",
            "",
            create_bit_range("[0:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
