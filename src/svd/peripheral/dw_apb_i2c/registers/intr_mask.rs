use crate::svd::{create_register, create_register_properties};
use crate::Result;

use super::create_intr_fields;

/// Creates the DesignWare I2C Interrupt Mask register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "intr_mask",
        "DesignWare I2C Interrupt Mask",
        0x30,
        create_register_properties(32, 0)?,
        Some(&create_intr_fields(svd::Access::ReadWrite)?),
        None,
    )?))
}
