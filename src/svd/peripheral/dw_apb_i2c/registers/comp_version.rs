use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the DesignWare I2C Compatibility Version register.
///
/// Hold MIN Version: eg. 0x3131312a ( "111*" == v1.11* )
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "comp_version",
        "DesignWare I2C Compatibility Version",
        0xf8,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "comp_version",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
