use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the DesignWare I2C RX TL register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rx_tl",
        "DesignWare I2C RX TL",
        0x38,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "rx_tl",
            "",
            create_bit_range("[7:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xff)?,
            None,
        )?]),
        None,
    )?))
}
