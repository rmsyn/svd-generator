use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the DesignWare I2C DATA CMD register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "data_cmd",
            "DesignWare I2C Data Command",
            0x10,
            create_register_properties(32, 0)?,
            Some(&[
                create_field_constraint(
                    "dat",
                    "Data Command Data Byte",
                    create_bit_range("[7:0]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0xff)?,
                    None,
                )?,
                create_field(
                    "read",
                    "Data Command READ Bit - 0: Write, 1: Read",
                    create_bit_range("[8:8]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field(
                    "stop",
                    "Data Command STOP Bit - 0: Non-terminal DATA command byte, 1: Terminal DATA command byte",
                    create_bit_range("[9:9]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field(
                    "restart",
                    "Data Command RESTART Bit - 0: Do not restart the transfer, 1: Restart the transfer",
                    create_bit_range("[10:10]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
                create_field(
                    "first_data_byte",
                    "Data Command First Data Byte - 0: False, 1: True",
                    create_bit_range("[11:11]")?,
                    svd::Access::ReadWrite,
                    None,
                )?,
            ]),
            None,
       )?
   ))
}
