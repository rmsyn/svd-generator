use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the DesignWare I2C SS SCL LCNT register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ss_scl_lcnt",
        "DesignWare I2C SS SCL LCNT",
        0x18,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "ss_scl_lcnt",
            "",
            create_bit_range("[15:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff)?,
            None,
        )?]),
        None,
    )?))
}
