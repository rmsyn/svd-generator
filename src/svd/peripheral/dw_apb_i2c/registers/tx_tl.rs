use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the DesignWare I2C TX TL register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "tx_tl",
        "DesignWare I2C TX TL",
        0x3c,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "tx_tl",
            "",
            create_bit_range("[7:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xff)?,
            None,
        )?]),
        None,
    )?))
}
