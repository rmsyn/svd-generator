use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the DesignWare I2C Clear General Call register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "clr_gen_call",
        "DesignWare I2C Clear General Call",
        0x68,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "clr_gen_call",
            "",
            create_bit_range("[0:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
