use crate::svd::{create_register, create_register_properties};
use crate::Result;

use super::create_intr_fields;

/// Creates the DesignWare I2C Raw Interrupt Status register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "raw_intr_stat",
        "DesignWare I2C Raw Interrupt Status",
        0x34,
        create_register_properties(32, 0)?,
        Some(&create_intr_fields(svd::Access::ReadOnly)?),
        None,
    )?))
}
