use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the DesignWare I2C Clear TX Abort register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "clr_tx_abrt",
        "DesignWare I2C Clear TX Abort",
        0x54,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "clr_tx_abrt",
            "",
            create_bit_range("[0:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
