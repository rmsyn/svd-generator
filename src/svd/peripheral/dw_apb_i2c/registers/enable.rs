use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the DesignWare I2C Enable register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "enable",
        "DesignWare I2C Enable",
        0x6c,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "abort",
            "",
            create_bit_range("[1:1]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
