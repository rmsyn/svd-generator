use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the DesignWare I2C Compatibility Parameter 1 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "comp_param_1",
        "DesignWare I2C Compatibility Parameter 1",
        0xf4,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "speed",
            "Speed mask - 01: Standard, 10: Full, 11: High",
            create_bit_range("[3:2]")?,
            svd::Access::ReadOnly,
            &[create_enum_values(&[
                create_enum_value("standard", "Standard speed", 0b01)?,
                create_enum_value("full", "Full speed", 0b10)?,
                create_enum_value("high", "High speed", 0b11)?,
            ])?],
            None,
        )?]),
        None,
    )?))
}
