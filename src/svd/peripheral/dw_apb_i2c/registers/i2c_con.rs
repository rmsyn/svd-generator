use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the DesignWare I2C CON register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "i2c_con",
        "DesignWare I2C CON",
        0x00,
        create_register_properties(32, 0x2)?,
        Some(&[
            create_field_enum(
                "master",
                "I2C Master Connection - 0: Slave, 1: Master",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("slave", "I2C slave connection", 0b0)?,
                    create_enum_value("master", "I2C master connection", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "speed",
                "I2C Speed - 01: Standard, 10: Fast, 11: High",
                create_bit_range("[2:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("standard", "Standard speed", 0b01)?,
                    create_enum_value("fast", "Fast speed", 0b10)?,
                    create_enum_value("high", "High speed", 0b11)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "slave_10bitaddr",
                "I2C Slave 10-bit Address - 0: False, 1: True",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("clear", "Does not use 10-bit addresses", 0b0)?,
                    create_enum_value("set", "Uses 10-bit addresses", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "master_10bitaddr",
                "I2C Master 10-bit Address - 0: False, 1: True",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("clear", "Does not use 10-bit addresses", 0b0)?,
                    create_enum_value("set", "Uses 10-bit addresses", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "restart_en",
                "I2C Restart Enable - 0: False, 1: True",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("clear", "Do not enable restart", 0b0)?,
                    create_enum_value("set", "Enable restart", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "slave_disable",
                "I2C Slave Disable - 0: False, 1: True",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("clear", "Do not disable", 0b0)?,
                    create_enum_value("set", "Disable", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "stop_det_ifaddressed",
                "I2C Stop DET If Addressed - 0: False, 1: True",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("clear", "Do not stop DET if addressed", 0b0)?,
                    create_enum_value("set", "Stop DET if addressed", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "tx_empty_ctrl",
                "I2C TX Empty Control - 0: False, 1: True",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("clear", "Do not empty TX", 0b0)?,
                    create_enum_value("set", "Empty TX", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "rx_fifo_full_hld_ctrl",
                "I2C RX FIFO Full Hold Control - 0: False, 1: True",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("clear", "RX FIFO does not use full HID control", 0b0)?,
                    create_enum_value("set", "RX FIFO uses full HID control", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "bus_clear_ctrl",
                "I2C Bus Clear Control - 0: False, 1: True",
                create_bit_range("[11:11]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("clear", "Do not clear the bus", 0b0)?,
                    create_enum_value("set", "Clear the bus", 0b1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
