use crate::svd::{create_register, create_register_properties};
use crate::Result;

use super::create_intr_fields;

/// Creates the DesignWare I2C Interrupt Status register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "intr_stat",
        "DesignWare I2C Interrupt Status",
        0x2c,
        create_register_properties(32, 0)?,
        Some(&create_intr_fields(svd::Access::ReadOnly)?),
        None,
    )?))
}
