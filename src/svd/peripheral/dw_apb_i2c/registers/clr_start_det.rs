use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the DesignWare I2C Clear Start DET register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "clr_start_det",
        "DesignWare I2C Clear Start DET",
        0x64,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "clr_start_det",
            "",
            create_bit_range("[0:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
