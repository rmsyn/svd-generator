use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the DesignWare I2C Clear Activity register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "clr_activity",
        "DesignWare I2C Clear Activity",
        0x5c,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "clr_activity",
            "",
            create_bit_range("[0:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
