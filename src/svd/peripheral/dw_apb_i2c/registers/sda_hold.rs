use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the DesignWare I2C SDA Hold register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "sda_hold",
        "DesignWare I2C SDA Hold",
        0x7c,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "sda_hold",
            "",
            create_bit_range("[23:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
