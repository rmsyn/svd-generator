use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the DesignWare I2C Compatibility Type register.
///
/// I2C Type: 0x44570140 ( "DW" + 0x0140 )
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "comp_type",
        "DesignWare I2C Compatibility Type",
        0xfc,
        create_register_properties(32, 0x44570140)?,
        Some(&[create_field(
            "comp_type",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
