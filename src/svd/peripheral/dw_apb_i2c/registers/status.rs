use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the DesignWare I2C Status register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "status",
        "DesignWare I2C Status",
        0x70,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "activity",
                "",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "tfe",
                "",
                create_bit_range("[2:2]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "rfne",
                "",
                create_bit_range("[3:3]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "master_activity",
                "",
                create_bit_range("[5:5]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "slave_activity",
                "",
                create_bit_range("[6:6]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
