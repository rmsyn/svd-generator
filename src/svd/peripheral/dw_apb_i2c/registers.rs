use crate::svd::{create_bit_range, create_field};
use crate::Result;

pub mod clr_activity;
pub mod clr_gen_call;
pub mod clr_intr;
pub mod clr_rd_req;
pub mod clr_restart_det;
pub mod clr_rx_done;
pub mod clr_rx_over;
pub mod clr_rx_under;
pub mod clr_start_det;
pub mod clr_stop_det;
pub mod clr_tx_abrt;
pub mod clr_tx_over;
pub mod comp_param_1;
pub mod comp_type;
pub mod comp_version;
pub mod data_cmd;
pub mod enable;
pub mod enable_status;
pub mod fs_scl_hcnt;
pub mod fs_scl_lcnt;
pub mod hs_scl_hcnt;
pub mod hs_scl_lcnt;
pub mod i2c_con;
pub mod intr_mask;
pub mod intr_stat;
pub mod raw_intr_stat;
pub mod rx_tl;
pub mod rxflr;
pub mod sar;
pub mod sda_hold;
pub mod ss_scl_hcnt;
pub mod ss_scl_lcnt;
pub mod status;
pub mod tar;
pub mod tx_abrt_source;
pub mod tx_tl;
pub mod txflr;

/// Creates Synopsys DesignWare APB I2C register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        i2c_con::create()?,
        tar::create()?,
        sar::create()?,
        data_cmd::create()?,
        ss_scl_hcnt::create()?,
        ss_scl_lcnt::create()?,
        fs_scl_hcnt::create()?,
        fs_scl_lcnt::create()?,
        hs_scl_hcnt::create()?,
        hs_scl_lcnt::create()?,
        intr_stat::create()?,
        intr_mask::create()?,
        raw_intr_stat::create()?,
        rx_tl::create()?,
        tx_tl::create()?,
        clr_intr::create()?,
        clr_rx_under::create()?,
        clr_rx_over::create()?,
        clr_tx_over::create()?,
        clr_rd_req::create()?,
        clr_tx_abrt::create()?,
        clr_rx_done::create()?,
        clr_activity::create()?,
        clr_stop_det::create()?,
        clr_start_det::create()?,
        clr_gen_call::create()?,
        enable::create()?,
        status::create()?,
        txflr::create()?,
        rxflr::create()?,
        sda_hold::create()?,
        tx_abrt_source::create()?,
        enable_status::create()?,
        clr_restart_det::create()?,
        comp_param_1::create()?,
        comp_version::create()?,
        comp_type::create()?,
    ]
    .into())
}

pub(crate) fn create_intr_fields(access: svd::Access) -> Result<[svd::Field; 14]> {
    Ok([
        create_field(
            "rx_under",
            "RX FIFO Underrun",
            create_bit_range("[0:0]")?,
            access,
            None,
        )?,
        create_field(
            "rx_over",
            "RX FIFO Overrun",
            create_bit_range("[1:1]")?,
            access,
            None,
        )?,
        create_field(
            "rx_full",
            "RX FIFO Full",
            create_bit_range("[2:2]")?,
            access,
            None,
        )?,
        create_field(
            "tx_over",
            "TX FIFO Overrun",
            create_bit_range("[3:3]")?,
            access,
            None,
        )?,
        create_field(
            "tx_empty",
            "TX FIFO Empty",
            create_bit_range("[4:4]")?,
            access,
            None,
        )?,
        create_field(
            "rd_req",
            "Read Request",
            create_bit_range("[5:5]")?,
            access,
            None,
        )?,
        create_field(
            "tx_abrt",
            "TX Abort",
            create_bit_range("[6:6]")?,
            access,
            None,
        )?,
        create_field(
            "rx_done",
            "RX Done",
            create_bit_range("[7:7]")?,
            access,
            None,
        )?,
        create_field(
            "activity",
            "Activity",
            create_bit_range("[8:8]")?,
            access,
            None,
        )?,
        create_field(
            "stop_det",
            "Stop DET",
            create_bit_range("[9:9]")?,
            access,
            None,
        )?,
        create_field(
            "start_det",
            "Start DET",
            create_bit_range("[10:10]")?,
            access,
            None,
        )?,
        create_field(
            "gen_call",
            "General Call",
            create_bit_range("[11:11]")?,
            access,
            None,
        )?,
        create_field(
            "restart_det",
            "Restart DET",
            create_bit_range("[12:12]")?,
            access,
            None,
        )?,
        create_field(
            "mst_on_hold",
            "Master on Hold",
            create_bit_range("[13:13]")?,
            access,
            None,
        )?,
    ])
}
