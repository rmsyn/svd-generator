use crate::Result;

pub mod cntr;
pub mod ctrl;
pub mod hrc;
pub mod lrc;

/// Creates Opencores PTC PWM v1 register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        cntr::create()?,
        hrc::create()?,
        lrc::create()?,
        ctrl::create()?,
    ]
    .into())
}
