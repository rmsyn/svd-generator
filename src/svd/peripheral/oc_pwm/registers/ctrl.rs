use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the Opencores PTC PWM v1 RPTC_CTRL register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ctrl",
        "Opencores PTC PWM v1 RPTC_CTRL register control operation of PTC core.", 
        0xc,
        create_register_properties(9, 0)?,
        Some(&[
            create_field_enum(
                "en",
                "When set, RPTC_CNTR can be incremented.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("no_increment", "RPTC_CNTR cannot be incremented.", 0)?,
                    create_enum_value("increment", "RPTC_CNTR can be incremented.", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "eclk",
                "When set, ptc_ecgt signal is used to increment RPTC_CNTR. When cleared, system clock is used instead.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("sys_clock", "System clock is used to increment the RPTC_CNTR.", 0)?,
                    create_enum_value("ptc_ecgt", "`ptc_ecgt` signal is used to increment the RPTC_CNTR.", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "nec",
                "When set, ptc_ecgt increments on negative edge and gates on low period. When cleared, ptc_ecgt increments on positive edge and gates on high period. This bit has effect only on `gating` function of ptc_ecgt when RPTC_CTRL[ECLK] bit is cleared.",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("pos_edge", "`ptc_ecgt` increments on positive edge, and gates on high period.", 0)?,
                    create_enum_value("neg_edge", "`ptc_ecgt` increments on negative edge, and gates on low period.", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "oe",
                "Inverted value of this bit is reflected on the ptc_oen signal. It is used to enable PWM output driver.",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("ptc_oen_high", "`ptc_oen` signal is set high.", 0)?,
                    create_enum_value("ptc_oen_low", "`ptc_oen` signal is set low.", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "single",
                "When set, RPTC_CNTR is not incremented anymore after it reaches value equal to the RPTC_LRC value. When cleared, RPTC_CNTR is restarted after it reaches value in the RPTC_LCR register.",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("reset", "RPTC_CNTR is restarted after it reaches value in RPTC_LCR register.", 0)?,
                    create_enum_value("no_reset", "RPTC_CNTR is not incremented after it reaches value in RPTC_LCR register.", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "inte",
                "When set, PTC asserts an interrupt when RPTC_CNTR value is equal to the value of RPTC_LRC or RPTC_HRC. When cleared, interrupts are masked.",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("mask", "PTC interrupts are masked.", 0)?,
                    create_enum_value("interrupt", "PTC asserts an interrupt when RPTC_CNTR value is equal to the value of RPTC_LRC or RPTC_HRC.", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "int",
                "When read, this bit represents pending interrupt. When it is set, an interrupt is pending. When this bit is written with `1`, interrupt request is cleared.",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("no_interrupt", "No interrupt is pending on read, no effect on write.", 0)?,
                    create_enum_value("pending_clear", "Pending interrupt on read, clears interrupt request on write.", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "cntrrst",
                "When set, RPTC_CNTR is under reset. When cleared, normal operation of the counter is allowed.",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("normal", "Normal operation of the counter is allowed.", 0)?,
                    create_enum_value("reset", "RPTC_CNTR is under reset.", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "capte",
                "When set, ptc_capt signal can be used to capture RPTC_CNTR into RPTC_LRC or RPTC_HRC registers. Into which reference/capture register capture occurs depends on edge of the ptc_capt signal. When cleared, capture function is masked.",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("masked", "Capture function is masked.", 0)?,
                    create_enum_value("capture", "`ptc_capt` signal can be used to capture RPTC_CNTR into RPTC_LRC or RPTC_HRC registers.", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
