use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the Opencores PTC PWM v1 RPTC_CNTR register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "cntr",
        "Opencores PTC PWM v1 CNTR is the actual counter register. It is incremented at every counter/timer clock cycle. Source clock is either system clock or ptc_ecgt eclk/gate input. Selection between both clocks is performed with the RPTC_CTRL[ECLK]. Active edge of external clock is selected with the RPTC_CTRL[NEC]. In order to count, RPTC_CNTR must first be enabled with the RPTC_CTRL[EN]. RPTC_CNTR can be reset with the RPTC_CTRL[RST]. RPTC_CNTR can operate in either single-run mode or continues mode. Mode is selected with the RPTC_CTRL[SINGLE].",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
                "cntr",
                "",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff_ffff)?,
                None,
        )?]),
        None,
    ).map(svd::RegisterCluster::Register)
}
