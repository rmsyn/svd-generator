use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the Opencores PTC PWM v1 RPTC_LRC register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "lrc",
        "Opencores PTC PWM v1 RPTC_LRC register is a 1st out of two reference/capture registers. It has two functions: - In reference mode it is used to assert low PWM output or to generate an interrupt - In capture mode it captures RPTC_CNTR value on low value of ptc_capt signal. The RPTC_LRC should have higher value than RPTC_HRC. This is because PWM output goes first high and later low.", 
        0x8,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
                "lrc",
                "",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff_ffff)?,
                None,
        )?]),
        None,
    ).map(svd::RegisterCluster::Register)
}
