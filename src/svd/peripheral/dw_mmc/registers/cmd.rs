use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare MMC Command register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cmd",
        "MMC command",
        0x2c,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "indx",
                "MMC command index",
                create_bit_range("[4:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
            create_field(
                "resp",
                "MMC command response",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                Some(
                    svd::DimElement::builder()
                        .dim(3)
                        .dim_increment(1)
                        .dim_index(Some(
                            [
                                String::from("_exp"),
                                String::from("_long"),
                                String::from("_crc"),
                            ]
                            .into(),
                        ))
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field(
                "dat",
                "MMC command data",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                Some(
                    svd::DimElement::builder()
                        .dim(2)
                        .dim_increment(1)
                        .dim_index(Some([String::from("_exp"), String::from("_wr")].into()))
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field(
                "strm_mode",
                "MMC command stream mode",
                create_bit_range("[11:11]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "send_stop",
                "MMC command send stop",
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "prv_data_wait",
                "MMC command private data wait",
                create_bit_range("[13:13]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "stop",
                "MMC command stop",
                create_bit_range("[14:14]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "init",
                "MMC command init",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "upd_clk",
                "MMC command update clock",
                create_bit_range("[21:21]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ceata_rd",
                "MMC command CEATA read",
                create_bit_range("[22:22]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ccs_exp",
                "MMC command CCS EXP",
                create_bit_range("[23:23]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "volt_switch",
                "MMC command volt switch",
                create_bit_range("[28:28]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "use_hold_reg",
                "MMC command use hold register",
                create_bit_range("[29:29]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "start",
                "MMC command start",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
