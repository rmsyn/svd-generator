use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare MMC UHS-1 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "uhs_reg",
        "MMC UHS-1 regulator",
        0x74,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "voltage",
                "MMC slot signal voltage",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("v33", "MMC slot 3.3v signal voltage", 0b0)?,
                    create_enum_value("v18", "MMC slot 1.8v signal voltage", 0b1)?,
                ])?],
                Some(
                    svd::DimElement::builder()
                        .dim(32)
                        .dim_increment(1)
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            // NOTE: The Linux driver defines SDMMC_UHS_DDR field, but it is unused (probably vestigial).
            // It also conflicts with slot_voltage16, so just leave it out.
        ]),
        None,
    )?))
}
