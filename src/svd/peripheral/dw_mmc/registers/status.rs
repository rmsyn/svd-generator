use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare MMC Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "status",
        "MMC status",
        0x48,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "busy",
                "MMC busy",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "fcnt",
                "MMC FCNT",
                create_bit_range("[29:17]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1fff)?,
                None,
            )?,
            create_field(
                "dma_req",
                "MMC DMA request",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
