use super::int::create_int_fields;
use crate::svd::{create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare MMC RINT Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rintsts",
        "MMC RINT status",
        0x44,
        create_register_properties(32, 0)?,
        Some(&[create_int_fields(
            "MMC RINT status",
            svd::Access::ReadWrite,
        )?]),
        None,
    )?))
}
