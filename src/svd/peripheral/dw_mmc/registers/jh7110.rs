use super::*;
use crate::svd::peripheral::dw_mmc::{DwMmcFifoDepth, DwMmcVersion};
use crate::svd::{create_bit_range, create_field_constraint, create_write_constraint};
use crate::Result;

/// Creates Synopsys DesignWare MMC JH7110-model register definitions.
///
/// Based on the register definitions from the Linux driver:
///
/// - <https://elixir.bootlin.com/linux/latest/source/drivers/mmc/host/dw_mmc.h>
/// - <https://elixir.bootlin.com/linux/latest/source/drivers/mmc/host/dw_mmc-starfive.c>
///
/// Original Author(s):
///
/// - Jaehoon Chung <jh80.chung@samsung.com>
/// - Will Newton <will.newton@imgtec.com>
/// - William Qiu <william.qiu@starfivetech.com>
pub fn create(fifo_depth: DwMmcFifoDepth) -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        ctrl::create()?,
        pwren::create()?,
        clk_ctrl::create()?,
        tmout::create()?,
        ctype::create()?,
        blksiz::create()?,
        bytcnt::create()?,
        intmask::create()?,
        cmdarg::create()?,
        cmd::create()?,
        resp::create()?,
        mintsts::create()?,
        rintsts::create()?,
        status::create()?,
        fifoth::create()?,
        cdetect::create()?,
        wrtprt::create()?,
        gpio::create()?,
        cnt::create()?,
        debnce::create()?,
        id::create()?,
        hcon::create()?,
        uhs_reg::create()?,
        rst_n::create()?,
        bmod::create()?,
        pldmnd::create()?,
        idmac_addr::create()?,
        cdthrctl::create(DwMmcVersion::Version280a)?,
        uhs_reg_ext::create(DwMmcVersion::Version280a, &create_uhs_reg_ext_fields()?)?,
        data::create(DwMmcVersion::Version280a, fifo_depth, 0x1000)?,
    ]
    .into())
}

/// Creates Synopsys DesignWare MMC UHS Regulator Extended field definitions.
pub fn create_uhs_reg_ext_fields() -> Result<[svd::Field; 1]> {
    create_field_constraint(
        "smpl_phase",
        "MMC drive and sample phase",
        create_bit_range("[20:16]")?,
        svd::Access::ReadWrite,
        create_write_constraint(0, 0x1f)?,
        None,
    )
    .map(|f| [f])
}
