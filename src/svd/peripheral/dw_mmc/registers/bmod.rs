use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare MMC Internal DMAC Bus Mode register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "bmod",
        "MMC DMAC bus mode",
        0x80,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "swreset",
                "MMC internal DMAC software reset",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "fb",
                "MMC internal DMAC FB",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "enable",
                "MMC internal DMAC enable",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
