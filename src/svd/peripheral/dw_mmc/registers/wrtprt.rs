use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare MMC Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "wrtprt",
        "MMC write protect",
        0x54,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "protect",
            "MMC card slot write protect",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("no_protect", "MMC card slot no write protect", 0b0)?,
                create_enum_value("protect", "MMC card slot write protect", 0b1)?,
            ])?],
            Some(
                svd::DimElement::builder()
                    .dim(32)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        None,
    )?))
}
