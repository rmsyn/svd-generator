use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare MMC Internal DMAC Interrupt Enable / DB Address register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "dscaddr_idinten64",
        "MMC internal DMAC DSC address / interrupt enable - HCON[ADDR_CONFIG] 32-bit(0): DSC address, HCON[ADDR_CONFIG] 64-bit(1): interrupt enable",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "dscaddr_idinten64",
                "MMC Internal DMAC DSC address / interrupt enable",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
