use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare MMC Internal DMAC DB Address register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "dbaddr_dbaddrl",
        "MMC internal DMAC DB address - HCON[ADDR_CONFIG] 32-bit(0): DB address, HCON[ADDR_CONFIG] 64-bit(1): DB address lower 32-bits",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "dbaddr_dbaddrl",
                "MMC Internal DMAC DB address",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
