use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare MMC Internal DMAC Buffer Address register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "bufaddr",
        "MMC internal DMAC reserved / buffer address - HCON[ADDR_CONFIG] 32-bit(0): reserved, HCON[ADDR_CONFIG] 64-bit(1): buffer address lower/upper 32-bits",
        0x18,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "bufaddr",
                "MMC Internal DMAC reserved / buffer address",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        Some(svd::DimElement::builder()
            .dim(2)
            .dim_increment(0x4)
            .dim_index(Some([
                String::from("l"),
                String::from("u"),
            ].into()))
            .build(svd::ValidateLevel::Strict)?),
    )?))
}
