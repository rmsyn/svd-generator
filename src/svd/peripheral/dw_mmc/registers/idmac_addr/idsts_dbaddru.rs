use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare MMC Internal DMAC Status / DB Address register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "idsts_dbaddru",
        "MMC internal DMAC status / DB address - HCON[ADDR_CONFIG] 32-bit(0): status, HCON[ADDR_CONFIG] 64-bit(1): DB address upper 32-bits",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "idsts_dbaddru",
                "MMC Internal DMAC status / DB address",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
