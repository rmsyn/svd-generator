use crate::svd::{create_bit_range, create_field};
use crate::Result;

/// Creates Synopsys DesignWare MMC Interrupt field definition.
pub fn create_int_fields(desc: &str, access: svd::Access) -> Result<svd::Field> {
    create_field(
        "int",
        desc,
        create_bit_range("[0:0]")?,
        access,
        Some(
            svd::DimElement::builder()
                .dim(32)
                .dim_increment(0x1)
                .dim_index(Some(
                    [
                        "_cd",
                        "_resp_err",
                        "_cmd_done",
                        "_data_over",
                        "_txdr",
                        "_rxdr",
                        "_rcrc",
                        "_dcrc",
                        "_rto",
                        "_drto",
                        "_hto_volt_switch",
                        "_frun",
                        "_hle",
                        "_sbe",
                        "_acd",
                        "_ebe",
                        "_sdio0",
                        "_sdio1",
                        "_sdio2",
                        "_sdio3",
                        "_sdio4",
                        "_sdio5",
                        "_sdio6",
                        "_sdio7",
                        "_sdio8",
                        "_sdio9",
                        "_sdio10",
                        "_sdio11",
                        "_sdio12",
                        "_sdio13",
                        "_sdio14",
                        "_sdio15",
                    ]
                    .map(String::from)
                    .into(),
                ))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
}
