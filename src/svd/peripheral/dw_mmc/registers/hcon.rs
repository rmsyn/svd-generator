use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare MMC HCON register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "hcon",
        "MMC HCON",
        0x70,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "slot_num",
                "MMC slot number",
                create_bit_range("[5:1]")?,
                svd::Access::ReadWrite,
                // NOTE: the Linux driver uses a one-indexed naming scheme here, but a zero-indexed
                // naming scheme everywhere else. Use zero-indexed scheme for consistency.
                &[create_enum_values(&[
                    create_enum_value("slot0", "MMC slot 0", 0b00000)?,
                    create_enum_value("slot1", "MMC slot 1", 0b00001)?,
                    create_enum_value("slot2", "MMC slot 2", 0b00010)?,
                    create_enum_value("slot3", "MMC slot 3", 0b00011)?,
                    create_enum_value("slot4", "MMC slot 4", 0b00100)?,
                    create_enum_value("slot5", "MMC slot 5", 0b00101)?,
                    create_enum_value("slot6", "MMC slot 6", 0b00110)?,
                    create_enum_value("slot7", "MMC slot 7", 0b00111)?,
                    create_enum_value("slot8", "MMC slot 8", 0b01000)?,
                    create_enum_value("slot9", "MMC slot 9", 0b01001)?,
                    create_enum_value("slot10", "MMC slot 10", 0b01010)?,
                    create_enum_value("slot11", "MMC slot 11", 0b01011)?,
                    create_enum_value("slot12", "MMC slot 12", 0b01100)?,
                    create_enum_value("slot13", "MMC slot 13", 0b01101)?,
                    create_enum_value("slot14", "MMC slot 14", 0b01110)?,
                    create_enum_value("slot15", "MMC slot 15", 0b01111)?,
                    create_enum_value("slot16", "MMC slot 16", 0b10000)?,
                    create_enum_value("slot17", "MMC slot 17", 0b10001)?,
                    create_enum_value("slot18", "MMC slot 18", 0b10010)?,
                    create_enum_value("slot19", "MMC slot 19", 0b10011)?,
                    create_enum_value("slot20", "MMC slot 20", 0b10100)?,
                    create_enum_value("slot21", "MMC slot 21", 0b10101)?,
                    create_enum_value("slot22", "MMC slot 22", 0b10110)?,
                    create_enum_value("slot23", "MMC slot 23", 0b10111)?,
                    create_enum_value("slot24", "MMC slot 24", 0b11000)?,
                    create_enum_value("slot25", "MMC slot 25", 0b11001)?,
                    create_enum_value("slot26", "MMC slot 26", 0b11010)?,
                    create_enum_value("slot27", "MMC slot 27", 0b11011)?,
                    create_enum_value("slot28", "MMC slot 28", 0b11100)?,
                    create_enum_value("slot29", "MMC slot 29", 0b11101)?,
                    create_enum_value("slot30", "MMC slot 30", 0b11110)?,
                    create_enum_value("slot31", "MMC slot 31", 0b11111)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hdata_width",
                "MMC HDATA width",
                create_bit_range("[9:7]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("width16", "Host data width: 16 bits", 0b000)?,
                    create_enum_value("width32", "Host data width: 32 bits", 0b001)?,
                    create_enum_value("width64", "Host data width: 64 bits", 0b010)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "trans_mode",
                "MMC transfer mode",
                create_bit_range("[17:16]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("idma", "No DMA interface - using internal DMA block", 0b00)?,
                    create_enum_value("dwdma", "DesignWare DMA interface", 0b01)?,
                    create_enum_value("gdma", "Generic DMA interface", 0b10)?,
                    create_enum_value("nodma", "Non-DesignWare DMA interface - pio only", 0b11)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "addr_config",
                "MMC address config",
                create_bit_range("[27:27]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("addr32", "IDMAC 32-bit address", 0b0)?,
                    create_enum_value("addr64", "IDMAC 64-bit address", 0b1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
