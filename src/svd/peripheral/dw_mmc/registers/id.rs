use crate::svd::create_cluster;
use crate::Result;

pub mod usrid;
pub mod verid;

/// Creates Synopsys DesignWare MMC ID register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "id",
        "MMC ID registers",
        0x68,
        &[usrid::create()?, verid::create()?],
        None,
    )?))
}
