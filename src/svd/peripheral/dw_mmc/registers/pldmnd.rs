use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare MMC PLDMND register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "pldmnd",
        "MMC PLDMND",
        0x84,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "start",
            "MMC Internal DMAC start",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
