use crate::svd::create_cluster;
use crate::Result;

pub mod bufaddr;
pub mod bufaddr_dscaddrl;
pub mod dbaddr_dbaddrl;
pub mod dscaddr_idinten64;
pub mod dscaddru;
pub mod idinten_idsts64;
pub mod idsts_dbaddru;

/// Creates Synopsys DesignWare MMC Internal DMAC Address register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "idmac_addr",
        "MMC Internal DMAC Address registers",
        0x88,
        &[
            dbaddr_dbaddrl::create()?,
            idsts_dbaddru::create()?,
            idinten_idsts64::create()?,
            dscaddr_idinten64::create()?,
            bufaddr_dscaddrl::create()?,
            dscaddru::create()?,
            bufaddr::create()?,
        ],
        None,
    )?))
}
