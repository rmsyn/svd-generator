use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare MMC Card Type register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ctype",
        "MMC card type",
        0x18,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "ctype",
            "MMC card type",
            create_bit_range("[16:0]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("type1", "1-bit serial transfer MMC card type.", 0x0_0000)?,
                create_enum_value("type4", "4-bit serial transfer MMC card type.", 0x0_0001)?,
                create_enum_value("type8", "8-bit serial transfer MMC card type.", 0x1_0000)?,
            ])?],
            None,
        )?]),
        None,
    )?))
}
