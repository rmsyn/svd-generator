use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare MMC Clock Enable register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "clken",
        "MMC Clock Enable",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "enable",
                "MMC Clock Enable",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "low_pwr",
                "MMC Clock Enable Low Power",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
