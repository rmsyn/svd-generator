use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare MMC Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "fifoth",
        "MMC FIFOTH",
        0x4c,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "tx_wmark",
                "MMC FIFOTH TX watermark",
                create_bit_range("[11:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xfff)?,
                None,
            )?,
            create_field_constraint(
                "rx_wmark",
                "MMC FIFOTH RX watermark",
                create_bit_range("[27:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xfff)?,
                None,
            )?,
            create_field_constraint(
                "msize",
                "MMC FIFOTH msize",
                create_bit_range("[30:28]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
        ]),
        None,
    )?))
}
