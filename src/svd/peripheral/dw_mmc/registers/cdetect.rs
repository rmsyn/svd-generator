use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare MMC Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cdetect",
        "MMC card detect",
        0x50,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "slot",
            "MMC card present in slot",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("present", "MMC card present in slot", 0b0)?,
                create_enum_value("not_present", "MMC card not present in slot", 0b1)?,
            ])?],
            Some(
                svd::DimElement::builder()
                    .dim(32)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        None,
    )?))
}
