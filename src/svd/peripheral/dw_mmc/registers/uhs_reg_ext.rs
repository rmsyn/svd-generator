use crate::svd::peripheral::dw_mmc::DwMmcVersion;
use crate::svd::{create_register, create_register_properties};
use crate::{Error, Result};

const EXPECTED_VER: DwMmcVersion = DwMmcVersion::Version280a;

/// Creates Synopsys DesignWare MMC Internal DMAC UHS Regulator Extended register definitions.
pub fn create(version: DwMmcVersion, fields: &[svd::Field]) -> Result<svd::RegisterCluster> {
    match version {
        DwMmcVersion::Version280a => Ok(svd::RegisterCluster::Register(create_register(
            "uhs_reg_ext",
            "MMC UHS regulator extended",
            0x108,
            create_register_properties(32, 0)?,
            Some(fields),
            None,
        )?)),
        _ => Err(Error::Svd(format!(
            "invalid DesignWare MMC version, have: {version}, expected: {EXPECTED_VER}"
        ))),
    }
}
