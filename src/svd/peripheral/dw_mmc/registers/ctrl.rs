use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare MMC Control register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ctrl",
        "MMC Control",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "reset",
                "MMC Control Reset",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                Some(
                    svd::DimElement::builder()
                        .dim(3)
                        .dim_increment(1)
                        .dim_index(Some(
                            [
                                String::from("_mmc"),
                                String::from("_fifo"),
                                String::from("_dma"),
                            ]
                            .into(),
                        ))
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field(
                "enable",
                "MMC Control Enable",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                Some(
                    svd::DimElement::builder()
                        .dim(2)
                        .dim_increment(1)
                        .dim_index(Some([String::from("_int"), String::from("_dma")].into()))
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field(
                "read_wait",
                "MMC Control Read Wait",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "send_irq_resp",
                "MMC Control Send IRQ Response",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "abrt_read_data",
                "MMC Control Abort Read Data",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "send",
                "MMC Control Send",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                Some(
                    svd::DimElement::builder()
                        .dim(2)
                        .dim_increment(1)
                        .dim_index(Some(
                            [String::from("_ccsd"), String::from("_as_ccsd")].into(),
                        ))
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field(
                "ceata_int_en",
                "MMC Control CEATA Interrupt Enable",
                create_bit_range("[11:11]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "use_idmac",
                "MMC Control Use IDMAC",
                create_bit_range("[25:25]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
