use crate::svd::peripheral::dw_mmc::{DwMmcFifoDepth, DwMmcVersion};
use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare MMC FIFO Data register definitions.
pub fn create(
    version: DwMmcVersion,
    fifo_depth: DwMmcFifoDepth,
    fifo_len: usize,
) -> Result<svd::RegisterCluster> {
    let depth = fifo_depth.into_bits();
    Ok(svd::RegisterCluster::Register(create_register(
        "data",
        "MMC FIFO data",
        version.data_offset(),
        create_register_properties(depth as u32, 0)?,
        Some(&[create_field_constraint(
            "data",
            "MMC FIFO data",
            create_bit_range(format!("[{}:0]", depth - 1).as_str())?,
            svd::Access::ReadWrite,
            create_write_constraint(0, ((1u128 << depth) - 1) as u64)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim((fifo_len as u32).saturating_div(4))
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
