use super::int::create_int_fields;
use crate::svd::{create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare MMC Interrupt Mask register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "intmask",
        "MMC interrupt mask",
        0x24,
        create_register_properties(32, 0)?,
        Some(&[create_int_fields(
            "MMC interrupt mask and status",
            svd::Access::ReadWrite,
        )?]),
        None,
    )?))
}
