use std::fmt;

use crate::{Error, Result};

/// Represents the implementation model for Synopsys DesignWare MMC peripherals.
// NOTE: probably overkill for the (currently) only implementation.
// However, the Linux driver includes a large number of implementations.
// Probably a good idea to future-proof for additional implementations.
#[repr(C)]
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum DwMmcModel {
    Jh7110,
}

impl From<&DwMmcModel> for &'static str {
    fn from(val: &DwMmcModel) -> Self {
        match val {
            DwMmcModel::Jh7110 => "jh7110",
        }
    }
}

impl From<DwMmcModel> for &'static str {
    fn from(val: DwMmcModel) -> Self {
        (&val).into()
    }
}

impl TryFrom<&str> for DwMmcModel {
    type Error = Error;

    fn try_from(val: &str) -> Result<Self> {
        match val {
            v if v.contains("jh7110-mmc") => Ok(Self::Jh7110),
            _ => Err(Error::Svd(format!("unknown DesignWare MMC model: {val}"))),
        }
    }
}

impl fmt::Display for DwMmcModel {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", <&str>::from(self))
    }
}
