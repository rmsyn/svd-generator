use super::{DwMmcFifoDepth, DwMmcModel};
use crate::Result;

pub mod blksiz;
pub mod bmod;
pub mod bytcnt;
pub mod cdetect;
pub mod cdthrctl;
pub mod clk_ctrl;
pub mod cmd;
pub mod cmdarg;
pub mod cnt;
pub mod ctrl;
pub mod ctype;
pub mod data;
pub mod debnce;
pub mod fifoth;
pub mod gpio;
pub mod hcon;
pub mod id;
pub mod idmac_addr;
pub mod int;
pub mod intmask;
pub mod jh7110;
pub mod mintsts;
pub mod pldmnd;
pub mod pwren;
pub mod resp;
pub mod rintsts;
pub mod rst_n;
pub mod status;
pub mod tmout;
pub mod uhs_reg;
pub mod uhs_reg_ext;
pub mod wrtprt;

/// Creates Synopsys DesignWare MMC register definitions.
///
/// Based on the register definitions from the Linux driver:
///
/// - <https://elixir.bootlin.com/linux/latest/source/drivers/mmc/host/dw_mmc.h>
///
/// Original Author(s):
///
/// - Jaehoon Chung <jh80.chung@samsung.com>
/// - Will Newton <will.newton@imgtec.com>
pub fn create(model: DwMmcModel, fifo_depth: DwMmcFifoDepth) -> Result<Vec<svd::RegisterCluster>> {
    match model {
        DwMmcModel::Jh7110 => jh7110::create(fifo_depth),
    }
}
