use crate::svd::{create_bit_range, create_field};
use crate::Result;

pub mod current_power_mode;
pub mod encourage_type_crd;
pub mod event_int_status;
pub mod hard_event_turn_on_mask;
pub mod hard_turn_on_power_mode;
pub mod hw_event_crd;
pub mod lp_timeout;
pub mod pch_active;
pub mod pch_bypass;
pub mod pch_pstate;
pub mod pch_timeout;
pub mod pdc;
pub mod soft_turn_off_power_mode;
pub mod soft_turn_on_power_mode;
pub mod sw_encourage;
pub mod tim;
pub mod timeout_seq_thd;

/// Creates StarFive JH7110 PMU (compatible) register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        hard_event_turn_on_mask::create()?,
        soft_turn_on_power_mode::create()?,
        soft_turn_off_power_mode::create()?,
        timeout_seq_thd::create()?,
        pdc::create()?,
        sw_encourage::create()?,
        tim::create()?,
        pch_bypass::create()?,
        pch_pstate::create()?,
        pch_timeout::create()?,
        lp_timeout::create()?,
        hard_turn_on_power_mode::create()?,
        current_power_mode::create()?,
        event_int_status::create()?,
        hw_event_crd::create()?,
        encourage_type_crd::create()?,
        pch_active::create()?,
    ]
    .into())
}

/// Convenience function to create power mode register fields.
pub(crate) fn create_fields_power_mode(mode: &str) -> Result<[svd::Field; 7]> {
    Ok([
        create_field(
            "systop_power_mode",
            format!("SYSTOP turn-{mode} power mode").as_str(),
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "cpu_power_mode",
            format!("CPU turn-{mode} power mode").as_str(),
            create_bit_range("[1:1]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "gpua_power_mode",
            format!("GPUA turn-{mode} power mode").as_str(),
            create_bit_range("[2:2]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "vdec_power_mode",
            format!("VDEC turn-{mode} power mode").as_str(),
            create_bit_range("[3:3]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "vout_power_mode",
            format!("VOUT turn-{mode} power mode").as_str(),
            create_bit_range("[4:4]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "isp_power_mode",
            format!("ISP turn-{mode} power mode").as_str(),
            create_bit_range("[5:5]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "venc_power_mode",
            format!("VENC turn-{mode} power mode").as_str(),
            create_bit_range("[6:6]")?,
            svd::Access::ReadWrite,
            None,
        )?,
    ])
}
