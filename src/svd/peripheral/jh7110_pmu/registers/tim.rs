use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the StarFive JH7110 PMU Timer Interrupt Mask (TIM) register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "tim",
        "Timer Interrupt Mask register",
        0x48,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "seq_done_mask",
                "Mask the sequence complete event. 0: mask, 1: unmask",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("mask", "Mask the sequence complete event", 0)?,
                    create_enum_value("unmask", "Unmask the sequence complete event", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hw_req_mask",
                "Mask the hardware encouragement request. 0: mask, 1: unmask",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("mask", "Mask the hardware encouragement request", 0)?,
                    create_enum_value("unmask", "Unmask the hardware encouragement request", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "sw_fail_mask",
                "Mask the software encouragement failure event. 0: mask, 1: unmask",
                create_bit_range("[3:2]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("mask", "Mask the software encouragement failure event", 0)?,
                    create_enum_value(
                        "unmask",
                        "Unmask the software encouragement failure event",
                        1,
                    )?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hw_fail_mask",
                "Mask the hardware encouragement failure event. 0: mask, 1: unmask",
                create_bit_range("[5:4]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("mask", "Mask the hardware encouragement failure event", 0)?,
                    create_enum_value(
                        "unmask",
                        "Unmask the hardware encouragement failure event",
                        1,
                    )?,
                ])?],
                None,
            )?,
            create_field_enum(
                "pch_fail_mask",
                "Mask the P-channel encouragement failure event. 0: mask, 1: unmask",
                create_bit_range("[8:6]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("mask", "Mask the P-channel encouragement failure event", 0)?,
                    create_enum_value(
                        "unmask",
                        "Unmask the P-channel encouragement failure event",
                        1,
                    )?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
