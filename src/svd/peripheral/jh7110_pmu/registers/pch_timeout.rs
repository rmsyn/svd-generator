use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the StarFive JH7110 PMU PCH Timeout Threshold register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "pch_timeout",
        "P-channel waiting device acknowledge timeout.",
        0x54,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "pch_timeout",
            "",
            create_bit_range("[7:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xff)?,
            None,
        )?]),
        None,
    )?))
}
