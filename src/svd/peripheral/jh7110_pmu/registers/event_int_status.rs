use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the StarFive JH7110 PMU Hardware Event and Interrupt Status registers.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "status",
        "Event and Interrupt Status registers",
        0x88,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "seq_done_event",
                "Sequence complete.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "hw_req_event",
                "Hardware encouragement request.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "sw_fail_event",
                "Software encouragement failure.",
                create_bit_range("[3:2]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "hw_fail_event",
                "Hardware encouragement failure.",
                create_bit_range("[5:4]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "pch_fail_event",
                "P-channel failure.",
                create_bit_range("[8:6]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some(["_event", "_interrupt"].map(String::from).into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
