use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the StarFive JH7110 PMU PDC (Power Domain Cascade) register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "pdc",
        "Power Domain Cascade register",
        0x18,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "pd0_off_cas",
                "Power domain 0 turn-off cascade. The register value indicates the power-off sequence of this domain. 0 means the highest priority. System only accepts value from 0 to 7 any other value is invalid.",
                create_bit_range("[4:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "pd0_on_cas",
                "Power domain 0 turn-on cascade. The register value indicates the power-on sequence of this domain. 0 means the highest priority. System only accepts value from 0 to 7 any other value is invalid.",
                create_bit_range("[9:5]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "pd1_off_cas",
                "Power domain 1 turn-off cascade. The register value indicates the power-off sequence of this domain. 0 means the highest priority. System only accepts value from 0 to 7 any other value is invalid.",
                create_bit_range("[14:10]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "pd1_on_cas",
                "Power domain 1 turn-off cascade. The register value indicates the power-on sequence of this domain. 0 means the highest priority. System only accepts value from 0 to 7 any other value is invalid.",
                create_bit_range("[19:15]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "pd2_off_cas",
                "Power domain 2 turn-off cascade. The register value indicates the power-off sequence of this domain. 0 means the highest priority. System only accepts value from 0 to 7 any other value is invalid.",
                create_bit_range("[24:20]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "pd2_on_cas",
                "Power domain 1 turn-on cascade. The register value indicates the power-on sequence of this domain. 0 means the highest priority. System only accepts value from 0 to 7 any other value is invalid.",
                create_bit_range("[29:25]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
        ]),
        Some(svd::DimElement::builder()
            .dim(3)
            .dim_increment(0x4)
            .build(svd::ValidateLevel::Strict)?),
    )?))
}
