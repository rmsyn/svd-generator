use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the StarFive JH7110 PMU P-channel PACTIVE Status register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "pch_active",
        "P-channel PACTIVE Status register",
        0x98,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "pch_active",
            "",
            create_bit_range("[10:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
