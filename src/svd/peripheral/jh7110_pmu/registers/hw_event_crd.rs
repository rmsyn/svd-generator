use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the StarFive JH7110 PMU Hardware Event Record register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "hw_event_crd",
        "Hardware Event Record register",
        0x90,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "hw_event_crd",
            "",
            create_bit_range("[7:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
