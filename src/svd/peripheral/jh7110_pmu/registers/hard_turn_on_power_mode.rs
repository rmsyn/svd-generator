use crate::svd::{create_register, create_register_properties};
use crate::Result;

use super::create_fields_power_mode;

/// Creates the StarFive JH7110 PMU Hardware Turn-On Power Mode register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "hard_turn_on_power_mode",
        "Hardware Turn-On Power Mode register",
        0x5c,
        create_register_properties(32, 0)?,
        Some(&create_fields_power_mode("on")?),
        None,
    )?))
}
