use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the StarFive JH7110 PMU Software Encouragement register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "sw_encourage",
        "Software encouragement register",
        0x44,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "sw_encourage",
            "Software encouragement variants.",
            create_bit_range("[7:0]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("off", "Disable Software Encourage.", 0x00)?,
                create_enum_value("en_lo", "Enable LO Software Encourage.", 0x05)?,
                create_enum_value("dis_lo", "Disable LO Software Encourage.", 0x0a)?,
                create_enum_value("en_hi", "Enable HI Software Encourage.", 0x50)?,
                create_enum_value("dis_hi", "Disable HI Software Encourage.", 0xa0)?,
                create_enum_value("on", "Enable Software Encourage.", 0xff)?,
            ])?],
            None,
        )?]),
        None,
    )?))
}
