use crate::svd::{create_register, create_register_properties};
use crate::Result;

use super::create_fields_power_mode;

/// Creates the StarFive JH7110 PMU Current Power Mode register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "current_power_mode",
        "Current Power Mode register",
        0x80,
        create_register_properties(32, 0)?,
        Some(&create_fields_power_mode("on")?),
        None,
    )?))
}
