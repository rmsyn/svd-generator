use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the StarFive JH7110 PMU PCH Bypass register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "pch_bypass",
        "P-channel Bypass register",
        0x4c,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "pch_bypass",
            "Bypass P-channel. 0: enable p-channel, 1: bypass p-channel",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
