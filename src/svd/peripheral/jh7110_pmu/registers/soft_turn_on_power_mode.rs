use crate::svd::{create_register, create_register_properties};
use crate::Result;

use super::create_fields_power_mode;

/// Creates the StarFive JH7110 PMU Software Turn-On Power Mode register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "soft_turn_on_power_mode",
        "Software Turn-On Power Mode",
        0xc,
        create_register_properties(32, 0)?,
        Some(&create_fields_power_mode("on")?),
        None,
    )?))
}
