use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the StarFive JH7110 PMU Hardware Event Type Record register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "encourage_type_crd",
        "Hardware Event Type Record register",
        0x94,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "hw_event_crd",
            "Hardware/Software encouragement type record. 0: Software, 1: Hardware.",
            create_bit_range("[0:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
