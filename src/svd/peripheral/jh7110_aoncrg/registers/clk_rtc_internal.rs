use crate::svd::jh7110;
use crate::Result;

/// Creates StarFive JH7110 AON CRG CLK_RTC_INTERNAL register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_rtc_internal",
        "RTC Internal Clock",
        0x2c,
        [1022, 750, 750, 750],
        None,
    )
}
