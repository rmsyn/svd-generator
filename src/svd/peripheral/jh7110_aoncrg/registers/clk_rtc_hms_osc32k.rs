use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates StarFive JH7110 AON CRG CLK_RTC_HMS_OSC32K register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_mux_sel_enum(
        "clk_rtc_hms_osc32k",
        "RTC HMS Clock Oscillator 32K",
        0x30,
        "clk_rtc, clk_rtc_internal",
        &[
            create_enum_value(
                "clk_rtc",
                "Select `clk_rtc` as the RTC HMC Oscillator 32K clock.",
                0b0,
            )?,
            create_enum_value(
                "clk_rtc_internal",
                "Select `clk_rtc_internal` as the RTC HMC Oscillator 32K clock.",
                0b1,
            )?,
        ],
        None,
    )
}
