use crate::svd::jh7110;
use crate::Result;

/// Creates StarFive JH7110 AON CRG SOFT_RST_ADDR_SEL register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_aon_rst_sel(
        "soft_rst_addr_sel",
        "Software RESET Address Selector",
        0x38,
        Some(0b11100011),
    )
}
