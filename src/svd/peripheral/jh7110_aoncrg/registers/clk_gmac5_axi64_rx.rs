use crate::svd::jh7110;
use crate::Result;

/// Creates StarFive JH7110 AON CRG CLK_GMAC5_AXI64_RX register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_dly_chain_sel(
        "clk_gmac5_axi64_rx",
        "GMAC5 AXI64 Clock Receiver",
        0x1c,
        None,
    )
}
