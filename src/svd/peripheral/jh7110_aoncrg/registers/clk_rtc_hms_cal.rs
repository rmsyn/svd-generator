use crate::svd::jh7110;
use crate::Result;

/// Creates StarFive JH7110 AON CRG CLK_RTC_HMS_CAL register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "clk_rtc_hms_cal",
        "RTC HMS Clock Calculator",
        0x34,
        None,
        None,
    )
}
