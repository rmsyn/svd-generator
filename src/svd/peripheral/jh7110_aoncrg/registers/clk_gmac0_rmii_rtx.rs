use crate::svd::jh7110;
use crate::Result;

/// Creates StarFive JH7110 AON CRG CLK_GMAC0_RMII_RTX register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_gmac0_rmii_rtx",
        "GMAC0 RMII RTX Clock",
        0x10,
        [30, 2, 2, 2],
        None,
    )
}
