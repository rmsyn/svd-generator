use crate::svd::jh7110;
use crate::Result;

/// Creates StarFive JH7110 AON CRG CLK_RTC_HMS_APB register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "clk_rtc_hms_apb",
        "RTC HMS APB Clock",
        0x28,
        Some(1 << 31),
        None,
    )
}
