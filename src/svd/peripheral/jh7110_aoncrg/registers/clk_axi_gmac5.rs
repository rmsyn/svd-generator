use crate::svd::jh7110;
use crate::Result;

/// Creates StarFive JH7110 AON CRG CLK_AXI_GMAC5 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_axi_gmac5", "AXI GMAC5 Clock", 0xc, None, None)
}
