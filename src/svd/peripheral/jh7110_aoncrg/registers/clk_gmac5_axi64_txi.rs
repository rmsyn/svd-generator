use crate::svd::jh7110;
use crate::Result;

/// Creates StarFive JH7110 AON CRG CLK_GMAC5_AXI64_TXI register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_clk_polarity(
        "clk_gmac5_axi64_txi",
        "GMAC5 AXI64 Clock Transmission Inverter",
        0x18,
        Some(1 << 30),
    )
}
