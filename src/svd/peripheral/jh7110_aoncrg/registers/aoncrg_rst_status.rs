use crate::svd::jh7110;
use crate::Result;

/// Creates StarFive JH7110 AON CRG SOFT_RST_ADDR_SEL register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_aon_rst_sel(
        "aoncrg_rst_status",
        "AONCRG RESET Status",
        0x3c,
        Some(0b11100011),
    )
}
