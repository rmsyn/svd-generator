use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates StarFive JH7110 AON CRG CLK_AON_APB register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_mux_sel_enum(
        "clk_aon_apb",
        "AON APB Function Clock",
        0x4,
        "clk_osc_div4, clk_osc",
        &[
            create_enum_value(
                "clk_osc_div4",
                "Select `clk_osc_div4` as AON APB clock.",
                0b0,
            )?,
            create_enum_value("clk_osc", "Select `clk_osc` as AON APB clock.", 0b1)?,
        ],
        None,
    )
}
