use crate::svd::{create_enum_value, jh7110};
use crate::Result;

/// Creates StarFive JH7110 AON CRG CLK_GMAC5_AXI64_TX register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg_mux_sel_enum(
        "clk_gmac5_axi64_tx",
        "GMAC5 AXI64 Clock Transmitter",
        0x14,
        "u0_sys_crg_clk_gmac0_gtxclk, clk_gmac0_rmii_rtx",
        &[
            create_enum_value(
                "u0_sys_crg_clk_gmac0_gtxclk",
                "Select `u0_sys_crg_clk_gmac0_gtxclk` as the GMAC5 AXI64 TX clock.",
                0b0,
            )?,
            create_enum_value(
                "clk_gmac0_rmii_rtx",
                "Select `clk_gmac0_rmii_rtx` as the GMAC5 AXI64 TX clock.",
                0b1,
            )?,
        ],
        None,
        None,
    )
}
