use crate::svd::jh7110;
use crate::Result;

/// Creates StarFive JH7110 AON CRG CLK_OPTC_APB register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg("clk_optc_apb", "OPTC APB Clock", 0x24, Some(1 << 31), None)
}
