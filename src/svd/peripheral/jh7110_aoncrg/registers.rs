use crate::Result;

pub mod aoncrg_rst_status;
pub mod clk_ahb_gmac5;
pub mod clk_aon_apb;
pub mod clk_axi_gmac5;
pub mod clk_gmac0_rmii_rtx;
pub mod clk_gmac5_axi64_rx;
pub mod clk_gmac5_axi64_rxi;
pub mod clk_gmac5_axi64_tx;
pub mod clk_gmac5_axi64_txi;
pub mod clk_optc_apb;
pub mod clk_osc;
pub mod clk_rtc_hms_apb;
pub mod clk_rtc_hms_cal;
pub mod clk_rtc_hms_osc32k;
pub mod clk_rtc_internal;
pub mod soft_rst_addr_sel;

/// Creates StarFive JH7110 AON CRG (compatible) register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        clk_osc::create()?,
        clk_aon_apb::create()?,
        clk_ahb_gmac5::create()?,
        clk_axi_gmac5::create()?,
        clk_gmac0_rmii_rtx::create()?,
        clk_gmac5_axi64_tx::create()?,
        clk_gmac5_axi64_txi::create()?,
        clk_gmac5_axi64_rx::create()?,
        clk_gmac5_axi64_rxi::create()?,
        clk_optc_apb::create()?,
        clk_rtc_hms_apb::create()?,
        clk_rtc_internal::create()?,
        clk_rtc_hms_osc32k::create()?,
        clk_rtc_hms_cal::create()?,
        soft_rst_addr_sel::create()?,
        aoncrg_rst_status::create()?,
    ]
    .into())
}
