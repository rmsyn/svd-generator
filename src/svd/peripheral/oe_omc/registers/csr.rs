use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the OpenEdges Orbit Memory Controller CSR register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "csr",
        "DDR Memory Control CSR register",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "csr",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(1024)
                .dim_increment(4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
