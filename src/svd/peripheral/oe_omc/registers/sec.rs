use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the OpenEdges Orbit Memory Controller SEC register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sec",
        "DDR Memory Control SEC register",
        0x1000,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "sec",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2048)
                .dim_increment(4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
