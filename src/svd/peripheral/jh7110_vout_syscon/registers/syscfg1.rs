use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the StarFive JH7110 VOUT SYSCFG 1 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "syscfg1",
        "VOUT SYSCFG 1: dom_vout_sysconsaif_4",
        0x4,
        create_register_properties(32, 0x80000)?,
        Some(&[
            create_field_constraint(
                "u0_cdns_dsitx_dsi_test_generic_status",
                "",
                create_bit_range("[15:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
            create_field(
                "u0_dc8200_cactive",
                "",
                create_bit_range("[16:16]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_dc8200_csysack",
                "",
                create_bit_range("[17:17]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_dc8200_csysreq",
                "",
                create_bit_range("[18:18]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_dc8200_disable_ram_clock_gating",
                "",
                create_bit_range("[19:19]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "u0_display_panel_mux_panel_sel",
                "DC8200 Panel - 0: Panel 0, 1: Panel 1",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("panel0", "DC8200 panel 0", 0b0)?,
                    create_enum_value("panel1", "DC8200 panel 1", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u0_dsitx_data_mapping_dp_mode",
                "DP color mode - 0: YUV420 CFG1, 1: YUV420 CFG3, 2: YUV422 CFG1 (Reserved), 3: RGB888, 4: RGB666, 5: RGB565",
                create_bit_range("[23:21]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("yuv420_cfg1", "DP color mode: YUV420 CFG1", 0)?,
                    create_enum_value("yuv420_cfg3", "DP color mode: YUV420 CFG3", 1)?,
                    create_enum_value("rgb888", "DP color mode: RGB888", 3)?,
                    create_enum_value("rgb666", "DP color mode: RGB666", 4)?,
                    create_enum_value("rgb565", "DP color mode: RGB565", 5)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u0_dsitx_data_mapping_dpi_dp_sel",
                "DC8200 DP/DPI interface for dsiTx - 0: DPI, 1: DP",
                create_bit_range("[24:24]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("dpi", "DC8200 DP/DPI interface for dsiTX: DPI", 0)?,
                    create_enum_value("dp", "DC8200 DP/DPI interface for dsiTX: DP", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u0_hdmi_data_mapping_dp_bit_depth",
                "DP Bit Depth - 0: 8-bit, 1: 10-bit",
                create_bit_range("[25:25]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("bit8", "DP Bit Depth: 8-bit", 0)?,
                    create_enum_value("bit10", "DP Bit Depth: 10-bit", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u0_hdmi_data_mapping_dp_yuv_mode",
                "DP YUV Mode - 0: YUV420, 1: YUV422, 2: YUV444, 3: RGB",
                create_bit_range("[27:26]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("yuv420", "DP YUV Mode: YUV420", 0)?,
                    create_enum_value("yuv422", "DP YUV Mode: YUV422", 1)?,
                    create_enum_value("yuv444", "DP YUV Mode: YUV444", 2)?,
                    create_enum_value("rgb", "DP YUV Mode: RGB", 3)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u0_hdmi_data_mapping_dpi_bit_depth",
                "DPI Bit Depth - 0: 8-bit, 1: 10-bit, 2: 6-bit CFG1 in DC8200, 3: RGB565 CFG1 in DC8200",
                create_bit_range("[29:28]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("bit8", "DPI Bit Depth: 8-bit", 0)?,
                    create_enum_value("bit10", "DPI Bit Depth: 10-bit", 1)?,
                    create_enum_value("bit6", "DPI Bit Depth: 6-bit CFG1 in DC8200", 2)?,
                    create_enum_value("rgb565", "DPI Bit Depth: RGB565 CFG1 in DC8200", 3)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "u0_hdmi_data_mapping_dpi_dp_sel",
                "DC8200 DP/DPI interface - 0: DPI, 1: DP",
                create_bit_range("[30:30]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("dpi", "DC8200 DP/DPI interface: DPI", 0)?,
                    create_enum_value("dp", "DC8200 DP/DPI interface: DP", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
