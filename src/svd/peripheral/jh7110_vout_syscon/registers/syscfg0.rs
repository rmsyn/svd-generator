use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties, jh7110,
};
use crate::Result;

/// Creates the StarFive JH7110 VOUT SYSCFG 0 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let [slp, sd, rtsel, ptsel, trb, wtsel, vs, vg] =
        jh7110::create_fields_sram_config("u0_cdns_dsitx", 0x0)?;

    create_register(
        "syscfg0",
        "VOUT SYSCFG 0: dom_vout_sysconsaif_0",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            slp,
            sd,
            rtsel,
            ptsel,
            trb,
            wtsel,
            vs,
            vg,
            create_field(
                "u0_cdns_dsitx_dsi_test_generic_ctrl",
                "",
                create_bit_range("[23:8]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
