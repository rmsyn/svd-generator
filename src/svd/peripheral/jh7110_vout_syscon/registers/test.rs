use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates the StarFive JH7110 VOUT SYSCFG Test registers.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "test",
        "VOUT SYSCFG 3-6: dom_vout_sysconsaif_12 - dom_vout_sysonsaif_24",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "test",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(4)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
