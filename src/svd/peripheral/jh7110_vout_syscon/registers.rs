use crate::Result;

pub mod syscfg0;
pub mod syscfg1;
pub mod syscfg2;
pub mod test;

/// Creates StarFive JH7110 VOUT SYSCON compatible register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        syscfg0::create()?,
        syscfg1::create()?,
        syscfg2::create()?,
        test::create()?,
    ]
    .into())
}
