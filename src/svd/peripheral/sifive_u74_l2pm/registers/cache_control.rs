use crate::svd::create_cluster;
use crate::Result;

pub mod config;
pub mod ecc;
pub mod ecc_inject_error;
pub mod flush;
pub mod way_enable;
pub mod way_mask;

/// Creates SiFive U74(MC) L2 Performance Monitor L2 Cache Control registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "cache_control",
        "L2 Cache Control registers.",
        0x0,
        &[
            config::create()?,
            way_enable::create()?,
            ecc_inject_error::create()?,
            ecc::create()?,
            flush::create()?,
            way_mask::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
