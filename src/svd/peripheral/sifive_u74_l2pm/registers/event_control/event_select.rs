use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates SiFive U74(MC) L2 Performance Monitor Event Select register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "event_select",
        "L2PM Event Control Event Select configuration.",
        0x0,
        create_register_properties(64, 1)?,
        Some(&[
            create_field_enum(
                "event_class",
                "L2PM Event Class.",
                create_bit_range("[7:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("transaction", "L2PM transaction events.", 1)?,
                    create_enum_value("l2_query_result", "L2PM L2 query result events.", 2)?,
                    create_enum_value("l2_request", "L2PM L2 request events.", 3)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "event_mask",
                "L2PM Event Mask for specifying the event type according to its `event_class`.",
                create_bit_range("[63:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff_ffff_ffff_ffff)?,
                None,
            )?,
        ]),
        Some(
            svd::DimElement::builder()
                .dim(6)
                .dim_increment(8)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
