use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates SiFive U74(MC) L2 Performance Monitor Event Select register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "client_filter",
        "L2PM Event Control Event Select configuration.",
        0x800,
        create_register_properties(64, 0)?,
        Some(&[
            create_field_enum(
                "debug",
                "Disable counter events originating from `Debug` client.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "axi4_front_port",
                "Disable counter events originating from `Debug` client.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                Some(
                    svd::DimElement::builder()
                        .dim(4)
                        .dim_increment(1)
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field_enum(
                "hart0_fetch_unit",
                "Disable counter events originating from `Hart 0 Fetch Unit` client.",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart0_dcache_mmio",
                "Disable counter events originating from `Hart 0 D-Cache MMIO` client.",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart1_fetch_unit",
                "Disable counter events originating from `Hart 1 Fetch Unit` client.",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart1_icache",
                "Disable counter events originating from `Hart 1 I-Cache` client.",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart1_dcache",
                "Disable counter events originating from `Hart 1 D-Cache` client.",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart1_dcache_mmio",
                "Disable counter events originating from `Hart 1 D-Cache MMIO` client.",
                create_bit_range("[10:10]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart1_l2_prefetcher",
                "Disable counter events originating from `Hart 1 L2 Prefetcher` client.",
                create_bit_range("[11:11]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart2_fetch_unit",
                "Disable counter events originating from `Hart 2 Fetch Unit` client.",
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart2_icache",
                "Disable counter events originating from `Hart 2 I-Cache` client.",
                create_bit_range("[13:13]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart2_dcache",
                "Disable counter events originating from `Hart 2 D-Cache` client.",
                create_bit_range("[14:14]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart2_dcache_mmio",
                "Disable counter events originating from `Hart 2 D-Cache MMIO` client.",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart2_l2_prefetcher",
                "Disable counter events originating from `Hart 2 L2 Prefetcher` client.",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart3_fetch_unit",
                "Disable counter events originating from `Hart 3 Fetch Unit` client.",
                create_bit_range("[17:17]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart3_icache",
                "Disable counter events originating from `Hart 3 I-Cache` client.",
                create_bit_range("[18:18]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart3_dcache",
                "Disable counter events originating from `Hart 3 D-Cache` client.",
                create_bit_range("[19:19]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart3_dcache_mmio",
                "Disable counter events originating from `Hart 3 D-Cache MMIO` client.",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart3_l2_prefetcher",
                "Disable counter events originating from `Hart 3 L2 Prefetcher` client.",
                create_bit_range("[21:21]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart4_fetch_unit",
                "Disable counter events originating from `Hart 4 Fetch Unit` client.",
                create_bit_range("[22:22]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart4_icache",
                "Disable counter events originating from `Hart 4 I-Cache` client.",
                create_bit_range("[23:23]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart4_dcache",
                "Disable counter events originating from `Hart 4 D-Cache` client.",
                create_bit_range("[24:24]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart4_dcache_mmio",
                "Disable counter events originating from `Hart 4 D-Cache MMIO` client.",
                create_bit_range("[25:25]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "hart4_l2_prefetcher",
                "Disable counter events originating from `Hart 4 L2 Prefetcher` client.",
                create_bit_range("[26:26]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable events from the client", 0)?,
                    create_enum_value("disable", "Disable events from the client", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
