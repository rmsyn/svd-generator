use crate::svd::{create_register, create_register_properties};
use crate::Result;

/// Creates SiFive U74(MC) L2 Performance Monitor L2 Cache Control ECC Type reserved register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "_ecc_reserved",
        "L2 Cache Control ECC Type (`directory`, `data`) reserved register.",
        0x1c,
        create_register_properties(32, 0x0)?,
        None,
        None,
    )
    .map(svd::RegisterCluster::Register)
}
