use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates SiFive U74(MC) L2 Performance Monitor L2 Cache Control ECC Type Address registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "addr",
        "L2 Cache Control ECC Type Address registers.\n\nContains the low- and high-address bits of the most recent failure.",
        0x0,
        create_register_properties(32, 0x0)?,
        Some(&[
            create_field(
                "addr",
                "ECC type most recent address to fail.",
                create_bit_range("[31:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        Some(svd::DimElement::builder()
            .dim(2)
            .dim_increment(4)
            .dim_index(Some([
                "_low",
                "_high",
            ].map(String::from).into()))
            .build(svd::ValidateLevel::Strict)?),
    ).map(svd::RegisterCluster::Register)
}
