use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates SiFive U74(MC) L2 Performance Monitor L2 Cache Control ECC Error Injection register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "count",
        "L2 Cache Control ECC Type Count register.\n\nReports the number of times an ECC error occured.",
        0x8,
        create_register_properties(32, 0x0)?,
        Some(&[
            create_field(
                "count",
                "Reports the number of times an ECC error occured.",
                create_bit_range("[31:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
