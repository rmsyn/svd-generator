use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates SiFive U74(MC) L2 Performance Monitor L2 Cache Control Way Mask Bank register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "bank",
        "L2 Cache Control Way Mask bank registers.\n\nConfigures the masks to enable cache bank ways.",
        0x0,
        create_register_properties(32, 0x0)?,
        Some(&[
            create_field(
                "way_mask",
                "Way enable mask.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                Some(svd::DimElement::builder()
                    .dim(16)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None
    ).map(svd::RegisterCluster::Register)
}
