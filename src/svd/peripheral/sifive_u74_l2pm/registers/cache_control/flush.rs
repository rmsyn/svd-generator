use crate::svd::create_cluster;
use crate::Result;

pub mod flush32;
pub mod flush64;

/// Creates SiFive U74(MC) L2 Performance Monitor L2 Cache Control Directory Flush registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "flush",
        "L2 Cache Control Directory Flush registers.\n\nCan be used for flushing specific cache blocks.",
        0x200,
        &[flush64::create()?, flush32::create()?],
        None,
    ).map(svd::RegisterCluster::Cluster)
}
