use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates SiFive U74(MC) L2 Performance Monitor L2 Cache Control Flush 64-bit register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "flush64",
        "L2 Cache Control Flush 64-bit register.\n\nFlushes the cache block at the 64-bit address written.",
        0x0,
        create_register_properties(64, 0x0)?,
        Some(&[
            create_field_constraint(
                "addr",
                "64-bit address of the cache block to flush.",
                create_bit_range("[63:0]")?,
                svd::Access::WriteOnly,
                create_write_constraint(0, 0xffff_ffff_ffff_ffff)?,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
