use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates SiFive U74(MC) L2 Performance Monitor L2 Cache Control Flush 32-bit register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "flush32",
        "L2 Cache Control Flush 32-bit register.\n\nFlushes the cache block at the 32-bit address shifted left by 4 bytes.",
        0x40,
        create_register_properties(32, 0x0)?,
        Some(&[
            create_field_constraint(
                "addr",
                "32-bit address of the cache block to flush.",
                create_bit_range("[31:0]")?,
                svd::Access::WriteOnly,
                create_write_constraint(0, 0xffff_ffff)?,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
