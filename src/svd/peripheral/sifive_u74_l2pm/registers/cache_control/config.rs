use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates SiFive U74(MC) L2 Performance Monitor L2 Cache Control Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "config",
        "L2 Cache Control configuration.",
        0x0,
        create_register_properties(32, 0x060a1002)?,
        Some(&[
            create_field(
                "banks",
                "Number of banks in the cache.",
                create_bit_range("[7:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "ways",
                "Number of ways per bank.",
                create_bit_range("[15:8]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "lg_sets",
                "Base-2 logarithm of the sets per bank.",
                create_bit_range("[23:16]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "lg_block_bytes",
                "Base-2 logarithm of the sets per bank.",
                create_bit_range("[31:24]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
