use crate::svd::create_cluster;
use crate::Result;

pub mod bank;
pub mod reserved;

/// Creates SiFive U74(MC) L2 Performance Monitor L2 Cache Control Way Mask registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "way_mask",
        "L2 Cache Control Way Mask registers.\n\nConfigures the masks to enable cache bank ways.",
        0x800,
        &[bank::create()?, reserved::create()?],
        Some(
            svd::DimElement::builder()
                .dim(27)
                .dim_increment(8)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Cluster)
}
