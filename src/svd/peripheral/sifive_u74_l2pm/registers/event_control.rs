use crate::svd::create_cluster;
use crate::Result;

pub mod client_filter;
pub mod event_select;

/// Creates SiFive U74(MC) L2 Performance Monitor Event Control registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "event_control",
        "L2PM Event Control registers.",
        0x2000,
        &[event_select::create()?, client_filter::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
