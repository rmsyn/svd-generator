use crate::Result;

pub mod msip;
pub mod mtime;
pub mod mtimecmp;

/// Creates the RISC-V CLINT register definitions.
pub fn create(harts: usize) -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        msip::create(harts)?,
        mtimecmp::create(harts)?,
        mtime::create()?,
    ]
    .into())
}
