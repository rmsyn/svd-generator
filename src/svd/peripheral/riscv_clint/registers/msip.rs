use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the RISC-V CLINT MSIP register.
pub fn create(harts: usize) -> Result<svd::RegisterCluster> {
    create_register(
        "msip",
        "CLINT MSIP (Machine Software Interrupt) register",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "control",
            "",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(harts as u32)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
