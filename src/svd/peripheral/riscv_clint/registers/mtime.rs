use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the RISC-V CLINT MTIME register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "mtime",
        "CLINT MTIME (Machine Time) register",
        0xBFF8,
        create_register_properties(64, 0)?,
        Some(&[create_field_constraint(
            "cycles",
            "",
            create_bit_range("[63:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff_ffff_ffff)?,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
