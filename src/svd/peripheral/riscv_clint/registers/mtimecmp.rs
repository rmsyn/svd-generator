use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the RISC-V CLINT MTIMECMP register.
pub fn create(harts: usize) -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "mtimecmp",
        "CLINT MTIMECMP (Machine Time Compare) register",
        0x4000,
        create_register_properties(64, 0)?,
        Some(&[create_field_constraint(
            "cycles",
            "",
            create_bit_range("[63:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff_ffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(harts as u32)
                .dim_increment(0x8)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
