use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "bus_mode",
        "DMA Bus Mode",
        // Offset inside the cluster, absolute offset: 0x1000
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "sft_reset",
                "Software Reset",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "da",
                "Arbitration Scheme",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "dsl",
                "Descriptor Skip Length (in DWORDS)",
                create_bit_range("[6:2]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
            create_field(
                "atds",
                "Alternate Descriptor Size",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "pbl",
                "Programmable Burst Length",
                create_bit_range("[13:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3f)?,
                None,
            )?,
            create_field_enum(
                "rx_tx_priority_ratio",
                "RX-TX Priority Ratio",
                create_bit_range("[15:14]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("double_ratio", "Double Ratio", 0x1)?,
                    create_enum_value("triple_ratio", "Triple Ratio", 0x2)?,
                    create_enum_value("quadruple_ratio", "Quadruple Ratio", 0x3)?,
                ])?],
                None,
            )?,
            create_field(
                "fb",
                "Fixed Burst",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "rfbl",
                "RX-Programmable Burst Length",
                create_bit_range("[22:17]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3f)?,
                None,
            )?,
            create_field(
                "usp",
                "USP",
                create_bit_range("[23:23]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "maxpbl",
                "Maximum Programmable Burst Length",
                create_bit_range("[24:24]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "aal",
                "AAL",
                create_bit_range("[25:25]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "mb",
                "Mixed Burst",
                create_bit_range("[26:26]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
