use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "control",
        "DMA Control",
        // Offset in the cluster, absolute offset: 0x1018
        0x18,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "sr",
                "Start/Stop Receive",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "osf",
                "Operate on Second Frame",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "rtc_control",
                "RX TC Control",
                create_bit_range("[4:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("rtc64", "RX TC 64", 0b00)?,
                    create_enum_value("rtc32", "RX TC 32", 0b01)?,
                    create_enum_value("rtc96", "RX TC 96", 0b10)?,
                    create_enum_value("rtc128", "RX TC 128", 0b11)?,
                ])?],
                None,
            )?,
            create_field(
                "fuf",
                "FUF",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "fef",
                "FEF",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "efc",
                "EFC",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "rfa",
                "Receive Flow Control Activation - Threshold for Activating the Flow Control",
                // NOTE: this is actually a split field, for whatever reason STMICRO decided to
                // split the meaningful bits... Only bits 23,10:9 are meaningful
                // FIXME: is there a way to define a split bit range in SVD without clobbering the
                // inner bits?
                create_bit_range("[10:9]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    // NOTE: enum values shifted to put the LSB at the zero position
                    create_enum_value(
                        "act_full_minus_1k_5k",
                        "rfa_hi = 0 (bit 23): Activate Full Minus 1KB (only valid when rxfifo >= 4KB and EFC enabled). rfa_hi = 1 (bit 23): Activate Full Minus 5KB (only valid when rxfifo > 8KB and EFC enabled).",
                        0b00,
                    )?,
                    create_enum_value(
                        "act_full_minus_2k_6k",
                        "rfa_hi = 0 (bit 23): Activate Full Minus 2KB (only valid when rxfifo >= 4KB and EFC enabled). rfa_hi = 1 (bit 23): Activate Full Minus 6KB (only valid when rxfifo > 8KB and EFC enabled).",
                        0b01,
                    )?,
                    create_enum_value(
                        "act_full_minus_3k_7k",
                        "rfa_hi = 0 (bit 23): Activate Full Minus 3KB (only valid when rxfifo >= 4KB and EFC enabled). rfa_hi = 1 (bit 23): Activate Full Minus 7KB (only valid when rxfifo > 8KB and EFC enabled).",
                        0b10,
                    )?,
                    create_enum_value(
                        "act_full_minus_4k",
                        "rfa_hi = 0 (bit 23): Activate Full Minus 4KB (only valid when rxfifo > 4KB and EFC enabled). rfa_hi = 1 (bit 23): Reserved.",
                        0b11,
                    )?,
                ])?],
                None,
            )?,
            create_field_enum(
                "rfd",
                "Receive Flow Control Deactivation - Threshold for Deactivating the Flow Control",
                // NOTE: this is actually a split field, for whatever reason STMICRO decided to
                // split the meaningful bits... Only bits 22,12:11 are meaningful
                create_bit_range("[12:11]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    // NOTE: enum values shifted to put the LSB at the zero position
                    create_enum_value(
                        "deac_full_minus_1k_5k",
                        "rfd_hi = 0 (bit 22): Deactivate Full Minus 1KB (only valid when rxfifo >= 4KB and EFC enabled). rfd_hi = 1 (bit 22): Deactivate Full Minus 5KB (only valid when rxfifo > 8KB and EFC enabled).",
                        0b00,
                    )?,
                    create_enum_value(
                        "deac_full_minus_2k_6k",
                        "rfd_hi = 0 (bit 22): Deactivate Full Minus 2KB (only valid when rxfifo >= 4KB and EFC enabled). rfd_hi = 1 (bit 22): Deactivate Full Minus 6KB (only valid when rxfifo > 8KB and EFC enabled).",
                        0b01,
                    )?,
                    create_enum_value(
                        "deac_full_minus_3k_7k",
                        "rfd_hi = 0 (bit 22): Deactivate Full Minus 3KB (only valid when rxfifo >= 4KB and EFC enabled). rfd_hi = 1 (bit 22): Deactivate Full Minus 7KB (only valid when rxfifo > 8KB and EFC enabled).",
                        0b10,
                    )?,
                    create_enum_value(
                        "deac_full_minus_4k",
                        "rfd_hi = 0 (bit 22): Deactivate Full Minus 4KB (only valid when rxfifo >= 4KB and EFC enabled). rfd_hi = 1 (bit 22): Reserved.",
                        0x003,
                    )?,
                ])?],
                None,
            )?,
            create_field(
                "st",
                "Start/Stop Transmission",
                create_bit_range("[13:13]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "ttc_control",
                "TX TC Control",
                create_bit_range("[16:14]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("ttc64", "TX TC 64", 0b000)?,
                    create_enum_value("ttc128", "TX TC 128", 0b001)?,
                    create_enum_value("ttc192", "TX TC 192", 0b010)?,
                    create_enum_value("ttc256", "TX TC 256", 0b011)?,
                    create_enum_value("ttc40", "TX TC 40", 0b100)?,
                    create_enum_value("ttc32", "TX TC 32", 0b101)?,
                    create_enum_value("ttc24", "TX TC 24", 0b110)?,
                    create_enum_value("ttc16", "TX TC 16", 0b111)?,
                ])?],
                None,
            )?,
            create_field(
                "ftf",
                "Flush Transmit FIFO",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "tsf",
                "Transmit Store and Forward",
                create_bit_range("[21:21]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rfd_hi",
                "Use the high values for RFD thresholds",
                create_bit_range("[22:22]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rfa_hi",
                "Use the high values for RFA thresholds",
                create_bit_range("[23:23]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "dff",
                "Disable Flushing",
                create_bit_range("[24:24]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rsf",
                "Receive Store and Forward",
                create_bit_range("[25:25]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "dt",
                "Disable Drop TCP/IP Checksum Error",
                create_bit_range("[26:26]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
