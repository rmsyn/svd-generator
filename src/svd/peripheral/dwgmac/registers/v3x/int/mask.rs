use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC Interrupt Mask register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "mask",
        "Interrupt Mask",
        // Offset inside cluster, absolute offset: 0x3c
        0x4,
        create_register_properties(32, 0x207)?,
        Some(&[
            create_field(
                "disable_rgmii",
                "Disable RGMII Interrupt",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "disable_pcslink",
                "Disable PCSLINK Interrupt",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "disable_pcsan",
                "Disable PCSAN Interrupt",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "disable_pmt",
                "Disable PMT Interrupt",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "disable_timestamp",
                "Disable Timestamp Interrupt",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
