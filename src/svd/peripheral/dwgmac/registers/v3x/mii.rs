use crate::svd::{
    create_bit_range, create_cluster, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC MII register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "mii",
        "MII registers",
        0x10,
        &[
            svd::RegisterCluster::Register(create_register(
                "addr",
                "MII Address",
                0x0,
                create_register_properties(32, 0)?,
                Some(&[create_field_constraint(
                    "addr",
                    "MII Address",
                    create_bit_range("[31:0]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0xffff_ffff)?,
                    None,
                )?]),
                None,
            )?),
            svd::RegisterCluster::Register(create_register(
                "data",
                "MII Data",
                0x4,
                create_register_properties(32, 0)?,
                Some(&[create_field_constraint(
                    "data",
                    "MII Data",
                    create_bit_range("[31:0]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0xffff_ffff)?,
                    None,
                )?]),
                None,
            )?),
        ],
        None,
    )?))
}
