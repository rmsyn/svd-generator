use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC Flow Control register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "flow_ctrl",
        "Flow Control",
        0x18,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "fcb_bpa",
                "Flow Control Busy",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "tfe",
                "TX Flow Control Enable",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rfe",
                "RX Flow Control Enable",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "up",
                "Unicast Pause Frame Enable",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "pt_mask",
                "Pause Time Mask",
                create_bit_range("[31:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
