use crate::svd::{
    create_bit_range, create_cluster, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC Energy Efficient Ethernet (EEE) LPI register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "mmc",
        "GMAC MMC registers",
        0x100,
        &[
            svd::RegisterCluster::Register(create_register(
                "ctrl",
                "MMC Control",
                0x0,
                create_register_properties(32, 0)?,
                Some(&[create_field_constraint(
                    "ctrl",
                    "MMC Control",
                    create_bit_range("[31:0]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0xffff_ffff)?,
                    None,
                )?]),
                None,
            )?),
            svd::RegisterCluster::Register(create_register(
                "rx_intr",
                "MMC RX Interrupt",
                0x4,
                create_register_properties(32, 0)?,
                Some(&[create_field_constraint(
                    "rx_intr",
                    "MMC RX Interrupt",
                    create_bit_range("[31:0]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0xffff_ffff)?,
                    None,
                )?]),
                None,
            )?),
            svd::RegisterCluster::Register(create_register(
                "tx_intr",
                "MMC TX Interrupt",
                0x8,
                create_register_properties(32, 0)?,
                Some(&[create_field_constraint(
                    "tx_intr",
                    "MMC TX Interrupt",
                    create_bit_range("[31:0]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0xffff_ffff)?,
                    None,
                )?]),
                None,
            )?),
            svd::RegisterCluster::Register(create_register(
                "rx_csum_offload",
                "MMC RX Checksum Offload",
                0x108,
                create_register_properties(32, 0)?,
                Some(&[create_field_constraint(
                    "rx_csum_offload",
                    "MMC RX Checksum Offload",
                    create_bit_range("[31:0]")?,
                    svd::Access::ReadWrite,
                    create_write_constraint(0, 0xffff_ffff)?,
                    None,
                )?]),
                None,
            )?),
        ],
        None,
    )?))
}
