use crate::svd::create_cluster;
use crate::Result;

pub mod mask;
pub mod status;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC Interrupt register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "int",
        "Interrupt registers",
        0x38,
        &[status::create()?, mask::create()?],
        None,
    )?))
}
