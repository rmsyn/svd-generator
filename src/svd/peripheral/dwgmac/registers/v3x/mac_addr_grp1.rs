use crate::svd::{
    create_bit_range, create_cluster, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC Hardware Address Group 1 register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "mac_addr_grp1",
        "MAC Address 16-31 registers",
        0x800,
        &[svd::RegisterCluster::Register(create_register(
            "addr",
            "MAC Address",
            0x0,
            create_register_properties(32, 0)?,
            Some(&[create_field_constraint(
                "addr",
                "MAC Address",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff_ffff)?,
                None,
            )?]),
            Some(
                svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(0x4)
                    .dim_index(Some([String::from("_high"), String::from("_low")].into()))
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?)],
        Some(
            svd::DimElement::builder()
                .dim(16)
                .dim_increment(0x8)
                .dim_index(Some(
                    [
                        "_16", "_17", "_18", "_19", "_20", "_21", "_22", "_23", "_24", "_25",
                        "_26", "_27", "_28", "_29", "_30", "_31",
                    ]
                    .map(String::from)
                    .into(),
                ))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
