use crate::svd::{
    create_bit_range, create_cluster, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC Hardware Address Group 0 register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "mac_addr_grp0",
        "MAC Address 0-15 registers",
        0x40,
        &[svd::RegisterCluster::Register(create_register(
            "addr",
            "MAC Address",
            0x0,
            create_register_properties(32, 0)?,
            Some(&[create_field_constraint(
                "addr",
                "MAC Address",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff_ffff)?,
                None,
            )?]),
            Some(
                svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(0x4)
                    .dim_index(Some([String::from("_high"), String::from("_low")].into()))
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?)],
        Some(
            svd::DimElement::builder()
                .dim(16)
                .dim_increment(0x8)
                .dim_index(Some(
                    [
                        "_0", "_1", "_2", "_3", "_4", "_5", "_6", "_7", "_8", "_9", "_10", "_11",
                        "_12", "_13", "_14", "_15",
                    ]
                    .map(String::from)
                    .into(),
                ))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
