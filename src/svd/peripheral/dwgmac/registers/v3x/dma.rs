use crate::svd::create_cluster;
use crate::svd::register::dwmac_dma;
use crate::Result;

pub mod bus_mode;
pub mod control;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC DMA register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "dma",
        "DMA registers",
        0x1000,
        &[
            bus_mode::create()?,
            dwmac_dma::poll_demand::create()?,
            dwmac_dma::base_addr::create()?,
            dwmac_dma::status::create()?,
            control::create()?,
            dwmac_dma::intr_ena::create()?,
            dwmac_dma::missed_frame_ctr::create()?,
            dwmac_dma::rx_watchdog::create()?,
            dwmac_dma::axi_bus_mode::create()?,
            dwmac_dma::cur_buf_addr::create()?,
            dwmac_dma::hw_feature::create()?,
        ],
        None,
    )?))
}
