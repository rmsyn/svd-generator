use crate::svd::create_cluster;
use crate::Result;

pub mod an_ctrl;
pub mod an_status;
pub mod ane;
pub mod ane_exp;
pub mod tbi;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC PCS register definitions.
///
/// Based on the register definitions from the Linux driver: <https://elixir.bootlin.com/linux/latest/source/drivers/net/ethernet/stmicro/stmmac/stmmac_pcs.h>
///
/// Original Author: Giuseppe Cavallaro <peppe.cavallaro@st.com>
pub fn create(offset: u32) -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "pcs",
        "PCS (AN/TBI/SGMII/RGMII) registers",
        offset,
        &[
            an_ctrl::create()?,
            an_status::create()?,
            ane::create()?,
            ane_exp::create()?,
            tbi::create()?,
        ],
        None,
    )?))
}
