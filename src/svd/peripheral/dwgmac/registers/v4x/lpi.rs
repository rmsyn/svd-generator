use crate::svd::create_cluster;
use crate::Result;

pub mod ctrl_status;
pub mod entry_timer;
pub mod timer_ctrl;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx LPI register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "lpi",
        "MAC LPI Energy Efficient Ethernet (EEE) registers",
        0xd0,
        &[
            ctrl_status::create()?,
            timer_ctrl::create()?,
            entry_timer::create()?,
        ],
        None,
    )?))
}
