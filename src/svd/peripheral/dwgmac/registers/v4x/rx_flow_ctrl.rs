use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx RX Flow Control register
/// definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rx_flow_ctrl",
        "MAC RX Flow Control",
        0x90,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "rfe",
            "Receive Flow Enable",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
