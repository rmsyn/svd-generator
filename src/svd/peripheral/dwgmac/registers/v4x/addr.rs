use crate::svd::create_cluster;
use crate::Result;

pub mod high;
pub mod low;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx Hardware Address register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "addr",
        "Hardware Address registers",
        0x300,
        &[high::create()?, low::create()?],
        Some(
            svd::DimElement::builder()
                // NOTE: corresponds to GMAC_MAX_PERFECT_ADDRESSES in the Linux driver.
                .dim(128)
                .dim_increment(0x8)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
