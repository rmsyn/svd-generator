use crate::svd::create_cluster;
use crate::Result;

pub mod enable;
pub mod status;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx Interrupt register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "int",
        "MAC Interrupt registers",
        0xb0,
        &[status::create()?, enable::create()?],
        None,
    )?))
}
