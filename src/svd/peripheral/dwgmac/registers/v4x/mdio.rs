use crate::svd::create_cluster;
use crate::Result;

pub mod addr;
pub mod data;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MDIO register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "mdio",
        "MDIO registers",
        0x200,
        &[data::create()?, addr::create()?],
        None,
    )?))
}
