use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx Hardware Features 1 register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "features1",
        "Hardware Features 1",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "rxfifosize",
                "RX FIFO Size",
                create_bit_range("[4:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
            create_field_constraint(
                "txfifosize",
                "TX FIFO Size",
                create_bit_range("[10:6]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
            create_field_constraint(
                "addr64",
                "Address 64",
                create_bit_range("[15:14]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "sphen",
                "SPH Enable",
                create_bit_range("[17:17]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "tsoen",
                "TSO Enable",
                create_bit_range("[18:18]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "avsel",
                "AV Select",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "hash_tb_sz",
                "Hash Table Size",
                create_bit_range("[25:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "l3l4fnum",
                "L3 L4 FNUM",
                create_bit_range("[30:27]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
        ]),
        None,
    )?))
}
