use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx Debug register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "debug",
        "MAC Debug",
        0x114,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "rpests",
                "Receive PE Status",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "rfcfcsts",
                "RFCFC Status",
                create_bit_range("[2:1]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "tpests",
                "Transmission PE Status",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "tfcsts",
                "Transmission Flow Control Status",
                create_bit_range("[18:17]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("idle", "Transmission Flow Control Idle Status", 0b00)?,
                    create_enum_value("wait", "Transmission Flow Control Wait Status", 0b01)?,
                    create_enum_value(
                        "gen_pause",
                        "Transmission Flow Control Gen Pause Status",
                        0b10,
                    )?,
                    create_enum_value("xfer", "Transmission Flow Control Transfer Status", 0b11)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
