use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx TX Queue Priority register
/// definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "tx_queue_priority",
        "MAC TX Queue Priority - tx_queue_priority0: queue 0-3, tx_queue_priority1: queue 4-7",
        0x98,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "priority",
            "Tranmission Queue Priority",
            create_bit_range("[7:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xff)?,
            Some(
                svd::DimElement::builder()
                    .dim(4)
                    .dim_increment(0x8)
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
