use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx Interrupt Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "status",
        "MAC Interrupt Status",
        0x0,
        create_register_properties(32, 0b111)?,
        Some(&[
            create_field(
                "rgsmiis",
                "RGSMIIS",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "pcs_link",
                "PCS Link",
                create_bit_range("[1:1]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "pcs_ane",
                "PCS ANE",
                create_bit_range("[2:2]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "pcs_phy",
                "PCS PHY",
                create_bit_range("[3:3]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "pmt_en",
                "PMT Enable",
                create_bit_range("[4:4]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "lpi_en",
                "LPI Enable",
                create_bit_range("[5:5]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "tsie",
                "TSIE",
                create_bit_range("[12:12]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
