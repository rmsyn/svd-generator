use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx VLAN Hash Table register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "vlan_hash_table",
        "MAC VLAN Hash Table",
        0x58,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "vlht",
            "VLAN Hash Table",
            create_bit_range("[15:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff)?,
            None,
        )?]),
        None,
    )?))
}
