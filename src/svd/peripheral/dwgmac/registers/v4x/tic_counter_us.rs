use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx 1-microsecond TIC Counter register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "tic_counter_us",
        "MAC TIC Counter 1 microsecond",
        0xdc,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "counter",
            "TIC Counter 1 microsecond",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
