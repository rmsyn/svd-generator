use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx VLAN Tag Data register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "vlan_tag_data",
        "MAC VLAN Tag Data",
        0x54,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "vid",
                "VLAN Tag Data VID",
                create_bit_range("[15:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "ven",
                "VLAN Tag Data Enable",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "etv",
                "VLAN Tag Data ETV",
                create_bit_range("[17:17]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
