use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx VLAN Tag register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "vlan_tag",
        "MAC VLAN Tag",
        0x50,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "ob",
                "VLAN OB",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ct",
                "VLAN CT",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "ofs",
                "VLAN OFS",
                create_bit_range("[6:2]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
            create_field(
                "vid",
                "VLAN Tag VID",
                create_bit_range("[15:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "etv",
                "VLAN Tag ETV",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "dovltc",
                "VLAN Tag DOVLTC",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "evls",
                "VLAN EVLS",
                create_bit_range("[22:21]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("strip_none", "Strip none", 0b00)?,
                    create_enum_value("strip_pass", "Strip pass", 0b01)?,
                    create_enum_value("strip_fail", "Strip fail", 0b10)?,
                    create_enum_value("strip_all", "Strip all", 0b11)?,
                ])?],
                None,
            )?,
            create_field(
                "evlrxs",
                "VLAN EVLRXS",
                create_bit_range("[24:24]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "vthm",
                "VLAN VTHM",
                create_bit_range("[25:25]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "edvlp",
                "VLAN EDVLP",
                create_bit_range("[26:26]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
