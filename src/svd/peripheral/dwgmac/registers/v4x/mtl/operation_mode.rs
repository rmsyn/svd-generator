use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL Operation Mode register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "operation_mode",
        "MTL Operation Mode",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "raa",
                "RAA",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("sp", "RAA SP", 0)?,
                    create_enum_value("wsp", "RAA SP", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "schalg",
                "Scheduling Algorithm",
                create_bit_range("[6:5]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("wrr", "Schedule Algorithm WRR", 0)?,
                    create_enum_value("wfq", "Schedule Algorithm WFQ", 1)?,
                    create_enum_value("dwrr", "Schedule Algorithm DWRR", 2)?,
                    create_enum_value("sp", "Schedule Algorithm SP", 3)?,
                ])?],
                None,
            )?,
            create_field(
                "frpe",
                "FRPE",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
