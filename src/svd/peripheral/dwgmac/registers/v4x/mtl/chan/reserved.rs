use crate::svd::{create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL Channel Reserved register definition.
///
/// **NOTE**: only defined to create a contiguous memory block in generated code.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "_reserved_chan",
        "MTL Channel Reserved",
        0x3c,
        create_register_properties(32, 0)?,
        None,
        None,
    )?))
}
