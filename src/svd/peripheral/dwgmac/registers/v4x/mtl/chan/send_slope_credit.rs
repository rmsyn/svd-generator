use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL Channel Send Slope Credit register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "send_slope_credit",
        "MTL Channel Send Slope Credit",
        0x1c,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "ssc",
            "MTL Channel Send Slope Credit",
            create_bit_range("[13:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0x3fff)?,
            None,
        )?]),
        None,
    )?))
}
