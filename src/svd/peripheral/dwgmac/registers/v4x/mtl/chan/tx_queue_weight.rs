use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL Channel TX Queue Weight register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "tx_queue_weight",
        "MTL Channel TX Queue Weight",
        0x18,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "iscqw",
            "MTL Channel ISC Queue Weight",
            create_bit_range("[20:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0x1f_ffff)?,
            None,
        )?]),
        None,
    )?))
}
