use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL Channel ETS Control register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ets_ctrl",
        "MTL Channel ETS Control",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "avalg",
                "MTL Channel ETS AV Algorithm",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "cc",
                "MTL Channel ETS CC",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
