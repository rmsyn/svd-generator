use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL Channel Credit register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "credit",
        "MTL Channel Credit - credit0: High, credit1: Low",
        0x20,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "credit",
            "MTL Channel Credit",
            create_bit_range("[28:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0x1fff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some([String::from("_high"), String::from("_low")].into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
