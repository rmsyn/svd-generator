use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL TX Debug register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "tx_debug",
        "MTL TX Debug",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "paused",
                "MTL TX Paused",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "trcsts",
                "MTL Debug TX FIFO Read Controller Status",
                create_bit_range("[2:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("idle", "MTL TX FIFO Idle", 0b00)?,
                    create_enum_value("read", "MTL TX FIFO Read", 0b01)?,
                    create_enum_value("wait", "MTL TX FIFO Wait", 0b10)?,
                    create_enum_value("write", "MTL TX FIFO Write", 0b11)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
