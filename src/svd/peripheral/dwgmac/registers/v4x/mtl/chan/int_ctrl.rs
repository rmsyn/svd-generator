use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL Channel Interrupt Control register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "int_ctrl",
        "MTL Channel Interrupt Control",
        0x2c,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "rx_overflow_int",
                "MTL RX Overflow Interrupt",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rx_overflow_int_en",
                "MTL RX Overflow Interrupt Enable",
                create_bit_range("[24:24]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
