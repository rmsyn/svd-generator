use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL Channel RX OP Mode register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rx_op_mode",
        "MTL Channel RX OP Mode",
        0x30,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "rsf",
                "MTL Channel RSF",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "rfa",
                "MTL Channel RX RFA",
                create_bit_range("[13:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3f)?,
                None,
            )?,
            create_field_constraint(
                "rfd",
                "MTL Channel RX RFD",
                create_bit_range("[19:14]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3f)?,
                None,
            )?,
            create_field_enum(
                "rtc",
                "MTL Channel RTC",
                create_bit_range("[4:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("rtc64", "MTL Channel RTC 64", 0b000)?,
                    create_enum_value("rtc32", "MTL Channel RTC 32", 0b001)?,
                    create_enum_value("rtc96", "MTL Channel RTC 96", 0b010)?,
                    create_enum_value("rtc128", "MTL Channel RTC 128", 0b011)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "rqs",
                "MTL Channel RQS",
                create_bit_range("[29:20]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3ff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
