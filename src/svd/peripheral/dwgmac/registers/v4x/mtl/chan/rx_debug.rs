use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL RX Debug register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rx_debug",
        "MTL RX Debug - GMII or MII Transmit Protocol Engine Status",
        0x38,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "rwcsts",
                "MTL RX Rreceive Write Controller Status",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "rrcsts",
                "MTL Debug RX FIFO Read Controller Status",
                create_bit_range("[2:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("idle", "MTL RX Controller Idle", 0b00)?,
                    create_enum_value("rdata", "MTL RX Controller Read Data", 0b01)?,
                    create_enum_value("rstat", "MTL RX Controller Read Status", 0b10)?,
                    create_enum_value("flush", "MTL RX Controller Flush", 0b11)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "rxfsts",
                "MTL Debug RX FIFO Status",
                create_bit_range("[5:4]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("empty", "MTL RX FIFO Empty", 0b00)?,
                    create_enum_value("bt", "MTL RX FIFO Below Threshold", 0b01)?,
                    create_enum_value("at", "MTL RX FIFO At/Above Threshold", 0b10)?,
                    create_enum_value("full", "MTL RX FIFO Full", 0b11)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )?))
}
