use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL Channel TX Operation Mode register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "tx_op_mode",
        "MTL Channel TX OP Mode",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "tsf",
                "MTL Channel TSF",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "txqen_av",
                "MTL Channel TXQEN AV",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "txqen",
                "MTL Channel TXQEN",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "ttc",
                "MTL Channel TTC",
                create_bit_range("[6:4]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("ttc32", "MTL Channel TTC 32", 0b000)?,
                    create_enum_value("ttc64", "MTL Channel TTC 64", 0b001)?,
                    create_enum_value("ttc96", "MTL Channel TTC 96", 0b010)?,
                    create_enum_value("ttc128", "MTL Channel TTC 128", 0b011)?,
                    create_enum_value("ttc192", "MTL Channel TTC 192", 0b100)?,
                    create_enum_value("ttc256", "MTL Channel TTC 256", 0b101)?,
                    create_enum_value("ttc384", "MTL Channel TTC 384", 0b110)?,
                    create_enum_value("ttc512", "MTL Channel TTC 512", 0b111)?,
                ])?],
                None,
            )?,
            create_field(
                "tqs",
                "MTL Channel TQS",
                create_bit_range("[24:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
