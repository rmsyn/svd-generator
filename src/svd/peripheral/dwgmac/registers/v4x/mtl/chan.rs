use crate::svd::create_cluster;
use crate::Result;

pub mod credit;
pub mod ets_ctrl;
pub mod int_ctrl;
pub mod reserved;
pub mod rx_debug;
pub mod rx_op_mode;
pub mod send_slope_credit;
pub mod tx_debug;
pub mod tx_op_mode;
pub mod tx_queue_weight;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL Channel register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "chan",
        "MTL Channel registers",
        0x100,
        &[
            tx_op_mode::create()?,
            tx_debug::create()?,
            ets_ctrl::create()?,
            tx_queue_weight::create()?,
            send_slope_credit::create()?,
            credit::create()?,
            int_ctrl::create()?,
            rx_op_mode::create()?,
            rx_debug::create()?,
            reserved::create()?,
        ],
        Some(
            svd::DimElement::builder()
                .dim(8)
                .dim_increment(0x40)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
