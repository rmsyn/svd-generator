use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx Timestamp register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "timestamp",
        "MAC Timestamp",
        0xb20,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "auxtstrig",
                "AUX Timestamp Trigger",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "atsns",
                "AUX Timestamp Nanosecond",
                create_bit_range("[29:25]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
        ]),
        None,
    )?))
}
