use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx Hardware Address High register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "high",
        "Hardware Address High",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "addr",
                "Hardware Address High",
                create_bit_range("[15:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
            create_field_constraint(
                "dcs",
                "Hardware Address High DCS",
                create_bit_range("[18:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field(
                "ae",
                "Hardware Address High Address Enable",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
