use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx Packet Filter register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "packet_filter",
        "MAC Packet Filter",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "pr",
                "PR",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "hmc",
                "HMC",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "pm",
                "PM",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "pcf",
                "PCF",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "hpf",
                "HPF",
                create_bit_range("[10:10]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "vtfe",
                "VTFE",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ipfe",
                "IPFE",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ra",
                "RA",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
