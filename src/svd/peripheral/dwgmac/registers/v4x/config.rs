use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx Configuration register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "config",
        "MAC Configuration",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "re",
                "Receive Enable",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "te",
                "Transmit Enable",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "dcrs",
                "DCRS",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "lm",
                "Loopback Mode",
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "dm",
                "Duplex Mode",
                create_bit_range("[13:13]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "speed",
                "Ethernet Speed",
                create_bit_range("[15:14]")?,
                svd::Access::ReadWrite,
                // NOTE: FES (bit 14) and PS (bit 15) bits configure the speed
                &[create_enum_values(&[
                    create_enum_value("speed10", "Speed 10 Mbits", 0b10)?,
                    create_enum_value("speed100", "Speed 100 Mbits", 0b11)?,
                    create_enum_value("speed1000", "Speed 1000 Mbits", 0b00)?,
                    create_enum_value("speed2500", "Speed 2500 Mbits", 0b01)?,
                ])?],
                None,
            )?,
            create_field(
                "je",
                "JE",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "jd",
                "JD",
                create_bit_range("[17:17]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "be",
                "BE",
                create_bit_range("[18:18]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "acs",
                "ACS",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "p2k",
                "Packet 2KB",
                create_bit_range("[22:22]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "ipg",
                "IPG",
                create_bit_range("[26:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field(
                "ipc",
                "IPC",
                create_bit_range("[27:27]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "sarc",
                "SARC",
                create_bit_range("[30:28]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field(
                "arpen",
                "ARP Enable",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
