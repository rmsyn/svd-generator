use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx VLAN register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "vlan",
        "MAC VLAN",
        0x60,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "vlht",
                "VLAN Hash Table ID",
                create_bit_range("[15:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
            create_field_constraint(
                "vlc",
                "VLAN VLC",
                create_bit_range("[17:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "csvl",
                "VLAN CSVL",
                create_bit_range("[19:19]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "vlti",
                "VLAN VLTI",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
