use crate::svd::create_cluster;
use crate::Result;

pub mod features0;
pub mod features1;
pub mod features2;
pub mod features3;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx Hardware Features register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "hw_feat",
        "Hardware Feature registers",
        0x11c,
        &[
            features0::create()?,
            features1::create()?,
            features2::create()?,
            features3::create()?,
        ],
        None,
    )?))
}
