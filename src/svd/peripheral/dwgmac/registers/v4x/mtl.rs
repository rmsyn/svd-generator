use crate::svd::create_cluster;
use crate::Result;

pub mod chan;
pub mod int_status;
pub mod operation_mode;
pub mod rx_queue_dma;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MTL register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "mtl",
        "MTL registers",
        0xc00,
        &[
            operation_mode::create()?,
            int_status::create()?,
            rx_queue_dma::create()?,
            chan::create()?,
        ],
        None,
    )?))
}
