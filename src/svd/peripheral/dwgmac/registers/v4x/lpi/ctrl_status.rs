use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx LPI Control and Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ctrl_status",
        "MAC LPI Control and Status",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "tlpi",
                "Transmit LPI - 0: Entry, 1: Exit",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("entry", "Transmit LPI entry", 0x0)?,
                    create_enum_value("exit", "Transmit LPI exit", 0x1)?,
                ])?],
                Some(
                    svd::DimElement::builder()
                        .dim(2)
                        .dim_increment(0x1)
                        .dim_index(Some([String::from("en"), String::from("ex")].into()))
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field_enum(
                "rlpi",
                "Receive LPI - 0: Entry, 1: Exit",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("entry", "Receive LPI entry", 0x0)?,
                    create_enum_value("exit", "Receive LPI exit", 0x1)?,
                ])?],
                Some(
                    svd::DimElement::builder()
                        .dim(2)
                        .dim_increment(0x1)
                        .dim_index(Some([String::from("en"), String::from("ex")].into()))
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field(
                "lpien",
                "LPI Enable",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "pls",
                "PHY Link Status",
                create_bit_range("[17:17]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "lpitxa",
                "Enable LPI TX Automate",
                create_bit_range("[19:19]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "lpiate",
                "LPI Timer Enable",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "lpitcse",
                "LPI TX Clock Stop Enable",
                create_bit_range("[21:21]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
