use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx LPI Entry Timer register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "entry_timer",
        "MAC LPI Entry Timer",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "entry_timer",
            "LPI Entry Timer",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
