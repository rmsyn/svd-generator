use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx MDIO Data register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "data",
        "MDIO Data",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "data",
            "MDIO Data",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
