use crate::svd::create_cluster;
use crate::Result;

pub mod l3_addr;
pub mod l3l4_ctrl;
pub mod l4_addr;
pub mod reserved;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx L3/L4 Filter register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "l3l4",
        "MAC L3/L4 Filter registers",
        0x900,
        &[
            l3l4_ctrl::create()?,
            l4_addr::create()?,
            l3_addr::create()?,
            reserved::create()?,
        ],
        Some(
            svd::DimElement::builder()
                .dim(8)
                .dim_increment(0x30)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
