use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx L3/L4 Filter L3 Address register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "l3_addr",
        "L3 Filter Address",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "addr",
            "L3 Filter Address",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
