use crate::svd::{create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx L3/L4 Filter Reserved register definition.
///
/// **NOTE** Only for creating a contiguous memory block in generated code.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "_reserved_l3l4",
        "Reserved",
        0x2c,
        create_register_properties(32, 0)?,
        None,
        None,
    )?))
}
