use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v4.xx L3/L4 Filter L4 Address register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "l4_addr",
        "L4 Filter Address",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "sp",
                "L4 Filter Address SP",
                create_bit_range("[15:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
            create_field_constraint(
                "dp",
                "L4 Filter Address DP",
                create_bit_range("[31:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
