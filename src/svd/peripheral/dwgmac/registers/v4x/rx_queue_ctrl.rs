use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet MAC v4.xx RX Queue Control register
/// definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rx_queue_ctrl",
        "MAC RX Queue Control",
        0xa0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "avcpq",
                "AVCPQ",
                create_bit_range("[2:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "ptpq",
                "PTPQ",
                create_bit_range("[6:4]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "dcbcpq",
                "DCBCPQ",
                create_bit_range("[10:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "upq",
                "UPQ",
                create_bit_range("[14:12]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "mcbcq",
                "MCBCQ",
                create_bit_range("[18:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field(
                "mcbcqen",
                "MCBCQ Enable",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "tacpqe",
                "TACPQE",
                create_bit_range("[21:21]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "fprq",
                "FPRQ",
                create_bit_range("[26:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
        ]),
        Some(
            svd::DimElement::builder()
                .dim(4)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
