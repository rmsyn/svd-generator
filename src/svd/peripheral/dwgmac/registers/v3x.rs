use super::{pcs, pmt};
use crate::Result;

pub mod control;
pub mod debug;
pub mod dma;
pub mod exthash;
pub mod flow_ctrl;
pub mod frame_filter;
pub mod hash;
pub mod int;
pub mod lpi;
pub mod mac_addr_grp0;
pub mod mac_addr_grp1;
pub mod mii;
pub mod mmc;
pub mod rgsmiiis;
pub mod vlan_tag;
pub mod wakeup_filter;

/// Creates Synopsys DesignWare Gigabit Ethernet MAC v3.xx register definitions.
///
/// Based on the register definitions from the Linux driver: <https://elixir.bootlin.com/linux/latest/source/drivers/net/ethernet/stmicro/stmmac/dwmac1000.h>
///
/// Original Author: Giuseppe Cavallaro <peppe.cavallaro@st.com>
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        control::create()?,
        frame_filter::create()?,
        hash::create()?,
        mii::create()?,
        flow_ctrl::create()?,
        vlan_tag::create()?,
        debug::create()?,
        pmt::create(0x2c)?,
        wakeup_filter::create()?,
        lpi::create()?,
        int::create()?,
        mac_addr_grp0::create()?,
        pcs::create(0xc0)?,
        rgsmiiis::create()?,
        mmc::create()?,
        exthash::create()?,
        mac_addr_grp1::create()?,
        dma::create()?,
    ]
    .into())
}
