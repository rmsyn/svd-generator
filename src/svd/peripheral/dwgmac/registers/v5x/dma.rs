use crate::svd::create_cluster;
use crate::Result;

pub mod ecc_int;
pub mod safety_int_status;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx DMA register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "dma",
        "DMA registers",
        0x1080,
        &[safety_int_status::create()?, ecc_int::create()?],
        None,
    )?))
}
