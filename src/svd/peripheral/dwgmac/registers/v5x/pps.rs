use crate::svd::create_cluster;
use crate::Result;

pub mod interval;
pub mod target_time_nsec;
pub mod target_time_sec;
pub mod width;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx PPS register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "pps",
        "PPS registers",
        0xb80,
        &[
            target_time_sec::create()?,
            target_time_nsec::create()?,
            interval::create()?,
            width::create()?,
        ],
        Some(
            svd::DimElement::builder()
                .dim(8)
                .dim_increment(0x10)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
