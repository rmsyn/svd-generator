use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet v5.xx MTL PPS Control register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "pps_ctrl",
        "MTL PPS Control and Status - pps_ctrl0: channel 0-3 control, pps_ctrl1: channel 4-7 control",
        0xb70,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "cmd0",
                "MTL PPS CMD",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field(
                "en0",
                "MTL PPS EN",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "tgtmodsel0",
                "MTL PPS Target Mode Select",
                create_bit_range("[6:5]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "mcgren0",
                "MTL PPS MCGR Enable",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "cmd1",
                "MTL PPS CMD",
                create_bit_range("[11:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field(
                "en1",
                "MTL PPS EN",
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "tgtmodsel1",
                "MTL PPS Target Mode Select",
                create_bit_range("[14:13]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "mcgren1",
                "MTL PPS MCGR Enable",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "cmd2",
                "MTL PPS CMD",
                create_bit_range("[19:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field(
                "en2",
                "MTL PPS EN",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "tgtmodsel2",
                "MTL PPS Target Mode Select",
                create_bit_range("[22:21]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "mcgren2",
                "MTL PPS MCGR Enable",
                create_bit_range("[23:23]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "cmd3",
                "MTL PPS CMD",
                create_bit_range("[27:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field(
                "en3",
                "MTL PPS EN",
                create_bit_range("[28:28]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "tgtmodsel3",
                "MTL PPS Target Mode Select",
                create_bit_range("[30:29]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "mcgren3",
                "MTL PPS MCGR Enable",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        Some(svd::DimElement::builder()
            .dim(2)
            .dim_increment(0x4)
            .build(svd::ValidateLevel::Strict)?),
    )?))
}
