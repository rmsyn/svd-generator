use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx DMA Safety Interrupt register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "safety_int_status",
        "DMA Safety Interrupt Status",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "decis",
                "DMA DEC Interrupt Status - Write 1 to clear interrupt",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "deuis",
                "DMA MEU Interrupt Status - Write 1 to clear interrupt",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "mscis",
                "DMA MSC Interrupt Status - Write 1 to clear interrupt",
                create_bit_range("[28:28]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "msuis",
                "DMA MSU Interrupt Status - Write 1 to clear interrupt",
                create_bit_range("[29:29]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
