use crate::svd::create_cluster;
use crate::Result;

pub mod enable;
pub mod status;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx DMA ECC Interrupt register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "ecc_int",
        "DMA ECC Interrupt registers",
        0x4,
        &[enable::create()?, status::create()?],
        None,
    )?))
}
