use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx DMA ECC Interrupt Enable register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "enable",
        "DMA ECC Interrupt Enable",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "tceie",
            "MTL ECC TCE Interrupt Enable",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
