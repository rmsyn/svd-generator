use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx DMA ECC Interrupt Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "status",
        "DMA ECC Interrupt Status",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "tceis",
            "MTL ECC TCE Interrupt Status - Write 1 to clear interrupt",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
