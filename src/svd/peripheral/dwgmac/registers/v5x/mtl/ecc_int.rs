use crate::svd::create_cluster;
use crate::Result;

pub mod enable;
pub mod status;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx MTL ECC Interrupt register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "ecc_int",
        "MTL ECC Interrupt registers",
        0xc8,
        &[enable::create()?, status::create()?],
        None,
    )?))
}
