use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet v5.xx MTL RXP IACC Control and Status
/// register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ctrl_status",
        "MTL RXP IACC Control and Status",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "addr",
                "MTL IACC Address",
                create_bit_range("[15:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
            create_field(
                "wrrdn",
                "MTL IACC WRRDN",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rxpeiee",
                "MTL IACC RXP EIEE",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "rxpeiec",
                "MTL IACC RXP EIEC",
                create_bit_range("[22:21]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "startbusy",
                "MTL IACC Start Busy",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
