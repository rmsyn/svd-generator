use crate::svd::create_cluster;
use crate::Result;

pub mod ctrl_status;
pub mod data;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx MTL RXP register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "iacc",
        "MTL RXP IACC registers",
        0x10,
        &[ctrl_status::create()?, data::create()?],
        None,
    )?))
}
