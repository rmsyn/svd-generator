use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a Synopsys DesignWare Gigabit Ethernet v5.xx MTL RXP Control and Status register
/// definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rxp_ctrl_status",
        "MTL RXP Control and Status",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "nve",
                "MTL RXP NVE",
                create_bit_range("[7:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field_constraint(
                "npe",
                "MTL RXP NPE",
                create_bit_range("[23:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field(
                "rxpi",
                "MTL RXP Interrupt",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
