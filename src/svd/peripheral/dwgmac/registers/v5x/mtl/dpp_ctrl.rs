use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx MTL DPP Control register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "dpp_ctrl",
        "MTL DPP Control",
        0xe0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "edpp",
                "MTL DPP EDPP",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ope",
                "MTL DPP OPE",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "epsi",
                "MTL DPP EPSI",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
