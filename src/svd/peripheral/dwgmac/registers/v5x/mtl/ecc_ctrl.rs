use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx MTL ECC Control register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ecc_ctrl",
        "MTL ECC Control",
        0xc0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "mtxee",
                "MTL ECC MTX EE",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "mrxee",
                "MTL ECC MRX EE",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "mestee",
                "MTL ECC MEST EE",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "mrxpee",
                "MTL ECC MRXP EE",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "tsoee",
                "MTL ECC TSO EE",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "meeao",
                "MTL ECC MEE AO",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
