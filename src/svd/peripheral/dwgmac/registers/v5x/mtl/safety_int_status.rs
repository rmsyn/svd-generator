use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx MTL register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "safety_int_status",
        "MTL Safety Interrupt Status",
        0xc4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "mecis",
                "MTL MEC Interrupt Status - Write 1 to clear interrupt",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "meuis",
                "MTL MEU Interrupt Status - Write 1 to clear interrupt",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "mcsis",
                "MTL MCS Interrupt Status - Write 1 to clear interrupt",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
