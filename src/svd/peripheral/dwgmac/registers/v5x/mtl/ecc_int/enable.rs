use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx MTL ECC Interrupt Enable register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "enable",
        "MTL ECC Interrupt Enable",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "txceie",
                "MTL ECC TXCE Interrupt Enable",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rxceie",
                "MTL ECC RXCE Interrupt Enable",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "eceie",
                "MTL ECC ECE Interrupt Enable",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rpceie",
                "MTL ECC RPCE Interrupt Enable",
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
