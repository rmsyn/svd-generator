use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx MTL ECC Interrupt Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "status",
        "MTL ECC Interrupt Status",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "txceis",
                "MTL ECC TXCE Interrupt Status - Write 1 to clear interrupt",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rxceis",
                "MTL ECC RXCE Interrupt Status - Write 1 to clear interrupt",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "eceis",
                "MTL ECC ECE Interrupt Status - Write 1 to clear interrupt",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rpceis",
                "MTL ECC RPCE Interrupt Status - Write 1 to clear interrupt",
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
