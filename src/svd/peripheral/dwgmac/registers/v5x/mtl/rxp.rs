use crate::svd::create_cluster;
use crate::Result;

pub mod ctrl_status;
pub mod iacc;

/// Creates Synopsys DesignWare Gigabit Ethernet v5.xx MTL RXP register definitions.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "rxp",
        "MTL RXP registers",
        0xa0,
        &[ctrl_status::create()?, iacc::create()?],
        None,
    )?))
}
