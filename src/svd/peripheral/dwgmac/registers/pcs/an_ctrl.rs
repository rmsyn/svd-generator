use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC PCS Auto-Negotiation Control register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "an_ctrl",
        "Auto-Negotiation Control",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "ran",
                "Restart Auto-Negotiation",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ane",
                "Auto-Negotiation Enable",
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ele",
                "External Loopback Enable",
                create_bit_range("[14:14]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ecd",
                "Enable Comma Detect",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "lr",
                "Lock to Reference",
                create_bit_range("[17:17]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "sgmral",
                "SGMII RAL Control",
                create_bit_range("[18:18]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
