use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC PCS Auto-Negotiation Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "an_status",
        "Auto-Negotiation Status",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "ls",
                "Link Status - 0: down, 1: up",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ana",
                "Auto-Negotiation Ability",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "anc",
                "Auto-Negotiation Complete",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "es",
                "Extended Status",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
