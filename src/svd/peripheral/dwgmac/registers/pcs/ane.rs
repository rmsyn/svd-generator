use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC PCS Auto-Negotiation Extend register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ane",
        "Auto-Negotiation Extend Advertisement and Link Partner Ability",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "fd",
                "ANE Full Duplex",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "hd",
                "ANE Half Duplex",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "pse",
                "ANE Pause",
                create_bit_range("[8:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "rfe",
                "ANE RFE",
                create_bit_range("[13:12]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "ack",
                "ANE ACK",
                create_bit_range("[14:14]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some([String::from("_adv"), String::from("_lpa")].into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
