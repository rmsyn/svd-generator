use crate::Result;

use super::DwGmacVersion;

pub mod pcs;
pub mod pmt;
pub mod v3x;
pub mod v4x;
pub mod v5x;

/// Creates Synopsys DesignWare Gigabit Ethernet MAC register definitions.
pub fn create(version: DwGmacVersion) -> Result<Vec<svd::RegisterCluster>> {
    match version {
        DwGmacVersion::Version340 | DwGmacVersion::Version350 => v3x::create(),
        DwGmacVersion::Version400 | DwGmacVersion::Version410 => v4x::create(),
        DwGmacVersion::Version500 | DwGmacVersion::Version510 | DwGmacVersion::Version520 => {
            v5x::create()
        }
    }
}
