use crate::svd::{create_derived_peripheral, create_peripheral};
use crate::Result;

mod fifo_depth;
mod model;
pub mod registers;
mod version;

pub use fifo_depth::*;
pub use model::*;
pub use version::*;

/// Represents a Synopsys DesignWare MMC (compatible) peripheral.
pub struct DwMmc {
    peripheral: svd::Peripheral,
}

impl DwMmc {
    /// Creates a new [DwMmc] peripheral.
    pub fn create(
        name: &str,
        base_address: u64,
        size: u32,
        interrupt: Option<Vec<svd::Interrupt>>,
        model: DwMmcModel,
        fifo_depth: DwMmcFifoDepth,
        count: usize,
    ) -> Result<Self> {
        let full_name = format!("{name}{count}");
        let desc = format!("Synopsys DesignWare MMC ({model}): {full_name}");

        if count == 0 {
            create_peripheral(
                full_name.as_str(),
                desc.as_str(),
                base_address,
                size,
                interrupt,
                Some(registers::create(model, fifo_depth)?),
                None,
                Some("MMC".into()),
            )
        } else {
            create_derived_peripheral(
                full_name.as_str(),
                desc.as_str(),
                base_address,
                size,
                interrupt,
                "MMC",
                format!("{name}0").as_str(),
            )
        }
        .map(|peripheral| Self { peripheral })
    }

    /// Gets a reference to the SVD [`Peripheral`](svd::Peripheral).
    pub const fn peripheral(&self) -> &svd::Peripheral {
        &self.peripheral
    }

    /// Gets a mutable reference to the SVD [`Peripheral`](svd::Peripheral).
    pub fn peripheral_mut(&mut self) -> &mut svd::Peripheral {
        &mut self.peripheral
    }

    /// Converts the [DwMmc] into the inner SVD [`Peripheral`](svd::Peripheral).
    pub fn to_inner(self) -> svd::Peripheral {
        self.peripheral
    }
}
