use crate::Result;

pub mod control;
pub mod ims;
pub mod int_clear;
pub mod itcr;
pub mod itop;
pub mod load;
pub mod lock;
pub mod ris;
pub mod value;

/// Creates StarFive JH7110 WDT register definitions.
///
/// Based on the Linux driver: <https://github.com/starfive-tech/linux/blob/06ad134b6efeb393868e300058752ecfbf7258d1/drivers/watchdog/starfive-wdt.c>
///
/// Original authors:
/// - Xingyu Wu <xingyu.wu@starfivetech.com>
/// - Samin Guo <samin.guo@starfivetech.com>
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        load::create()?,
        value::create()?,
        control::create()?,
        int_clear::create()?,
        ris::create()?,
        ims::create()?,
        lock::create()?,
        itcr::create()?,
        itop::create()?,
    ]
    .into())
}
