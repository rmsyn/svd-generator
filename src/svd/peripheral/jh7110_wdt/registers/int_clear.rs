use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 WDT Interrupt Clear register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "int_clear",
        "StarFive JH7110 Watchdog Interrupt Clear register.",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "int_clear",
            "Clear interrupt, and reload the counter - 0: no-op, 1: clear/reload.",
            create_bit_range("[0:0]")?,
            svd::Access::WriteOnly,
            &[create_enum_values(&[create_enum_value(
                "clear",
                "Clear the interrupt, and reload the counter",
                1,
            )?])?],
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
