use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 WDT Raw Interrupt Status register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ris",
        "StarFive JH7110 Watchdog Raw Interrupt Status register.",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "ris",
            "Raw interrupt status from the watchdog counter.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
