use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 WDT Integration Test Operation register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "itop",
        "StarFive JH7110 Watchdog Integration Test Operation register.",
        0xf04,
        create_register_properties(32, 0xe5331aae)?,
        Some(&[
            create_field(
                "wdogres",
                "Integration test value output on WDOGRES in Integration Test mode - 0: disable, 1: enable",
                create_bit_range("[0:0]")?,
                svd::Access::WriteOnly,
                None,
            )?,
            create_field(
                "wdogint",
                "Integration test value output on WDOGINT in Integration Test mode - 0: disable, 1: enable",
                create_bit_range("[1:1]")?,
                svd::Access::WriteOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
