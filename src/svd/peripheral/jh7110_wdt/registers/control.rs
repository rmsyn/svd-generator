use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 WDT Control register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "control",
        "StarFive JH7110 Watchdog Control register.",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "reset",
                "Watchdog reset enable - 0: disable, 1: enable.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "enable",
                "Watchdog interrupt enable, WDT enable, reload counter - 0: disable/no-op, 1: enable/reload.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
