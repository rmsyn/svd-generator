use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 WDT Interrupt Masked Status register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ims",
        "StarFive JH7110 Watchdog Interrupt Masked Status register.",
        0x14,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "ims",
            "interrupt masked status from the watchdog counter.",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
