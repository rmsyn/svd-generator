use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Interrupt Status register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "status",
        "Interrupt Status Register - shows the status of the interrupts after masking. A HIGH bit indicates that a specific DMA channel interrupt request is active. You can generate the request from either the error or terminal count interrupt requests.",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "status",
                "Status of the DMA interrupts after masking",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                &[create_enum_values(&[
                    create_enum_value("clear", "DMA channel interrupt is clear", 0)?,
                    create_enum_value("active", "DMA channel interrupt is active", 1)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(8)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
