use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Interrupt Terminal Count Status register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "tc_status",
        "Interrupt Terminal Count Status Register - indicates the status of the terminal count after masking. You must use this register in conjunction with the DMACIntStatus Register if you use the combined interrupt request, DMACINTR, to request interrupts. If you use the DMACINTTC interrupt request, then you only have to read the DMACIntTCStatus Register to ascertain the source of the interrupt request.",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "tc_status",
                "Interrupt terminal count request status",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                &[create_enum_values(&[
                    create_enum_value("clear", "Terminal count interrupt is clear", 0)?,
                    create_enum_value("active", "Terminal count interrupt is active", 1)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(8)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
