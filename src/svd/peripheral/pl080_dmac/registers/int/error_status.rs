use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Interrupt Error Status register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "error_status",
        "Interrupt Error Status Register - indicates the status of the error request after masking. You must use this register in conjunction with the DMACIntStatus Register if you use the combined interrupt request, DMACINTR, to request interrupts. If you use the DMACINTERR interrupt request, then only read the DMACIntErrorStatus Register.",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "error_status",
                "Interrupt error status.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                &[create_enum_values(&[
                    create_enum_value("clear", "Interrupt error status is clear", 0)?,
                    create_enum_value("active", "Interrupt error status is active", 1)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(8)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
