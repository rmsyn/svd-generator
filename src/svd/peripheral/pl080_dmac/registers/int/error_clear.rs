use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Interrupt Error Clear register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "error_clear",
        "Interrupt Error Clear Register - clears the error interrupt requests. When writing to this register, each data bit that is HIGH causes the corresponding bit in the Status Register to be cleared. Data bits that are LOW have no effect on the corresponding bit in the register.",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "error_clear",
                "Interrupt error clear.",
                create_bit_range("[0:0]")?,
                svd::Access::WriteOnly,
                &[create_enum_values(&[
                    create_enum_value("clear", "Clears the error status (`error_status`) register", 1)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(8)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
