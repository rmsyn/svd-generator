use crate::svd::create_cluster;
use crate::Result;

pub mod error_clear;
pub mod error_status;
pub mod status;
pub mod tc_clear;
pub mod tc_status;

/// Creates ARM PL080 DMA Controller Interrupt registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "int",
        "DMAC Interrupt registers",
        0x0,
        &[
            status::create()?,
            tc_status::create()?,
            tc_clear::create()?,
            error_status::create()?,
            error_clear::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
