use crate::svd::create_cluster;
use crate::Result;

pub mod periph_id0;
pub mod periph_id1;
pub mod periph_id2;
pub mod periph_id3;

/// Creates ARM PL080 DMA Controller Peripheral ID registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "periph_id",
        "DMAC Peripheral ID registers - You can treat the registers conceptually as a 32-bit register. These read-only registers provide the following peripheral options :: PartNumber[11:0] This identifies the peripheral. The three digit product code 0x080 is used. :: Designer ID[19:12] This is the identification of the designer. ARM Limited is 0x41, (ASCII A). :: Revision[23:20] This is the revision number of the peripheral. The revision number starts from 0. :: Configuration[31:24] This is the configuration option of the peripheral.",
        0xfe0,
        &[
            periph_id0::create()?,
            periph_id1::create()?,
            periph_id2::create()?,
            periph_id3::create()?,
        ],
        None,
    ).map(svd::RegisterCluster::Cluster)
}
