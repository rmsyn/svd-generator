use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Integration Test Output 1 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "itop1",
        "DMA Integration Test Output 1 register - controls and reads the DMACCLR[15:0] output lines in test mode.",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "clr",
                "Controls and reads the DMACCLR output line in test mode.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("clear", "DMACCLR test output line is clear", 0)?,
                    create_enum_value("active", "DMACCLR test output line is active", 1)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(16)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
