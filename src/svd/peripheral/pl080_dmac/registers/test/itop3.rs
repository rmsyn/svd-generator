use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Integration Test Output 3 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "itop3",
        "DMA Integration Test Output 3 register - controls and reads the interrupt request output lines in test mode. The DMACINTR interrupt request signal combines both interrupt requests, DMACINTTC and DMACINTERR, into one interrupt request signal. Therefore, if you set either the TC or E bits, then DMACINTR is active.",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "tc",
                "You can set the DMACINTTC interrupt request to a certain value in test mode by writing to the register. A read returns the value on the output, after the test multiplexor.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "err",
                "You can set the DMACINTERR interrupt request to a certain value in test mode by writing to the register. A read returns the value on the output, after the test multiplexor.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
