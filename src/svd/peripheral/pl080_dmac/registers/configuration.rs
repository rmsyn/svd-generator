use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "configuration",
        "DMA Configuration register - configures the operation of the DMAC. You can alter the endianness of the individual AHB master interfaces by writing to the M1 and M2 bits of this register. The M1 bit enables you to alter the endianness of AHB master interface 1. The M2 bit enables you to alter the endianness of AHB master interface 2. The AHB master interfaces are set to little-endian mode on reset.",
        0x30,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "e",
                "DMAC enable - disabling the DMAC reduces power consumption.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disable", "DMAC disable", 0)?,
                    create_enum_value("enable", "DMAC enable", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "m",
                "DMAC AHB Master - 0: little-endian mode, 1: big-endian mode. This bit is reset to 0.",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("little_endian", "DMAC AHB Master endianness: little-endian", 0)?,
                    create_enum_value("big_endian", "DMAC AHB Master endianness: big-endian", 1)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(2)
                    .dim_increment(0x1)
                    .dim_index(Some([
                        "1",
                        "2",
                    ].map(String::from).into()))
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
