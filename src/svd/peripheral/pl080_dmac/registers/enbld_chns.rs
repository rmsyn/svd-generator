use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Enabled Channels register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "enbld_chns",
        "DMA Enabled Channels register - indicates the DMA channels that are enabled, as indicated by the Enable bit in the DMACCxConfiguration Register. A HIGH bit indicates that a DMA channel is enabled. A bit is cleared on completion of the DMA transfer.",
        0x1c,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "channel_status",
                "Channel enable status.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                &[create_enum_values(&[
                    create_enum_value("disabled", "DMAC channel is disabled", 0)?,
                    create_enum_value("enabled", "DMAC channel is enabled", 1)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(8)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
