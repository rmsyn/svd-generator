use crate::svd::create_cluster;
use crate::Result;

pub mod itcr;
pub mod itop1;
pub mod itop2;
pub mod itop3;

/// Creates ARM PL080 DMA Controller Test registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "test",
        "DMAC Test registers - controls and reads registers used in peripheral integration tests.",
        0x500,
        &[
            itcr::create()?,
            itop1::create()?,
            itop2::create()?,
            itop3::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
