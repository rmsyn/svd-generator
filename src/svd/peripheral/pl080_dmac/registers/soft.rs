use crate::svd::create_cluster;
use crate::Result;

pub mod burst_req;
pub mod last_burst_req;
pub mod last_single_req;
pub mod single_req;

/// Creates ARM PL080 DMA Controller Software registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "soft",
        "DMAC Software registers",
        0x20,
        &[
            burst_req::create()?,
            single_req::create()?,
            last_burst_req::create()?,
            last_single_req::create()?,
        ],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
