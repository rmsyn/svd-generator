use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Channel Configuration register defintion.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "configuration",
        "DMA Channel Configuration register",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "enable",
                "Channel enable.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disabled", "Channel disabled", 0)?,
                    create_enum_value("enabled", "Channel enabled", 1)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "src_peripheral",
                "Source peripheral. This value selects the DMA source request peripheral. This field is ignored if the source of the transfer is from memory.",
                create_bit_range("[4:1]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "dst_peripheral",
                "Destination peripheral. This value selects the DMA destination request peripheral. This field is ignored if the destination of the transfer is to memory.",
                create_bit_range("[9:6]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_enum(
                "flow_cntrl",
                "Flow control and transfer type. This value indicates the flow controller and transfer type. The flow controller can be the DMAC, the source peripheral, or the destination peripheral. The transfer type can be memory-to-memory, memory-to-peripheral, peripheral-to-memory, or peripheral-to-peripheral. **NOTE**: enum values best-guess, please report errors via an issue and/or pull-request.",
                create_bit_range("[13:11]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("dmac_mtm", "Flow Controller: DMAC, Transfer type: memory-to-memory", 0b000)?,
                    create_enum_value("dmac_mtp", "Flow Controller: DMAC, Transfer type: memory-to-peripheral", 0b001)?,
                    create_enum_value("dmac_ptm", "Flow Controller: DMAC, Transfer type: peripheral-to-memory", 0b010)?,
                    create_enum_value("dmac_ptp", "Flow Controller: DMAC, Transfer type: peripheral-to-peripheral", 0b011)?,
                    create_enum_value("source_ptm", "Flow Controller: source peripheral, Transfer type: peripheral-to-memory", 0b100)?,
                    create_enum_value("source_ptp", "Flow Controller: source peripheral, Transfer type: peripheral-to-peripheral", 0b101)?,
                    create_enum_value("destination_ptm", "Flow Controller: destination peripheral, Transfer type: peripheral-to-memory", 0b110)?,
                    create_enum_value("destination_ptp", "Flow Controller: destination peripheral, Transfer type: peripheral-to-peripheral", 0b111)?,
                ])?],
                None,
            )?,
            create_field(
                "iem",
                "Interrupt error mask. When cleared, this bit masks out the error interrupt of the relevant channel.",
                create_bit_range("[14:14]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "itc",
                "Terminal count interrupt mask. When cleared, this bit masks out the terminal count interrupt of the relevant channel.",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "lock",
                "Lock. When set, this bit enables locked transfers. For details of how lock control works.",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("unlock", "Disable locked transfers", 0)?,
                    create_enum_value("lock", "Enable locked transfers", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "active",
                "Active channel FIFO data.",
                create_bit_range("[17:17]")?,
                svd::Access::ReadOnly,
                &[create_enum_values(&[
                    create_enum_value("no_data", "The channel FIFO has no data", 0)?,
                    create_enum_value("data", "The channel FIFO has data", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "halt",
                "Halt - the contents of the channels FIFO are drained. You can use this value with the Active and Channel Enable bits to cleanly disable a DMA channel.",
                create_bit_range("[18:18]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Enable DMA requests", 0)?,
                    create_enum_value("ignore", "Ignore extra source DMA requests", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
