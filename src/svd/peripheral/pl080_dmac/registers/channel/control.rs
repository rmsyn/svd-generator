use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Channel Control register defintion.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "control",
        "DMA Channel Control register",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "transfer_size",
                "Transfer size. A write to this field sets the size of the transfer when the DMAC is the flow controller. This value counts down from the original value to zero, and so its value indicates the number of transfers left to complete. A read from this field provides the number of transfers still to be completed on the destination bus. Reading the register when the channel is active does not give useful information because by the time the software has processed the value read, the channel might have progressed. Only use it when a channel is enabled, and then disabled. The ARM PrimeCell DMA Controller (PL080) Design Manual provides more information about the use of this field. Program the transfer size value to zero if the DMAC is not the flow controller. If you program the TransferSize to a non-zero value, the DMAC might attempt to use this value instead of ignoring the TransferSize.",
                create_bit_range("[11:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xfff)?,
                None,
            )?,
            create_field_enum(
                "sb_size",
                "Source burst size - indicates the number of transfers that make up a source burst. You must set this value to the burst size of the source peripheral, or if the source is memory, to the memory boundary size. The burst size is the amount of data that is transferred when the DMACxBREQ signal goes active in the source peripheral. The burst size is not related to the AHB HBURST signal.",
                create_bit_range("[14:12]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("burst1", "Source burst size: 1", 0)?,
                    create_enum_value("burst4", "Source burst size: 4", 1)?,
                    create_enum_value("burst8", "Source burst size: 8", 2)?,
                    create_enum_value("burst16", "Source burst size: 16", 3)?,
                    create_enum_value("burst32", "Source burst size: 32", 4)?,
                    create_enum_value("burst64", "Source burst size: 64", 5)?,
                    create_enum_value("burst128", "Source burst size: 128", 6)?,
                    create_enum_value("burst256", "Source burst size: 256", 7)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "db_size",
                "Destination burst size - indicates the number of transfers that make up a destination burst transfer request. You must set this value to the burst size of the destination peripheral, or if the destination is memory, to the memory boundary size. The burst size is the amount of data that is transferred when the DMACxBREQ signal goes active in the destination peripheral. The burst size is not related to the AHB HBURST signal.",
                create_bit_range("[17:15]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("burst1", "Destination burst size: 1", 0)?,
                    create_enum_value("burst4", "Destination burst size: 4", 1)?,
                    create_enum_value("burst8", "Destination burst size: 8", 2)?,
                    create_enum_value("burst16", "Destination burst size: 16", 3)?,
                    create_enum_value("burst32", "Destination burst size: 32", 4)?,
                    create_enum_value("burst64", "Destination burst size: 64", 5)?,
                    create_enum_value("burst128", "Destination burst size: 128", 6)?,
                    create_enum_value("burst256", "Destination burst size: 256", 7)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "src_width",
                "Source transfer width - transfers wider than the AHB master bus width are illegal. The source and destination widths can be different from each other. The hardware automatically packs and unpacks the data when required.",
                create_bit_range("[20:18]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("bit8", "Source transfer width: 8-bit", 0)?,
                    create_enum_value("bit16", "Source transfer width: 16-bit", 1)?,
                    create_enum_value("bit32", "Source transfer width: 32-bit", 2)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "dst_width",
                "Destination transfer width - transfers wider than the AHB master bus width are illegal. The source and destination widths can be different from each other. The hardware automatically packs and unpacks the data when required.",
                create_bit_range("[23:21]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("bit8", "Destination transfer width: 8-bit", 0)?,
                    create_enum_value("bit16", "Destination transfer width: 16-bit", 1)?,
                    create_enum_value("bit32", "Destination transfer width: 32-bit", 2)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "src",
                "Source AHB master select.",
                create_bit_range("[24:24]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("ahb1_master", "Source AHB select: AHB master 1 selected", 0)?,
                    create_enum_value("ahb2_master", "Source AHB select: AHB master 2 selected", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "dst",
                "Destination AHB master select.",
                create_bit_range("[25:25]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("ahb_master1", "Destination AHB select: AHB master 1 selected", 0)?,
                    create_enum_value("ahb_master2", "Destination AHB select: AHB master 2 selected", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "src_inc",
                "Source increment. When set, the source address is incremented after each transfer.",
                create_bit_range("[26:26]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("no_increment", "Source address is not incremented after each transfer", 0)?,
                    create_enum_value("increment", "Source address is incremented after each transfer", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "dst_inc",
                "Destination increment. When set, the destination address is incremented after each transfer.",
                create_bit_range("[27:27]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("no_increment", "Destination address is not incremented after each transfer", 0)?,
                    create_enum_value("increment", "Destination address is incremented after each transfer", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "prot_user",
                "Protection - indicates whether the access is in User or Privileged mode. This bit controls the AHB HPROT[1] signal.",
                create_bit_range("[28:28]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("user", "Protection mode: user", 0)?,
                    create_enum_value("privileged", "Protection mode: privileged", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "prot_buf",
                "Protection - indicates whether or not the access can be buffered. This bit indicates whether or not the access is bufferable. For example, you can use this bit to indicate to an AMBA bridge that the read can complete in zero wait states on the source bus without waiting for it to arbitrate for the destination bus and for the slave to accept the data. This bit controls the AHB HPROT[2] signal.",
                create_bit_range("[29:29]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("non_bufferable", "Protection buffer mode: non-bufferable", 0)?,
                    create_enum_value("bufferable", "Protection buffer mode: bufferable", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "prot_cache",
                "Protection - 0: non-cacheable, 1: cacheable. This bit indicates whether or not the access is cacheable. For example, you can use this bit to indicate to an AMBA bridge that when it saw the first read of a burst of eight it can transfer the whole burst of eight reads on the destination bus, rather than pass the transactions through one at a time. This bit controls the AHB HPROT[3] signal.",
                create_bit_range("[30:30]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("non_cacheable", "Protection cache mode: non-cacheable", 0)?,
                    create_enum_value("cacheable", "Protection cache mode: cacheable", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "ie",
                "Terminal count interrupt enable bit. It controls whether the current LLI is expected to trigger the terminal count interrupt.",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disable", "Terminal count interrupt: disabled", 0)?,
                    create_enum_value("enable", "Terminal count interrupt: enabled", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
