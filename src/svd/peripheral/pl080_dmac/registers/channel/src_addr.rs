use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Channel Source Address register defintion.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "src_addr",
        "DMAC Source Address register - contain the current source address, byte-aligned, of the data to be transferred. Software programs each register directly before the appropriate channel is enabled.",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "src_addr",
                "DMA source address.",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff_ffff)?,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
