use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Channel Linked List Item register defintion.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "lli",
        "DMA Linked List Item register",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "lm",
                "AHB master select for loading the next LLI.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("ahb_master1", "Next LLI AHB select: AHB master 1 selected", 0)?,
                    create_enum_value("ahb_master2", "Next LLI AHB select: AHB master 2 selected", 1)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "lli",
                "Linked list item. Bits [31:2] of the address for the next LLI. Address bits [1:0] are 0.",
                create_bit_range("[31:2]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3fff_ffff)?,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
