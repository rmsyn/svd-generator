use crate::svd::register::{create_register, create_register_properties};
use crate::Result;

/// Creates ARM PL080 DMA Controller Channel Reserved register defintion.
///
/// **NOTE**: This register is only defined to create a contiguous block in generated code. It is
/// not meant to be accessed by users.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "_reserved_channel",
        "Reserved Channel - not meant for actual use.",
        0x1c,
        create_register_properties(32, 0)?,
        None,
        None,
    )
    .map(svd::RegisterCluster::Register)
}
