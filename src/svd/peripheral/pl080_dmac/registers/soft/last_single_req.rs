use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Software Software Last Single Request register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "last_single_req",
        "Software Last Single Request Register - enables software to generate DMA last single requests. You can generate a DMA request for each source by writing a 1 to the corresponding register bit. A register bit is cleared when the transaction has completed. Writing 0 to this register has no effect. Reading the register indicates the sources that are requesting last single DMA transfers. You can generate a request from either a peripheral or the software request register.",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "last_single_req",
                "Software last single request.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("nop", "Software last single request, read: none, write: no-op", 0)?,
                    create_enum_value("generate", "Software last single request, read: active request, write: generate a request", 1)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(16)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
