use crate::svd::register::create_cluster;
use crate::Result;

pub mod configuration;
pub mod control;
pub mod dst_addr;
pub mod lli;
pub mod reserved;
pub mod src_addr;

/// Creates ARM PL080 DMA Controller Channel registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "channel",
        "DMAC Channel registers",
        0x100,
        &[
            src_addr::create()?,
            dst_addr::create()?,
            lli::create()?,
            control::create()?,
            configuration::create()?,
            reserved::create()?,
        ],
        Some(
            svd::DimElement::builder()
                .dim(8)
                .dim_increment(0x20)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Cluster)
}
