use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates ARM PL080 DMA Controller Peripheral ID 2 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "periph_id2",
        "DMA Peripheral ID 2 register - is hard-coded and the fields in the register determine the reset value.",
        0x8,
        create_register_properties(32, 0x14)?,
        Some(&[
            create_field(
                "designer1",
                "These bits read back as 0x4",
                create_bit_range("[3:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "revision",
                "These bits read back as 0x1",
                create_bit_range("[7:4]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
