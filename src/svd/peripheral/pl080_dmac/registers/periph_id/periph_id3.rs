use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Peripheral ID 3 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "periph_id3",
        "DMA Peripheral ID 3 register - is hard-coded and the fields in the register determine the reset value.",
        0xc,
        create_register_properties(32, 0x6)?,
        Some(&[
            create_field_enum(
                "num_chan",
                "Indicates the number of channels - this peripheral is set to 8 channels (`0b010`).",
                create_bit_range("[2:0]")?,
                svd::Access::ReadOnly,
                &[create_enum_values(&[
                    create_enum_value("channels2", "Number of channels: 2", 0b000)?,
                    create_enum_value("channels4", "Number of channels: 4", 0b001)?,
                    create_enum_value("channels8", "Number of channels: 8", 0b010)?,
                    create_enum_value("channels16", "Number of channels: 16", 0b011)?,
                    create_enum_value("channels32", "Number of channels: 32", 0b100)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "num_ahb",
                "Indicates the number of AHB masters.",
                create_bit_range("[3:3]")?,
                svd::Access::ReadOnly,
                &[create_enum_values(&[
                    create_enum_value("ahb1", "Number of AHB interfaces: 1", 0)?,
                    create_enum_value("ahb2", "Number of AHB interfaces: 2", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "ahb_bus_width",
                "Indicates the AHB bus width - this peripheral is set to 32-bit (`0b000`).",
                create_bit_range("[6:4]")?,
                svd::Access::ReadOnly,
                &[create_enum_values(&[
                    create_enum_value("bit32", "AHB bus width: 32-bit", 0b000)?,
                    create_enum_value("bit64", "AHB bus width: 64-bit", 0b001)?,
                    create_enum_value("bit128", "AHB bus width: 128-bit", 0b010)?,
                    create_enum_value("bit256", "AHB bus width: 256-bit", 0b011)?,
                    create_enum_value("bit512", "AHB bus width: 512-bit", 0b100)?,
                    create_enum_value("bit1024", "AHB bus width: 1024-bit", 0b101)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "num_src_req",
                "Indicates the number of DMA source requestors for the DMAC configuration - this peripheral is set to 16 requestors (`0`).",
                create_bit_range("[7:7]")?,
                svd::Access::ReadOnly,
                &[create_enum_values(&[
                    create_enum_value("req16", "Number of DMA source requestors: 16", 0)?,
                    create_enum_value("req32", "Number of DMA source requestors: 32", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
