use crate::svd::create_cluster;
use crate::Result;

pub mod pcell_id0;
pub mod pcell_id1;
pub mod pcell_id2;
pub mod pcell_id3;

/// Creates ARM PL080 DMA Controller Primecell ID registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "pcell_id",
        "DMAC PrimeCell ID registers - You can treat the registers conceptually as a 32-bit register. The register is a standard cross-peripheral identification system. The DMACPCellID Register is set to 0xB105F00D.",
        0xff0,
        &[
            pcell_id0::create()?,
            pcell_id1::create()?,
            pcell_id2::create()?,
            pcell_id3::create()?,
        ],
        None,
    ).map(svd::RegisterCluster::Cluster)
}
