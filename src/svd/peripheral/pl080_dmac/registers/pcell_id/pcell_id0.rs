use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Primecell ID 0 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "pcell_id0",
        "DMA PrimeCell ID 0 register - is hard-coded and the fields in the register determine the reset value.",
        0x0,
        create_register_properties(32, 0x0D)?,
        Some(&[
            create_field(
                "pcell_id",
                "These bits read back as 0x0D",
                create_bit_range("[7:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
