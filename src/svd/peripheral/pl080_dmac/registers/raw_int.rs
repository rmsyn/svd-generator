use crate::svd::create_cluster;
use crate::Result;

pub mod error_status;
pub mod tc_status;

/// Creates ARM PL080 DMA Controller Raw Interrupt registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "raw_int",
        "DMAC Raw Interrupt registers",
        0x14,
        &[tc_status::create()?, error_status::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
