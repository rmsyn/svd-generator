use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates ARM PL080 DMA Controller Synchronization register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sync",
        "DMA Synchronization register - enables or disables synchronization logic for the DMA request signals. A bit set to 0 enables the synchronization logic for a particular group of DMA requests. A bit set to 1 disables the synchronization logic for a particular group of DMA requests. This register is reset to 0, and synchronization logic enabled.",
        0x34,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_enum(
                "sync",
                "DMAC Sync - synchronization logic for DMA request signals enabled or disabled. A LOW bit indicates that the synchronization logic for the request signals is enabled. A HIGH bit indicates that the synchronization logic is disabled.",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("enable", "Indicates that the synchronization logic for the request signals is enabled", 0)?,
                    create_enum_value("disable", "Indicates that the synchronization logic for the request signals is disabled", 1)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(16)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
