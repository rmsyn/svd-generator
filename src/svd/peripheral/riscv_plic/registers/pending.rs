use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the RISC-V PLIC Pending register.
pub fn create(interrupts: usize) -> Result<svd::RegisterCluster> {
    let num_elem = interrupts.saturating_div(32) as u32 + (interrupts % 32 != 0) as u32;

    create_register(
        "pending",
        "RISC-V PLIC Pending: 32-bit register indicating if there is a pending interrupt, e.g. pending[0][0] is interrupt 0, pending[0][31] is interrupt 31, pending[1][0] is interrupt 32.",
        0x1000,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
                "pending",
                "Bit index that indicates whether the interrupt source is pending",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("clear", "The interrupt source has no pending interrupt", 0)?,
                    create_enum_value("pending", "The interrupt source has a pending interrupt", 1)?,
                ])?],
                Some(svd::DimElement::builder()
                    .dim(32)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?),
            )?,
        ]),
        Some(svd::DimElement::builder()
            .dim(num_elem)
            .dim_increment(4)
            .build(svd::ValidateLevel::Strict)?),
    ).map(svd::RegisterCluster::Register)
}
