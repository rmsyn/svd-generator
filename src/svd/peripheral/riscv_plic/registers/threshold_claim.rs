use crate::svd::create_cluster;
use crate::Result;

pub mod claim_complete;
pub mod reserved;
pub mod threshold;

/// Creates the RISC-V Threshold Claim registers.
pub fn create(harts: usize) -> Result<svd::RegisterCluster> {
    create_cluster(
        "threshold_claim",
        "PLIC threshold and claim_complete registers",
        0x200000,
        &[
            threshold::create()?,
            claim_complete::create()?,
            reserved::create()?,
        ],
        Some(
            svd::DimElement::builder()
                .dim(harts as u32)
                .dim_increment(0x1000)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Cluster)
}
