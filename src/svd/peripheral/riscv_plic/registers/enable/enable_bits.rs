use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the RISC-V PLIC Enable Bits register.
pub fn create(num_elem: u32) -> Result<svd::RegisterCluster> {
    create_register(
        "enable_bits",
        "Interrupt source enable bits.",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "enable",
            "Enables the interrupt source associated with the bit index.",
            create_bit_range("[0:0]")?,
            svd::Access::ReadWrite,
            Some(
                svd::DimElement::builder()
                    .dim(32)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(num_elem)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
