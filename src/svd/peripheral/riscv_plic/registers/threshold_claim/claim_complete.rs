use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the RISC-V PLIC Threshold Claim Complete register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
            "claim_complete",
            "Interrupt source `claim` (read) and complete (write) register. The PLIC will write pending interrupt source information to the `claim` register. When the interrupt handler is finished, the interrupt source idendification should be written to the corresponding `complete` register.",
            0x4,
            create_register_properties(32, 0)?,
            Some(&[
                create_field(
                    "claim",
                    "Claim interrupt source ID of highest-priority pending interrupt. Value of `0` indicates no pending interrupt.",
                    create_bit_range("[31:0]")?,
                    svd::Access::ReadOnly,
                    None,
                )?,
                create_field_constraint(
                    "complete",
                    "Completes handling for interrupt source ID.",
                    create_bit_range("[31:0]")?,
                    svd::Access::WriteOnly,
                    create_write_constraint(0, 0xffff_ffff)?,
                    None,
                )?,
            ]),
            None,
    ).map(svd::RegisterCluster::Register)
}
