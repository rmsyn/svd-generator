use crate::svd::{create_register, create_register_properties};
use crate::Result;

/// Creates the RISC-V PLIC Threshold Reserved register.
///
/// **NOTE**: this register is not intended for actual use, and only exists for alignment purposes.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "_threshold_reserved",
        "PLIC Threshold Reserved register used for alignment.",
        0xffc,
        create_register_properties(32, 0)?,
        None,
        None,
    )
    .map(svd::RegisterCluster::Register)
}
