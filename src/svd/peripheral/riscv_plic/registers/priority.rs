use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the RISC-V PLIC Priority register.
pub fn create(interrupts: usize) -> Result<svd::RegisterCluster> {
    create_register(
        "priority",
        "RISC-V PLIC Interrupt Source Priority.",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
                "priority",
                "Represents interrupt source priority: `0` is reserved to mean `never interrupt`, and interrupt priority increases with increasing integer values.",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff_ffff)?,
                None,
            )?,
        ]),
        Some(svd::DimElement::builder()
            .dim(interrupts as u32)
            .dim_increment(4)
            .build(svd::ValidateLevel::Strict)?),
    ).map(svd::RegisterCluster::Register)
}
