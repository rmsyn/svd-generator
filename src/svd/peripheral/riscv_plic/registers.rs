use crate::Result;

pub mod enable;
pub mod pending;
pub mod priority;
pub mod threshold_claim;

/// Creates the RISC-V PLIC register definitions.
pub fn create(harts: usize, interrupts: usize) -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        priority::create(interrupts)?,
        pending::create(interrupts)?,
        enable::create(harts, interrupts)?,
        threshold_claim::create(harts)?,
    ]
    .into())
}
