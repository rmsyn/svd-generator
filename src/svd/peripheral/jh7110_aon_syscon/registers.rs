use crate::Result;

pub mod aon_syscfg_0;
pub mod aon_syscfg_1;
pub mod aon_syscfg_10;
pub mod aon_syscfg_2;
pub mod aon_syscfg_3;
pub mod aon_syscfg_4;
pub mod aon_syscfg_5;
pub mod aon_syscfg_6;
pub mod aon_syscfg_7;
pub mod aon_syscfg_8;
pub mod aon_syscfg_9;

/// Creates StarFive JH7110 AON Syscon (compatible) register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        aon_syscfg_0::create()?,
        aon_syscfg_1::create()?,
        aon_syscfg_2::create()?,
        aon_syscfg_3::create()?,
        aon_syscfg_4::create()?,
        aon_syscfg_5::create()?,
        aon_syscfg_6::create()?,
        aon_syscfg_7::create()?,
        aon_syscfg_8::create()?,
        aon_syscfg_9::create()?,
        aon_syscfg_10::create()?,
    ]
    .into())
}
