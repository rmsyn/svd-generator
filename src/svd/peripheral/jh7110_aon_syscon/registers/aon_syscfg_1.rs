use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the StarFive JH7110 AON Syscon SYSCFG 1 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "aon_syscfg_1",
        "AON SYSCONSAIF SYSCFG 4",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "u0_boot_ctrl_boot_status",
            "",
            create_bit_range("[3:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
