use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the StarFive JH7110 AON Syscon Boot SYSCFG 2 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "aon_syscfg_2",
        "AON SYSCONSAIF SYSCFG 8",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "u0_boot_ctrl_boot_vector_0_31",
            "Boot vectors 0-31 (little-endian)",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
