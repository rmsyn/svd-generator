use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the StarFive JH7110 AON Syscon SYSCFG 4 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "aon_syscfg_4",
        "AON SYSCONSAIF SYSCFG 16",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "gmac5_axi64_ptp_timestamp_o_0_31",
            "GMAC5 PTP timestamps 0-31 (little-endian)",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )?))
}
