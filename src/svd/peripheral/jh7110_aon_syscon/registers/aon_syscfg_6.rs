use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the StarFive JH7110 AON Syscon SYSCFG 6 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "aon_syscfg_6",
        "AON SYSCONSAIF SYSCFG 24",
        0x18,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "u0_otpc_chip_mode",
                "",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_otpc_crc_pass",
                "",
                create_bit_range("[1:1]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_otpc_dbg_enable",
                "",
                create_bit_range("[2:2]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
