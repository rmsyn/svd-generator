use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the StarFive JH7110 AON Syscon SYSCFG 10 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "aon_syscfg_10",
        "AON SYSCONSAIF SYSCFG 40",
        0x28,
        create_register_properties(32, 0x20)?,
        Some(&[
            create_field(
                "u0_otpc_fl_sec_boot_lmt",
                "",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_otpc_fl_xip",
                "",
                create_bit_range("[1:1]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_otpc_load_busy",
                "",
                create_bit_range("[2:2]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_reset_ctrl_clr_reset_status",
                "",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_reset_ctrl_pll_timecnt_finish",
                "",
                create_bit_range("[4:4]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "u0_reset_ctrl_rstn_sw",
                "",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "u0_reset_ctrl_sys_reset_status",
                "",
                create_bit_range("[9:6]")?,
                svd::Access::ReadOnly,
                None,
            )?,
        ]),
        None,
    )?))
}
