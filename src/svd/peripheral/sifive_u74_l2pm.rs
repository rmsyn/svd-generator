use crate::svd::create_peripheral;
use crate::Result;

pub mod registers;

/// Represents a SiFive U74(MC) L2 Performance Monitor memory region.
pub struct SiFiveU74L2pm {
    peripheral: svd::Peripheral,
}

impl SiFiveU74L2pm {
    /// Creates a new [SiFiveU74L2pm] peripheral.
    pub fn create(
        name: &str,
        base_address: u64,
        size: u32,
        interrupt: Option<Vec<svd::Interrupt>>,
        dim: u32,
    ) -> Result<Self> {
        let dim_element = if dim > 0 {
            Some(
                svd::DimElement::builder()
                    .dim(dim)
                    .dim_increment(size)
                    .build(svd::ValidateLevel::Strict)?,
            )
        } else {
            None
        };

        let peripheral = create_peripheral(
            name,
            format!("SiFive U74(MC) L2 Performance Monitor: {name}").as_str(),
            base_address,
            size,
            interrupt,
            Some(registers::create()?),
            dim_element,
            Some("L2PM".into()),
        )?;

        Ok(Self { peripheral })
    }

    /// Gets a reference to the SVD [`Peripheral`](svd::Peripheral).
    pub const fn peripheral(&self) -> &svd::Peripheral {
        &self.peripheral
    }

    /// Gets a mutable reference to the SVD [`Peripheral`](svd::Peripheral).
    pub fn peripheral_mut(&mut self) -> &mut svd::Peripheral {
        &mut self.peripheral
    }

    /// Converts the [SiFiveU74L2pm] into the inner SVD [`Peripheral`](svd::Peripheral).
    pub fn to_inner(self) -> svd::Peripheral {
        self.peripheral
    }
}
