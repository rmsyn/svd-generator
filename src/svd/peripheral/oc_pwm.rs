use crate::svd::create_peripheral;
use crate::Result;

pub mod registers;

/// Represents an Opencores PTC PWM v1 (compatible) peripheral
///
/// # Documentation
///
/// - Project page: <https://opencores.org/projects/ptc>
/// - Peripheral specification: <https://opencores.org/websvn/filedetails?repname=ptc&path=%2Fptc%2Ftags%2Fa%2Fdoc%2Fptc_spec.pdf>
pub struct OcPwm {
    peripheral: svd::Peripheral,
}

impl OcPwm {
    /// Creates a new [OcPwm] peripheral.
    pub fn create(
        name: &str,
        base_address: u64,
        size: u32,
        interrupt: Option<Vec<svd::Interrupt>>,
        dim: u32,
    ) -> Result<Self> {
        let dim_element = if dim > 0 {
            Some(
                svd::DimElement::builder()
                    .dim(dim)
                    .dim_increment(size)
                    .build(svd::ValidateLevel::Strict)?,
            )
        } else {
            None
        };

        let peripheral = create_peripheral(
            name,
            format!("Opencores PTC PWM v1: {name}").as_str(),
            base_address,
            size,
            interrupt,
            Some(registers::create()?),
            dim_element,
            Some("PWM".into()),
        )?;

        Ok(Self { peripheral })
    }

    /// Gets a reference to the SVD [`Peripheral`](svd::Peripheral).
    pub const fn peripheral(&self) -> &svd::Peripheral {
        &self.peripheral
    }

    /// Gets a mutable reference to the SVD [`Peripheral`](svd::Peripheral).
    pub fn peripheral_mut(&mut self) -> &mut svd::Peripheral {
        &mut self.peripheral
    }

    /// Converts the [OcPwm] into the inner SVD [`Peripheral`](svd::Peripheral).
    pub fn to_inner(self) -> svd::Peripheral {
        self.peripheral
    }
}
