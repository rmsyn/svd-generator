use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the SSPCR1 control register 1.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ssp_cr1",
        "SSPCR1 is the control register 1 and contains four different bit fields, that control various functions within the PrimeCell SSP.",
        0x04,
        create_register_properties(16, 0)?,
        Some(&[
            create_field_enum(
                "lbm",
                "Loop back mode",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("normal", "Loop back mode: normal serial port operation", 0)?,
                    create_enum_value("shifter", "Loop back mode: output of transmit serial shifter is connected to input of receive serial shifter internally", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "sse",
                "Synchronous serial port enable",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("disabled", "Synchronous serial port: disabled", 0)?,
                    create_enum_value("enabled", "Synchronous serial port: enabled", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "ms",
                "Master or slave mode select. This bit can be modified only when the PrimeCell SSP is disabled, SSE=0.",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("master", "Mode select: master", 0)?,
                    create_enum_value("slave", "Mode select: slave", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "sod",
                "Slave-mode output disable. This bit is relevant only in the slave mode, MS=1. In multiple-slave systems, it is possible for an PrimeCell SSP master to broadcast a message to all slaves in the system while ensuring that only one slave drives data onto its serial output line. In such systems the RXD lines from multiple slaves could be tied together. To operate in such systems, the SOD bit can be set if the PrimeCell SSP slave is not supposed to drive the SSPTXD line - 0: SSP can drive the SSPTXD output in slave mode, 1: SSP must not drive the SSPTXD output in slave mode",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("drive", "SSPTXD output disable: SSP can drive the SSPTXD output", 0)?,
                    create_enum_value("no_drive", "SSPTXD output disable: SSP must not drive the SSPTXD output", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
