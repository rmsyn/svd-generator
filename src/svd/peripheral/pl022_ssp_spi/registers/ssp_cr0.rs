use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the SSPCR0 control register 0.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ssp_cr0",
        "SSPCR0 is control register 0 and contains five bit fields that control various functions within the PrimeCell SSP.",
        0x00,
        create_register_properties(16, 0)?,
        Some(&[
            create_field_enum(
                "dss",
                "Data Size Select",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("four", "Data size select: 4-bit", 0b0011)?,
                    create_enum_value("five", "Data size select: 5-bit", 0b0100)?,
                    create_enum_value("six", "Data size select: 6-bit", 0b0101)?,
                    create_enum_value("seven", "Data size select: 7-bit", 0b0110)?,
                    create_enum_value("eight", "Data size select: 8-bit", 0b0111)?,
                    create_enum_value("nine", "Data size select: 9-bit", 0b1000)?,
                    create_enum_value("ten", "Data size select: 10-bit", 0b1001)?,
                    create_enum_value("eleven", "Data size select: 11-bit", 0b1010)?,
                    create_enum_value("twelve", "Data size select: 12-bit", 0b1011)?,
                    create_enum_value("thirteen", "Data size select: 13-bit", 0b1100)?,
                    create_enum_value("fourteen", "Data size select: 14-bit", 0b1101)?,
                    create_enum_value("fifteen", "Data size select: 15-bit", 0b1110)?,
                    create_enum_value("sixteen", "Data size select: 16-bit", 0b1111)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "frf",
                "Frame format - 00: Motorola SPI frame format, 01: TI synchronous serial frame format, 10: National Microwire frame format, 11: Reserved",
                create_bit_range("[5:4]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("spi", "Frame format: Motorola SPI", 0b00)?,
                    create_enum_value("sync_serial", "Frame format: TI synchronous serial", 0b01)?,
                    create_enum_value("microwire", "Frame format: National Microwire", 0b10)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "spo",
                "SSPCLKOUT polarity, applicable to Motorola SPI frame format only.",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("low", "SSPCLKOUT polarity: steady state low", 0)?,
                    create_enum_value("high", "SSPCLKOUT polarity: steady state high", 1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "sph",
                "SSPCLKOUT phase, applicable to Motorola SPI frame format only.",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("first_edge", "SSPCLKOUT phase: data captured on first clock edge", 0)?,
                    create_enum_value("second_edge", "SSPCLKOUT phase: data captured on second clock edge", 1)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "scr",
                "Serial clock rate. The value SCR is used to generate the transmit and receive bit rate of the PrimeCell SSP. The bit rate is: (F[sspclk] / (CPSDVR * (1 + SCR))), where CPSDVSR is an even value from [2:254], programmed through the SSPCPSR register and SCR is a value from [0:255]",
                create_bit_range("[15:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None
            )?
        ]),
        None
    ).map(svd::RegisterCluster::Register)
}
