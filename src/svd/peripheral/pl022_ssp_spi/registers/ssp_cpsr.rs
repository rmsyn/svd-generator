use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the SSPCPSR clock prescale register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ssp_cpsr",
        "SSPCPSR is the clock prescale register and specifies the division factor by which the input SSPCLK must be internally divided before further use. The value programmed into this register must be an even number between [2:254]. The least significant bit of the programmed number is hard-coded to zero. If an odd number is written to this register, data read back from this register has the least significant bit as zero.",
        0x10,
        create_register_properties(16, 0)?,
        Some(&[
            create_field_constraint(
                "cpsdvsr",
                "Clock prescale divisor. Must be an even number from 2-254, depending on the frequency of SSPCLK. The least significant bit always returns zero on reads.",
                create_bit_range("[7:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(2, 254)?,
                None,
            )?,
        ]),
        None
    ).map(svd::RegisterCluster::Register)
}
