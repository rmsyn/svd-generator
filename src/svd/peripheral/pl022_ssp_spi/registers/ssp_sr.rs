use crate::svd::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates the SSPSR read-only status register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(
        create_register(
            "ssp_sr",
            "SSPSR is a RO status register that contains bits that indicate the FIFO fill status and the PrimeCell SSP busy status.",
            0x0c,
            create_register_properties(16, 0)?,
            Some(&[
                create_field_enum(
                    "tfe",
                    "Transmit FIFO empty.",
                    create_bit_range("[0:0]")?,
                    svd::Access::ReadOnly,
                    &[create_enum_values(&[
                        create_enum_value("not_empty", "Transmit FIFO is not empty", 0)?,
                        create_enum_value("empty", "Transmit FIFO is empty", 1)?,
                    ])?],
                    None,
                )?,
                create_field_enum(
                    "tnf",
                    "Transmit FIFO not full.",
                    create_bit_range("[1:1]")?,
                    svd::Access::ReadOnly,
                    &[create_enum_values(&[
                        create_enum_value("full", "Transmit FIFO is full", 0)?,
                        create_enum_value("not_full", "Transmit FIFO is not full", 1)?,
                    ])?],
                    None,
                )?,
                create_field_enum(
                    "rne",
                    "Receive FIFO not empty.",
                    create_bit_range("[2:2]")?,
                    svd::Access::ReadOnly,
                    &[create_enum_values(&[
                        create_enum_value("empty", "Receive FIFO is empty", 0)?,
                        create_enum_value("not_empty", "Receive FIFO is not empty", 1)?,
                    ])?],
                    None,
                )?,
                create_field_enum(
                    "rff",
                    "Receive FIFO full.",
                    create_bit_range("[3:3]")?,
                    svd::Access::ReadOnly,
                    &[create_enum_values(&[
                        create_enum_value("not_full", "Receive FIFO is not full", 0)?,
                        create_enum_value("full", "Receive FIFO is full", 1)?,
                    ])?],
                    None,
                )?,
                create_field_enum(
                    "bsy",
                    "PrimeCell SSP busy flag.",
                    create_bit_range("[4:4]")?,
                    svd::Access::ReadOnly,
                    &[create_enum_values(&[
                        create_enum_value("idle", "SSP is idle", 0)?,
                        create_enum_value("busy", "SSP is currently transmitting and/or receiving a frame or the transmit FIFO is not empty", 1)?,
                    ])?],
                    None,
                )?
            ]),
            None,
        )?
    ))
}
