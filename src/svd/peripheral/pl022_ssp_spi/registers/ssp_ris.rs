use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the SSPRIS raw interrupt status register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ssp_ris",
        "The SSPRIS register is the raw interrupt status register. It is a RO register. On a read this register gives the current raw status value of the corresponding interrupt prior to masking. A write has no effect.",
        0x18,
        create_register_properties(16, 0)?,
        Some(&[
            create_field(
                "rorris",
                "Gives the raw interrupt state, prior to masking, of the SSPRORINTR interrupt",
                create_bit_range("[0:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "rtris",
                "Gives the raw interrupt state, prior to masking, of the SSPRTINTR interrupt",
                create_bit_range("[1:1]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "rxris",
                "Gives the raw interrupt state, prior to masking, of the SSPRXINTR interrupt",
                create_bit_range("[2:2]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "txris",
                "Gives the raw interrupt state, prior to masking, of the SSPTXINTR interrupt",
                create_bit_range("[3:3]")?,
                svd::Access::ReadOnly,
                None,
            )?
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
