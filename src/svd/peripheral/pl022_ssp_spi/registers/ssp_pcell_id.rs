use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the SPPCellID0-3 registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ssp_pcell_id",
        "The SSPPCellID0-3 registers are four 8-bit wide registers, that span address locations 0xFF0-0xFFC. The registers can conceptually be treated as a 32-bit register. The register is used as a standard cross-peripheral identification system. The SSPPCellID register is set to 0xB105F00D.",
        0xff0,
        create_register_properties(16, 0)?,
        Some(&[
            create_field(
                "ssp_pcell_id",
                "The bits of the SSPCELLID are read as: [0x0d, 0xf0, 0x05, 0xb1]",
                create_bit_range("[7:0]")?,
                svd::Access::ReadOnly,
                None,
            )?
        ]),
        Some(
            // There are 4 registers, 4 bytes apart (even though they are 16-bit registers)
            svd::DimElement::builder()
                .dim(4)
                .dim_increment(4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    ).map(svd::RegisterCluster::Register)
}
