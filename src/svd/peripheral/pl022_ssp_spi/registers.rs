use crate::Result;

pub mod ssp_cpsr;
pub mod ssp_cr0;
pub mod ssp_cr1;
pub mod ssp_dmacr;
pub mod ssp_dr;
pub mod ssp_icr;
pub mod ssp_imsc;
pub mod ssp_mis;
pub mod ssp_pcell_id;
pub mod ssp_periph_id0;
pub mod ssp_periph_id1;
pub mod ssp_periph_id2;
pub mod ssp_periph_id3;
pub mod ssp_ris;
pub mod ssp_sr;

/// Creates ARM pl022 SSP SPI register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        ssp_cr0::create()?,
        ssp_cr1::create()?,
        ssp_dr::create()?,
        ssp_sr::create()?,
        ssp_cpsr::create()?,
        ssp_imsc::create()?,
        ssp_ris::create()?,
        ssp_mis::create()?,
        ssp_icr::create()?,
        ssp_dmacr::create()?,
        ssp_periph_id0::create()?,
        ssp_periph_id1::create()?,
        ssp_periph_id2::create()?,
        ssp_periph_id3::create()?,
        ssp_pcell_id::create()?,
    ]
    .into())
}
