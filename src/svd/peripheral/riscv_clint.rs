use crate::svd::create_peripheral;
use crate::Result;

pub mod registers;

/// Represents a RISC-V CLINT (Core-local Interrupt Controller) peripheral.
pub struct RiscvClint {
    peripheral: svd::Peripheral,
}

impl RiscvClint {
    /// Creates a new [RiscvClint] peripheral.
    pub fn create(base_address: u64, size: u32, harts: usize) -> Result<Self> {
        let name = "clint";
        let peripheral = create_peripheral(
            name,
            format!("RISC-V CLINT: {name}").as_str(),
            base_address,
            size,
            None,
            Some(registers::create(harts)?),
            None,
            Some("CLINT".into()),
        )?;

        Ok(Self { peripheral })
    }

    /// Gets a reference to the SVD [`Peripheral`](svd::Peripheral).
    pub const fn peripheral(&self) -> &svd::Peripheral {
        &self.peripheral
    }

    /// Gets a mutable reference to the SVD [`Peripheral`](svd::Peripheral).
    pub fn peripheral_mut(&mut self) -> &mut svd::Peripheral {
        &mut self.peripheral
    }

    /// Converts the [DwApUart] into the inner SVD [`Peripheral`](svd::Peripheral).
    pub fn to_inner(self) -> svd::Peripheral {
        self.peripheral
    }
}
