use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 TRNG SMODE register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "smode",
        "TRNG SMODE Register",
        0xc,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "nonce_mode",
                "Nonce operation mode",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "mission_mode",
                "Mission operation mode",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "max_rejects",
                "TRNG Maximum Rejects",
                create_bit_range("[31:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
