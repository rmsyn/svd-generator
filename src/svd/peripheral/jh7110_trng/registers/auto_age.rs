use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 TRNG Auto Age register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "auto_age",
        "Auto-reseeding after specified timer countdowns to 0: 0 - disable timer, other - reload value for internal timer",
        0x64,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "age",
                "Countdown value for auto-reseed timer",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff_ffff)?,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
