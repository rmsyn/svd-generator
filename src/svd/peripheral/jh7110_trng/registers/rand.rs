use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 TRNG RAND register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "rand",
        "TRNG RAND Register",
        0x20,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "rand",
            "TRNG random number bits",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(8)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
