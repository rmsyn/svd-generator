use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 TRNG Auto Requests register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "auto_rqsts",
        "Auto-reseeding after random number requests by host reaches specified counter: 0 - disable counter, other - reload value for internal counter",
        0x60,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "rqsts",
                "Threshold number of reseed requests for auto-reseed counter",
                create_bit_range("[31:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff_ffff)?,
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
