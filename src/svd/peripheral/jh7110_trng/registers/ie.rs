use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 TRNG IE register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ie",
        "TRNG Interrupt Enable Register",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "rand_rdy_en",
                "RAND Ready Enable",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "seed_done_en",
                "Seed Done Enable",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "lfsr_lockup_en",
                "LFSR Lockup Enable",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "glbl_en",
                "Global Enable",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
