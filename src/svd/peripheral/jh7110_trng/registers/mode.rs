use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field_enum, create_register,
    create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 TRNG MODE register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "mode",
        "TRNG MODE Register",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[create_field_enum(
            "r256",
            "256-bit operation mode: 0 - 128-bit mode, 1 - 256-bit mode",
            create_bit_range("[3:3]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("bit128", "128-bit operation mode", 0b0)?,
                create_enum_value("bit256", "256-bit operation mode", 0b1)?,
            ])?],
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
