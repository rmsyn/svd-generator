use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 SYSCRG Clock AHB register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "ctrl",
        "TRNG Control Register",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "exec_nop",
                "Execute a NOP instruction",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "gen_rand",
                "Generate a random number",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "reseed",
                "Reseed the TRNG from noise sources",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
