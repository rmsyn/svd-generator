use crate::Result;

pub mod auto_age;
pub mod auto_rqsts;
pub mod ctrl;
pub mod ie;
pub mod istat;
pub mod mode;
pub mod rand;
pub mod smode;
pub mod stat;

/// Creates StarFive JH7110 TRNG (compatible) register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        ctrl::create()?,
        stat::create()?,
        mode::create()?,
        smode::create()?,
        ie::create()?,
        istat::create()?,
        rand::create()?,
        auto_rqsts::create()?,
        auto_age::create()?,
    ]
    .into())
}
