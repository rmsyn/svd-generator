use crate::svd::create_cluster;
use crate::Result;

pub mod reset_vector_31_0_4;
pub mod reset_vector_35_0;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG Reset Vector registers.
pub fn create() -> Result<svd::RegisterCluster> {
    create_cluster(
        "sys_syscfg_reset_vector",
        "SYS SYSCONSAIF SYSCFG 104 - 128: Reset Vector registers.",
        0x68,
        &[reset_vector_35_0::create()?, reset_vector_31_0_4::create()?],
        None,
    )
    .map(svd::RegisterCluster::Cluster)
}
