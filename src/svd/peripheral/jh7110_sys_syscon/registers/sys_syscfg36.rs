use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 36 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sys_syscfg36",
        "SYS SYSCONSAIF SYSCFG 144",
        0x90,
        create_register_properties(32, 0x4)?,
        Some(&[
            create_field(
                "gmac5_axi64_mac_speed",
                "",
                create_bit_range("[1:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field_enum(
                "gmac5_axi64_phy_intf_sel",
                "Active PHY Selected | When you have multiple GMAC PHY interfaces in your configuration, this field indicates the sampled value of the PHY selector during reset de-assertion.",
                create_bit_range("[4:2]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("gmii", "Active PHY Selected: GMII or MII, None", 0)?,
                    create_enum_value("rgmii", "Active PHY Selected: RGMII", 1)?,
                    create_enum_value("sgmii", "Active PHY Selected: SGMII", 2)?,
                    create_enum_value("tbi", "Active PHY Selected: TBI", 3)?,
                    create_enum_value("rmii", "Active PHY Selected: RMII", 4)?,
                    create_enum_value("rtbi", "Active PHY Selected: RTBI", 5)?,
                    create_enum_value("smii", "Active PHY Selected: SMII", 6)?,
                    create_enum_value("revmii", "Active PHY Selected: REVMII", 7)?,
                ])?],
                None,
            )?,
        ]),
        None,
    ).map(svd::RegisterCluster::Register)
}
