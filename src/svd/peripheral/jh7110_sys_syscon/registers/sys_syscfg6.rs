use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_enum,
    create_register, create_register_properties, jh7110,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 6 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let [slp, sd, rtsel, ptsel, trb, wtsel, vs, vg] =
        jh7110::create_fields_sram_config("intmem_rom_sram_config", 3)?;

    create_register(
        "sys_syscfg6",
        "SYS SYSCONSAIF SYSCFG 24",
        0x18,
        create_register_properties(32, 0x4d_ea80)?,
        Some(&[
            create_field_enum(
                "sdio_m_hbig_endian",
                "AHB master bus interface endianess",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("little_endian", "Little-endian AHB bus interface.", 0)?,
                    create_enum_value("big_endian", "Big-endian AHB bus interface.", 1)?,
                ])?],
                None,
            )?,
            create_field(
                "i2srx_adc_en",
                "I2S RX ADC enable",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "intmem_rom_sram_scfg_disable_rom",
                "Internal Memory ROM SRAM SCFG Disable ROM",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            slp,
            sd,
            rtsel,
            ptsel,
            trb,
            wtsel,
            vs,
            vg,
            create_field(
                "jtag_en",
                "JTAG daisy-chain enable",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                Some(
                    svd::DimElement::builder()
                        .dim(2)
                        .dim_increment(1)
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field(
                "pdrstn_split_sw_usbpipe_plugen",
                "PD RSTN Split Software USB Pipe Plug enable",
                create_bit_range("[17:17]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "pll0_cpi_bias",
                "PLL0 CPI bias",
                create_bit_range("[20:18]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "pll0_cpp_bias",
                "PLL0 CPP bias",
                create_bit_range("[23:21]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_enum(
                "pll0_dacpd",
                "PLL0 DACPD.",
                create_bit_range("[24:24]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("off", "Disable PLL0 DACPD.", 0b0)?,
                    create_enum_value("on", "Enable PLL0 DACPD.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "pll0_dsmpd",
                "PLL0 DSMPD.",
                create_bit_range("[25:25]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("off", "Disable PLL0 DSMPD.", 0b0)?,
                    create_enum_value("on", "Enable PLL0 DSMPD.", 0b1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
