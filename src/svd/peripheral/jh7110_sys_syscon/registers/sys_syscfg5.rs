use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
    jh7110,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 5 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let [qspi_slp, qspi_sd, qspi_rtsel, qspi_ptsel, qspi_trb, qspi_wtsel, qspi_vs, qspi_vg] =
        jh7110::create_fields_sram_config("u0_cdns_qspi_scfg_sram_config", 0)?;

    let [spdif_slp, spdif_sd, spdif_rtsel, spdif_ptsel, spdif_trb, spdif_wtsel, spdif_vs, spdif_vg] =
        jh7110::create_fields_sram_config("u0_cdns_spdif_scfg_sram_config", 12)?;

    create_register(
        "sys_syscfg5",
        "SYS SYSCONSAIF SYSCFG 20",
        0x14,
        create_register_properties(32, 0xd5_4d54)?,
        Some(&[
            qspi_slp,
            qspi_sd,
            qspi_rtsel,
            qspi_ptsel,
            qspi_trb,
            qspi_wtsel,
            qspi_vs,
            qspi_vg,
            spdif_slp,
            spdif_sd,
            spdif_rtsel,
            spdif_ptsel,
            spdif_trb,
            spdif_wtsel,
            spdif_vs,
            spdif_vg,
            create_field_enum(
                "spdif_trmodeo",
                "SPDIF transfer mode",
                create_bit_range("[24:24]")?,
                svd::Access::ReadOnly,
                &[create_enum_values(&[
                    create_enum_value("receiver", "SPDIF mode: receiver", 0)?,
                    create_enum_value("transmitter", "SPDIF mode: transmitter", 1)?,
                ])?],
                None,
            )?,
            create_field(
                "i2c_ic_en",
                "I2C interface enable",
                create_bit_range("[25:25]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field_constraint(
                "sdio_data_strobe_phase_ctrl",
                "Data strobe delay chain select",
                create_bit_range("[30:26]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
            create_field_enum(
                "sdio_hbig_endian",
                "AHB bus interface endianness",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("little_endian", "Little-endian AHB bus interface", 0)?,
                    create_enum_value("big_endian", "Big-endian AHB bus interface", 1)?,
                ])?],
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
