use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 7 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sys_syscfg7",
        "SYS SYSCONSAIF SYSCFG 28",
        0x1c,
        create_register_properties(32, 0x53)?,
        Some(&[create_field_constraint(
            "pll0_fbdiv",
            "",
            create_bit_range("[11:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xfff)?,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
