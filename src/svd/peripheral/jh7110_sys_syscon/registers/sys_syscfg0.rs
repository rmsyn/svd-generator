use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 0 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sys_syscfg0",
        "SYS SYSCONSAIF SYSCFG 0",
        0x0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "e24_remap_haddr",
                "",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "hifi4_idma_remap_araddr",
                "",
                create_bit_range("[7:4]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "hifi4_idma_remap_awaddr",
                "",
                create_bit_range("[11:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "hifi4_sys_remap_araddr",
                "",
                create_bit_range("[15:12]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "hifi4_sys_remap_awaddr",
                "",
                create_bit_range("[19:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "jpg_remap_araddr",
                "",
                create_bit_range("[23:20]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "jpg_remap_awaddr",
                "",
                create_bit_range("[27:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "sd0_remap_araddr",
                "",
                create_bit_range("[31:28]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
