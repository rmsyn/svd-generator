use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 37 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sys_syscfg37",
        "SYS SYSCONSAIF SYSCFG 148",
        0x94,
        create_register_properties(32, 0x0)?,
        Some(&[create_field(
            "gmac5_axi64_ptp_timestamp_31_0",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
