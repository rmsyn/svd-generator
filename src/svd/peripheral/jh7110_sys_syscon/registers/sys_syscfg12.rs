use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 12 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sys_syscfg12",
        "SYS SYSCONSAIF SYSCFG 48",
        0x30,
        create_register_properties(32, 0x4133_3333)?,
        Some(&[
            create_field_constraint(
                "pll2_frac",
                "PLL2 frac value.",
                create_bit_range("[23:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff_ffff)?,
                None,
            )?,
            create_field_constraint(
                "pll2_gvco_bias",
                "PLL2 GVCO bias.",
                create_bit_range("[25:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field(
                "pll2_lock",
                "PLL2 lock.",
                create_bit_range("[26:26]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field_enum(
                "pll2_pd",
                "PLL2 PD enable setting - driving the register low turns PD `on`.",
                create_bit_range("[27:27]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("on", "Enable the PLL2 PD.", 0b0)?,
                    create_enum_value("off", "Disable the PLL2 PD.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "pll2_postdiv1",
                "PLL2 postdiv1 value.",
                create_bit_range("[29:28]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "pll2_postdiv2",
                "PLL2 postdiv2 value.",
                create_bit_range("[31:30]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
