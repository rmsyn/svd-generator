use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 2 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sys_syscfg2",
        "SYS SYSCONSAIF SYSCFG 8",
        0x8,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "vout0_remap_awaddr",
                "",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "vout1_remap_araddr",
                "",
                create_bit_range("[7:4]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "vout1_remap_awaddr",
                "",
                create_bit_range("[11:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
