use super::{create_field_noc_bus_oic_evemon, create_fields_noc_bus_oic_evemon};
use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 12 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let fields: Vec<svd::Field> = [
        create_field_constraint(
            "pll2_prediv",
            "",
            create_bit_range("[5:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0x3f)?,
            None,
        )?,
        create_field(
            "pll2_testen",
            "",
            create_bit_range("[6:6]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field_constraint(
            "pll2_testsel",
            "",
            create_bit_range("[8:7]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0x3)?,
            None,
        )?,
        create_field(
            "pll_test_mode",
            "PLL test mode, only used for PLL BIST through jtag2apb",
            create_bit_range("[9:9]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field_constraint(
            "audio_i2sdin_sel",
            "",
            create_bit_range("[17:10]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xff)?,
            None,
        )?,
        create_field(
            "noc_bus_clock_gating_off",
            "",
            create_bit_range("[18:18]")?,
            svd::Access::ReadWrite,
            None,
        )?,
    ]
    .into_iter()
    .chain(
        (0..6)
            .filter_map(|idx| {
                Some(
                    create_fields_noc_bus_oic_evemon(idx, 19 + (idx * 2))
                        .ok()?
                        .into_iter(),
                )
            })
            .flatten(),
    )
    .chain([create_field_noc_bus_oic_evemon(
        "start",
        6,
        31,
        svd::Access::ReadWrite,
    )?])
    .collect();

    create_register(
        "sys_syscfg13",
        "SYS SYSCONSAIF SYSCFG 52",
        0x34,
        create_register_properties(32, 0x1)?,
        Some(fields.as_ref()),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
