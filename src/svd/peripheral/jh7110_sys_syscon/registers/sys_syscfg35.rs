use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties, jh7110,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 35 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let [slp, sd, rtsel, ptsel, trb, wtsel, vs, vg] =
        jh7110::create_fields_sram_config("u1_gmac5_axi64_scfg_ram_cfg", 19)?;

    create_register(
        "sys_syscfg35",
        "SYS SYSCONSAIF SYSCFG 140",
        0x8c,
        create_register_properties(32, 0x6aa0_0000)?,
        Some(&[
            create_field(
                "can_ctrl_host_if_1",
                "",
                create_bit_range("[18:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            slp,
            sd,
            rtsel,
            ptsel,
            trb,
            wtsel,
            vs,
            vg,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
