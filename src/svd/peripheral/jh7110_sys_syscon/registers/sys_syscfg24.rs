use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 24 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let fields: Vec<svd::Field> = [
        create_field(
            "tdm1616slot_clkpol",
            "",
            create_bit_range("[0:0]")?,
            svd::Access::ReadOnly,
            None,
        )?,
        create_field(
            "tdm1616slot_pcm_ms",
            "",
            create_bit_range("[1:1]")?,
            svd::Access::ReadOnly,
            None,
        )?,
    ]
    .into_iter()
    .chain(
        (0..3)
            .filter_map(|idx| {
                let in0_bit = (idx * 10) + 2;
                let in0_bit_end = in0_bit + 4;

                let in1_bit = in0_bit_end + 1;
                let in1_bit_end = in1_bit + 4;

                Some(
                    [
                        create_field(
                            format!("u0_trace_mtx_in0_c{idx}").as_str(),
                            "",
                            create_bit_range(format!("[{in0_bit_end}:{in0_bit}]").as_str()).ok()?,
                            svd::Access::ReadWrite,
                            None,
                        )
                        .ok()?,
                        create_field(
                            format!("u0_trace_mtx_in1_c{idx}").as_str(),
                            "",
                            create_bit_range(format!("[{in1_bit_end}:{in1_bit}]").as_str()).ok()?,
                            svd::Access::ReadWrite,
                            None,
                        )
                        .ok()?,
                    ]
                    .into_iter(),
                )
            })
            .flatten(),
    )
    .collect();

    create_register(
        "sys_syscfg24",
        "SYS SYSCONSAIF SYSCFG 96",
        0x60,
        create_register_properties(32, 0)?,
        Some(fields.as_ref()),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
