use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 39 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let fields: Vec<svd::Field> = [
        create_field(
            "u1_i2c_ic_en",
            "I2C interface enable.",
            create_bit_range("[0:0]")?,
            svd::Access::ReadOnly,
            None,
        )?,
        create_field_constraint(
            "u1_sdio_data_strobe_phase_ctrl",
            "Data strobe delay chain select.",
            create_bit_range("[5:1]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0x1f)?,
            None,
        )?,
        create_field_enum(
            "u1_sdio_hbig_endian",
            "AHB bus interface endianness",
            create_bit_range("[6:6]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("little_endian", "Little-endian AHB bus interface", 0)?,
                create_enum_value("big_endian", "Big-endian AHB bus interface", 1)?,
            ])?],
            None,
        )?,
        create_field_enum(
            "u1_sdio_m_hbig_endian",
            "AHB bus interface endianness",
            create_bit_range("[7:7]")?,
            svd::Access::ReadWrite,
            &[create_enum_values(&[
                create_enum_value("little_endian", "Little-endian AHB bus interface", 0)?,
                create_enum_value("big_endian", "Big-endian AHB bus interface", 1)?,
            ])?],
            None,
        )?,
        create_field(
            "u1_reset_ctrl_clr_reset_status",
            "",
            create_bit_range("[8:8]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "u1_reset_ctrl_pll_timecnt_finish",
            "",
            create_bit_range("[9:9]")?,
            svd::Access::ReadOnly,
            None,
        )?,
        create_field(
            "u1_reset_ctrl_rstn_sw",
            "",
            create_bit_range("[10:10]")?,
            svd::Access::ReadWrite,
            None,
        )?,
        create_field(
            "u1_reset_ctrl_sys_reset_status",
            "",
            create_bit_range("[14:11]")?,
            svd::Access::ReadOnly,
            None,
        )?,
    ]
    .into_iter()
    .chain((2..7).filter_map(|idx| {
        create_field(
            format!("u{idx}_i2c_ic_en").as_str(),
            "I2C interface enable.",
            create_bit_range(format!("[{}:{}]", idx + 13, idx + 13).as_str()).ok()?,
            svd::Access::ReadOnly,
            None,
        )
        .ok()
    }))
    .collect();

    create_register(
        "sys_syscfg39",
        "SYS SYSCONSAIF SYSCFG 156",
        0x9c,
        create_register_properties(32, 0x400)?,
        Some(fields.as_ref()),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
