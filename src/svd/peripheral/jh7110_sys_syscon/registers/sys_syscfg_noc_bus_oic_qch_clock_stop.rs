use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 8 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sys_syscfg_noc_bus_oic_qch_clock_stop",
        "SYS SYSCONSAIF SYSCFG 60 - 92: NOC Bus OIC QCH Clock Stop Threshold registers.",
        0x3c,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "threshold",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(9)
                .dim_increment(0x4)
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
