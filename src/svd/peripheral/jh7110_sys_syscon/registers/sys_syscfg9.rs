use crate::svd::register::{
    create_bit_range, create_enum_value, create_enum_values, create_field, create_field_constraint,
    create_field_enum, create_register, create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 9 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sys_syscfg9",
        "SYS SYSCONSAIF SYSCFG 36",
        0x24,
        create_register_properties(32, 0xb0_2601)?,
        Some(&[
            create_field_constraint(
                "pll0_prediv",
                "",
                create_bit_range("[5:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3f)?,
                None,
            )?,
            create_field(
                "pll0_testen",
                "",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "pll0_testsel",
                "",
                create_bit_range("[8:7]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "pll1_cpi_bias",
                "",
                create_bit_range("[11:9]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_constraint(
                "pll1_cpp_bias",
                "",
                create_bit_range("[14:12]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field_enum(
                "pll1_dacpd",
                "PLL1 DACPD.",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("off", "Disable PLL1 DACPD.", 0b0)?,
                    create_enum_value("on", "Enable PLL1 DACPD.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_enum(
                "pll1_dsmpd",
                "PLL1 DSMPD.",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                &[create_enum_values(&[
                    create_enum_value("off", "Disable PLL1 DSMPD.", 0b0)?,
                    create_enum_value("on", "Enable PLL1 DSMPD.", 0b1)?,
                ])?],
                None,
            )?,
            create_field_constraint(
                "pll1_fbdiv",
                "",
                create_bit_range("[28:17]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xfff)?,
                None,
            )?,
        ]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
