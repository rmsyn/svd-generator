use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 38 register.
pub fn create() -> Result<svd::RegisterCluster> {
    create_register(
        "sys_syscfg38",
        "SYS SYSCONSAIF SYSCFG 152",
        0x98,
        create_register_properties(32, 0x0)?,
        Some(&[create_field(
            "gmac5_axi64_ptp_timestamp_63_32",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadOnly,
            None,
        )?]),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
