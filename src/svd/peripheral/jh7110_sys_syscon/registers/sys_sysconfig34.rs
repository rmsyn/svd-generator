use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties, jh7110,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 34 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let [slp, sd, rtsel, ptsel, trb, wtsel, vs, vg] =
        jh7110::create_fields_sram_config("u0_venc_int_sram_config", 0)?;

    Ok(svd::RegisterCluster::Register(create_register(
        "sys_syscfg33",
        "SYS SYSCONSAIF SYSCFG 132",
        0x88,
        create_register_properties(32, 0xd54)?,
        Some(&[
            slp,
            sd,
            rtsel,
            ptsel,
            trb,
            wtsel,
            vs,
            vg,
            create_field(
                "wave420l_ipu_current_buffer",
                "This signal indicates which buffer is currently active so that the VPU can correctly use the ipu_end_of_row signal for row counter.",
                create_bit_range("[14:12]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "wave420l_ipu_end_of_row",
                "This signal is flipped every time when the IPU completes writing a row.",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "wave420l_ipu_new_frame",
                "This signal is flipped every time when the IPU completes writing a new frame.",
                create_bit_range("[16:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "wave420l_vpu_idle",
                "VPU monitoring signal. This signal gives out an opposite value of VPU_BUSY register.",
                create_bit_range("[17:17]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field(
                "can_ctrl_fd_enable_1",
                "", create_bit_range("[18:18]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "can_ctrl_host_ecc_disable_1",
                "",
                create_bit_range("[19:19]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
