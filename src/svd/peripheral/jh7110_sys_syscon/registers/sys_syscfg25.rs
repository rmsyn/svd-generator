use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates a StarFive JH7110 SYS SYSCON SYSCFG 25 register.
pub fn create() -> Result<svd::RegisterCluster> {
    let fields: Vec<svd::Field> = (3..5)
        .filter_map(|idx| {
            let in0_bit = ((idx - 3) * 10) + 2;
            let in0_bit_end = in0_bit + 4;

            let in1_bit = in0_bit_end + 1;
            let in1_bit_end = in1_bit + 4;

            Some(
                [
                    create_field_constraint(
                        format!("u0_trace_mtx_scfg_in0_c{idx}").as_str(),
                        "",
                        create_bit_range(format!("[{in0_bit_end}:{in0_bit}]").as_str()).ok()?,
                        svd::Access::ReadWrite,
                        create_write_constraint(0, 0x1f).ok()?,
                        None,
                    )
                    .ok()?,
                    create_field_constraint(
                        format!("u0_trace_mtx_scfg_in1_c{idx}").as_str(),
                        "",
                        create_bit_range(format!("[{in1_bit_end}:{in1_bit}]").as_str()).ok()?,
                        svd::Access::ReadWrite,
                        create_write_constraint(0, 0x1f).ok()?,
                        None,
                    )
                    .ok()?,
                ]
                .into_iter(),
            )
        })
        .flatten()
        .chain((0..10).filter_map(|idx| {
            let bit = 20 + idx;
            if idx < 5 {
                create_field(
                    format!("u0_cease_from_tile{idx}").as_str(),
                    "",
                    create_bit_range(format!("[{bit}:{bit}]").as_str()).ok()?,
                    svd::Access::ReadOnly,
                    None,
                )
                .ok()
            } else {
                let i = idx - 5;
                create_field(
                    format!("u0_halt_from_tile{i}").as_str(),
                    "",
                    create_bit_range(format!("[{bit}:{bit}]").as_str()).ok()?,
                    svd::Access::ReadOnly,
                    None,
                )
                .ok()
            }
        }))
        .collect();

    create_register(
        "sys_syscfg25",
        "SYS SYSCONSAIF SYSCFG 96",
        0x64,
        create_register_properties(32, 0)?,
        Some(fields.as_ref()),
        None,
    )
    .map(svd::RegisterCluster::Register)
}
