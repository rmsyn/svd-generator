use crate::svd::register::{create_bit_range, create_field};
use crate::Result;

pub mod sys_syscfg0;
pub mod sys_syscfg1;
pub mod sys_syscfg10;
pub mod sys_syscfg11;
pub mod sys_syscfg12;
pub mod sys_syscfg13;
pub mod sys_syscfg14;
pub mod sys_syscfg2;
pub mod sys_syscfg24;
pub mod sys_syscfg25;
pub mod sys_syscfg3;
pub mod sys_syscfg33;
pub mod sys_syscfg34;
pub mod sys_syscfg35;
pub mod sys_syscfg36;
pub mod sys_syscfg37;
pub mod sys_syscfg38;
pub mod sys_syscfg39;
pub mod sys_syscfg4;
pub mod sys_syscfg5;
pub mod sys_syscfg6;
pub mod sys_syscfg7;
pub mod sys_syscfg8;
pub mod sys_syscfg9;
pub mod sys_syscfg_noc_bus_oic_qch_clock_stop;
pub mod sys_syscfg_reset_vector;

/// Creates StarFive JH7110 SYS Syscon (compatible) register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        sys_syscfg0::create()?,
        sys_syscfg1::create()?,
        sys_syscfg2::create()?,
        sys_syscfg3::create()?,
        sys_syscfg4::create()?,
        sys_syscfg5::create()?,
        sys_syscfg6::create()?,
        sys_syscfg7::create()?,
        sys_syscfg8::create()?,
        sys_syscfg9::create()?,
        sys_syscfg10::create()?,
        sys_syscfg11::create()?,
        sys_syscfg12::create()?,
        sys_syscfg13::create()?,
        sys_syscfg14::create()?,
        sys_syscfg_noc_bus_oic_qch_clock_stop::create()?,
        sys_syscfg24::create()?,
        sys_syscfg25::create()?,
        sys_syscfg_reset_vector::create()?,
        sys_syscfg33::create()?,
        sys_syscfg34::create()?,
        sys_syscfg35::create()?,
        sys_syscfg36::create()?,
        sys_syscfg37::create()?,
        sys_syscfg38::create()?,
        sys_syscfg39::create()?,
    ]
    .into())
}

pub(crate) fn create_fields_noc_bus_oic_evemon(idx: u32, bit: u32) -> Result<[svd::Field; 2]> {
    Ok([
        create_field_noc_bus_oic_evemon("start", idx, bit, svd::Access::ReadWrite)?,
        create_field_noc_bus_oic_evemon("trigger", idx, bit + 1, svd::Access::ReadOnly)?,
    ])
}

pub(crate) fn create_field_noc_bus_oic_evemon(
    field: &str,
    idx: u32,
    bit: u32,
    access: svd::Access,
) -> Result<svd::Field> {
    create_field(
        format!("noc_bus_oic_evemon_{field}{idx}").as_str(),
        "",
        create_bit_range(format!("[{bit}:{bit}]").as_str())?,
        access,
        None,
    )
}
