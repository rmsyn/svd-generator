use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the Cadence QSPI NOR Extension Lower register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "ext_lower",
        "Cadence QSPI Extension Lower",
        0xe0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "stig",
                "",
                create_bit_range("[15:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
            create_field_constraint(
                "write",
                "",
                create_bit_range("[23:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field_constraint(
                "read",
                "",
                create_bit_range("[31:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
