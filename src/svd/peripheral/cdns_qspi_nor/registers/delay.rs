use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the Cadence QSPI NOR Delay register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "delay",
        "Cadence QSPI Delay",
        0x0c,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "tslch",
                "TSLCH Delay Value",
                create_bit_range("[7:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field_constraint(
                "tchsh",
                "TCHSH Delay Value",
                create_bit_range("[15:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field_constraint(
                "tsd2d",
                "TSD2D Delay Value",
                create_bit_range("[23:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field_constraint(
                "tshsl",
                "TSHSL Delay Value",
                create_bit_range("[31:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
