use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the Cadence QSPI NOR Read Instruction register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "rd_instr",
        "Cadence QSPI Read Instruction",
        0x04,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "opcode",
                "Instruction Opcode",
                create_bit_range("[7:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field_constraint(
                "type_instr",
                "Type of Instruction",
                create_bit_range("[9:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0b11)?,
                None,
            )?,
            create_field_constraint(
                "type_addr",
                "Type of Address",
                create_bit_range("[13:12]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0b11)?,
                None,
            )?,
            create_field_constraint(
                "type_data",
                "",
                create_bit_range("[17:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0b11)?,
                None,
            )?,
            create_field(
                "mode_en",
                "Mode enable",
                create_bit_range("[20:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "dummy",
                "Send dummy signal to stall the device",
                create_bit_range("[28:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
        ]),
        None,
    )?))
}
