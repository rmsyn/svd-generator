use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the Cadence QSPI NOR Command Control register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cmd_ctrl",
        "Cadence QSPI Command Control",
        0x90,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "execute",
                "Execute-in-Place (XIP)",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "in_progress",
                "Command in progress",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "dummy",
                "Dummy command",
                create_bit_range("[11:7]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
            create_field_constraint(
                "wr_bytes",
                "Write bytes",
                create_bit_range("[14:12]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0b11)?,
                None,
            )?,
            create_field(
                "wr_en",
                "Write enable",
                create_bit_range("[15:15]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "add_bytes",
                "Add command bytes",
                create_bit_range("[17:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0b11)?,
                None,
            )?,
            create_field(
                "addr_en",
                "Address enable",
                create_bit_range("[19:19]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "rd_bytes",
                "Read bytes",
                create_bit_range("[22:20]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0b111)?,
                None,
            )?,
            create_field(
                "rd_en",
                "Read enable",
                create_bit_range("[23:23]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "opcode",
                "Command opcode",
                create_bit_range("[31:24]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
