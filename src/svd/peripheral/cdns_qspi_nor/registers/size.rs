use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the Cadence QSPI NOR Size Configuration register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "size",
        "Cadence QSPI Size Configuration",
        0x14,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "address",
                "Address Size in Bytes",
                create_bit_range("[3:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
            create_field_constraint(
                "page",
                "Page Size in Bytes",
                create_bit_range("[15:4]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xfff)?,
                None,
            )?,
            create_field_constraint(
                "block",
                "Block Size in Bytes",
                create_bit_range("[21:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3f)?,
                None,
            )?,
        ]),
        None,
    )?))
}
