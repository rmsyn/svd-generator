use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the Cadence QSPI NOR Command Read at Lower register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cmd_read_at_lower",
        "Cadence QSPI Command Read at Lower",
        0xa0,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "read_at_lower",
            "",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
