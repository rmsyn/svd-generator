use crate::svd::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates the Cadence QSPI NOR Read Capture register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "read_capture",
        "Cadence QSPI Read Capture",
        0x10,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "bypass",
                "Bypass the Read Capture",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "delay",
                "Read Capture Delay Value",
                create_bit_range("[4:1]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xf)?,
                None,
            )?,
        ]),
        None,
    )?))
}
