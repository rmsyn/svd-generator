use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the Cadence QSPI NOR Direct Memory Access register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "dma",
        "Cadence QSPI Direct Memory Access",
        0x20,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "single",
                "",
                create_bit_range("[7:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
            create_field_constraint(
                "burst",
                "",
                create_bit_range("[15:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
