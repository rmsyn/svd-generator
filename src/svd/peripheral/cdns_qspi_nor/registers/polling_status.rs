use crate::svd::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates the Cadence QSPI NOR Polling Status register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "polling_status",
        "Cadence QSPI Polling Status",
        0xb0,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "status",
                "",
                create_bit_range("[15:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0xffff)?,
                None,
            )?,
            create_field_constraint(
                "dummy",
                "",
                create_bit_range("[20:16]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
        ]),
        None,
    )?))
}
