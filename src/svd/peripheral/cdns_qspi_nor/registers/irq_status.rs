use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Cadence QSPI NOR IRQ Status register.
///
/// Reset value: (`0b1_1111_1111_1111`)
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "irq_status",
        "Cadence QSPI IRQ Status",
        0x40,
        create_register_properties(32, 0x1_ffff)?,
        Some(&[
            create_field(
                "mode_err",
                "Mode error interrupt",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "underflow",
                "Buffer underflow interrupt",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ind_comp",
                "Indirect computation interrupt",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ind_rd_reject",
                "Indirect read rejection interrupt",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "wr_protected_err",
                "Write protected error interrupt",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "illegal_ahb_err",
                "Illegal AHB clock error interrupt",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "watermark",
                "Watermark interrupt",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ind_sram_full",
                "Indirect SRAM full interrupt",
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
