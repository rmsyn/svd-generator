use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Cadence QSPI NOR Indirect Write register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "indirect_wr",
        "Cadence QSPI Indirect Write",
        0x70,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "start",
                "Start indirect write",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "cancel",
                "Cancel indirect write",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "done",
                "Indirect write done",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
