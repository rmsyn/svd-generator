use crate::Result;

pub mod cmd_address;
pub mod cmd_ctrl;
pub mod cmd_read_at_lower;
pub mod cmd_read_at_upper;
pub mod cmd_write_at_lower;
pub mod cmd_write_at_upper;
pub mod config;
pub mod delay;
pub mod dma;
pub mod ext_lower;
pub mod indirect_rd;
pub mod indirect_trigger;
pub mod indirect_wr;
pub mod indirect_wr_bytes;
pub mod indirect_wr_start_addr;
pub mod indirect_wr_watermark;
pub mod irq_mask;
pub mod irq_status;
pub mod mode_bit;
pub mod polling_status;
pub mod rd_instr;
pub mod read_capture;
pub mod remap;
pub mod sdram_level;
pub mod size;
pub mod sram_partition;
pub mod wr_completion_ctrl;
pub mod wr_instr;

/// Creates Cadence QSPI NOR register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        config::create()?,
        rd_instr::create()?,
        wr_instr::create()?,
        delay::create()?,
        read_capture::create()?,
        size::create()?,
        sram_partition::create()?,
        indirect_trigger::create()?,
        dma::create()?,
        remap::create()?,
        mode_bit::create()?,
        sdram_level::create()?,
        wr_completion_ctrl::create()?,
        irq_status::create()?,
        irq_mask::create()?,
        indirect_rd::create()?,
        indirect_wr::create()?,
        indirect_wr_watermark::create()?,
        indirect_wr_start_addr::create()?,
        indirect_wr_bytes::create()?,
        cmd_ctrl::create()?,
        cmd_address::create()?,
        cmd_read_at_lower::create()?,
        cmd_read_at_upper::create()?,
        cmd_write_at_lower::create()?,
        cmd_write_at_upper::create()?,
        polling_status::create()?,
        ext_lower::create()?,
    ]
    .into())
}
