use crate::Result;

pub mod syscfg0;
pub mod syscfg1;
pub mod syscfg10;
pub mod syscfg11;
pub mod syscfg12;
pub mod syscfg2;
pub mod syscfg25;
pub mod syscfg3;
pub mod syscfg4;
pub mod syscfg5;
pub mod syscfg6;
pub mod syscfg7;
pub mod syscfg8;
pub mod syscfg9;
pub mod syscfg_xcfgi;

/// Creates StarFive JH7110 MIPI TX DPHY compatible register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        syscfg0::create()?,
        syscfg1::create()?,
        syscfg2::create()?,
        syscfg3::create()?,
        syscfg4::create()?,
        syscfg5::create()?,
        syscfg6::create()?,
        syscfg7::create()?,
        syscfg8::create()?,
        syscfg9::create()?,
        syscfg10::create()?,
        syscfg11::create()?,
        syscfg12::create()?,
        syscfg_xcfgi::create()?,
        syscfg25::create()?,
    ]
    .into())
}
