use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX DPHY SYSCFG 25 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg25",
        "MIPITX DPHY SYSCFG 25: mipitx_apbifsaif_syscfg_100",
        0x64,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "dbg1_mux_dout",
                "DBG1 Mux DOUT: u0_mipitx_dphy_dbg1_mux_dout",
                create_bit_range("[7:0]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field_constraint(
                "dbg1_mux_sel",
                "DBG1 Mux Select: u0_mipitx_dphy_dbg1_mux_sel",
                create_bit_range("[12:8]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
            create_field(
                "dbg2_mux_dout",
                "DBG2 Mux DOUT: u0_mipitx_dphy_dbg2_mux_dout",
                create_bit_range("[20:13]")?,
                svd::Access::ReadOnly,
                None,
            )?,
            create_field_constraint(
                "dbg2_mux_sel",
                "DBG2 Mux Select: u0_mipitx_dphy_dbg2_mux_sel",
                create_bit_range("[25:21]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
            create_field_constraint(
                "refclk_in_sel",
                "Reference Clock In Select: u0_mipitx_dphy_refclk_in_sel",
                create_bit_range("[28:26]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                None,
            )?,
            create_field(
                "resetb",
                "Reset B: u0_mipitx_dphy_resetb",
                create_bit_range("[29:29]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
