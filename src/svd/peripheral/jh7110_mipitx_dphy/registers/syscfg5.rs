use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX SYSCFG 5 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg5",
        "MIPITX DPHY SYSCFG 5: mipitx_apbifsaif_syscfg_20",
        0x14,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "rg_cdtx_pll_fbk_fra",
            "RG CDTX PLL FBK FRA: u0_mipitx_dphy_RG_CDTX_PLL_FBK_FRA",
            create_bit_range("[23:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
