use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX DPHY SYSCFG 12 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg12",
        "MIPITX DPHY SYSCFG 12: mipitx_apbifsaif_syscfg_48",
        0x30,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "scfg_dphy_src_sel",
                "SCFG DPHY Source Select: u0_mipitx_dphy_SCFG_dphy_src_sel",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "scfg_dsi_txready_esc_sel",
                "SCFG DSI TX Ready Escape Select: u0_mipitx_dphy_SCFG_dsi_txready_esc_sel",
                create_bit_range("[2:1]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "scfg_ppi_c_ready_sel",
                "SCFG PPI C Ready Select: u0_mipitx_dphy_SCFG_ppi_c_ready_sel",
                create_bit_range("[4:3]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "vcontrol",
                "VCONTROL: u0_mipitx_dphy_VCONTROL",
                create_bit_range("[9:5]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
        ]),
        None,
    )?))
}
