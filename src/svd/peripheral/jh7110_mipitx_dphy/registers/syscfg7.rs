use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX DPHY SYSCFG 0 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg7",
        "MIPITX DPHY SYSCFG 7: mipitx_apbifsaif_syscfg_28",
        0x1c,
        create_register_properties(32, 0)?,
        Some(&[
            create_field_constraint(
                "rg_cdtx_pll_ssc_delta_init",
                "RG CDTX PLL SSC DELTA INIT: u0_mipitx_dphy_RG_CDTX_PLL_SSC_DELTA_INIT",
                create_bit_range("[17:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3_ffff)?,
                None,
            )?,
            create_field(
                "rg_cdtx_pll_ssc_en",
                "RG CDTX PLL SSC EN: u0_mipitx_dphy_RG_CDTX_PLL_SSC_EN",
                create_bit_range("[18:18]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "rg_cdtx_pll_ssc_prd",
                "RG CDTX PLL SSC PRD: u0_mipitx_dphy_RG_CDTX_PLL_SSC_PRD",
                create_bit_range("[28:19]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3ff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
