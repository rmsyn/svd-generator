use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX DPHY SYSCFG 0 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg0",
        "MIPITX DPHY SYSCFG 0: mipitx_apbifsaif_syscfg_0",
        0x0,
        create_register_properties(32, 0x111_a021)?,
        Some(&[
            create_field(
                "aon_power_ready_n",
                "Always-on Power Ready: u0_mipitx_dphy_AON_POWER_READY_N",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "cklan",
                "Configure CKLAN: u0_mipitx_dphy_CFG_CKLANE_SET",
                create_bit_range("[5:1]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
            create_field(
                "databus16_sel",
                "Configure DATABUS16_SEL: u0_mipitx_dphy_CFG_DATABUS16_SEL",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "dpdn_swap",
                "Configure DPDN_SWAP: u0_mipitx_dphy_CFG_DPDN_SWAP",
                create_bit_range("[11:7]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1f)?,
                None,
            )?,
            create_field_constraint(
                "swap_sel",
                "Configure SWAP_SEL: u0_mipitx_dphy_CFG_L[n]_SWAP_SEL",
                create_bit_range("[14:12]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x7)?,
                Some(
                    svd::DimElement::builder()
                        .dim(5)
                        .dim_increment(3)
                        .dim_index(Some(
                            ["_l0", "_l1", "_l2", "_l3", "_l4"].map(String::from).into(),
                        ))
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
        ]),
        None,
    )?))
}
