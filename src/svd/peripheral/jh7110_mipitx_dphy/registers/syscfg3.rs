use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX SYSCFG 3 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg3",
        "MIPITX DPHY SYSCFG 3: mipitx_apbifsaif_syscfg_12",
        0xc,
        create_register_properties(32, 0x2108_4210)?,
        Some(&[create_field_constraint(
            "rg_cdtx",
            "RG CDTX L1-L3: u0_mipitx_dphy_RG_CDTX",
            create_bit_range("[4:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0x1f)?,
            Some(
                svd::DimElement::builder()
                    .dim(6)
                    .dim_increment(5)
                    .dim_index(Some(
                        [
                            "_l1n_hstx_res",
                            "_l1p_hstx_res",
                            "_l2n_hstx_res",
                            "_l2p_hstx_res",
                            "_l3n_hstx_res",
                            "_l3p_hstx_res",
                        ]
                        .map(String::from)
                        .into(),
                    ))
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        None,
    )?))
}
