use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX DPHY SYSCFG 1 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg1",
        "MIPITX DPHY SYSCFG 1: mipitx_apbifsaif_syscfg_4",
        0x4,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "mposv",
            "MPOSV: u0_mipitx_dphy_MPOSVn",
            create_bit_range("[0:0]")?,
            svd::Access::ReadOnly,
            Some(
                svd::DimElement::builder()
                    .dim(32)
                    .dim_increment(1)
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        None,
    )?))
}
