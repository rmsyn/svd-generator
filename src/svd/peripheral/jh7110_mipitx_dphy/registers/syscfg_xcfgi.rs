use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX DPHY SYSCFG XCFGI registers.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg_xcfgi",
        "MIPITX DPHY SYSCFG 13-24: mipitx_apbifsaif_syscfg_52-96",
        0x34,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "dw",
            "XCFGI DW: u0_mipitx_dphy_XCFGI_DW",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(12)
                .dim_increment(0x4)
                .dim_index(Some(
                    [
                        "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "0A", "0B",
                    ]
                    .map(String::from)
                    .into(),
                ))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
