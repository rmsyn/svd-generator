use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX DPHY SYSCFG 10 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg10",
        "MIPITX DPHY SYSCFG 10: mipitx_apbifsaif_syscfg_40",
        0x28,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "rg_extd_cycle_sel",
            "RG EXTD Cycle Select: u0_mipitx_dphy_RG_EXTD_CYCLE_SEL",
            create_bit_range("[2:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0x7)?,
            None,
        )?]),
        None,
    )?))
}
