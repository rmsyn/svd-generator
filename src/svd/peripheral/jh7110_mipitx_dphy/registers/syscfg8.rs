use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX DPHY SYSCFG 8 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg8",
        "MIPITX DPHY CLANE: mipitx_apbifsaif_syscfg_32",
        0x20,
        create_register_properties(32, 0x530b_0000)?,
        Some(&[create_field_constraint(
            "rg_clane",
            "RG CLANE: u0_mipitx_dphy_RG_CLANE",
            create_bit_range("[7:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xff)?,
            Some(
                svd::DimElement::builder()
                    .dim(4)
                    .dim_increment(0x8)
                    .dim_index(Some(
                        [
                            "_hs_clk_post_time",
                            "_hs_clk_pre_time",
                            "_hs_pre_time",
                            "_hs_trail_time",
                        ]
                        .map(String::from)
                        .into(),
                    ))
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        None,
    )?))
}
