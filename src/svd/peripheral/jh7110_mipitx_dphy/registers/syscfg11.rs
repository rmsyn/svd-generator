use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX DPHY SYSCFG 11 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg11",
        "MIPITX DPHY SYSCFG 11: mipitx_apbifsaif_syscfg_44",
        0x2c,
        create_register_properties(32, 0)?,
        Some(&[create_field_constraint(
            "scfg_c_hs_pre_zero_time",
            "SCFG C HS Pre Zero Time: u0_mipitx_dphy_SCFG_c_hs_pre_zero_time",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xffff_ffff)?,
            None,
        )?]),
        None,
    )?))
}
