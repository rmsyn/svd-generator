use crate::svd::register::{
    create_bit_range, create_field, create_field_constraint, create_register,
    create_register_properties, create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX SYSCFG 5 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg6",
        "MIPITX DPHY SYSCFG 6: mipitx_apbifsaif_syscfg_24",
        0x18,
        create_register_properties(32, 0x864)?,
        Some(&[
            create_field_constraint(
                "rg_cdtx_pll_fbk_int",
                "RG CDTX PLL FBK INT: u0_mipitx_dphy_RG_CDTX_PLL_FBK_INT",
                create_bit_range("[8:0]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x1ff)?,
                None,
            )?,
            create_field(
                "rg_cdtx_pll_fm_en",
                "RG CDTX PLL FM EM: u0_mipitx_dphy_RG_CDTX_PLL_FM_EN",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rg_cdtx_pll_ldo_stb_x2_en",
                "RG CDTX PLL LDO STB X2 EN: u0_mipitx_dphy_RG_CDTX_PLL_LDO_STB_X2_EN",
                create_bit_range("[10:10]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field_constraint(
                "rg_cdtx_pll_pre_div",
                "RG CDTX PLL PRE DIV: u0_mipitx_dphy_RG_CDTX_PLL_PRE_DIV",
                create_bit_range("[12:11]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3)?,
                None,
            )?,
            create_field_constraint(
                "rg_cdtx_pll_ssc_delta",
                "RG CDTX PLL SSC DELTA: u0_mipitx_dphy_RG_CDTX_PLL_SSC_DELTA",
                create_bit_range("[30:13]")?,
                svd::Access::ReadWrite,
                create_write_constraint(0, 0x3_ffff)?,
                None,
            )?,
        ]),
        None,
    )?))
}
