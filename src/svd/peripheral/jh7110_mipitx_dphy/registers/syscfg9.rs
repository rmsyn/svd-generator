use crate::svd::register::{
    create_bit_range, create_field_constraint, create_register, create_register_properties,
    create_write_constraint,
};
use crate::Result;

/// Creates StarFive JH7110 MIPI TX DPHY SYSCFG 9 register.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "syscfg9",
        "MIPITX DPHY CLANE and DLANE: mipitx_apbifsaif_syscfg_36",
        0x24,
        create_register_properties(32, 0x2116_0e16)?,
        Some(&[create_field_constraint(
            "rg",
            "RG CLANE and DLANE TIME: u0_mipitx_dphy_RG_CLANE and u0_mipitx_dphy_RG_DLANE",
            create_bit_range("[7:0]")?,
            svd::Access::ReadWrite,
            create_write_constraint(0, 0xff)?,
            Some(
                svd::DimElement::builder()
                    .dim(4)
                    .dim_increment(0x8)
                    .dim_index(Some(
                        [
                            "_clane_hs_zero_time",
                            "_dlane_hs_pre_time",
                            "_dlane_hs_trail_time",
                            "_dlane_hs_zero_tim",
                        ]
                        .map(String::from)
                        .into(),
                    ))
                    .build(svd::ValidateLevel::Strict)?,
            ),
        )?]),
        None,
    )?))
}
