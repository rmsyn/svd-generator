use crate::svd::register::jh7110;
use crate::Result;

/// Creates StarFive JH7110 VOUTCRG Clock U0 HDMI Transmit registers.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "clk_u0_hdmi_tx",
        "Clock U0 HDMI Transmit registers",
        0x3c,
        None,
        Some(
            svd::DimElement::builder()
                .dim(3)
                .dim_increment(0x4)
                .dim_index(Some(["_mclk", "_bclk", "_sys"].map(String::from).into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
}
