use crate::svd::register::jh7110;
use crate::Result;

/// Creates StarFive JH7110 VOUTCRG Clock DSI System register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg("clk_dsi_sys", "Clock DSI System", 0x8, [31, 4, 4, 4], None)
}
