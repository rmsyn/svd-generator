use crate::svd::register::{
    create_bit_range, create_field, create_register, create_register_properties,
};
use crate::Result;

/// Creates StarFive JH7110 VOUTCRG RESET registers.
pub fn create() -> Result<svd::RegisterCluster> {
    const FIELD_DESC: &str = "0: De-assert reset, 1: Assert reset";

    create_register(
        "rst",
        "VOUT CRG RESET register",
        0x48,
        create_register_properties(32, 0xfff)?,
        Some(&[
            create_field(
                "rstn_u0_dc8200_axi",
                FIELD_DESC,
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_dc8200_ahb",
                FIELD_DESC,
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_dc8200_core",
                FIELD_DESC,
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_cdns_dsitx_dpi",
                FIELD_DESC,
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_cdns_dsitx_apb",
                FIELD_DESC,
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_cdns_dsitx_rxesc",
                FIELD_DESC,
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_cdns_dsitx_sys",
                FIELD_DESC,
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_cdns_dsitx_txbytehs",
                FIELD_DESC,
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_cdns_dsitx_txesc",
                FIELD_DESC,
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_hdmi_tx_hdmi",
                FIELD_DESC,
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_mipitx_dphy_sys",
                FIELD_DESC,
                create_bit_range("[10:10]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rstn_u0_mipitx_dphy_txbytehs",
                FIELD_DESC,
                create_bit_range("[11:11]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some(
                    ["_software_assert0_addr_assert_sel", "_voutcrg_status"]
                        .map(String::from)
                        .into(),
                ))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )
    .map(svd::RegisterCluster::Register)
}
