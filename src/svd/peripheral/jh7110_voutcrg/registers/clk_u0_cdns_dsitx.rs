use crate::svd::{create_cluster, create_enum_value, jh7110};
use crate::Result;

/// Creates StarFive JH7110 VOUTCRG Clock U0 Cadence DSI Transmit registers.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "clk_u0_cdns_dsitx",
        "Clock U0 Cadence DSI Transmit registers",
        0x28,
        &[
            jh7110::create_register_icg(
                "apb",
                "Clock U0 Cadence DSI Transmit APB",
                0x0,
                None,
                None,
            )?,
            jh7110::create_register_icg(
                "sys",
                "Clock U0 Cadence DSI Transmit SYS",
                0x4,
                None,
                None,
            )?,
            jh7110::create_register_icg_mux_sel_enum(
                "dpi",
                "Clock U0 Cadence DSI Transmit DPI",
                0x8,
                "clk_dc8200_pix0, clk_hdmitx0_pixelclk",
                &[
                    create_enum_value(
                        "clk_dc8200_pix0",
                        "Select `clk_dc8200_pix0` as the U0 Cadence DSI Transmit DPI clock.",
                        0b0,
                    )?,
                    create_enum_value(
                        "clk_hdmitx0_pixelclk",
                        "Select `clk_hdmitx0_pixelclk` as the U0 Cadence DSI Transmit DPI clock.",
                        0b1,
                    )?,
                ],
                None,
                None,
            )?,
            jh7110::create_register_icg(
                "txesc",
                "Clock U0 Cadence DSI Transmit TXESC",
                0xc,
                None,
                None,
            )?,
        ],
        None,
    )?))
}
