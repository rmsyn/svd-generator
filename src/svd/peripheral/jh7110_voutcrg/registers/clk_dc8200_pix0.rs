use crate::svd::register::jh7110;
use crate::Result;

/// Creates StarFive JH7110 VOUTCRG Clock DC8200 Pixel 0 register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_divcfg(
        "clk_dc8200_pix0",
        "Clock DC8200 Pixel 0",
        0x4,
        [63, 4, 4, 4],
        None,
    )
}
