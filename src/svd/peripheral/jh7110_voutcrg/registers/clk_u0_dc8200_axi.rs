use crate::svd::register::jh7110;
use crate::Result;

/// Creates StarFive JH7110 VOUTCRG Clock U0 DC8200 AXI Escape register.
pub fn create() -> Result<svd::RegisterCluster> {
    jh7110::create_register_icg(
        "clk_u0_dc8200_axi",
        "Clock U0 DC8200 AXI",
        0x10,
        None,
        None,
    )
}
