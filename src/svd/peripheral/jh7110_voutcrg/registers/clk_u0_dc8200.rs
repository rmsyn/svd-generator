use crate::svd::{create_cluster, create_enum_value, jh7110};
use crate::Result;

/// Creates StarFive JH7110 VOUTCRG Clock U0 DC8200 registers.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Cluster(create_cluster(
        "clk_u0_dc8200",
        "Clock U0 DC8200 registers",
        0x10,
        &[
            jh7110::create_register_icg("axi", "Clock U0 DC8200 AXI", 0x0, None, None)?,
            jh7110::create_register_icg("core", "Clock U0 DC8200 Core", 0x4, None, None)?,
            jh7110::create_register_icg("ahb", "Clock U0 DC8200 AHB", 0x8, None, None)?,
            jh7110::create_register_icg_mux_sel_enum(
                "pix",
                "Clock U0 DC8200 Pixel Clock selector",
                0xc,
                "clk_dc8200_pix[n], clk_hdmitx0_pixelclk",
                &[
                    create_enum_value(
                        "clk_dc8200_pix",
                        "Select `clk_dc8200_pix` as the U0 DC8200 clock.",
                        0b0,
                    )?,
                    create_enum_value(
                        "clk_hdmitx0_pixelclk",
                        "Select `clk_hdmitx0_pixelclk` as the U0 DC8200 clock.",
                        0b1,
                    )?,
                ],
                None,
                Some(
                    svd::DimElement::builder()
                        .dim(2)
                        .dim_increment(0x4)
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
        ],
        None,
    )?))
}
