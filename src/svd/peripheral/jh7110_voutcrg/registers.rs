use crate::Result;

pub mod clk_apb;
pub mod clk_dc8200_pix0;
pub mod clk_dom_vout_top_lcd;
pub mod clk_dsi_sys;
pub mod clk_tx_esc;
pub mod clk_u0_cdns_dsitx;
pub mod clk_u0_dc8200;
pub mod clk_u0_hdmi_tx;
pub mod clk_u0_mipitx_dphy_txesc;
pub mod rst;

/// Creates StarFive JH7110 VOUTCRG compatible register definitions.
pub fn create() -> Result<Vec<svd::RegisterCluster>> {
    Ok([
        clk_apb::create()?,
        clk_dc8200_pix0::create()?,
        clk_dsi_sys::create()?,
        clk_tx_esc::create()?,
        clk_u0_dc8200::create()?,
        clk_dom_vout_top_lcd::create()?,
        clk_u0_cdns_dsitx::create()?,
        clk_u0_mipitx_dphy_txesc::create()?,
        clk_u0_hdmi_tx::create()?,
        rst::create()?,
    ]
    .into())
}
