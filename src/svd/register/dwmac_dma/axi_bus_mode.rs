use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC DMA AXI Master Bus Mode register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "axi_bus_mode",
        "AXI Master Bus Mode",
        // NOTE: this register is meant to be included in a DMA register cluster.
        // So, the offset from the base is used instead of the full address.
        0x28,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "undef",
                "Undefined",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "blen",
                "Burst Length",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                Some(
                    svd::DimElement::builder()
                        .dim(7)
                        .dim_increment(1)
                        .dim_index(Some(
                            [
                                String::from("4"),
                                String::from("8"),
                                String::from("16"),
                                String::from("32"),
                                String::from("64"),
                                String::from("128"),
                                String::from("256"),
                            ]
                            .into(),
                        ))
                        .build(svd::ValidateLevel::Strict)?,
                ),
            )?,
            create_field(
                "aal",
                "AAL",
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "be1kb",
                "1KB Burst Enable",
                create_bit_range("[13:13]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rd_osr_lmt",
                "Read OSR Limit",
                create_bit_range("[19:16]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "wr_osr_lmt",
                "Write OSR Limit",
                create_bit_range("[23:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "lpi_xit_frm",
                "LPI XIT Frame",
                create_bit_range("[30:30]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "en_lpi",
                "Enable LPI",
                create_bit_range("[31:31]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
