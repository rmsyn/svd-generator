use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC DMA Current Hardware Feature register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "hw_feature",
        "Hardware Feature",
        // NOTE: this register is meant to be included in a DMA register cluster.
        // So, the offset from the base is used instead of the full address.
        0x58,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "hw_feature",
            "Hardware Feature",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        None,
    )?))
}
