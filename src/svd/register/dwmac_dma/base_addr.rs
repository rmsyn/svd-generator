use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC DMA Receive/Transmit List Base register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "base_addr",
        "List Base - base_addr0: Receive, base_addr1: Transmit",
        // Offset in cluster, absolute offset: 0x100c-0x1010
        0xc,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "base_addr",
            "List Base",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some([String::from("_rx"), String::from("_tx")].into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
