use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates a Synopsys DesignWare Ethernet MAC DMA Status register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "status",
        "DMA Status",
        // NOTE: this register is meant to be included in a DMA register cluster.
        // So, the offset from the base is used instead of the full address.
        0x14,
        create_register_properties(32, 0)?,
        Some(&[
            create_field(
                "ti",
                "Transmit Interrupt",
                create_bit_range("[0:0]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "tps",
                "Transmit Process Stopped",
                create_bit_range("[1:1]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "tu",
                "Transmit Buffer Unavailable",
                create_bit_range("[2:2]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "tjt",
                "Transmit Jabber Timeout",
                create_bit_range("[3:3]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ovf",
                "Receive Overflow",
                create_bit_range("[4:4]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "unf",
                "Transmit Underflow",
                create_bit_range("[5:5]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ri",
                "Receive Interrupt",
                create_bit_range("[6:6]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ru",
                "Receive Buffer Unavailable",
                create_bit_range("[7:7]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rps",
                "Receive Process Stopped",
                create_bit_range("[8:8]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rwt",
                "Receive Watchdog Timeout",
                create_bit_range("[9:9]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "eti",
                "Early Transmit Interrupt",
                create_bit_range("[10:10]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "fbei",
                "Fatal Bus Error Interrupt",
                create_bit_range("[11:11]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "eri",
                "Early Receive Interrupt",
                create_bit_range("[12:12]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ais",
                "Abnormal Interrupt Summary",
                create_bit_range("[13:13]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "nis",
                "Normal Interrupt Summary",
                create_bit_range("[14:14]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "rs",
                "Receive Process State - 4: TX Abort (other side hung up)",
                create_bit_range("[19:17]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "ts",
                "Transmit Process State - 1: RX Abort (other side hung up)",
                create_bit_range("[22:20]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "gli",
                "GMAC Line Interface Interrupt",
                create_bit_range("[26:26]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "gmi",
                "MMC Interrupt",
                create_bit_range("[27:27]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "gpi",
                "PMT Interrupt",
                create_bit_range("[28:28]")?,
                svd::Access::ReadWrite,
                None,
            )?,
            create_field(
                "glpii",
                "GMAC LPI Interrupt",
                create_bit_range("[30:30]")?,
                svd::Access::ReadWrite,
                None,
            )?,
        ]),
        None,
    )?))
}
