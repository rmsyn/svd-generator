use crate::svd::{create_bit_range, create_field, create_register, create_register_properties};
use crate::Result;

/// Creates the Synopsys DesignWare Gigabit Ethernet MAC DMA Current Host RX Buffer register definition.
pub fn create() -> Result<svd::RegisterCluster> {
    Ok(svd::RegisterCluster::Register(create_register(
        "cur_buf_addr",
        "Current Host Buffer - cur_buf_addr0: Transmit, cur_buf_addr1: Receive",
        // Offset in cluster, absolute offset: 0x1050-0x1054
        0x50,
        create_register_properties(32, 0)?,
        Some(&[create_field(
            "cur_buf_addr",
            "Current Host Buffer",
            create_bit_range("[31:0]")?,
            svd::Access::ReadWrite,
            None,
        )?]),
        Some(
            svd::DimElement::builder()
                .dim(2)
                .dim_increment(0x4)
                .dim_index(Some([String::from("_tx"), String::from("_rx")].into()))
                .build(svd::ValidateLevel::Strict)?,
        ),
    )?))
}
