//! Common DMA registers for Synopsys DesignWare Ethernet MAC peripherals.

pub mod axi_bus_mode;
pub mod base_addr;
pub mod cur_buf_addr;
pub mod hw_feature;
pub mod intr_ena;
pub mod missed_frame_ctr;
pub mod poll_demand;
pub mod rx_watchdog;
pub mod status;
