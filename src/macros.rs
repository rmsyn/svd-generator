/// Convenience macro to create arrays of non-`Copy` types.
#[macro_export]
macro_rules! array {
    [$ty:expr; $num:expr] => {
        [0; $num].map(|_| $ty)
    }
}
