use alloc::string::String;
use core::fmt;

/// Convenience alias for [`Result`](core::result::Result) type for the library.
pub type Result<T> = core::result::Result<T, Error>;

/// Represents error types for the library.
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Error {
    Io(String),
    DeviceTree(String),
    Svd(String),
    InvalidBitRange,
    InvalidWriteConstraint((u64, u64)),
    InvalidVariant { field: &'static str, value: usize },
}

impl Error {
    /// Creates a invalid variant [Error].
    pub const fn invalid_variant(field: &'static str, value: usize) -> Self {
        Self::InvalidVariant { field, value }
    }
}

impl From<fdt::Error> for Error {
    fn from(err: fdt::Error) -> Self {
        Self::DeviceTree(format!("{err:?}"))
    }
}

#[cfg(feature = "std")]
impl From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        Self::Io(format!("{err}"))
    }
}

#[cfg(feature = "std")]
impl From<svd::SvdError> for Error {
    fn from(err: svd::SvdError) -> Self {
        Self::Svd(format!("{err}"))
    }
}

#[cfg(feature = "std")]
impl From<svd_encoder::EncodeError> for Error {
    fn from(err: svd_encoder::EncodeError) -> Error {
        Self::Svd(format!("{err}"))
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::Io(err) => write!(f, "I/O error: {err}"),
            Self::DeviceTree(err) => write!(f, "DeviceTree error: {err}"),
            Self::Svd(err) => write!(f, "SVD error: {err}"),
            Self::InvalidBitRange => write!(f, "invalid bit range"),
            Self::InvalidWriteConstraint((min, max)) => write!(
                f,
                "write-constraint reversed range, min: {min} > max: {max}"
            ),
            Self::InvalidVariant { field, value } => write!(f, "invalid {field} variant: {value}"),
        }
    }
}
