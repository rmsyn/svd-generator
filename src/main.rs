use std::fs;
use std::io::{Read, Write};

use clap::Parser;

use svd_generator::svd::*;
use svd_generator::Result;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Flattened Devicetree input file
    #[arg(short = 'f', long)]
    fdt: String,

    /// Output SVD file
    #[arg(short = 'o', long, default_value = "out.svd")]
    out: String,
}

fn main() -> Result<()> {
    let mut builder = env_logger::Builder::new();
    builder.filter_level(log::LevelFilter::Info);
    builder.parse_default_env();
    builder.init();

    let Args { fdt, out } = Args::parse();

    log::info!("Loading flattened device tree from: {fdt}...");

    let md = fs::metadata(fdt.as_str())?;
    let mut dt_buf = Vec::with_capacity(md.len() as usize);

    fs::File::open(fdt.as_str())?.read_to_end(&mut dt_buf)?;

    let dt = fdt::Fdt::new(dt_buf.as_ref())?;

    log::info!("FDT loaded successfully!");

    log::trace!("{dt:?}");

    let model = dt.root()?.model();
    let peripherals = parse_peripherals(&dt)?;
    let device_desc = format!("From {model},model device generator");

    let device = svd::Device::builder()
        .name(model.into())
        .description(device_desc)
        .version("0.1".into())
        .address_unit_bits(8)
        .width(32)
        .peripherals(peripherals)
        .build(svd::ValidateLevel::Strict)?;

    let device_str = svd_encoder::encode(&device)?;

    log::trace!("Generated SVD device description:\n\n{device_str}");

    log::info!("Writing SVD to output file: {out}");

    fs::File::create(out.as_str())?.write_all(device_str.as_bytes())?;

    log::info!("SVD converted successfully!");

    Ok(())
}
