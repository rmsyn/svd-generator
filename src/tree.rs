use fdt::Fdt;

mod compatible;
mod node;

pub use compatible::*;
pub use node::*;

/// Represents a DeviceTree loaded from an FDT description.
pub struct Tree<'a> {
    dt: Fdt<'a>,
}

impl<'a> Tree<'a> {
    /// Gets a reference to the inner [`DeviceTree`].
    pub const fn device_tree(&self) -> &Fdt {
        &self.dt
    }
}

impl<'a> From<Fdt<'a>> for Tree<'a> {
    fn from(val: Fdt<'a>) -> Self {
        Self { dt: val }
    }
}
