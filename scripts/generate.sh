#!/usr/bin/env bash

in_dtb=$1
if [[ $in_dtb == "" ]]; then
	in_dtb="test.dtb"
fi

out_svd=$2
if [[ $out_svd == "" ]]; then
	out_svd="out.svd"
fi

svd2rust=$(which svd2rust)
if [[ $? != 0 ]]; then
	cargo install svd2rust form
fi

# Run svd-generator to convert DTB to SVD
cargo run -- -f $in_dtb -o $out_svd

# Make temporary output directories
mkdir -p /tmp/svd-gen-out/src

# Generate Rust project from the output SVD
svd2rust --target=riscv -i $out_svd -o /tmp/svd-gen-out
form -i /tmp/svd-gen-out/lib.rs -o /tmp/svd-gen-out/src

# Run Cargo project setup and formatting on the new project
pushd /tmp/svd-gen-out
rm lib.rs
cargo init
cargo fmt
popd
